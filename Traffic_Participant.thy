section \<open>Model of Other Traffic Participants\<close>

theory Traffic_Participant
  imports Complex_Main
begin

locale traffic_participant =
  fixes v_max :: real
  fixes v_s :: real
  fixes a_max :: real
  fixes u1 :: "real \<Rightarrow> real"
  fixes u2 :: "real \<Rightarrow> real"
  assumes u1_normalized: "-1 \<le> u1 t" "u1 t \<le> 1"
  assumes u2_normalized: "-1 \<le> u2 t" "u2 t \<le> 1"
  assumes a_max_nonneg: "0 \<le> a_max"
  assumes v_s_pos: "0 < v_s"
  assumes v_s_lt_v_max: "v_s < v_max"
begin

lemma vmax_pos: "0 < v_max"
  using v_s_pos v_s_lt_v_max by auto

text \<open>
  Mathematical model of other participants

  Parameters :
  1. @{term v\<^sub>_max}:
       maximum speed (either certain percentage above speed limit or physical constraint)
  2. @{term v_s}:
       speed above which the acceleration is limited by the engine power and no longer by the tire
       friction
  3. @{term a_max}:
       maximum absolute acceleration
  4. @{term "u1 t"}:
       normalized steering input (@{term "u1 t = 1"} refers to full steering to the left,
       @{term "u1 t = -1"} refers to full steering to the right)
  5. @{term "u2 t"}:
       normalized control input for longitudinal acceleration (@{term "u2 t = 1"} refers to full
       acceleration, @{term "u2 t = -1"} refers to full braking)
\<close>

text \<open>
  C4: maximum absolute acceleration is limited by @{term a_max}
\<close>

definition a_lat :: "real \<Rightarrow> real" where
  "a_lat t = a_max * u1 t"

lemma a_lat_c4: "\<bar>a_lat t\<bar> \<le> a_max"
proof -
  have "\<bar>u1 t\<bar> \<le> 1" using u1_normalized[of t] by auto
  have "\<bar>a_lat t\<bar> = \<bar>a_max * u1 t\<bar>" unfolding a_lat_def by auto
  also have "\<dots> = \<bar>a_max\<bar> * \<bar>u1 t\<bar>" using abs_mult by auto
  also have "\<dots> \<le> \<bar>a_max\<bar>" using mult_left_mono[OF \<open>\<bar>u1 t\<bar> \<le> 1\<close>] by auto
  also have "\<dots> = a_max" using a_max_nonneg by auto
  finally show ?thesis .
qed

definition a_long_constraint1 :: "real \<Rightarrow> real" where
  "a_long_constraint1 t = sqrt (a_max\<^sup>2 - (a_lat t)\<^sup>2)"

lemma a_long_constraint1_wd: "a_max\<^sup>2 - (a_lat t)\<^sup>2 \<ge> 0"
  using power_mono[OF a_lat_c4[of t] abs_ge_zero, of 2] real_sqrt_pow2_iff by auto

text \<open>
  C4: maximum absolute acceleration is limited by @{term a_max}
\<close>
lemma a_long_constraint1_c4: "(a_lat t)\<^sup>2 + (a_long_constraint1 t)\<^sup>2 = a_max\<^sup>2"
proof -
  have "(a_lat t)\<^sup>2 + (a_long_constraint1 t)\<^sup>2 = (a_lat t)\<^sup>2 + (sqrt (a_max\<^sup>2 - (a_lat t)\<^sup>2))\<^sup>2"
    unfolding a_long_constraint1_def by auto
  also have "\<dots> = (a_lat t)\<^sup>2 + a_max\<^sup>2 - (a_lat t)\<^sup>2"
    using a_long_constraint1_wd real_sqrt_pow2_iff by auto
  also have "\<dots> = a_max\<^sup>2" by auto
  finally show ?thesis .
qed

lemma a_long_constraint1_nonneg: "a_long_constraint1 t \<ge> 0"
  unfolding a_long_constraint1_def using a_long_constraint1_wd by auto

definition v :: "real \<Rightarrow> real" where
  "v t = undefined" (* TODO *)

definition a_long_constraint2 :: "real \<Rightarrow> real" where
  "a_long_constraint2 t = (if ((v_s < v t \<and> v t < v_max) \<and> u2 t > 0)
                           then a_max * v_s / v t
                           else (if ((0 < v t \<and> v t \<le> v_s) \<or> (v t > v_s \<and> u2 t \<le> 0))
                                 then a_max
                                 else 0))"

lemma a_long_constraint2_nonneg: "a_long_constraint2 t \<ge> 0"
  unfolding a_long_constraint2_def using a_max_nonneg v_s_pos by auto

text \<open>
  C1: positive longitudinal acceleration is stopped when @{term v_max} is reached.
\<close>
lemma a_long_constraint2_c1:
  assumes "v t \<ge> v_max"
  assumes "u2 t > 0"
  shows "a_long_constraint2 t = 0"
  unfolding a_long_constraint2_def using assms v_s_lt_v_max by auto

text \<open>
  C2: positive longitudinal acceleration is inversely proportional to speed above speed @{term v_s}
  (modeling a maximum engine power).
\<close>
lemma a_long_constraint2_c2:
  assumes "v_s < v t"
  assumes "v t < v_max"
  assumes "u2 t > 0"
  shows "a_long_constraint2 t = a_max * v_s / v t"
  unfolding a_long_constraint2_def using assms by auto

text \<open>
  C3: driving backwards in a lane is not allowed.
\<close>
lemma a_long_constraint2_c3:
  assumes "v t \<le> 0"
  shows "a_long_constraint2 t = 0"
  unfolding a_long_constraint2_def using assms v_s_pos by auto

definition a_long :: "real \<Rightarrow> real" where
  "a_long t = (if (a_long_constraint2 t * \<bar>u2 t\<bar> \<le> a_long_constraint1 t)
               then a_long_constraint2 t * u2 t
               else a_long_constraint1 t * sgn (u2 t))"

lemma u2_nonneg_imp_a_long_nonneg:
  assumes "u2 t \<ge> 0"
  shows "a_long t \<ge> 0"
  unfolding a_long_def using assms a_long_constraint1_nonneg[of t] a_long_constraint2_nonneg[of t] by auto

lemma a_long_le_a_long_constraint1: "\<bar>a_long t\<bar> \<le> a_long_constraint1 t"
proof -
  have "\<bar>u2 t\<bar> \<le> 1" using u2_normalized[of t] by auto
  show ?thesis
  proof (cases "a_long_constraint2 t * \<bar>u2 t\<bar> \<le> a_long_constraint1 t")
    case True
    then have "\<bar>a_long t\<bar> = \<bar>a_long_constraint2 t * u2 t\<bar>" unfolding a_long_def by auto
    also have "\<dots> = \<bar>a_long_constraint2 t\<bar> * \<bar>u2 t\<bar>" using abs_mult by auto
    also have "\<dots> \<le> a_long_constraint2 t * \<bar>u2 t\<bar>" using a_long_constraint2_nonneg by auto
    also have "\<dots> \<le> a_long_constraint1 t" using True by auto
    finally show ?thesis .
  next
    case False
    then have "\<bar>a_long t\<bar> = \<bar>a_long_constraint1 t * sgn (u2 t)\<bar>" unfolding a_long_def by auto
    also have "\<dots> = \<bar>a_long_constraint1 t\<bar> * \<bar>sgn (u2 t)\<bar>" using abs_mult by blast
    also have "\<dots> \<le> \<bar>a_long_constraint1 t\<bar>" by auto
    also have "\<dots> = a_long_constraint1 t" using a_long_constraint1_nonneg by auto
    finally show ?thesis .
  qed
qed

lemma a_long_le_a_long_constraint2: "\<bar>a_long t\<bar> \<le> a_long_constraint2 t"
proof -
  have "\<bar>u2 t\<bar> \<le> 1" using u2_normalized[of t] by auto
  show ?thesis
  proof (cases "a_long_constraint2 t * \<bar>u2 t\<bar> \<le> a_long_constraint1 t")
    case True
    then have "\<bar>a_long t\<bar> = \<bar>a_long_constraint2 t * u2 t\<bar>" unfolding a_long_def by auto
    also have "\<dots> = \<bar>a_long_constraint2 t\<bar> * \<bar>u2 t\<bar>" using abs_mult by auto
    also have "\<dots> \<le> \<bar>a_long_constraint2 t\<bar>"
      using u2_normalized mult_left_mono[OF \<open>\<bar>u2 t\<bar> \<le> 1\<close>] by auto
    also have "\<dots> \<le> a_long_constraint2 t" using a_long_constraint2_nonneg by auto
    finally show ?thesis .
  next
    case False
    then have "\<bar>a_long t\<bar> = \<bar>a_long_constraint1 t * sgn (u2 t)\<bar>" unfolding a_long_def by auto
    also have "\<dots> = \<bar>a_long_constraint1 t\<bar> * \<bar>sgn (u2 t)\<bar>" using abs_mult by blast
    also have "\<dots> \<le> \<bar>a_long_constraint1 t\<bar>" by auto
    also have "\<dots> = a_long_constraint1 t" using a_long_constraint1_nonneg by auto
    also have "\<dots> \<le> a_long_constraint2 t * \<bar>u2 t\<bar>" using False by auto
    also have "\<dots> \<le> a_long_constraint2 t"
      using u2_normalized mult_left_mono[OF \<open>\<bar>u2 t\<bar> \<le> 1\<close>] a_long_constraint2_nonneg by auto
    finally show ?thesis .
  qed
qed

lemma a_long_c1:
  assumes "v t \<ge> v_max"
  assumes "u2 t > 0"
  shows "\<bar>a_long t\<bar> = 0"
  using a_long_le_a_long_constraint2[of t] a_long_constraint2_c1[OF assms] by auto

lemma a_long_c2:
  assumes "v_s < v t"
  assumes "v t < v_max"
  assumes "u2 t > 0"
  shows "\<bar>a_long_constraint2 t\<bar> \<le> a_max * v_s / v t"
  using a_long_le_a_long_constraint2[of t] a_long_constraint2_c2[OF assms] by auto

lemma a_long_c3:
  assumes "v t \<le> 0"
  shows "\<bar>a_long t\<bar> = 0"
  using a_long_le_a_long_constraint2[of t] a_long_constraint2_c3[OF assms] by auto

lemma a_long_c4: "(a_long t)\<^sup>2 + (a_lat t)\<^sup>2 \<le> a_max\<^sup>2"
  using power_mono[OF a_long_le_a_long_constraint1[of t] abs_ge_zero, of 2] a_long_constraint1_c4[of t] by auto

end

end
