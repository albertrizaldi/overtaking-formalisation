(*  Author : Jonas Keinholz 
             Albert Rizaldi 
*)

theory Occupancy
  imports Complex_Main 
          "~~/src/HOL/HOL/Limits"
          "./safe_distance/Safe_Distance_Isar"
          "Affine_Arithmetic/Polygon"
          "Affine_Arithmetic/Intersection"
          "Environment_Executable"
          "~~/src/HOL/ex/Cubic_Quartic"
begin


type_synonym real2 = "real \<times> real"

section \<open>Auxiliary theories\<close>

lemma quadratic_eq:
  fixes a b c :: real
  assumes "a \<noteq> 0"
  shows "a * x\<^sup>2 + b * x + c = 0 \<longleftrightarrow> b\<^sup>2 - 4 * a * c \<ge> 0 \<and> 
                                         (\<exists>\<sigma> \<in> {-1,1}. x = (- b + \<sigma> * sqrt (b\<^sup>2 - 4 * a * c))/(2*a))"
  using assms quadratic_eq_zeroes_iff by auto

abbreviation power3 :: "'a :: power \<Rightarrow> 'a"  ("(_\<^sup>3)" [1000] 999)
  where "x\<^sup>3 \<equiv> x ^ 3"

theorem [derivative_intros]: "(power3 has_real_derivative 3 * t\<^sup>2) (at t within s')"
  by (auto intro:derivative_eq_intros)

abbreviation power4 :: "'a :: power \<Rightarrow> 'a"  ("(_\<^sup>4)" [1000] 999)
  where "x\<^sup>4 \<equiv> x ^ 4"

abbreviation power6 :: "'a :: power \<Rightarrow> 'a"  ("(_\<^sup>6)" [1000] 999)
  where "x\<^sup>6 \<equiv> x ^ 6"

lemma lhopital1: 
  fixes t ::real
  shows "\<And>n::nat. (\<lambda>t'. t^n - t'^n) \<midarrow>t\<rightarrow> 0"
proof -
  fix n
  have "(\<lambda>t'. t'^n) \<midarrow>t\<rightarrow> t^n"
    using Limits.tendsto_power[where n = n] by auto
  then show "(\<lambda>t'. t^n - t'^n) \<midarrow>t\<rightarrow> 0"
    using Limits.tendsto_diff[where f = "\<lambda>t'. t^n" and g = "\<lambda>t'. t'^n" and a = "t^n" and b = "t^n"]
    by auto
qed

lemma lhopital2: 
  fixes t :: real
  shows "\<forall>\<^sub>F t' in at t. t - t' \<noteq> 0"
proof -
  have "\<forall>\<^sub>F t' in at_left t. t - t' \<noteq> 0"
  proof -
    have "(\<exists>b<t. \<forall>y>b. y < t \<longrightarrow> t - y \<noteq> 0)" by (simp add: lt_ex)
    then show ?thesis using eventually_at_left by auto
  qed
  moreover have "\<forall>\<^sub>F t' in at_right t. t - t' \<noteq> 0"
  proof -
    have "(\<exists>b>t. \<forall>y<b. y < t \<longrightarrow> t - y \<noteq> 0)" by (simp add: gt_ex)
    then show ?thesis using eventually_at_right by auto
  qed
  ultimately show ?thesis using eventually_at_split by auto
qed

lemma lhopital3: 
  "\<And>n. \<forall>\<^sub>F t' in at t. ((\<lambda>t'. t^n - t'^n) has_real_derivative - n * t'^(n-1)) (at t')"
proof -
  fix n
  {
    fix t'
    have "((\<lambda>t'. t^n) has_real_derivative 0) (at t')" by auto
    moreover have "((\<lambda>t'. t'^n) has_real_derivative n * t'^(n-1)) (at t')"
      using DERIV_power[where f = "\<lambda>t'. t'" and D = 1 and n = n]
      by auto
    ultimately have "((\<lambda>t'. t^n - t'^n) has_real_derivative - n * t'^(n-1)) (at t')"
      using DERIV_diff[where f = "\<lambda>t'. t^n" and g = "\<lambda>t'. t'^n"]
      by fastforce
  }
  then show "\<forall>\<^sub>F t' in at t. ((\<lambda>t'. t^n - t'^n) has_real_derivative - n * t'^(n-1)) (at t')"
    by auto
qed

lemma lhopital_diff_power:
  fixes t::real
  shows "\<And>n. n \<noteq> 0 \<Longrightarrow> (\<lambda>t'. (t^n - t'^n)/(t - t')) \<midarrow>t\<rightarrow> n * t^(n-1)"
proof -
  fix n :: nat
  assume "n \<noteq> 0"
  show "(\<lambda>t'. (t^n - t'^n)/(t - t')) \<midarrow>t\<rightarrow> n * t^(n-1)"
  proof (rule lhopital[where f' = "\<lambda>t'. - n * t'^(n-1)" and g' = "\<lambda>t'. (-1::real)"])
    show "(\<lambda>t'. t^n - t'^n) \<midarrow>t\<rightarrow> 0" using lhopital1[where n = n] by auto
  next
    show "(\<lambda>t'. t - t') \<midarrow>t\<rightarrow> 0"
      using lhopital1[where n = 1] by auto
  next
    show "\<forall>\<^sub>F t' in at t. t - t' \<noteq> 0" using lhopital2 by auto
  next
    have "(-1::real) \<noteq> 0" by auto
    then show "\<forall>\<^sub>F t' in at t. (- 1::real) \<noteq> 0" by auto
  next
    show "\<forall>\<^sub>F t' in at t. ((\<lambda>t'. t^n - t'^n) has_real_derivative - n * t'^(n-1)) (at t')"
      using lhopital3[where n = n] by auto
  next
    show "\<forall>\<^sub>F t' in at t. (op - t has_real_derivative - 1) (at t')"
      using lhopital3[where n = 1] by auto
  next
    have "(\<lambda>t'. t'^(n - 1) / - 1) \<midarrow>t\<rightarrow> - (t^(n - 1))"
      using Limits.tendsto_divide[OF Limits.tendsto_power[where n = "n-1"], where g = "\<lambda>t'. - 1" and b = "- 1"]
      by auto
    then have "(\<lambda>t'. -n * t' ^ (n - 1) / - 1) \<midarrow>t\<rightarrow> - n * - (t ^ (n - 1))"
      using Limits.tendsto_mult_left[where c = "- n" and f = "\<lambda>t'. t'^(n - 1) / - 1" and l = "- (t^(n - 1))"]
      by auto
    then show "(\<lambda>t'. -n * t' ^ (n - 1) / - 1) \<midarrow>t\<rightarrow> n * t ^ (n - 1)"
      by (rule LIM_cong_limit) auto
  qed
qed

lemma obt_pos_diff:
  fixes x y :: real
  assumes "x < y"
  obtains k where "0 < k" and "y = x + k"
proof -
  from assms have "\<exists>k. 0 < k \<and> y = x + k"
    by (auto intro:exI[where x="y - x"])
  thus "(\<And>k. 0 < k \<Longrightarrow> y = x + k \<Longrightarrow> thesis) \<Longrightarrow> thesis"
    by auto
qed

lemma obt_neg_diff:
  fixes x y :: real
  assumes "x < y"
  obtains l where "0 < l" and "x = y - l"
proof -
  from assms have "\<exists>l. 0 < l \<and> x = y - l"
    by (auto intro:exI[where x="y - x"])
  thus  "(\<And>l. 0 < l \<Longrightarrow> x = y - l \<Longrightarrow> thesis) \<Longrightarrow> thesis"  
    by auto
qed

lemma det3_aux1:   
  assumes "y2 < y1"
  assumes "0 < k"
  shows "det3 (x1, y1) (x2, y2) (x2 + k, y2) > 0"
  using assms unfolding det3_def'
  by (auto simp add:field_simps)

lemma det3_aux1':   
  assumes "y2 \<le> y1"
  assumes "0 < k"
  shows "det3 (x1, y1) (x2, y2) (x2 + k, y2) \<ge> 0"
  using assms unfolding det3_def'
  by (auto simp add:field_simps)

lemma det3_aux1'':   
  assumes "y2 < y1"
  assumes "0 \<le> k"
  assumes "y2 \<le> y3"
  assumes "x1 \<le> x2"
  shows "det3 (x1, y1) (x2, y2) (x2 + k, y3) \<ge> 0"
proof -
  have "x1 * (y3 - y2) \<le> x2 * (y3 - y2)" (is "?lhs1 \<le> ?rhs1") using `x1 \<le> x2` `y2 \<le> y3` 
    using mult_right_mono[OF `x1 \<le> x2`, of "y3 - y2"] by auto
  moreover have "0 \<le> k * (y1 - y2)" using `0 \<le> k` `y2 < y1` by auto
  ultimately have "?lhs1 \<le> k * (y1 - y2) + ?rhs1" by auto
  hence "k * y2 + (x1 * y3 + x2 * y2) \<le> k * y1 + (x1 * y2 + x2 * y3)"
    by (auto simp add:field_simps)
  thus ?thesis unfolding det3_def' by (auto simp add:field_simps)
qed

lemma det3_aux2:
  assumes "0 < l" and "0 < k"
  shows "det3 (x1, y1) (x1 + l, y1) (x3, y1 + k) > 0"
  using assms unfolding det3_def'
  by (auto simp add:field_simps)

lemma det3_aux2':
  assumes "0 < l" and "0 \<le> k"
  shows "det3 (x1, y1) (x1 + l, y1) (x3, y1 + k) \<ge> 0"
  using assms unfolding det3_def'
  by (auto simp add:field_simps)

lemma det3_aux3:
  assumes "0 < l" and "0 < k"
  shows "det3 (x1, y1) (x1, y1 + l) (x1 - k, y1 + l) > 0"
  using assms unfolding det3_def'
  by (auto simp add:field_simps)

lemma det3_aux3':
  assumes "0 < l" and "0 \<le> k"
  shows "det3 (x1, y1) (x1, y1 + l) (x1 - k, y2) \<ge> 0"
  using assms unfolding det3_def'
  by (auto simp add:field_simps)

lemma det3_aux4:
  assumes "0 < l" and "y3 < y1"
  shows "det3 (x1, y1) (x1 - l, y1) (x3, y3) > 0"
  using assms unfolding det3_def'
  by (auto simp add:field_simps)

lemma det3_aux4':
  assumes "0 < l" and "y3 \<le> y1"
  shows "det3 (x1, y1) (x1 - l, y1) (x3, y3) \<ge> 0"
  using assms unfolding det3_def'
  by (auto simp add:field_simps)

lemma det3_aux4'':
  assumes "0 \<le> l" and "y3 < y2" and "y1 \<le> y2" and "x3 \<le> x1 - l"
  shows "det3 (x1, y1) (x1 - l, y2) (x3, y3) \<ge> 0"
proof -
  have "x3 * (y2 - y1) \<le> (x1 - l) * (y2 - y1)"
    using mult_right_mono[OF `x3 \<le> x1 - l`, of "y2 - y1"] using `y1 \<le> y2`
    by auto
  hence "x3 * (y2 - y1) \<le> x1 * (y2 - y1) - l * (y2 - y1)"
    by (auto simp add:field_simps)
  hence 0: "x3 * (y2 - y1) + l * y2 \<le> l * y1 + x1 * (y2 - y1)"
    by (auto simp add:field_simps)
  have "l * y3 \<le> l * y2" using `y3 < y2` and `0 \<le> l` 
    using mult_left_mono[OF _ `0 \<le> l`, of "y3" "y2"] `y3 < y2` by auto
  with 0 have "l * y3 + x3 * (y2 - y1) \<le> l * y1 + x1 * (y2 - y1)"
    by auto
  hence "l * y3 + (x1 * y1 + x3 * y2) \<le> l * y1 + (x1 * y2 + x3 * y1)"
    by (auto simp add:field_simps)
  thus ?thesis 
    unfolding det3_def' by (auto simp add:field_simps)
qed

lemma det3_aux5:
  assumes "0 < m" and "0 < l" and "0 < k" and "k < m"
  shows "det3 (x1, y1) (x1 - l, y1 - k) (x1 - l, y1 - m) > 0"
  using assms unfolding det3_def'
  by (auto simp add:field_simps)

lemma det3_aux6: 
  assumes "x3 < x1" and "y1 < y2"
  shows "det3 (x1, y1) (x1, y2) (x3, y3) > 0"
  using assms unfolding det3_rotate[of "(x1, y1)"] det3_def' 
  by (smt fst_conv left_diff_distrib' linordered_comm_semiring_strict_class.comm_mult_strict_left_mono 
      mult.commute snd_conv)

lemma det3_aux7:
  assumes "x1 < x3" and "y2 < y1"
  shows "det3 (x1, y1) (x1, y2) (x3, y3) > 0"
  using assms unfolding det3_def' 
  by (smt left_diff_distrib' linordered_comm_semiring_strict_class.comm_mult_strict_left_mono 
      mult.commute prod.sel(1) snd_conv)

lemma power2_mono_lt:
  fixes x y :: real
  assumes "x < y" and "0 \<le> x"
  shows "x\<^sup>2 < y\<^sup>2"
  using mult_strict_mono[OF assms(1) assms(1)] assms
  unfolding power2_eq_square by auto

lemma power2_mono_leq:
  fixes x y :: real
  assumes "x \<le> y" and "0 \<le> x"
  shows "x\<^sup>2 \<le> y\<^sup>2"
  using mult_mono[OF assms(1) assms(1)] assms
  unfolding power2_eq_square by auto

section \<open>Circles\<close>

text \<open>A circle consists of a center and a radius.\<close>

record circle =
  Center :: real2
  Radius :: real

definition on_circle :: "circle \<Rightarrow> real2 \<Rightarrow> bool" where
  "on_circle c p \<longleftrightarrow> (fst p - fst (Center c))\<^sup>2 + (snd p - snd (Center c))\<^sup>2 = (Radius c)\<^sup>2"

definition range_of_circle :: "circle \<Rightarrow> (real \<times> real) set" where
  "range_of_circle c = {(x, y). (x - fst (Center c))\<^sup>2 + (y - snd (Center c))\<^sup>2 \<le> (Radius c)\<^sup>2}"

lemma on_circle_inside_range:
  assumes "on_circle c p"
  shows "p \<in> range_of_circle c"
  using assms unfolding on_circle_def range_of_circle_def by auto

definition left_half_range :: "circle \<Rightarrow> (real \<times> real) set" where
  "left_half_range c = (let x1 = fst (Center c); y1 = snd (Center c); r = Radius c 
                        in {(x, y). x1 - \<bar>r\<bar> \<le> x \<and> x \<le> x1 \<and> (x - x1)\<^sup>2 + (y - y1)\<^sup>2 \<le> r\<^sup>2})"

definition right_half_range :: "circle \<Rightarrow> (real \<times> real) set" where
  "right_half_range c = (let x1 = fst (Center c); y1 = snd (Center c); r = Radius c
                         in {(x, y). x1 \<le> x \<and> x \<le> x1 + \<bar>r\<bar> \<and> (x - x1)\<^sup>2 + (y - y1)\<^sup>2 \<le> r\<^sup>2})"

lemma range_circle_decompose:
  "range_of_circle c = left_half_range c \<union> right_half_range c"
proof (rule equalityI, rule_tac[!] subsetI)
  fix p 
  assume "p \<in> range_of_circle c"
  then obtain x and y where p_def: "p = (x, y)"  by fastforce
  define x1 where "x1 = fst (Center c)"
  define y1 where "y1 = snd (Center c)"
  define r where "r = Radius c"

  have 0: "(x - x1)\<^sup>2 + (y - y1)\<^sup>2 \<le> r\<^sup>2" using `p = (x, y)` `p \<in> range_of_circle c`
    unfolding range_of_circle_def  x1_def y1_def r_def by auto
  hence "(x - x1)\<^sup>2 \<le> r\<^sup>2" (is "?lhs1 \<le> ?rhs1") using zero_le_power2[of "y - y1"] by linarith
  hence "x1 - \<bar>r\<bar> \<le> x \<and> x \<le> x1 \<or> x1 \<le> x \<and> x \<le> x1 + \<bar>r\<bar>" (is "?case1 \<or> ?case2")
    using real_sqrt_le_iff[of "(x-x1)\<^sup>2" "r\<^sup>2"] by auto
  thus "p \<in> left_half_range c \<union> right_half_range c"
    unfolding left_half_range_def right_half_range_def Let_def 
    x1_def[THEN sym] r_def[THEN sym] y1_def[THEN sym] p_def using 0 by auto
next
  fix p
  assume asm: "p \<in> left_half_range c \<union> right_half_range c"
  then obtain x and y where p_def: "p = (x, y)" by fastforce
  define x1 where "x1 = fst (Center c)"
  define y1 where "y1 = snd (Center c)"
  define r where "r = Radius c"
  from asm have "p \<in> left_half_range c \<or> p \<in> right_half_range c" 
    by auto
  moreover
  { assume "p \<in> left_half_range c"
    hence "x1 - \<bar>r\<bar> \<le> x \<and> x \<le> x1 \<and> (x - x1)\<^sup>2 + (y - y1)\<^sup>2 \<le> r\<^sup>2 "
      unfolding left_half_range_def p_def x1_def y1_def r_def Let_def
      by auto
    hence "p \<in> range_of_circle c"
      unfolding range_of_circle_def p_def x1_def y1_def r_def Let_def 
      by auto }
  moreover
  { assume "p \<in> right_half_range c"
    hence "x1 \<le> x \<and> x \<le> x1 + \<bar>r\<bar> \<and> (x - x1)\<^sup>2 + (y - y1)\<^sup>2 \<le> r\<^sup>2"
      unfolding  right_half_range_def p_def x1_def y1_def r_def Let_def
      by auto
    hence "p \<in> range_of_circle c"
      unfolding range_of_circle_def p_def x1_def y1_def r_def Let_def
      by auto }
  ultimately show "p \<in> range_of_circle c"
    by auto
qed
  
text \<open>@{term "on_circle c p"} states that point @{term "p :: real2"} lies on circle 
      @{term "c :: circle"}.\<close>

section \<open>Circle intersection\<close>

text \<open>Closed form of the two possible intersection points of two circles with different 
      x-coordinates of center. Has a similar assumption to @{thm quadratic_eq}.\<close>


locale circle_intersection2 =
  fixes c1 c2 :: circle
  fixes x1 x2 y1 y2 r1 r2 :: real
  assumes c1_def: "Center c1 = (x1, y1)" and c2_def: "Center c2 = (x2, y2)" and 
          r1_def: "Radius c1 = r1" and r2_def: "Radius c2 = r2"

begin

lemmas ci_axioms = c1_def c2_def r1_def r2_def fst_conv snd_conv

subsection "Finding quadratic equation when two circle intersects"

definition \<alpha>\<^sub>1 where "\<alpha>\<^sub>1 \<equiv> (y2 - y1)/(x1 - x2)"
definition \<beta>\<^sub>1 where "\<beta>\<^sub>1 \<equiv> (x1\<^sup>2 - x2\<^sup>2 + y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2))"

lemma beta1_alt_def:
  assumes "x1 \<noteq> x2"
  shows "\<beta>\<^sub>1 = (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - (x1 - x2)/2 + x1"
proof -
  have "\<beta>\<^sub>1 - x1 = (x1\<^sup>2 - x2\<^sup>2 + y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - x1"
    unfolding \<beta>\<^sub>1_def by auto
  also have "\<dots> = (x1\<^sup>2 - x2\<^sup>2 + y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - (x1 * 2 * (x1 - x2))/(2 * (x1 - x2))"
  proof -
    have "2 * (x1 - x2) \<noteq> 0"
      using assms unfolding ci_axioms by auto
    then have "x1 = x1 * (2 * (x1 - x2))/(2 * (x1 - x2))"
      by auto
    also have "\<dots> = (x1 * 2 * (x1 - x2))/(2 * (x1 - x2))"
      by algebra
    finally show ?thesis by auto
  qed
  also have "\<dots> = ((x1\<^sup>2 - x2\<^sup>2 + y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) - x1 * 2 * (x1 - x2))/(2 * (x1 - x2))"
    by algebra
  also have "\<dots> = (x1\<^sup>2 - x2\<^sup>2 + y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2 - 2 * x1\<^sup>2 + 2 * x1 * x2)/(2 * (x1 - x2))"
    by algebra
  also have "\<dots> = (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2 - x1\<^sup>2 - x2\<^sup>2 + 2 * x1 * x2)/(2 * (x1 - x2))"
    by auto
  also have "\<dots> = (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2 - (x1 - x2)\<^sup>2)/(2 * (x1 - x2))"
    by algebra
  also have "\<dots> = (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - (x1 - x2) * (x1 - x2)/(2 * (x1 - x2))"
    by algebra
  also have "\<dots> = (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - (x1 - x2)/2"
    using assms mult_divide_mult_cancel_left[where a = "x1 - x2" and b = 2 and c = "x1 - x2"]
    unfolding ci_axioms by argo
  finally show ?thesis using assms by auto
qed

definition \<alpha>\<^sub>2 where \<alpha>\<^sub>2: "\<alpha>\<^sub>2 \<equiv> 1 + \<alpha>\<^sub>1\<^sup>2" 
definition \<beta>\<^sub>2 where \<beta>\<^sub>2: "\<beta>\<^sub>2 \<equiv> 2 * \<alpha>\<^sub>1 * \<beta>\<^sub>1 - 2 * \<alpha>\<^sub>1 * x1 - 2 * y1" 
definition \<gamma>\<^sub>2 where \<gamma>\<^sub>2: "\<gamma>\<^sub>2 \<equiv> (\<beta>\<^sub>1 - x1)\<^sup>2 + y1\<^sup>2 - r1\<^sup>2"

lemma alpha2_neq_zero:
  "\<alpha>\<^sub>2 \<noteq> 0" using \<alpha>\<^sub>2 zero_le_power2[of \<alpha>\<^sub>1] by argo

lemma ci_solution:
  assumes "x1 \<noteq> x2"
  shows "on_circle c1 (x, y) \<and> on_circle c2 (x, y) \<longleftrightarrow> 
                    \<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2 \<ge> 0
                 \<and> (\<exists>\<sigma> \<in> {-1,1}. (x, y) = ( (- \<beta>\<^sub>2 + \<sigma> * sqrt (\<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2))/(2 * \<alpha>\<^sub>2) * \<alpha>\<^sub>1 + \<beta>\<^sub>1
                                     , (- \<beta>\<^sub>2 + \<sigma> * sqrt (\<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2))/(2 * \<alpha>\<^sub>2)))"
proof -
  have "on_circle c1 (x, y) \<and> on_circle c2 (x, y) \<longleftrightarrow> 
                                           (x - x1)\<^sup>2 + (y - y1)\<^sup>2 = r1\<^sup>2 \<and> (x - x2)\<^sup>2 + (y - y2)\<^sup>2 = r2\<^sup>2"
    unfolding on_circle_def ci_axioms by auto
  also have "\<dots> \<longleftrightarrow> x\<^sup>2 - 2 * x * x1 + x1\<^sup>2 + y\<^sup>2 - 2 * y * y1 + y1\<^sup>2 = r1\<^sup>2
                    \<and> x\<^sup>2 - 2 * x * x1 + x1\<^sup>2 + y\<^sup>2 - 2 * y * y1 + y1\<^sup>2 - (x\<^sup>2 - 2 * x * x2 + x2\<^sup>2 + y\<^sup>2 - 2 * y * y2 + y2\<^sup>2) = r1\<^sup>2 - r2\<^sup>2"
    using power2_diff[where x = x] power2_diff[where x = y] by auto    
  also have "\<dots> \<longleftrightarrow> x\<^sup>2 - 2 * x * x1 + x1\<^sup>2 + y\<^sup>2 - 2 * y * y1 + y1\<^sup>2 = r1\<^sup>2
                    \<and> x = y * \<alpha>\<^sub>1 + \<beta>\<^sub>1"
  proof -
    have "x\<^sup>2 - 2 * x * x1 + x1\<^sup>2 + y\<^sup>2 - 2 * y * y1 + y1\<^sup>2 - (x\<^sup>2 - 2 * x * x2 + x2\<^sup>2 + y\<^sup>2 - 2 * y * y2 + y2\<^sup>2) = r1\<^sup>2 - r2\<^sup>2
          \<longleftrightarrow>  x = ((- 2 * y * (y1 - y2)) + (x1\<^sup>2 - x2\<^sup>2 + y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(2 * (x1 - x2))"
      using assms eq_divide_eq[where c = "2 * (x1 - x2)"] unfolding ci_axioms 
      by (auto simp add:field_simps)
    also have "\<dots> \<longleftrightarrow> x = (- 2 * y * (y1 - y2))/(2 * (x1 - x2)) + (x1\<^sup>2 - x2\<^sup>2 + y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2))"
      using add_divide_distrib[where a = "- 2 * y * (y1 - y2)" and c = "2 * (x1 - x2)"] by auto
    also have "\<dots> \<longleftrightarrow> x = y * \<alpha>\<^sub>1 + \<beta>\<^sub>1"
    proof -
      have "(- 2 * y * (y1 - y2))/(2 * (x1 - x2)) = y * (2 * (y2 - y1)/(2 * (x1 - x2)))"
        by algebra
      also have "\<dots> = y * (y2 - y1)/(x1 - x2)"
        using mult_divide_mult_cancel_left[where a = "y2 - y1" and b = "x1 - x2" and c = 2] by auto
      finally show ?thesis using \<alpha>\<^sub>1_def \<beta>\<^sub>1_def by auto
    qed
    finally show ?thesis by auto
  qed
  also have "\<dots> \<longleftrightarrow> y\<^sup>2 * \<alpha>\<^sub>2 + y * \<beta>\<^sub>2 + \<gamma>\<^sub>2 = 0 \<and> x = y * \<alpha>\<^sub>1 + \<beta>\<^sub>1"
    using \<alpha>\<^sub>2 \<beta>\<^sub>2 \<gamma>\<^sub>2 by algebra
  also have "\<dots> \<longleftrightarrow> \<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2 \<ge> 0
                    \<and> (\<exists>\<sigma> \<in> {-1,1}. y = (- \<beta>\<^sub>2 + \<sigma> * sqrt (\<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2))/(2 * \<alpha>\<^sub>2))
                    \<and> x = y * \<alpha>\<^sub>1 + \<beta>\<^sub>1"
  proof -
    have "y\<^sup>2 * \<alpha>\<^sub>2 + y * \<beta>\<^sub>2 + \<gamma>\<^sub>2 = 0 \<longleftrightarrow> \<alpha>\<^sub>2 * y\<^sup>2 + \<beta>\<^sub>2 * y + \<gamma>\<^sub>2 = 0"
      by algebra
    thus ?thesis using quadratic_eq[OF alpha2_neq_zero] by auto
  qed
  also have "\<dots> \<longleftrightarrow> \<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2 \<ge> 0
                    \<and> (\<exists>\<sigma> \<in> {-1,1}. (x, y) = ( (- \<beta>\<^sub>2 + \<sigma> * sqrt (\<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2))/(2 * \<alpha>\<^sub>2) * \<alpha>\<^sub>1 + \<beta>\<^sub>1
                                        , (- \<beta>\<^sub>2 + \<sigma> * sqrt (\<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2))/(2 * \<alpha>\<^sub>2)))"
    using prod.collapse[of "(x, y)"] by fastforce
  finally show ?thesis by auto
qed

subsection "More explicit expression of discriminant"

lemma \<alpha>\<^sub>2_unfold: "\<alpha>\<^sub>2 = 1 + (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2"
  using \<alpha>\<^sub>1_def \<alpha>\<^sub>2 power_divide by auto

lemma \<beta>\<^sub>2_unfold: 
  assumes "x1 \<noteq> x2"
  shows "\<beta>\<^sub>2 = ((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 - y1 - y2"
proof -
  have "\<beta>\<^sub>2 = 2 * \<alpha>\<^sub>1 * \<beta>\<^sub>1 - 2 * \<alpha>\<^sub>1 * x1 - 2 * y1"
    using \<beta>\<^sub>2 by auto
  also have "\<dots> = 2 * \<alpha>\<^sub>1 * (\<beta>\<^sub>1 - x1) - 2 * y1"
    by algebra
  also have "\<dots> = 2 * \<alpha>\<^sub>1 * ((y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - (x1 - x2)/2) - 2 * y1"
    using beta1_alt_def assms unfolding \<beta>\<^sub>1_def  by auto
  also have "\<dots> = 2 * (y2 - y1)/(x1 - x2) * ((y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - (x1 - x2)/2) - 2 * y1"
    using \<alpha>\<^sub>1_def by auto
  also have "\<dots> = 2 * (y2 - y1)/(x1 - x2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - 2 * (y2 - y1)/(x1 - x2) * (x1 - x2)/2 - 2 * y1"
    by algebra
  also have "\<dots> = ((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/((x1 - x2)\<^sup>2) - y1 - y2"
  proof -
    have "2 * (y2 - y1)/(x1 - x2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) = ((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/((x1 - x2)\<^sup>2)"
    proof -
      have "2 * (y2 - y1)/(x1 - x2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) =  2 * ((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2) * 1/(2 * (x1 - x2))"
        by auto
      also have "\<dots> = 2 * ((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/((x1 - x2) * 2 * (x1 - x2))"
        by auto
      also have "\<dots> = 2 * ((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(2 * (x1 - x2)\<^sup>2)"
        by algebra
      also have "\<dots> = ((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2"
        by auto
      finally show ?thesis .
    qed
    moreover have "- 2 * (y2 - y1)/(x1 - x2) * (x1 - x2)/2 = y1 - y2"
    proof -
      have "- 2 * (y2 - y1)/(x1 - x2) * (x1 - x2)/2 = - ((x1 - x2) * (y2 - y1)/(x1 - x2))"
        by algebra
      also have "\<dots> = - (y2 - y1)"
        using assms unfolding ci_axioms by auto
      also have "\<dots> = y1 - y2"
        by auto
      finally show ?thesis .
    qed
    ultimately show ?thesis by linarith
  qed
  finally show ?thesis .
qed

lemma minus_\<beta>\<^sub>2_unfold: 
  assumes "x1 \<noteq> x2"
  shows "-\<beta>\<^sub>2 = ((y1 - y2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 + y1 + y2"
proof -
  have "-\<beta>\<^sub>2 = - (((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 - y1 - y2)"
    using \<beta>\<^sub>2_unfold[OF assms] by auto
  also have "\<dots> = (-(y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 + y1 + y2"
    by linarith
  also have "\<dots> = ((y1 - y2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 + y1 + y2"
    by auto
  finally show ?thesis .
qed

lemma \<gamma>\<^sub>2_unfold: 
  assumes "x1 \<noteq> x2"
  shows "\<gamma>\<^sub>2 = ((y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - (x1 - x2)/2)\<^sup>2 + y1\<^sup>2 - r1\<^sup>2"
proof -
  have "\<gamma>\<^sub>2 = (\<beta>\<^sub>1 - x1)\<^sup>2 + y1\<^sup>2 - r1\<^sup>2"
    using \<gamma>\<^sub>2 by auto
  also have "\<dots> = ((y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - (x1 - x2)/2)\<^sup>2 + y1\<^sup>2 - r1\<^sup>2"
    using beta1_alt_def[OF assms] by auto
  finally show ?thesis .
qed

lemma discriminant_unfold: 
  assumes "x1 \<noteq> x2"
  shows "\<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2 = (- 1 * y1^4 - 6 * y1\<^sup>2 * y2\<^sup>2 + 4 * y1^3 * y2 + 4 * y1 * y2^3 - y2^4 + 2 * (y1\<^sup>2 + y2\<^sup>2) * (r1\<^sup>2 + r2\<^sup>2) - (r1\<^sup>2 - r2\<^sup>2)\<^sup>2 - 4 * y1 * y2 * (r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 - 2 * (y1 - y2)\<^sup>2 + 2 * (r1\<^sup>2 + r2\<^sup>2) - (x1 - x2)\<^sup>2"
proof -
  have "\<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2 = (((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 - y1 - y2)\<^sup>2 - 4 * (1 + (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2) * (((y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - (x1 - x2)/2)\<^sup>2 + y1\<^sup>2 - r1\<^sup>2)"
    using \<alpha>\<^sub>2_unfold \<beta>\<^sub>2_unfold[OF assms] \<gamma>\<^sub>2_unfold[OF assms] by auto
  also have "\<dots> = ((y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2)/(x1 - x2)^4 + 2 * ((y1\<^sup>2 - y2\<^sup>2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 + (y1 + y2)\<^sup>2 - 4 * (1 + (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2) * (((y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - (x1 - x2)/2)\<^sup>2 + y1\<^sup>2 - r1\<^sup>2)"
  proof -
    have "(((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 - y1 - y2)\<^sup>2 = (((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 - (y1 + y2))\<^sup>2"
      by algebra
    also have "\<dots> = (((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 - (y1 + y2)*(x1 - x2)\<^sup>2/(x1 - x2)\<^sup>2)\<^sup>2"
      using assms unfolding ci_axioms by auto
    also have "\<dots> = (((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) - (y1 + y2) * (x1 - x2)\<^sup>2)/(x1 - x2)\<^sup>2)\<^sup>2"
      by algebra
    also have "\<dots> = ((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) - (y1 + y2) * (x1 - x2)\<^sup>2)\<^sup>2/((x1 - x2)\<^sup>2)\<^sup>2"
      using power_divide by blast
    also have "\<dots> = ((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) - (y1 + y2) * (x1 - x2)\<^sup>2)\<^sup>2/(x1 - x2)^4"
      by auto
    also have "\<dots> = (((y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))\<^sup>2 - 2 * (y2 - y1) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) * (y1 + y2) * (x1 - x2)\<^sup>2 + ((y1 + y2) * (x1 - x2)\<^sup>2)\<^sup>2)/(x1 - x2)^4"
      by algebra
    also have "\<dots> = ((y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2 + 2 * (y1 - y2) * (y1 + y2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) * (x1 - x2)\<^sup>2 + (y1 + y2)\<^sup>2 * (x1 - x2)^4)/(x1 - x2)^4"
      by algebra
    also have "\<dots> = ((y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2 + 2 * (y1\<^sup>2 - y2\<^sup>2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) * (x1 - x2)\<^sup>2 + (y1 + y2)\<^sup>2 * (x1 - x2)^4)/(x1 - x2)^4"
      by algebra
    also have "\<dots> = ((y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2)/(x1 - x2)^4 + 2 * (x1 - x2)\<^sup>2 * ((y1\<^sup>2 - y2\<^sup>2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)^4 + (x1 - x2)^4 * ((y1 + y2)\<^sup>2)/(x1 - x2)^4"
      by algebra
    also have "\<dots> = ((y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2)/(x1 - x2)^4 + 2 * ((y1\<^sup>2 - y2\<^sup>2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 + (y1 + y2)\<^sup>2"
    proof -
      have " 2 * (x1 - x2)\<^sup>2 * ((y1\<^sup>2 - y2\<^sup>2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)^4 = 2 * ((y1\<^sup>2 - y2\<^sup>2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2"
      proof -
        have "(x1 - x2)\<^sup>2 \<noteq> 0" using assms unfolding ci_axioms by auto
        then show ?thesis
          using mult_divide_mult_cancel_left[where a = "(y1\<^sup>2 - y2\<^sup>2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)" and b = "(x1 - x2)\<^sup>2" and c = "(x1 - x2)\<^sup>2"]
          by auto
      qed
      moreover have "(x1 - x2)^4 * ((y1 + y2)\<^sup>2)/(x1 - x2)^4 = (y1 + y2)\<^sup>2"
        using assms mult_divide_mult_cancel_left unfolding ci_axioms by auto
      ultimately show ?thesis by auto
    qed
    finally show ?thesis by auto
  qed
  also have "\<dots> = ((y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2)/(x1 - x2)^4 + 2 * ((y1\<^sup>2 - y2\<^sup>2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 + (y1 + y2)\<^sup>2 - (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(x1 - x2)\<^sup>2 + 2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) - (x1 - x2)\<^sup>2 - 4 * y1\<^sup>2 + 4 * r1\<^sup>2 - (y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(x1 - x2)^4 + 2 * (y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(x1 - x2)\<^sup>2 - (y2 - y1)\<^sup>2 - 4 * (y2 - y1)\<^sup>2 * y1\<^sup>2/(x1 - x2)\<^sup>2 + 4 * (y2 - y1)\<^sup>2 * r1\<^sup>2 / (x1 - x2)\<^sup>2"
  proof -
    have "- 4 * (1 + (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2) * (((y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - (x1 - x2)/2)\<^sup>2 + y1\<^sup>2 - r1\<^sup>2) = - 4 * (1 + (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2) * ((y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(4 * (x1 - x2)\<^sup>2) - (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/2 + (x1 - x2)\<^sup>2/4 + y1\<^sup>2 - r1\<^sup>2)"
    proof -
      have "((y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) - (x1 - x2)/2)\<^sup>2 = ((y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)))\<^sup>2 - 2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) * (x1 - x2)/2 + ((x1 - x2)/2)\<^sup>2"
        using power2_diff[where x = "(y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2))"] by auto
      also have "\<dots> = (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(2 * (x1 - x2))\<^sup>2 - 2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) * (x1 - x2)/2 + ((x1 - x2)/2)\<^sup>2"
        using power_divide by auto
      also have "\<dots> = (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(2 * (x1 - x2))\<^sup>2 - 2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) * (x1 - x2)/2 + (x1 - x2)\<^sup>2/4"
        using power_divide[where a = "x1 - x2"] by auto
      also have "\<dots> = (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(4 * (x1 - x2)\<^sup>2) - 2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2)) * (x1 - x2)/2 + (x1 - x2)\<^sup>2/4"
        by algebra
      also have "\<dots> = (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(4 * (x1 - x2)\<^sup>2) - (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(x1 - x2) * (x1 - x2)/2 + (x1 - x2)\<^sup>2/4"
        using mult_divide_mult_cancel_left[where a = "y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2" and b = "x1 - x2" and c = 2] by auto
      also have "\<dots> = (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(4 * (x1 - x2)\<^sup>2) - (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/2 + (x1 - x2)\<^sup>2/4"
        using assms unfolding ci_axioms by auto
      finally show ?thesis by auto
    qed
    also have "\<dots> = - 4 * ((y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(4 * (x1 - x2)\<^sup>2) - (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/2 + (x1 - x2)\<^sup>2/4 + y1\<^sup>2 - r1\<^sup>2) - 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * ((y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(4 * (x1 - x2)\<^sup>2) - (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/2 + (x1 - x2)\<^sup>2/4 + y1\<^sup>2 - r1\<^sup>2)"
      by algebra
    also have "\<dots> = - 4 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(4 * (x1 - x2)\<^sup>2) + 4 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/2 - 4 * (x1 - x2)\<^sup>2/4 - 4 * y1\<^sup>2 + 4 * r1\<^sup>2 - 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(4 * (x1 - x2)\<^sup>2) + 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/2 - 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * (x1 - x2)\<^sup>2/4 - 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * y1\<^sup>2 + 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * r1\<^sup>2"
      by algebra
    also have "\<dots> = - (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(x1 - x2)\<^sup>2 + 2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) - (x1 - x2)\<^sup>2 - 4 * y1\<^sup>2 + 4 * r1\<^sup>2 - 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(4 * (x1 - x2)\<^sup>2) + 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/2 - 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * (x1 - x2)\<^sup>2/4 - 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * y1\<^sup>2 + 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * r1\<^sup>2"
      by linarith
    also have "\<dots> = - (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(x1 - x2)\<^sup>2 + 2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) - (x1 - x2)\<^sup>2 - 4 * y1\<^sup>2 + 4 * r1\<^sup>2 - (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(x1 - x2)\<^sup>2 + 2 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) - (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * (x1 - x2)\<^sup>2 - 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * y1\<^sup>2 + 4 * (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2 * r1\<^sup>2"
      by auto
    also have "\<dots> = - (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(x1 - x2)\<^sup>2 + 2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) - (x1 - x2)\<^sup>2 - 4 * y1\<^sup>2 + 4 * r1\<^sup>2 - (y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(x1 - x2)^4 + 2 * (y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(x1 - x2)\<^sup>2 - (y2 - y1)\<^sup>2 - 4 * (y2 - y1)\<^sup>2 * y1\<^sup>2/(x1 - x2)\<^sup>2 + 4 * (y2 - y1)\<^sup>2 * r1\<^sup>2 / (x1 - x2)\<^sup>2"
      using assms unfolding ci_axioms by auto
    finally show ?thesis by linarith
  qed
  also have "\<dots> = 2 * ((y1\<^sup>2 - y2\<^sup>2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 + (y1 + y2)\<^sup>2 - (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(x1 - x2)\<^sup>2 + 2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) - (x1 - x2)\<^sup>2 - 4 * y1\<^sup>2 + 4 * r1\<^sup>2 + 2 * (y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(x1 - x2)\<^sup>2 - (y2 - y1)\<^sup>2 - 4 * (y2 - y1)\<^sup>2 * y1\<^sup>2/(x1 - x2)\<^sup>2 + 4 * (y2 - y1)\<^sup>2 * r1\<^sup>2 / (x1 - x2)\<^sup>2"
    by auto
  also have "\<dots> = 2 * (y1^4 - 2 * y1\<^sup>2 * y2\<^sup>2 - y1\<^sup>2 * r1\<^sup>2 + y1\<^sup>2 * r2\<^sup>2 + y2^4 + y2\<^sup>2 * r1\<^sup>2 - y2\<^sup>2 * r2\<^sup>2)/(x1 - x2)\<^sup>2
    + y1\<^sup>2 + 2 * y1 * y2 + y2\<^sup>2
    - (y1^4 - 2 * y1\<^sup>2 * y2\<^sup>2 + y2^4 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 + 2 * y2\<^sup>2 * r1\<^sup>2 - 2 * y2\<^sup>2 * r2\<^sup>2 + r1^4 - 2 * r1\<^sup>2 * r2\<^sup>2 + r2^4)/(x1 - x2)\<^sup>2
    + 2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)
    - (x1\<^sup>2 - 2 * x1 * x2 + x2\<^sup>2)
    - 4 * y1\<^sup>2 + 4 * r1\<^sup>2
    + (2 * y1^4 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 - 4 * y1^3 * y2 + 4 * y1 * y2^3 + 4 * y1 * y2 * r1\<^sup>2 - 4 * y1 * y2 * r2\<^sup>2 - 2 * y2^4 - 2 * y2\<^sup>2 * r1\<^sup>2 + 2 * y2\<^sup>2 * r2\<^sup>2)/(x1 - x2)\<^sup>2
    - (y1\<^sup>2 - 2 * y1 * y2 + y2\<^sup>2)
    - (4 * y1^4 - 8 * y1^3 * y2 + 4 * y1\<^sup>2 * y2\<^sup>2)/(x1 - x2)\<^sup>2
    + (4 * y1\<^sup>2 * r1\<^sup>2 - 8 * y1 * y2 * r1\<^sup>2 + 4 * y2\<^sup>2 * r1\<^sup>2)/(x1 - x2)\<^sup>2"
  proof -
    have "2 * ((y1\<^sup>2 - y2\<^sup>2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 = 2 * (y1^4 - 2 * y1\<^sup>2 * y2\<^sup>2 - y1\<^sup>2 * r1\<^sup>2 + y1\<^sup>2 * r2\<^sup>2 + y2^4 + y2\<^sup>2 * r1\<^sup>2 - y2\<^sup>2 * r2\<^sup>2)/(x1 - x2)\<^sup>2"
    proof -
      have "(y1\<^sup>2 - y2\<^sup>2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) = y1\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) - y2\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)"
        by algebra
      also have "\<dots> = y1^4 - y1\<^sup>2 * y2\<^sup>2 - y1\<^sup>2 * r1\<^sup>2 + y1\<^sup>2 * r2\<^sup>2 - y2\<^sup>2 * y1\<^sup>2 + y2^4 + y2\<^sup>2 * r1\<^sup>2 - y2\<^sup>2 * r2\<^sup>2"
        by algebra
      also have "\<dots> = y1^4 - 2 * y1\<^sup>2 * y2\<^sup>2 - y1\<^sup>2 * r1\<^sup>2 + y1\<^sup>2 * r2\<^sup>2 + y2^4 + y2\<^sup>2 * r1\<^sup>2 - y2\<^sup>2 * r2\<^sup>2"
        by auto
      finally show ?thesis by auto
    qed
    moreover have "(y1 + y2)\<^sup>2 = y1\<^sup>2 + 2 * y1 * y2 + y2\<^sup>2"
      by algebra
    moreover have "(y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2/(x1 - x2)\<^sup>2 = (y1^4 - 2 * y1\<^sup>2 * y2\<^sup>2 + y2^4 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 + 2 * y2\<^sup>2 * r1\<^sup>2 - 2 * y2\<^sup>2 * r2\<^sup>2 + r1^4 - 2 * r1\<^sup>2 * r2\<^sup>2 + r2^4)/(x1 - x2)\<^sup>2"
    proof -
      have "(y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)\<^sup>2 = (y1\<^sup>2 - y2\<^sup>2 - (r1\<^sup>2 - r2\<^sup>2))\<^sup>2"
        by algebra
      also have "\<dots> = (y1\<^sup>2 - y2\<^sup>2)\<^sup>2 - 2 * (y1\<^sup>2 - y2\<^sup>2) * (r1\<^sup>2 - r2\<^sup>2) + (r1\<^sup>2 - r2\<^sup>2)\<^sup>2"
        using power2_diff[where x = "y1\<^sup>2 - y2\<^sup>2"] by auto
      also have "\<dots> = y1^4 - 2 * y1\<^sup>2 * y2\<^sup>2 + y2^4 - 2 * (y1\<^sup>2 * r1\<^sup>2 - y1\<^sup>2 * r2\<^sup>2 - y2\<^sup>2 * r1\<^sup>2 + y2\<^sup>2 * r2\<^sup>2) + r1^4 - 2 * r1\<^sup>2 * r2\<^sup>2 + r2^4"
        by algebra
      also have "\<dots> = y1^4 - 2 * y1\<^sup>2 * y2\<^sup>2 + y2^4 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 + 2 * y2\<^sup>2 * r1\<^sup>2 - 2 * y2\<^sup>2 * r2\<^sup>2 + r1^4 - 2 * r1\<^sup>2 * r2\<^sup>2 + r2^4"
        by auto
      finally show ?thesis by auto
    qed
    moreover have "(x1 - x2)\<^sup>2 = x1\<^sup>2 - 2 * x1 * x2 + x2\<^sup>2"
      by algebra
    moreover have "2 * (y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(x1 - x2)\<^sup>2 = (2 * y1^4 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 - 4 * y1^3 * y2 + 4 * y1 * y2^3 + 4 * y1 * y2 * r1\<^sup>2 - 4 * y1 * y2 * r2\<^sup>2 - 2 * y2^4 - 2 * y2\<^sup>2 * r1\<^sup>2 + 2 * y2\<^sup>2 * r2\<^sup>2)/(x1 - x2)\<^sup>2"
    proof -
      have "2 * (y2 - y1)\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) = 2 * (y1\<^sup>2 - 2 * y1 * y2 + y2\<^sup>2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)"
        by algebra
      also have "\<dots> = 2 * (y1\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) - 2 * y1 * y2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) + y2\<^sup>2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))"
        by algebra
      also have "\<dots> = 2 * (y1^4 - y1\<^sup>2 * y2\<^sup>2 - y1\<^sup>2 * r1\<^sup>2 + y1\<^sup>2 * r2\<^sup>2  - 2 * y1^3 * y2 + 2 * y1 * y2^3  + 2 * y1 * y2 * r1\<^sup>2 - 2 * y1 * y2 * r2\<^sup>2 + y1\<^sup>2 * y2\<^sup>2 - y2^4 - y2\<^sup>2 * r1\<^sup>2 + y2\<^sup>2 * r2\<^sup>2)"
        by algebra
      also have "\<dots> = 2 * (y1^4 - y1\<^sup>2 * r1\<^sup>2 + y1\<^sup>2 * r2\<^sup>2 - 2 * y1^3 * y2 + 2 * y1 * y2^3 + 2 * y1 * y2 * r1\<^sup>2 - 2 * y1 * y2 * r2\<^sup>2 - y2^4 - y2\<^sup>2 * r1\<^sup>2 + y2\<^sup>2 * r2\<^sup>2)"
        by auto
      also have "\<dots> = 2 * y1^4 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 - 4 * y1^3 * y2 + 4 * y1 * y2^3 + 4 * y1 * y2 * r1\<^sup>2 - 4 * y1 * y2 * r2\<^sup>2 - 2 * y2^4 - 2 * y2\<^sup>2 * r1\<^sup>2 + 2 * y2\<^sup>2 * r2\<^sup>2"
        by auto
      finally show ?thesis by auto
    qed
    moreover have "(y2 - y1)\<^sup>2 = y1\<^sup>2 - 2 * y1 * y2 + y2\<^sup>2"
      by algebra
    moreover have "4 * (y2 - y1)\<^sup>2 * y1\<^sup>2/(x1 - x2)\<^sup>2 = (4 * y1^4 - 8 * y1^3 * y2 + 4 * y1\<^sup>2 * y2\<^sup>2)/(x1 - x2)\<^sup>2"
    proof -
      have "4 * (y2 - y1)\<^sup>2 * y1\<^sup>2 = 4 * (y1\<^sup>2 - 2 * y1 * y2 + y2\<^sup>2) * y1\<^sup>2"
        by algebra
      also have "\<dots> = 4 * y1^4 - 8 * y1^3 * y2 + 4 * y1\<^sup>2 * y2\<^sup>2"
        by algebra
      finally show ?thesis by auto
    qed
    moreover have "4 * (y2 - y1)\<^sup>2 * r1\<^sup>2/(x1 - x2)\<^sup>2 = (4 * y1\<^sup>2 * r1\<^sup>2 - 8 * y1 * y2 * r1\<^sup>2 + 4 * y2\<^sup>2 * r1\<^sup>2)/(x1 - x2)\<^sup>2"
    proof -
      have "4 * (y2 - y1)\<^sup>2 * r1\<^sup>2 = 4 * (y1\<^sup>2 - 2 * y1 * y2 + y2\<^sup>2) * r1\<^sup>2"
        by algebra
      also have "\<dots> = 4 * y1\<^sup>2 * r1\<^sup>2 - 8 * y1 * y2 * r1\<^sup>2 + 4 * y2\<^sup>2 * r1\<^sup>2"
        by algebra
      finally show ?thesis by auto
    qed
    ultimately show ?thesis
      by auto
  qed
  also have "\<dots> =
    (2 * y1^4 - 4 * y1\<^sup>2 * y2\<^sup>2 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 + 2 * y2^4 + 2 * y2\<^sup>2 * r1\<^sup>2 - 2 * y2\<^sup>2 * r2\<^sup>2)/(x1 - x2)\<^sup>2
    + y1\<^sup>2 + 2 * y1 * y2 + y2\<^sup>2
    - (y1^4 - 2 * y1\<^sup>2 * y2\<^sup>2 + y2^4 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 + 2 * y2\<^sup>2 * r1\<^sup>2 - 2 * y2\<^sup>2 * r2\<^sup>2 + r1^4 - 2 * r1\<^sup>2 * r2\<^sup>2 + r2^4)/(x1 - x2)\<^sup>2
    + 2 * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)
    - (x1\<^sup>2 - 2 * x1 * x2 + x2\<^sup>2)
    - 4 * y1\<^sup>2 + 4 * r1\<^sup>2
    + (2 * y1^4 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 - 4 * y1^3 * y2 + 4 * y1 * y2^3 + 4 * y1 * y2 * r1\<^sup>2 - 4 * y1 * y2 * r2\<^sup>2 - 2 * y2^4 - 2 * y2\<^sup>2 * r1\<^sup>2 + 2 * y2\<^sup>2 * r2\<^sup>2)/(x1 - x2)\<^sup>2
    - (y1\<^sup>2 - 2 * y1 * y2 + y2\<^sup>2)
    - (4 * y1^4 - 8 * y1^3 * y2 + 4 * y1\<^sup>2 * y2\<^sup>2)/(x1 - x2)\<^sup>2
    + (4 * y1\<^sup>2 * r1\<^sup>2 - 8 * y1 * y2 * r1\<^sup>2 + 4 * y2\<^sup>2 * r1\<^sup>2)/(x1 - x2)\<^sup>2"
    by auto
  also have "\<dots> = (2 * y1^4 - 4 * y1\<^sup>2 * y2\<^sup>2 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 + 2 * y2^4 + 2 * y2\<^sup>2 * r1\<^sup>2 - 2 * y2\<^sup>2 * r2\<^sup>2 - (y1^4 - 2 * y1\<^sup>2 * y2\<^sup>2 + y2^4 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 + 2 * y2\<^sup>2 * r1\<^sup>2 - 2 * y2\<^sup>2 * r2\<^sup>2 + r1^4 - 2 * r1\<^sup>2 * r2\<^sup>2 + r2^4) + 2 * y1^4 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 - 4 * y1^3 * y2 + 4 * y1 * y2^3 + 4 * y1 * y2 * r1\<^sup>2 - 4 * y1 * y2 * r2\<^sup>2 - 2 * y2^4 - 2 * y2\<^sup>2 * r1\<^sup>2 + 2 * y2\<^sup>2 * r2\<^sup>2 - (4 * y1^4 - 8 * y1^3 * y2 + 4 * y1\<^sup>2 * y2\<^sup>2) + 4 * y1\<^sup>2 * r1\<^sup>2 - 8 * y1 * y2 * r1\<^sup>2 + 4 * y2\<^sup>2 * r1\<^sup>2)/(x1 - x2)\<^sup>2
    + y1\<^sup>2 + 2 * y1 * y2 + y2\<^sup>2 + 2 * y1\<^sup>2 - 2 * y2\<^sup>2 - 2 * r1\<^sup>2 + 2 * r2\<^sup>2 - x1\<^sup>2 + 2 * x1 * x2 - x2\<^sup>2 - 4 * y1\<^sup>2 + 4 * r1\<^sup>2 - y1\<^sup>2 + 2 * y1 * y2 - y2\<^sup>2"
    by algebra
  also have "\<dots> = (2 * y1^4 - 4 * y1\<^sup>2 * y2\<^sup>2 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 + 2 * y2^4 + 2 * y2\<^sup>2 * r1\<^sup>2 - 2 * y2\<^sup>2 * r2\<^sup>2 - y1^4 + 2 * y1\<^sup>2 * y2\<^sup>2 - y2^4 + 2 * y1\<^sup>2 * r1\<^sup>2 - 2 * y1\<^sup>2 * r2\<^sup>2 - 2 * y2\<^sup>2 * r1\<^sup>2 + 2 * y2\<^sup>2 * r2\<^sup>2 - r1^4 + 2 * r1\<^sup>2 * r2\<^sup>2 - r2^4 + 2 * y1^4 - 2 * y1\<^sup>2 * r1\<^sup>2 + 2 * y1\<^sup>2 * r2\<^sup>2 - 4 * y1^3 * y2 + 4 * y1 * y2^3 + 4 * y1 * y2 * r1\<^sup>2 - 4 * y1 * y2 * r2\<^sup>2 - 2 * y2^4 - 2 * y2\<^sup>2 * r1\<^sup>2 + 2 * y2\<^sup>2 * r2\<^sup>2 - 4 * y1^4 + 8 * y1^3 * y2 - 4 * y1\<^sup>2 * y2\<^sup>2 + 4 * y1\<^sup>2 * r1\<^sup>2 - 8 * y1 * y2 * r1\<^sup>2 + 4 * y2\<^sup>2 * r1\<^sup>2)/(x1 - x2)\<^sup>2
    + y1\<^sup>2 + 2 * y1 * y2 + y2\<^sup>2 + 2 * y1\<^sup>2 - 2 * y2\<^sup>2 - 2 * r1\<^sup>2 + 2 * r2\<^sup>2 - x1\<^sup>2 + 2 * x1 * x2 - x2\<^sup>2 - 4 * y1\<^sup>2 + 4 * r1\<^sup>2 - y1\<^sup>2 + 2 * y1 * y2 - y2\<^sup>2"
    by algebra
  also have "\<dots> = (-1 * y1^4 - 6 * y1\<^sup>2 * y2\<^sup>2 + 2 * y1\<^sup>2 * r1\<^sup>2  + 2 * y1\<^sup>2 * r2\<^sup>2 - y2^4 + 2 * y2\<^sup>2 * r1\<^sup>2 + 2 * y2\<^sup>2 * r2\<^sup>2 - r1^4 + 2 * r1\<^sup>2 * r2\<^sup>2 - r2^4 + 4 * y1^3 * y2 + 4 * y1 * y2^3 - 4 * y1 * y2 * r1\<^sup>2 - 4 * y1 * y2 * r2\<^sup>2)/(x1 - x2)\<^sup>2
    - 2 * y1\<^sup>2 + 4 * y1 * y2 - 2 * y2\<^sup>2 + 2 * r1\<^sup>2 + 2 * r2\<^sup>2 - x1\<^sup>2 + 2 * x1 * x2 - x2\<^sup>2"
    by algebra
  also have "\<dots> = (- 1 * y1^4 - 6 * y1\<^sup>2 * y2\<^sup>2 + 4 * y1^3 * y2 + 4 * y1 * y2^3 - y2^4 + 2 * (y1\<^sup>2 + y2\<^sup>2) * (r1\<^sup>2 + r2\<^sup>2) - (r1\<^sup>2 - r2\<^sup>2)\<^sup>2 - 4 * y1 * y2 * (r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 - 2 * (y1 - y2)\<^sup>2 + 2 * (r1\<^sup>2 + r2\<^sup>2) - (x1 - x2)\<^sup>2"
    by algebra
  finally show ?thesis .
qed

abbreviation discriminant where 
  "discriminant \<equiv> 
      (- y1\<^sup>4 - 6 * y1\<^sup>2 * y2\<^sup>2 
          + 4 * y1\<^sup>3 * y2 
          + 4 * y1 * y2\<^sup>3 
          - y2\<^sup>4 
          + 2 * (y1\<^sup>2 + y2\<^sup>2) * (r1\<^sup>2 + r2\<^sup>2) - (r1\<^sup>2 - r2\<^sup>2)\<^sup>2 
          - 4 * y1 * y2 * (r1\<^sup>2 + r2\<^sup>2)) /(x1 - x2)\<^sup>2 
       - 2 * (y1 - y2)\<^sup>2 
       + 2 * (r1\<^sup>2 + r2\<^sup>2) 
       - (x1 - x2)\<^sup>2"

abbreviation a2_expr where 
  "a2_expr \<equiv> 1 + (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2"

abbreviation b2_expr' where 
  "b2_expr' \<equiv> (y1 - y2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2) / (x1 - x2)\<^sup>2 
               + y1 + y2"

abbreviation a1_expr where "a1_expr \<equiv> (y2 - y1)/(x1 - x2)"
abbreviation b1_expr where "b1_expr \<equiv> (x1\<^sup>2 - x2\<^sup>2 + y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2))"


schematic_goal circle_intersection:
  assumes "x1 \<noteq> x2"
  shows "on_circle c1 (x, y) \<and> on_circle c2 (x, y) \<longleftrightarrow> ?X"
proof -
  have  "on_circle c1 (x, y) \<and> on_circle c2 (x, y) \<longleftrightarrow> \<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2 \<ge> 0
                    \<and> (\<exists>\<sigma> \<in> {-1,1}. (x, y) = ( (- \<beta>\<^sub>2 + \<sigma> * sqrt (\<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2))/(2 * \<alpha>\<^sub>2) * \<alpha>\<^sub>1 + \<beta>\<^sub>1
                                        , (- \<beta>\<^sub>2 + \<sigma> * sqrt (\<beta>\<^sub>2\<^sup>2 - 4 * \<alpha>\<^sub>2 * \<gamma>\<^sub>2))/(2 * \<alpha>\<^sub>2)))"
    using ci_solution[OF assms] by auto
  also have  "\<dots> \<longleftrightarrow> (- 1 * y1^4 - 6 * y1\<^sup>2 * y2\<^sup>2 + 4 * y1^3 * y2 + 4 * y1 * y2^3 - y2^4 + 2 * (y1\<^sup>2 + y2\<^sup>2) * (r1\<^sup>2 + r2\<^sup>2) - (r1\<^sup>2 - r2\<^sup>2)\<^sup>2 - 4 * y1 * y2 * (r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 - 2 * (y1 - y2)\<^sup>2 + 2 * (r1\<^sup>2 + r2\<^sup>2) - (x1 - x2)\<^sup>2 \<ge> 0
                    \<and> (\<exists>\<sigma> \<in> {-1,1}. (x, y) = ( (((y1 - y2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 + y1 + y2 + \<sigma> * sqrt ((- 1 * y1^4 - 6 * y1\<^sup>2 * y2\<^sup>2 + 4 * y1^3 * y2 + 4 * y1 * y2^3 - y2^4 + 2 * (y1\<^sup>2 + y2\<^sup>2) * (r1\<^sup>2 + r2\<^sup>2) - (r1\<^sup>2 - r2\<^sup>2)\<^sup>2 - 4 * y1 * y2 * (r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 - 2 * (y1 - y2)\<^sup>2 + 2 * (r1\<^sup>2 + r2\<^sup>2) - (x1 - x2)\<^sup>2))/(2 * (1 + (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2)) * (y2 - y1)/(x1 - x2) + (x1\<^sup>2 - x2\<^sup>2 + y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2)/(2 * (x1 - x2))
                                        , (((y1 - y2) * (y1\<^sup>2 - y2\<^sup>2 - r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 + y1 + y2 + \<sigma> * sqrt ((- 1 * y1^4 - 6 * y1\<^sup>2 * y2\<^sup>2 + 4 * y1^3 * y2 + 4 * y1 * y2^3 - y2^4 + 2 * (y1\<^sup>2 + y2\<^sup>2) * (r1\<^sup>2 + r2\<^sup>2) - (r1\<^sup>2 - r2\<^sup>2)\<^sup>2 - 4 * y1 * y2 * (r1\<^sup>2 + r2\<^sup>2))/(x1 - x2)\<^sup>2 - 2 * (y1 - y2)\<^sup>2 + 2 * (r1\<^sup>2 + r2\<^sup>2) - (x1 - x2)\<^sup>2))/(2 * (1 + (y2 - y1)\<^sup>2 / (x1 - x2)\<^sup>2))))"

    using \<alpha>\<^sub>1_def \<beta>\<^sub>1_def \<alpha>\<^sub>2_unfold minus_\<beta>\<^sub>2_unfold[OF assms] discriminant_unfold[OF assms] by auto
  also have  "\<dots> \<longleftrightarrow> 
        discriminant \<ge> 0
        \<and> (\<exists>\<sigma> \<in> {-1,1}. (x, y) = 
                      ((b2_expr' + \<sigma> * sqrt discriminant) / (2 * a2_expr) * a1_expr + b1_expr, 
                       (b2_expr' + \<sigma> * sqrt discriminant) / (2 * a2_expr)))"
    (is "_ \<longleftrightarrow> (0 \<le> ?Di \<and> ?rhsi)")
    by auto
  finally show "(on_circle c1 (x, y) \<and> on_circle c2 (x, y)) \<longleftrightarrow> (0 \<le> ?Di \<and> ?rhsi)" 
    by auto
qed                 

abbreviation x1_sol where "x1_sol \<equiv>
  b2_expr' + sqrt discriminant / (2 * a2_expr) * a1_expr + b1_expr"

abbreviation x2_sol where "x2_sol \<equiv>
  b2_expr' - sqrt discriminant / (2 * a2_expr) * a1_expr + b1_expr"

abbreviation y1_sol where "y1_sol \<equiv> 
  b2_expr' + sqrt discriminant / (2 * a2_expr)"

abbreviation y2_sol where "y2_sol \<equiv>
  b2_expr' - sqrt discriminant / (2 * a2_expr)"

end


section \<open>Acceleration-Based Occupancy (Abstraction M1)\<close>

text \<open>
  Acceleration-based occupancy considers circles around the possible positions of a vehicle
  at time @{term "t :: real"}. The locale has the following parameters:
  \begin{itemize}
    \item @{term "a_max :: real"} limits the maximum absolute acceleration (C4),
    \item @{term "s :: real2"} is the initial position of the vehicle,
    \item @{term "v :: real2"} is the initial velocity of the vehicle.
  \end{itemize}

  We assume that @{term "fst (v :: real2) \<noteq> 0"} is non-negative (at least @{term "(v :: real) \<noteq> 0"}
  is needed so that the boundary makes sense).

  Furthermore, @{term "(t::real)\<^sup>2 * a_max\<^sup>2 \<le> (fst v)\<^sup>2 + (snd v)\<^sup>2"} is needed so that the occupancy
  circles intersect. TODO: interpretation emergency brake.
\<close>

locale occupancy_acceleration =
  fixes a_max :: real
  fixes s :: real2
  fixes v :: real2
  fixes sx sy vx vy :: real
  assumes v\<^sub>x_wd: "fst v \<noteq> 0"
  assumes sx_def: "sx = fst s" and sy_def: "sy = snd s" 
      and vx_def: "vx = fst v" and vy_def: "vy = snd v"
  assumes a_max_neq_zero: "0 < a_max"
begin

definition valid_time :: "real \<Rightarrow> bool" where 
  "valid_time t = (t\<^sup>2 * a_max\<^sup>2 \<le> (fst v)\<^sup>2 + (snd v)\<^sup>2)"

theorem valid_timeI:
  "0 \<le> t \<Longrightarrow> t \<le> norm v / a_max \<Longrightarrow> valid_time t"
proof -
  assume "0 \<le> t"
  assume "t \<le> norm v / a_max"
  have "t\<^sup>2 \<le> (norm v / a_max)\<^sup>2"
    using power2_mono_leq[OF `t \<le> norm v / a_max` `0 \<le> t`] by auto
  hence "t\<^sup>2 \<le> (norm v)\<^sup>2 / a_max\<^sup>2" 
    using power_divide[where n="2" and a="norm v" and b="a_max"] by auto
  hence "t\<^sup>2 * a_max\<^sup>2 \<le> (norm v)\<^sup>2"
    using a_max_neq_zero by (auto simp add:divide_simps field_simps)
  thus ?thesis unfolding valid_time_def using norm_Pair[of "fst v" "snd v"]
    by auto
qed

theorem mono_valid_time:
  assumes "valid_time t"
  assumes "0 \<le> t'" and "t' \<le> t"
  shows "valid_time t'"
proof -
  from assms(1) have "t\<^sup>2 * a_max\<^sup>2 \<le> (fst v)\<^sup>2 + (snd v)\<^sup>2"
    unfolding valid_time_def by auto
  have "t' = 0 \<or> 0 < t'" using assms(2) by auto
  moreover
  { assume "t' = 0"
    hence "valid_time t'" unfolding valid_time_def by auto }
  moreover
  { assume "0 < t'"  
    have "t'\<^sup>2 \<le> t\<^sup>2"
      unfolding power2_eq_square  using mult_mono[OF assms(3) assms(3)] assms(2-3)
      by auto
    have "t'\<^sup>2 * a_max\<^sup>2 \<le> t\<^sup>2 * a_max\<^sup>2" using mult_right_mono[OF `t'\<^sup>2 \<le> t\<^sup>2`, of "a_max\<^sup>2"]
      by auto
    hence "valid_time t'" using assms(1) unfolding valid_time_def by auto }
  ultimately show ?thesis by auto
qed

lemmas oa_axioms = sx_def sy_def vx_def vy_def

text \<open>
  C4 (maximum absolute acceleration is limited by @{term a_max}) implies that the over-approximated
  occupancy at time t can be described by a circle with the vehicle's position as center
  (according to its initial velocity) and the maximal possible difference in position as radius
  (according to the maximal acceleration).
\<close>

definition occupancy_circle :: "real \<Rightarrow> circle" where
  "occupancy_circle t = \<lparr> Center = (fst s + fst v * t, snd s + snd v * t)
                        , Radius = 1/2 * a_max * t\<^sup>2\<rparr>"

abbreviation "on_occupancy_circle t p \<equiv> on_circle (occupancy_circle t) p"

definition discr where 
  "discr t t'\<equiv>  (vy\<^sup>4 * (- t\<^sup>4 - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t' + 4 * t * t'\<^sup>3 - t'\<^sup>4) 
                      + 1 / 2 * vy\<^sup>2 * (t - t')\<^sup>2 * a_max\<^sup>2 * (t\<^sup>4 + t'\<^sup>4) 
                      - 1 / 16 * a_max\<^sup>4 * (t\<^sup>4 - t'\<^sup>4)\<^sup>2) / (vx * (t - t'))\<^sup>2 
                  - 2 * vy\<^sup>2 * (t - t')\<^sup>2 
                  + 1 / 2 * a_max\<^sup>2 * (t\<^sup>4 + t'\<^sup>4) 
                  - vx\<^sup>2 * (t - t')\<^sup>2" 

definition "b2_expr' t t' \<equiv> 
  2 * sy * vy\<^sup>2/vx\<^sup>2 + ((vy\<^sup>3/vx\<^sup>2) * ((t\<^sup>2 - t'\<^sup>2)/(t - t')) - (1/4 * vy * a_max\<^sup>2/vx\<^sup>2) * ((t\<^sup>4 - t'\<^sup>4)/(t - t')))
+ 2 * sy + vy * (t + t')"

definition "a2_expr \<equiv> (1 + vy\<^sup>2 / vx\<^sup>2)"
definition "a1_expr \<equiv> - vy / vx"
definition "b1_expr t t' \<equiv> 
        (2 * (sx * vx + sy * vy) * (t - t') 
          + ((vx)\<^sup>2 + (vy)\<^sup>2) * (t\<^sup>2 - t'\<^sup>2) 
          - 1/4 * a_max\<^sup>2 * (t\<^sup>4 - t'\<^sup>4)) / (2 * vx * (t - t'))"

interpretation int2: circle_intersection2  
  "occupancy_circle t" "occupancy_circle t'"
  "fst (Center (occupancy_circle t))" "fst (Center (occupancy_circle t'))"
  "snd (Center (occupancy_circle t))" "snd (Center (occupancy_circle t'))"
  "Radius (occupancy_circle t)" "Radius (occupancy_circle t')"
  rewrites "int2.discriminant = discr t t'" and
           "int2.b2_expr'= (if t \<noteq> t' then b2_expr' t t' else snd (Center (occupancy_circle t)) + snd (Center (occupancy_circle t')))" and
           "int2.a2_expr = (if t \<noteq> t' then a2_expr else  1)" and
           "int2.a1_expr = (if t \<noteq> t' then a1_expr else  0)" and
           "int2.b1_expr = (if t \<noteq> t' then b1_expr t t' else 0)"
proof (unfold_locales, simp, simp, simp, simp)
  let ?x1 = "fst (Center (occupancy_circle t))"
  let ?x2 = "fst (Center (occupancy_circle t'))"
  let ?y1 = "snd (Center (occupancy_circle t))"
  let ?y2 = "snd (Center (occupancy_circle t'))"
  let ?r1 = "Radius (occupancy_circle t)"
  let ?r2 = "Radius (occupancy_circle t')" 
  have " (- ?y1\<^sup>4 - 6 * ?y1\<^sup>2 * ?y2\<^sup>2  + 4 * ?y1\<^sup>3 * ?y2  + 4 * ?y1 * ?y2\<^sup>3  - ?y2\<^sup>4  
          + 2 * (?y1\<^sup>2 + ?y2\<^sup>2) * (?r1\<^sup>2 + ?r2\<^sup>2) - (?r1\<^sup>2 - ?r2\<^sup>2)\<^sup>2  - 4 * ?y1 * ?y2 * (?r1\<^sup>2 + ?r2\<^sup>2)) /(?x1 - ?x2)\<^sup>2 
       - 2 * (?y1 - ?y2)\<^sup>2  + 2 * (?r1\<^sup>2 + ?r2\<^sup>2)  - (?x1 - ?x2)\<^sup>2=
         (- ((sy + vy * t)\<^sup>4) - 6 * (sy + vy * t)\<^sup>2 * (sy + vy * t')\<^sup>2
    + 4 * (sy + vy * t)\<^sup>3 * (sy + vy * t') + 4 * (sy + vy * t) * (sy + vy * t')\<^sup>3
    - (sy + vy * t')\<^sup>4 + 2 * ((sy + vy * t)\<^sup>2 + (sy + vy * t')\<^sup>2) * ((1/2 * a_max * t\<^sup>2)\<^sup>2 + (1/2 * a_max * t'\<^sup>2)\<^sup>2) - ((1/2 * a_max * t\<^sup>2)\<^sup>2 - (1/2 * a_max * t'\<^sup>2)\<^sup>2)\<^sup>2
    - 4 * (sy + vy * t) * (sy + vy * t') * ((1/2 * a_max * t\<^sup>2)\<^sup>2 + (1/2 * a_max * t'\<^sup>2)\<^sup>2))/(sx + vx * t - (sx + vx * t'))\<^sup>2
    - 2 * (sy + vy * t - (sy + vy * t'))\<^sup>2 + 2 * ((1/2 * a_max * t\<^sup>2)\<^sup>2 + (1/2 * a_max * t'\<^sup>2)\<^sup>2) - (sx + vx * t - (sx + vx * t'))\<^sup>2"
    unfolding occupancy_circle_def oa_axioms by auto
  also have "\<dots> = discr t t'"          
    unfolding discr_def by algebra
  finally show " (- ?y1\<^sup>4 - 6 * ?y1\<^sup>2 * ?y2\<^sup>2  + 4 * ?y1\<^sup>3 * ?y2  + 4 * ?y1 * ?y2\<^sup>3  - ?y2\<^sup>4  
          + 2 * (?y1\<^sup>2 + ?y2\<^sup>2) * (?r1\<^sup>2 + ?r2\<^sup>2) - (?r1\<^sup>2 - ?r2\<^sup>2)\<^sup>2  - 4 * ?y1 * ?y2 * (?r1\<^sup>2 + ?r2\<^sup>2)) /(?x1 - ?x2)\<^sup>2 
       - 2 * (?y1 - ?y2)\<^sup>2  + 2 * (?r1\<^sup>2 + ?r2\<^sup>2)  - (?x1 - ?x2)\<^sup>2 = discr t t'" 
    by auto
next  
  let ?x1 = "fst (Center (occupancy_circle t))"
  let ?x2 = "fst (Center (occupancy_circle t'))"
  let ?y1 = "snd (Center (occupancy_circle t))"
  let ?y2 = "snd (Center (occupancy_circle t'))"
  let ?r1 = "Radius (occupancy_circle t)"
  let ?r2 = "Radius (occupancy_circle t')" 
  have "t \<noteq> t' \<or> t = t'" by auto
  moreover
  { assume "t \<noteq> t'"
    have "(?y1 - ?y2) * (?y1\<^sup>2 - ?y2\<^sup>2 - ?r1\<^sup>2 + ?r2\<^sup>2) / (?x1 - ?x2)\<^sup>2 
                 + ?y1 + ?y2 =
      ((sy + vy * t - (sy + vy * t')) * ((sy + vy * t)\<^sup>2 - (sy + vy * t')\<^sup>2 - (1/2 * a_max * t\<^sup>2)\<^sup>2 + (1/2 * a_max * t'\<^sup>2)\<^sup>2)) / (sx + vx * t - (sx + vx * t'))\<^sup>2 
      + sy + vy * t + sy + vy * t'"
      (is "?lhs = _")
      unfolding occupancy_circle_def oa_axioms by auto
    also have "... 
      = (2 * sy * vy\<^sup>2 * (t - t')\<^sup>2 + vy\<^sup>3 * (t\<^sup>2 - t'\<^sup>2) * (t - t') - 1/4 * vy * a_max\<^sup>2 * (t\<^sup>4 - t'\<^sup>4) * (t - t'))/(vx\<^sup>2 * (t - t')\<^sup>2) + 2 * sy + vy * (t + t')"
        by algebra
    also have "\<dots> = (2 * sy * vy\<^sup>2 * (t - t')\<^sup>2)/(vx\<^sup>2 * (t - t')\<^sup>2) + (vy\<^sup>3 * (t\<^sup>2 - t'\<^sup>2) * (t - t'))/(vx\<^sup>2 * (t - t') * (t - t')) - (1/4 * vy * a_max\<^sup>2 * (t\<^sup>4 - t'\<^sup>4) * (t - t'))/(vx\<^sup>2 * (t - t') * (t - t'))
                    + 2 * sy + vy * (t + t')"
      by algebra
    also have "\<dots> = (2 * sy * vy\<^sup>2)/vx\<^sup>2 + (vy\<^sup>3 * (t\<^sup>2 - t'\<^sup>2))/(vx\<^sup>2 * (t - t')) - (1/4 * vy * a_max\<^sup>2 * (t\<^sup>4 - t'\<^sup>4))/(vx\<^sup>2 * (t - t')) + 2 * sy + vy * (t + t')"
      using \<open>t \<noteq> t'\<close> by auto
    also have "\<dots> = 2 * sy * vy\<^sup>2/vx\<^sup>2 + ((vy\<^sup>3/vx\<^sup>2) * ((t\<^sup>2 - t'\<^sup>2)/(t - t')) - (1/4 * vy * a_max\<^sup>2/vx\<^sup>2) * ((t\<^sup>4 - t'\<^sup>4)/(t - t'))) + 2 * sy + vy * (t + t')"
      by auto
    finally have "(?y1 - ?y2) * (?y1\<^sup>2 - ?y2\<^sup>2 - ?r1\<^sup>2 + ?r2\<^sup>2) / (?x1 - ?x2)\<^sup>2  + ?y1 + ?y2 = 
      (if t \<noteq> t' then b2_expr' t t' else ?y1 + ?y2)" 
      unfolding b2_expr'_def by auto }
  moreover
  { assume "t = t'"
    hence "(?y1 - ?y2) * (?y1\<^sup>2 - ?y2\<^sup>2 - ?r1\<^sup>2 + ?r2\<^sup>2) / (?x1 - ?x2)\<^sup>2  + ?y1 + ?y2 = 
      (if t \<noteq> t' then b2_expr' t t' else ?y1 + ?y2)" 
      unfolding b2_expr'_def by auto }
  ultimately show "(?y1 - ?y2) * (?y1\<^sup>2 - ?y2\<^sup>2 - ?r1\<^sup>2 + ?r2\<^sup>2) / (?x1 - ?x2)\<^sup>2  + ?y1 + ?y2 = 
      (if t \<noteq> t' then b2_expr' t t' else ?y1 + ?y2)" 
    by auto
next
  let ?x1 = "fst (Center (occupancy_circle t))"
  let ?x2 = "fst (Center (occupancy_circle t'))"
  let ?y1 = "snd (Center (occupancy_circle t))"
  let ?y2 = "snd (Center (occupancy_circle t'))"
  let ?r1 = "Radius (occupancy_circle t)"
  let ?r2 = "Radius (occupancy_circle t')" 
  have "t \<noteq> t' \<or> t = t'" by auto
  moreover
  { assume "t \<noteq> t'"
    have " 2 * (1 + (?y2 - ?y1)\<^sup>2 / (?x1 - ?x2)\<^sup>2) = 
           2 * (1 + (sy + vy * t' - (sy + vy * t))\<^sup>2 
         / (sx + vx * t - (sx + vx * t'))\<^sup>2)"
      unfolding occupancy_circle_def oa_axioms by auto
    also have "\<dots> = 2 * (1 + (vy)\<^sup>2 * (t - t')\<^sup>2 / ((vx)\<^sup>2 * (t - t')\<^sup>2))"
      by algebra
    also have "\<dots> = 2 * (1 + (vy)\<^sup>2 / (vx)\<^sup>2)"
      using \<open>t \<noteq> t'\<close> by auto
    finally have "1 + (?y2 - ?y1)\<^sup>2 / (?x1 - ?x2)\<^sup>2 = a2_expr" 
      unfolding a2_expr_def by auto 
    with \<open>t \<noteq> t'\<close> have " 1 + (?y2 - ?y1)\<^sup>2 / (?x1 - ?x2)\<^sup>2 =
                          (if t \<noteq> t' then a2_expr else 1)"
      by auto }
  moreover
  { assume "t = t'"
    hence "?x1 = ?x2" by auto
    with \<open>t = t'\<close> have "1 + (?y2 - ?y1)\<^sup>2 / (?x1 - ?x2)\<^sup>2 =
                          (if t \<noteq> t' then a2_expr else 1)" by auto }
  ultimately show "1 + (?y2 - ?y1)\<^sup>2 / (?x1 - ?x2)\<^sup>2 =
                          (if t \<noteq> t' then a2_expr else 1)" by auto
next
  let ?x1 = "fst (Center (occupancy_circle t))"
  let ?x2 = "fst (Center (occupancy_circle t'))"
  let ?y1 = "snd (Center (occupancy_circle t))"
  let ?y2 = "snd (Center (occupancy_circle t'))"
  let ?r1 = "Radius (occupancy_circle t)"
  let ?r2 = "Radius (occupancy_circle t')" 
  have "t \<noteq> t' \<or> t = t'" by auto
  moreover
  { assume "t \<noteq> t'"
    have "  (?y2 - ?y1)/(?x1 - ?x2)
          = (sy + vy * t' - (sy + vy * t))
                    / 
            (sx + vx * t - (sx + vx * t'))"
      unfolding occupancy_circle_def oa_axioms by auto
    also have "\<dots> = - ((t - t') * vy/(vx * (t - t')))"
      by algebra
    also have "\<dots> = - vy / vx"
      using \<open>t \<noteq> t'\<close> by auto
    finally have "(?y2 - ?y1)/(?x1 - ?x2) = (if t \<noteq> t' then a1_expr else  0)" 
      unfolding a1_expr_def by auto }
  moreover
  { assume "t = t'"
    hence "?x1 = ?x2" by auto
    hence "(?y2 - ?y1)/(?x1 - ?x2) = (if t \<noteq> t' then a1_expr else  0)" 
      using \<open>t = t'\<close> unfolding a1_expr_def by auto }
  ultimately show "(?y2 - ?y1)/(?x1 - ?x2) = (if t \<noteq> t' then a1_expr else  0)"
    by auto
next
  let ?x1 = "fst (Center (occupancy_circle t))"
  let ?x2 = "fst (Center (occupancy_circle t'))"
  let ?y1 = "snd (Center (occupancy_circle t))"
  let ?y2 = "snd (Center (occupancy_circle t'))"
  let ?r1 = "Radius (occupancy_circle t)"
  let ?r2 = "Radius (occupancy_circle t')" 

  have "t \<noteq> t' \<or> t = t'" by auto
  moreover
  { assume "t \<noteq> t'"
    have "((?x1)\<^sup>2 -  (?x2)\<^sup>2 +  (?y1)\<^sup>2 -  (?y2)\<^sup>2 -  (?r1)\<^sup>2 + (?r2)\<^sup>2)/(2 * (?x1 - ?x2)) = 
      ((sx + vx * t)\<^sup>2 - (sx + vx * t')\<^sup>2 + (sy + vy * t)\<^sup>2 - (sy + vy * t')\<^sup>2 - (1/2 * a_max * t\<^sup>2)\<^sup>2 + (1/2 * a_max * t'\<^sup>2)\<^sup>2)/(2 * (sx + vx * t - (sx + vx * t')))"
      unfolding occupancy_circle_def oa_axioms  by auto
    also have "\<dots> = (2 * (sx * vx + sy * vy) * (t - t') + ((vx)\<^sup>2 + (vy)\<^sup>2) * (t\<^sup>2 - t'\<^sup>2) - 1/4 * a_max\<^sup>2 * (t\<^sup>4 - t'\<^sup>4))/(2 * vx * (t - t'))"
      by algebra
    finally have "(?x1\<^sup>2 - ?x2\<^sup>2 + ?y1\<^sup>2 - ?y2\<^sup>2 - ?r1\<^sup>2 + ?r2\<^sup>2)/(2 * (?x1 - ?x2)) = 
          (if t \<noteq> t' then b1_expr t t' else 0)"
      unfolding b1_expr_def by auto }
  moreover
  { assume "t = t'"
    hence "(?x1\<^sup>2 - ?x2\<^sup>2 + ?y1\<^sup>2 - ?y2\<^sup>2 - ?r1\<^sup>2 + ?r2\<^sup>2)/(2 * (?x1 - ?x2)) = 
          (if t \<noteq> t' then b1_expr t t' else 0)"
      by auto }
  ultimately show "(?x1\<^sup>2 - ?x2\<^sup>2 + ?y1\<^sup>2 - ?y2\<^sup>2 - ?r1\<^sup>2 + ?r2\<^sup>2)/(2 * (?x1 - ?x2)) = 
          (if t \<noteq> t' then b1_expr t t' else 0)"
    by auto
qed

theorem occupancy_circle_intersection:
  assumes "t \<noteq> t'"
  shows "on_occupancy_circle t p \<and> on_occupancy_circle t' p \<longleftrightarrow> 
            discr t t' \<ge> 0
         \<and> (\<exists>\<sigma> \<in> {-1,1}. p = ( (b2_expr' t t' + \<sigma> * sqrt (discr t t')) / (2 * a2_expr) * a1_expr + b1_expr t t'
                             , (b2_expr' t t' + \<sigma> * sqrt (discr t t')) / (2 * a2_expr)))"
proof - 
  let ?x1 = "fst (Center (occupancy_circle t))"
  let ?x2 = "fst (Center (occupancy_circle t'))"
  have "?x1 \<noteq> ?x2" unfolding occupancy_circle_def using assms v\<^sub>x_wd by auto
  thus "?thesis" using int2.circle_intersection[of _ _ "fst p" "snd p"] \<open>?x1 \<noteq> ?x2\<close>      
    by auto
qed      

section "Defining boundary"

abbreviation "occupancy_circle_intersection_wd t t' \<equiv> t \<noteq> t' \<and> discr t t' \<ge> 0"

text \<open>
  @{term "occupancy_circle_intersection_wd t t'"} states that @{term "occupancy_circle t"} and
  @{term "occupancy_circle t'"} have an intersection.
\<close>

definition is_boundary :: "(real \<Rightarrow> real2) \<Rightarrow> bool" where
  "is_boundary b \<longleftrightarrow> 
          (\<exists>p. \<forall>t. (\<forall>t'. occupancy_circle_intersection_wd t t' \<longrightarrow> 
                 on_occupancy_circle t (p t t') \<and> on_occupancy_circle t' (p t t')) \<and> p t \<midarrow>t\<rightarrow> b t)"

text \<open>
  We are interested in the boundary that encloses all possible circles. For that, we consider a
  function @{term "p :: real \<Rightarrow> real \<Rightarrow> real2"} whereas @{term "p t t' :: real2"}
  is an intersection of @{term "occupancy_circle t"} and @{term "occupancy_circle t'"} whenever
  such an intersection exists (i.e. if @{term "occupancy_circle_intersection_wd t t'"}). The limit
  @{term "b :: real \<Rightarrow> real2"} (parameterized by @{term "t :: real"}) of
  @{term "p :: real \<Rightarrow> real \<Rightarrow> real2"} for $t' \to t$ is called a boundary.
\<close>

abbreviation boundary_x :: "real \<Rightarrow> real \<Rightarrow> real" where
  "boundary_x \<sigma> t \<equiv> 
       ( 2 * sy * vy\<^sup>2 / vx\<^sup>2 
         + 2 * t * vy\<^sup>3 / vx\<^sup>2 
         - t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2 
         + 2 * sy + 2 * t * vy 
         + \<sigma> * sqrt (t\<^sup>4 * a_max\<^sup>2 * (1 + vy\<^sup>2 / vx\<^sup>2)- t\<^sup>6 * a_max\<^sup>4 / vx\<^sup>2)) /  (2 * (1 + vy\<^sup>2 / vx\<^sup>2)) * (- vy / vx) 
      + (sx * vx + sy * vy) / vx   
      + t * (vx\<^sup>2 + vy\<^sup>2) / vx 
      - t\<^sup>3 * a_max\<^sup>2 / (2 * vx)"

abbreviation boundary_y :: "real \<Rightarrow> real \<Rightarrow> real" where
  "boundary_y \<sigma> t \<equiv> 
       ( 2 * sy * vy\<^sup>2 / vx\<^sup>2 
         + 2 * t * vy\<^sup>3 / vx\<^sup>2 
         - t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2 
         + 2 * sy + 2 * t * vy 
         + \<sigma> * sqrt (t\<^sup>4 * a_max\<^sup>2 * (1 + vy\<^sup>2 / vx\<^sup>2)- t\<^sup>6 * a_max\<^sup>4 / vx\<^sup>2)) / (2 * (1 + vy\<^sup>2 / vx\<^sup>2))"

definition boundary where
  "boundary \<sigma> t = (boundary_x \<sigma> t, boundary_y \<sigma> t)"

text \<open>
  Since we already know the closed form of the two possible intersections of the occupancy circles,
  we know that @{term "boundary \<sigma>"} for $\sigma \in \{-1, 1\}$ are the only two possible boundaries.
\<close>

text \<open>                                     
  States that the boundary is well-defined (i.e., the discriminant of the root in the boundary is
  non-negative).
\<close>

theorem boundary_wd: 
  assumes "valid_time t"
  shows "t\<^sup>4 * a_max\<^sup>2 * (1 + vy\<^sup>2 / vx\<^sup>2)- t\<^sup>6 * a_max\<^sup>4 / vx\<^sup>2 \<ge> 0"
proof (cases "t = 0 \<or> a_max = 0")
  case True
  then show ?thesis by auto
next
  case False
  have "0 \<le> t\<^sup>4 * a_max\<^sup>2 * (1 + vy\<^sup>2/vx\<^sup>2)- t\<^sup>6 * a_max\<^sup>4/vx\<^sup>2 \<longleftrightarrow>  
                                                  t\<^sup>4 * t\<^sup>2 * a_max\<^sup>4/vx\<^sup>2 \<le> t\<^sup>4 * a_max\<^sup>2 * (1 + vy\<^sup>2/vx\<^sup>2)"
    by auto
  also have "\<dots> \<longleftrightarrow> t\<^sup>2 * a_max\<^sup>4/vx\<^sup>2 \<le> a_max\<^sup>2 * (1 + vy\<^sup>2/vx\<^sup>2)"
    using mult_le_cancel_left[where c = "t\<^sup>4" and a = "t\<^sup>2 * a_max\<^sup>4/vx\<^sup>2" and b = "a_max\<^sup>2 * (1 + vy\<^sup>2/vx\<^sup>2)"]
    False by auto
  also have "\<dots> \<longleftrightarrow> a_max\<^sup>2 * t\<^sup>2 * a_max\<^sup>2/vx\<^sup>2 \<le> a_max\<^sup>2 * (1 + vy\<^sup>2/vx\<^sup>2)"
    using ord_eq_le_trans[where a = "a_max\<^sup>2 * t\<^sup>2 * a_max\<^sup>2/vx\<^sup>2" and b = "t\<^sup>2 * a_max\<^sup>4/vx\<^sup>2"]
          ord_eq_le_trans[where b = "a_max\<^sup>2 * t\<^sup>2 * a_max\<^sup>2/vx\<^sup>2" and a = "t\<^sup>2 * a_max\<^sup>4/vx\<^sup>2"]
    by auto                      
  also have "\<dots> \<longleftrightarrow> t\<^sup>2 * a_max\<^sup>2/vx\<^sup>2 \<le> 1 + vy\<^sup>2/vx\<^sup>2"
    using mult_le_cancel_left[where c = "a_max\<^sup>2" and a = "t\<^sup>2 * a_max\<^sup>2/vx\<^sup>2" and b = "1 + vy\<^sup>2/vx\<^sup>2"]
    by auto
  also have "\<dots> \<longleftrightarrow> (t\<^sup>2 * a_max\<^sup>2 - vy\<^sup>2)/vx\<^sup>2 \<le> 1"
  proof -
    have "t\<^sup>2 * a_max\<^sup>2/vx\<^sup>2 - vy\<^sup>2/vx\<^sup>2 = (t\<^sup>2 * a_max\<^sup>2 - vy\<^sup>2)/vx\<^sup>2" by algebra
    then show ?thesis by auto
  qed
  also have "\<dots> \<longleftrightarrow> t\<^sup>2 * a_max\<^sup>2 \<le> vx\<^sup>2 + vy\<^sup>2"
    using v\<^sub>x_wd oa_axioms by auto 
  finally show ?thesis using assms oa_axioms unfolding valid_time_def by auto
qed                     

text \<open>
  States that the given @{term "boundary \<sigma>"} is indeed a boundary.
\<close>

section "Proving defined boundary is indeed a boundary by finding limits"

subsection \<open>Tearing apart @{term "b2_expr'"} and its limit(s).\<close> 

abbreviation "b21_expr' t t' \<equiv> (vy\<^sup>3/vx\<^sup>2) * ((t\<^sup>2 - t'\<^sup>2)/(t - t'))"

lemma limit_b21_expr: 
  "b21_expr' t \<midarrow>t\<rightarrow>  2 * t * vy\<^sup>3/vx\<^sup>2"
proof -
  have "(\<lambda>t'. (t\<^sup>2 - t'\<^sup>2)/(t - t')) \<midarrow>t\<rightarrow> 2 * t"
    using lhopital_diff_power[where n = 2] by auto
  then have "(\<lambda>t'. ((vy)^3/vx\<^sup>2) * ((t\<^sup>2 - t'\<^sup>2)/(t - t'))) \<midarrow>t\<rightarrow> ((vy)^3/vx\<^sup>2) * (2 * t)"
    using Limits.tendsto_mult_left[where f = "\<lambda>t'. (t\<^sup>2 - t'\<^sup>2)/(t - t')" and l = "2 * t" and c = "(vy)^3/vx\<^sup>2"] by auto
  then show ?thesis
    by (auto intro: LIM_cong_limit)
qed

abbreviation "b22_expr' t t' \<equiv> (1/4 * vy * a_max\<^sup>2/vx\<^sup>2) * ((t\<^sup>4 - t'\<^sup>4)/(t - t'))"

lemma limit_b22_expr: 
  "b22_expr' t \<midarrow>t\<rightarrow>  t\<^sup>3 * vy * a_max\<^sup>2/vx\<^sup>2"
proof -
  have "(\<lambda>t'. (t\<^sup>4 - t'\<^sup>4)/(t - t')) \<midarrow>t\<rightarrow> 4 * t\<^sup>3"
    using lhopital_diff_power[where n = 4] by auto
  then have "(\<lambda>t'. (1/4 * vy * a_max\<^sup>2/vx\<^sup>2) * ((t\<^sup>4 - t'\<^sup>4)/(t - t'))) \<midarrow>t\<rightarrow> (1/4 * vy * a_max\<^sup>2/vx\<^sup>2) * (4 * t\<^sup>3)"
    using Limits.tendsto_mult_left[where f = "\<lambda>t'. (t\<^sup>4 - t'\<^sup>4)/(t - t')" and l = "4 * t\<^sup>3" and c = "(1/4 * vy * a_max\<^sup>2/vx\<^sup>2)"] by auto
  then show ?thesis
    by (auto intro: LIM_cong_limit)
qed

abbreviation "b23_expr' t t' \<equiv> 2 * sy + vy * (t + t')"

lemma limit_b23_expr: "b23_expr' t \<midarrow>t\<rightarrow> 2 * sy + 2 * t * vy"
proof -
  have "(\<lambda>t'. t + t') \<midarrow>t\<rightarrow> 2 * t"
    using Limits.tendsto_add[where f = "\<lambda>t'. t" and g = "\<lambda>t'. t'" and a = t and b = t] by auto
  then have "(\<lambda>t'. vy * (t + t')) \<midarrow>t\<rightarrow> vy * (2 * t)"
    by (auto intro: Limits.tendsto_mult_left)
  then have "(\<lambda>t'. vy * (t + t')) \<midarrow>t\<rightarrow> 2 * t * vy"
    by (auto intro: LIM_cong_limit)
  then show ?thesis by (auto intro: Limits.tendsto_add)
qed

theorem b2_expr_unfold:
  "b2_expr' t t' = 2 * sy * vy\<^sup>2/vx\<^sup>2 + (b21_expr' t t' - b22_expr' t t') + b23_expr' t t'"
  unfolding b2_expr'_def by auto

definition b2_lim where "b2_lim t \<equiv> 
        2 * sy * vy\<^sup>2/vx\<^sup>2 + 2 * t * vy\<^sup>3/vx\<^sup>2 - t\<^sup>3 * vy * a_max\<^sup>2/vx\<^sup>2 +  2 * sy + 2 * t * vy"

theorem limit_b2_expr:
  "b2_expr' t \<midarrow>t\<rightarrow> b2_lim t"
  unfolding b2_lim_def
proof -
  have 0: "(\<lambda>t'. 2 * sy * vy\<^sup>2/vx\<^sup>2 + (b21_expr' t t' - b22_expr' t t'))
    \<midarrow>t\<rightarrow> 2 * sy * vy\<^sup>2/vx\<^sup>2 + (2 * t * vy\<^sup>3/vx\<^sup>2 - t\<^sup>3 * vy * a_max\<^sup>2/vx\<^sup>2)"
    using Limits.tendsto_add[OF _ Limits.tendsto_diff[OF limit_b21_expr[of "t"] limit_b22_expr[of "t"]], 
          of "\<lambda>x. 2 * sy * vy\<^sup>2/vx\<^sup>2" "2 * sy * vy\<^sup>2/vx\<^sup>2"] by auto
  have 1: "b2_expr' t \<midarrow>t\<rightarrow> 2 * sy * vy\<^sup>2/vx\<^sup>2 + (2 * t * vy\<^sup>3/vx\<^sup>2 - t\<^sup>3 * vy * a_max\<^sup>2/vx\<^sup>2) +  (2 * sy + 2 * t * vy)"
    unfolding b2_expr_unfold using Limits.tendsto_add[OF 0 limit_b23_expr] by auto
  thus "b2_expr' t \<midarrow>t\<rightarrow> 2 * sy * vy\<^sup>2 / vx\<^sup>2 + 2 * t * vy\<^sup>3 / vx\<^sup>2 - t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2 + 2 * sy + 2 * t * vy" 
    by (auto simp add:algebra_simps)
qed

subsection \<open>Tearing @{term "b1_expr"} and finding its limits.\<close>

abbreviation "b12_expr t t' \<equiv> (vx\<^sup>2 + vy\<^sup>2) * (t\<^sup>2 - t'\<^sup>2) / (2 * vx * (t - t'))"

lemma limit_b12:  "b12_expr t \<midarrow>t\<rightarrow>  t * (vx\<^sup>2 + vy\<^sup>2) / vx" (is "?f \<midarrow>?x\<rightarrow> ?L")
proof -
  have "(\<lambda>t'. (t\<^sup>2 - t'\<^sup>2)/(t - t')) \<midarrow>t\<rightarrow> 2 * t"
    using lhopital_diff_power[where n = 2] by auto
  then have "(\<lambda>t'. ((vx\<^sup>2 + vy\<^sup>2)/(2 * vx)) * ((t\<^sup>2 - t'\<^sup>2)/(t - t'))) \<midarrow>t\<rightarrow> (vx\<^sup>2 + vy\<^sup>2)/(2 * vx) * (2 * t)"
    using Limits.tendsto_mult[where f = "\<lambda>t. (vx\<^sup>2 + vy\<^sup>2)/(2 * vx)" and
                                    g = "\<lambda>t'. (t\<^sup>2 - t'\<^sup>2)/(t - t')" and
                                    a = "(vx\<^sup>2 + vy\<^sup>2)/(2 * vx)" and
                                    b = "2 * t"]
    by auto
  then have "?f \<midarrow>t\<rightarrow> (vx\<^sup>2 + vy\<^sup>2)/(2 * vx) * (2 * t)"
    by (auto intro: LIM_equal)
  then show ?thesis
    by (auto intro: LIM_cong_limit)
qed

abbreviation "b13_expr t t' \<equiv> 1/4 * a_max\<^sup>2 * (t\<^sup>4 - t'\<^sup>4) / (2 * vx * (t - t'))"

lemma limit_b13:  "b13_expr t  \<midarrow>t\<rightarrow>  t\<^sup>3 * a_max\<^sup>2 / (2 * vx)"
proof -
  have "(\<lambda>t'. (t\<^sup>4 - t'\<^sup>4)/(t - t')) \<midarrow>t\<rightarrow> 4 * t\<^sup>3"
    using lhopital_diff_power[where n = 4] by auto
  then have "(\<lambda>t'. (1/4 * a_max\<^sup>2/(2 * vx)) * ((t\<^sup>4 - t'\<^sup>4)/(t - t'))) \<midarrow>t\<rightarrow> 1/4 * a_max\<^sup>2/(2 * vx) * (4 * t\<^sup>3)"
    using Limits.tendsto_mult[where f = "\<lambda>t'. 1/4 * a_max\<^sup>2/(2 * vx)" and
                                    g = "\<lambda>t'. (t\<^sup>4 - t'\<^sup>4)/(t - t')" and
                                    a = "1/4 * a_max\<^sup>2/(2 * vx)" and
                                    b = "4 * t\<^sup>3"]
    by auto
  then have "(\<lambda>t'. 1/4 * a_max\<^sup>2/(2 * vx) * (t\<^sup>4 - t'\<^sup>4)/(t - t')) \<midarrow>t\<rightarrow> 1/4 * a_max\<^sup>2/(2 * vx) * (4 * t\<^sup>3)"
    by (auto intro: LIM_equal)
  then show ?thesis 
    by (auto intro: LIM_cong_limit simp add:field_simps)
qed

abbreviation "b11_expr t t' \<equiv> 2 * (sx * vx + sy * vy) * (t - t') / (2 * vx * (t - t'))"

lemma helper:
  fixes t :: real
  shows "(\<lambda>t'. (t - t') / (t - t')) \<midarrow>t\<rightarrow> 1"
  unfolding LIM_def by auto

lemma helper':
  fixes t :: real
  shows "(\<lambda>t'. (t - t')\<^sup>2 / (t - t')\<^sup>2) \<midarrow>t\<rightarrow> 1"
  unfolding LIM_def by auto

lemma limit_b11: "b11_expr t \<midarrow>t\<rightarrow> (sx * vx + sy * vy) / vx"
proof -
  have 0: "b11_expr t = (\<lambda>t'. (t - t') / (t - t') * (sx * vx + sy * vy) / vx)"
    (is "_ = ?rhs") by (auto simp add:divide_simps)
  have 1: "(\<lambda>x. (sx * vx + sy * vy) / vx) \<midarrow>t\<rightarrow> (sx * vx + sy * vy) / vx"
    by (auto intro:tendsto_intros)
  have "?rhs \<midarrow>t\<rightarrow> (sx * vx + sy * vy) / vx"
    using tendsto_mult[OF helper 1] by auto
  from LIM_cong_limit[OF this]  show ?thesis unfolding 0 by auto
qed

theorem b1_expr_alt_def:
  "b1_expr t t' = b11_expr t t' + b12_expr t t' - b13_expr t t'"
  unfolding b1_expr_def
  using diff_divide_distrib[of "2 * (sx * vx + sy * vy) * (t - t') + (vx\<^sup>2 + vy\<^sup>2) * (t\<^sup>2 - t'\<^sup>2)"
                               "1 / 4 * a_max\<^sup>2 * (t\<^sup>4 - t'\<^sup>4)" "2 * vx * (t - t')"]
  add_divide_distrib[of "2 * (sx * vx + sy * vy) * (t - t')" "(vx\<^sup>2 + vy\<^sup>2) * (t\<^sup>2 - t'\<^sup>2)" 
                              "2 * vx * (t - t')"]
  by auto

definition "b1_lim t \<equiv> (sx * vx + sy * vy) / vx + t * (vx\<^sup>2 + vy\<^sup>2) / vx - t\<^sup>3 * a_max\<^sup>2 / (2 * vx)"

theorem limit_b1:
  "b1_expr t \<midarrow>t\<rightarrow> b1_lim t"
  unfolding b1_expr_alt_def b1_lim_def
  using tendsto_diff[OF tendsto_add[OF limit_b11 limit_b12] limit_b13]
  by auto
  
subsection \<open>Tearing apart @{term "discr"} and its limit(s).\<close>

lemma helper8: 
  fixes t::real
  shows "(\<lambda>t'. (- (t\<^sup>4) - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t' + 4 * t * t'^3 - t'\<^sup>4)/(t - t')\<^sup>2) \<midarrow>t\<rightarrow> 0"
proof (rule lhopital[where f' = "\<lambda>t'. - 12 * t\<^sup>2 * t' + 4 * t\<^sup>3 + 12 * t * t'\<^sup>2 - 4 * t'^3" and g' = "\<lambda>t'. - 2 * (t - t')"])
  have "(\<lambda>t'. - (t\<^sup>4)) \<midarrow>t\<rightarrow> - (t\<^sup>4)" by auto
  moreover have "(\<lambda>t'. 6 * t\<^sup>2 * t'\<^sup>2)  \<midarrow>t\<rightarrow> 6 * t\<^sup>2 * t\<^sup>2"
    using Limits.tendsto_mult_left[where c = "6 * t\<^sup>2", OF Limits.tendsto_power[where n =2]]
    by auto
  ultimately have "(\<lambda>t'. - (t\<^sup>4) - 6 * t\<^sup>2 * t'\<^sup>2) \<midarrow>t\<rightarrow> - (t\<^sup>4) - 6 * t\<^sup>2 * t\<^sup>2"
    using Limits.tendsto_diff by blast
  moreover have "(\<lambda>t'. 4 * t\<^sup>3 * t') \<midarrow>t\<rightarrow> 4 * t\<^sup>3 * t"
    using Limits.tendsto_mult_left[where c = "4 * t\<^sup>3"] by auto
  ultimately have "(\<lambda>t'. - (t\<^sup>4) - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t') \<midarrow>t\<rightarrow> - (t\<^sup>4) - 6 * t\<^sup>2 * t\<^sup>2 + 4 * t\<^sup>3 * t"
    using Limits.tendsto_add by blast
  moreover have "(\<lambda>t'. 4 * t * t'^3) \<midarrow>t\<rightarrow> 4 * t * t\<^sup>3"
    using Limits.tendsto_mult_left[where c = "4 * t", OF Limits.tendsto_power[where n = 3]]
    by auto
  ultimately have "(\<lambda>t'. - (t\<^sup>4) - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t' + 4 * t * t'^3) \<midarrow>t\<rightarrow> - (t\<^sup>4) - 6 * t\<^sup>2 * t\<^sup>2 + 4 * t\<^sup>3 * t + 4 * t * t\<^sup>3"
    using Limits.tendsto_add by blast
  moreover have "(\<lambda>t'. t'\<^sup>4) \<midarrow>t\<rightarrow> t\<^sup>4"
    using Limits.tendsto_power[where n = 4] by auto
  ultimately have "(\<lambda>t'. - (t\<^sup>4) - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t' + 4 * t * t'^3 - t'\<^sup>4) \<midarrow>t\<rightarrow> - (t\<^sup>4) - 6 * t\<^sup>2 * t\<^sup>2 + 4 * t\<^sup>3 * t + 4 * t * t\<^sup>3 - t\<^sup>4"
    using Limits.tendsto_diff by blast
  then show "(\<lambda>t'. - (t\<^sup>4) - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t' + 4 * t * t'^3 - t'\<^sup>4) \<midarrow>t\<rightarrow> 0"
   by (rule LIM_cong_limit) algebra
next
  have "(\<lambda>t'. t - t') \<midarrow>t\<rightarrow> 0" using lhopital1[where n = 1] by auto
  then show "(\<lambda>t'. (t - t')\<^sup>2) \<midarrow>t\<rightarrow> 0"
    using Limits.tendsto_power[where n = 2 and f = "\<lambda>t'. t - t'"]
    by fastforce
next
  have "\<forall>\<^sub>F t' in at_left t. (t - t')\<^sup>2 \<noteq> 0"
  proof -
    have "(\<exists>b<t. \<forall>y>b. y < t \<longrightarrow> (t - y)\<^sup>2 \<noteq> 0)" by (simp add: lt_ex)
    then show ?thesis using eventually_at_left by auto
  qed
  moreover have "\<forall>\<^sub>F t' in at_right t. (t - t')\<^sup>2 \<noteq> 0"
  proof -
    have "(\<exists>b>t. \<forall>y<b. y < t \<longrightarrow> (t - y)\<^sup>2 \<noteq> 0)" by (simp add: gt_ex)
    then show ?thesis using eventually_at_right by auto
  qed
  ultimately show "\<forall>\<^sub>F t' in at t. (t - t')\<^sup>2 \<noteq> 0" using eventually_at_split by auto
next
  have "\<forall>\<^sub>F t' in at_left t. - 2 * (t - t') \<noteq> 0"
  proof -
    have "(\<exists>b<t. \<forall>y>b. y < t \<longrightarrow> - 2 * (t - y) \<noteq> 0)" by (simp add: lt_ex)
    then show ?thesis using eventually_at_left by auto
  qed
  moreover have "\<forall>\<^sub>F t' in at_right t. - 2 * (t - t') \<noteq> 0"
  proof -
    have "(\<exists>b>t. \<forall>y<b. y < t \<longrightarrow> - 2 * (t - y) \<noteq> 0)" by (simp add: gt_ex)
    then show ?thesis using eventually_at_right by auto
  qed
  ultimately show "\<forall>\<^sub>F t' in at t. - 2 * (t - t') \<noteq> 0" using eventually_at_split by auto
next
  {
    fix t'
    have "((\<lambda>t'. 6 * t\<^sup>2 * t'\<^sup>2) has_real_derivative 12 * t\<^sup>2 * t') (at t')"
    proof (rule DERIV_cong)
      have "((\<lambda>t'. t'\<^sup>2) has_real_derivative 2 * t') (at t')"
        using DERIV_power[where f = "\<lambda>t'. t'" and D = 1 and n = 2] by auto
      then show "((\<lambda>t'. 6 * t\<^sup>2 * t'\<^sup>2)  has_real_derivative 6 * t\<^sup>2 * (2 * t')) (at t')"
        using DERIV_cmult[where c = "6 * t\<^sup>2" and f = "\<lambda>t'. t'\<^sup>2" and D = "2 * t'"]
        by auto
    qed auto
    then have "((\<lambda>t'. - (t\<^sup>4) - 6 * t\<^sup>2 * t'\<^sup>2) has_real_derivative - 12 * t\<^sup>2 * t') (at t')"
      using DERIV_diff[where f = "\<lambda>t'. - (t\<^sup>4)" and g = "\<lambda>t'. 6 * t\<^sup>2 * t'\<^sup>2" and D = 0]
      by auto
    moreover have "((\<lambda>t'. 4 * t\<^sup>3 * t') has_real_derivative 4 * t\<^sup>3) (at t')"
      by auto
    ultimately have "((\<lambda>t'. - (t\<^sup>4) - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t') has_real_derivative - 12 * t\<^sup>2 * t' + 4 * t\<^sup>3) (at t')"
      using DERIV_add by blast
    moreover have "((\<lambda>t'. 4 * t * t'^3) has_real_derivative 12 * t * t'\<^sup>2) (at t')"
    proof (rule DERIV_cong)
      have "((\<lambda>t'. t'^3) has_real_derivative 3 * t'\<^sup>2) (at t')"
        using DERIV_power[where f = "\<lambda>t'. t'" and D = 1 and n = 3] by auto
      then show "((\<lambda>t'. 4 * t * t'^3) has_real_derivative 4 * t * (3 * t'\<^sup>2)) (at t')"
        using DERIV_cmult[where c = "4 * t" and f = "\<lambda>t'. t'^3" and D = "3 * t'\<^sup>2"]
        by auto
    qed auto
    ultimately have "((\<lambda>t'. - (t\<^sup>4) - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t' + 4 * t * t'^3) has_real_derivative - 12 * t\<^sup>2 * t' + 4 * t\<^sup>3 + 12 * t * t'\<^sup>2) (at t')"
      using DERIV_add by blast
    moreover have "((\<lambda>t'. t'\<^sup>4) has_real_derivative 4 * t'^3) (at t')"
      using DERIV_power[where f = "\<lambda>t'. t'" and D = 1 and n = 4] by auto
    ultimately have "((\<lambda>t'. - (t\<^sup>4) - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t' + 4 * t * t'^3 - t'\<^sup>4) has_real_derivative - 12 * t\<^sup>2 * t' + 4 * t\<^sup>3 + 12 * t * t'\<^sup>2 - 4 * t'^3) (at t')"
      using DERIV_diff by auto
  }
  then show "\<forall>\<^sub>F t' in at t. ((\<lambda>t'. - (t\<^sup>4) - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t' + 4 * t * t'^3 - t'\<^sup>4) has_real_derivative - 12 * t\<^sup>2 * t' + 4 * t\<^sup>3 + 12 * t * t'\<^sup>2 - 4 * t'^3) (at t')"
    by auto
next
  {
    fix t'
    have "((\<lambda>t'. (t - t')\<^sup>2) has_real_derivative - 2 * (t - t')) (at t')"
    proof -
      have "((\<lambda>t'. t - t') has_real_derivative - 1) (at t')"
        using DERIV_diff[where f = "\<lambda>t'. t" and g = "\<lambda>t'. t'" and D = 0] by auto
      then show "((\<lambda>t'. (t - t')\<^sup>2) has_real_derivative - 2 * (t - t')) (at t')"
        using DERIV_power[where f = "\<lambda>t'. t - t'" and n = 2] by fastforce
    qed
  }
  then show "\<forall>\<^sub>F t' in at t. ((\<lambda>t'. (t - t')\<^sup>2) has_real_derivative- 2 * (t - t')) (at t')"
    by auto
next
  show "(\<lambda>t'. (- 12 * t\<^sup>2 * t' + 4 * t ^ 3 + 12 * t * t'\<^sup>2 - 4 * t' ^ 3) / (- 2 * (t - t'))) \<midarrow>t\<rightarrow> 0"
  proof (rule lhopital[where f' = "\<lambda>t'. - 12 * t\<^sup>2 + 24 * t * t' - 12 * t'\<^sup>2" and g' = "\<lambda>t'. 2"])
    have "(\<lambda>t'. - 12 * t\<^sup>2 * t') \<midarrow>t\<rightarrow> - 12 * t\<^sup>3"
    proof -
      have "(\<lambda>t'. - 12 * t\<^sup>2 * t') \<midarrow>t\<rightarrow> - 12 * t\<^sup>2 * t"
        using Limits.tendsto_mult_left[where c = "- 12 * t\<^sup>2"]
        by auto
      moreover have "- 12 * t\<^sup>2 * t = - 12 * t\<^sup>3" by algebra
      ultimately show ?thesis by (auto intro: LIM_cong_limit)
    qed
    then have "(\<lambda>t'. - 12 * t\<^sup>2 * t' + 4 * t\<^sup>3) \<midarrow>t\<rightarrow> - 12 * t\<^sup>3 + 4 * t\<^sup>3"
      using Limits.tendsto_add by blast
    moreover have "(\<lambda>t'. 12 * t * t'\<^sup>2) \<midarrow>t\<rightarrow> 12 * t\<^sup>3"
    proof -
      have "(\<lambda>t'. t'\<^sup>2) \<midarrow>t\<rightarrow> t\<^sup>2"
        using Limits.tendsto_power[where n = 2] by auto
      then have "(\<lambda>t'. 12 * t * t'\<^sup>2) \<midarrow>t\<rightarrow> 12 * t * t\<^sup>2"
        using Limits.tendsto_mult_left[where c = "12 * t"]
        by auto
      moreover have "12 * t * t\<^sup>2 = 12 * t\<^sup>3" by algebra
      ultimately show ?thesis by (auto intro: LIM_cong_limit)
    qed
    ultimately have "(\<lambda>t'. - 12 * t\<^sup>2 * t' + 4 * t\<^sup>3 + 12 * t * t'\<^sup>2) \<midarrow>t\<rightarrow> - 12 * t\<^sup>3 + 4 * t\<^sup>3 + 12 * t\<^sup>3"
      using Limits.tendsto_add by blast
    moreover have "(\<lambda>t'. 4 * t'^3) \<midarrow>t\<rightarrow> 4 * t\<^sup>3"
      using Limits.tendsto_power[where n = 3] Limits.tendsto_mult_left by blast
    ultimately show "(\<lambda>t'. (- 12 * t\<^sup>2 * t' + 4 * t\<^sup>3 + 12 * t * t'\<^sup>2 - 4 * t'^3)) \<midarrow>t\<rightarrow> 0"
      using Limits.tendsto_diff[where f = "(\<lambda>t'. - 12 * t\<^sup>2 * t' + 4 * t\<^sup>3 + 12 * t * t'\<^sup>2)" and
                                      g = "\<lambda>t'. 4 * t'^3" and
                                      a = "- 12 * t\<^sup>3 + 4 * t\<^sup>3 + 12 * t\<^sup>3" and
                                      b = "4 * t\<^sup>3"]
      by auto
  next
    have "(\<lambda>t'. t - t') \<midarrow>t\<rightarrow> 0"
      using Limits.tendsto_diff[where f = "\<lambda>t'. t" and a = t and g = "\<lambda>t'. t'" and b = t]
      by auto
    then show "(\<lambda>t'. - 2 * (t - t')) \<midarrow>t\<rightarrow> 0"
      using Limits.tendsto_mult_right_zero by blast
  next
    have "\<forall>\<^sub>F t' in at_left t. - 2 * (t - t') \<noteq> 0"
    proof -
      have "(\<exists>b<t. \<forall>y>b. y < t \<longrightarrow> - 2 * (t - y) \<noteq> 0)" by (simp add: lt_ex)
      then show ?thesis using eventually_at_left by auto
    qed
    moreover have "\<forall>\<^sub>F t' in at_right t. - 2 * (t - t') \<noteq> 0"
    proof -
      have "(\<exists>b>t. \<forall>y<b. y < t \<longrightarrow> - 2 * (t - y) \<noteq> 0)" by (simp add: gt_ex)
      then show ?thesis using eventually_at_right by auto
    qed
    ultimately show "\<forall>\<^sub>F t' in at t. - 2 * (t - t') \<noteq> 0" using eventually_at_split by auto
  next
    show "\<forall>\<^sub>F t' in at t. (2::real) \<noteq> 0" by auto
  next
    {
      fix t'
      have "((\<lambda>t'. - 12 * t\<^sup>2 * t') has_real_derivative - 12 * t\<^sup>2) (at t')"
        using DERIV_cmult_Id by blast
      then have "((\<lambda>t'. - 12 * t\<^sup>2 * t' + 4 * t\<^sup>3) has_real_derivative - 12 * t\<^sup>2) (at t')"
        using DERIV_add[where f = "\<lambda>t'. - 12 * t\<^sup>2 * t'" and g = "\<lambda>t'. 4 * t ^ 3" and E = 0]
        by auto
      moreover have "((\<lambda>t'. 12 * t * t'\<^sup>2) has_real_derivative 24 * t * t') (at t')"
      proof (rule DERIV_cong)
        have "((\<lambda>t'. t'\<^sup>2) has_real_derivative 2 * t') (at t')"
          using DERIV_power[where f = "\<lambda>t'. t'" and D = 1 and n = 2] by auto
        then show "((\<lambda>t'. 12 * t * t'\<^sup>2) has_real_derivative 12 * t * (2 * t')) (at t')"
          using DERIV_cmult[where c = "12 * t" and f = "\<lambda>t'. t'\<^sup>2" and D = "2 * t'"]
          by auto
      qed auto
      ultimately have "((\<lambda>t'. - 12 * t\<^sup>2 * t' + 4 * t\<^sup>3 + 12 * t * t'\<^sup>2) has_real_derivative - 12 * t\<^sup>2  + 24 * t * t') (at t')"
        using DERIV_add by blast
      moreover have "((\<lambda>t'. 4 * t'^3) has_real_derivative 12 * t'\<^sup>2) (at t')"
      proof (rule DERIV_cong)
        have "((\<lambda>t'. t'^3) has_real_derivative 3 * t'\<^sup>2) (at t')"
          using DERIV_power[where f = "\<lambda>t'. t'" and D = 1 and n = 3] by auto
        then show "((\<lambda>t'. 4 * t'^3) has_real_derivative 4 * (3 * t'\<^sup>2)) (at t')"
          using DERIV_cmult[where c = 4 and f = "\<lambda>t'. t'^3" and D = "3 * t'\<^sup>2"]
          by auto
      qed auto
      ultimately have "((\<lambda>t'. - 12 * t\<^sup>2 * t' + 4 * t ^ 3 + 12 * t * t'\<^sup>2 - 4 * t' ^ 3) has_real_derivative - 12 * t\<^sup>2 + 24 * t * t' - 12 * t'\<^sup>2) (at t')"
        using DERIV_diff by blast
    }
    then show "\<forall>\<^sub>F t' in at t. ((\<lambda>t'. - 12 * t\<^sup>2 * t' + 4 * t ^ 3 + 12 * t * t'\<^sup>2 - 4 * t' ^ 3) has_real_derivative - 12 * t\<^sup>2 + 24 * t * t' - 12 * t'\<^sup>2) (at t')"
      by auto
  next
    {
      fix t'                  
      have "((\<lambda>t'. t - t') has_real_derivative - 1) (at t')"
        using DERIV_diff[where f = "\<lambda>t'. t" and D = 0] by auto
      then have "((\<lambda>t'. - 2 * (t - t')) has_real_derivative 2) (at t')"
        using DERIV_cmult[where f = "\<lambda>t'. t - t'" and c = "- 2" and D = "- 1"] by auto
    }
    then show "\<forall>\<^sub>F t' in at t. ((\<lambda>t'. - 2 * (t - t')) has_real_derivative 2) (at t')"
      by auto
  next
    have "(\<lambda>t'. - 12 * t\<^sup>2) \<midarrow>t\<rightarrow> - 12 * t\<^sup>2"
      by auto   
    moreover have "(\<lambda>t'. 24 * t * t') \<midarrow>t\<rightarrow> 24 * t * t"
      using Limits.tendsto_mult_left[where c = "24 * t"] by auto
    ultimately have "(\<lambda>t'. - 12 * t\<^sup>2 + 24 * t * t') \<midarrow>t\<rightarrow> - 12 * t\<^sup>2 + 24 * t * t"
      using Limits.tendsto_add by blast
    moreover have "(\<lambda>t'. 12 * t'\<^sup>2) \<midarrow>t\<rightarrow> 12 * t\<^sup>2"
      using Limits.tendsto_mult_left[OF Limits.tendsto_power[where f = "\<lambda>t'. t'" and n = 2]]
      by auto
    ultimately have "(\<lambda>t'. - 12 * t\<^sup>2 + 24 * t * t' - 12 * t'\<^sup>2) \<midarrow>t\<rightarrow> - 12 * t\<^sup>2 + 24 * t * t - 12 * t\<^sup>2"
      using Limits.tendsto_diff by blast
    then have "(\<lambda>t'. - 12 * t\<^sup>2 + 24 * t * t' - 12 * t'\<^sup>2) \<midarrow>t\<rightarrow> 0"
      by (rule LIM_cong_limit) algebra
    then show "(\<lambda>t'. (- 12 * t\<^sup>2 + 24 * t * t' - 12 * t'\<^sup>2) / 2) \<midarrow>t\<rightarrow> 0"
      using Limits.tendsto_divide[where f = "\<lambda>t'. - 12 * t\<^sup>2 + 24 * t * t' - 12 * t'\<^sup>2" and a = 0 and g = "\<lambda>t'. 2" and b = 2]
      by auto
  qed
qed

abbreviation "discr1 t t' \<equiv> (vy\<^sup>4 / vx\<^sup>2) * ((- (t\<^sup>4) - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t' + 4 * t * t'^3 - t'\<^sup>4)/(t - t')\<^sup>2)"

lemma limit_discr1: "discr1 t \<midarrow>t\<rightarrow> 0"
  using  Limits.tendsto_mult_right_zero[OF helper8, of "vy\<^sup>4 / vx\<^sup>2" "t"] by auto

abbreviation "discr2 t t' \<equiv> 1/2 * vy\<^sup>2/vx\<^sup>2 * a_max\<^sup>2 * (t\<^sup>4 + t'\<^sup>4)" 

lemma limit_discr2:  "discr2 t \<midarrow>t\<rightarrow> t\<^sup>4 * vy\<^sup>2/vx\<^sup>2 * a_max\<^sup>2"
proof -
  have "(\<lambda>t'. t\<^sup>4 + t'\<^sup>4) \<midarrow>t\<rightarrow> 2 * t\<^sup>4"
  proof -
    have "(\<lambda>t'. t'\<^sup>4) \<midarrow>t\<rightarrow> t\<^sup>4" by (auto intro: Limits.tendsto_power)
    then show ?thesis
      using Limits.tendsto_add[where f = "\<lambda>t'. t\<^sup>4" and g = "\<lambda>t'. t'\<^sup>4" and a = "t\<^sup>4" and b = "t\<^sup>4"] by auto
  qed
  then have "(\<lambda>t'. 1/2 * vy\<^sup>2/vx\<^sup>2 * a_max\<^sup>2 * (t\<^sup>4 + t'\<^sup>4)) \<midarrow>t\<rightarrow> 1/2 * vy\<^sup>2/vx\<^sup>2 * a_max\<^sup>2 * (2 * t\<^sup>4)"
    using Limits.tendsto_mult_left[where f = "\<lambda>t'. t\<^sup>4 + t'\<^sup>4" and l = "2 * t\<^sup>4" and c = "1/2 * vy\<^sup>2/vx\<^sup>2 * a_max\<^sup>2"] by auto
  then show ?thesis
    by (auto intro: LIM_cong_limit)
qed

abbreviation "discr2' t t' \<equiv> (t - t')\<^sup>2 / (t - t')\<^sup>2 * discr2 t t'"

lemma limit_discr2': "discr2' t \<midarrow>t\<rightarrow> t\<^sup>4 * vy\<^sup>2/vx\<^sup>2 * a_max\<^sup>2"
    using tendsto_mult[OF helper' limit_discr2] by auto

abbreviation "discr3 t t' \<equiv> 1/16 * a_max^4/vx\<^sup>2 * (t\<^sup>4 - t'\<^sup>4)\<^sup>2/(t - t')\<^sup>2"

lemma limit_discr3:  "discr3 t \<midarrow>t\<rightarrow> t^6 * a_max^4/vx\<^sup>2"
proof -
  have "(\<lambda>t'. (t\<^sup>4 - t'\<^sup>4)\<^sup>2/(t - t')\<^sup>2) \<midarrow>t\<rightarrow> 16 * t^6"
  proof -
    have "(\<lambda>t'. ((t\<^sup>4 - t'\<^sup>4)/(t - t'))\<^sup>2) \<midarrow>t\<rightarrow> 16 * t^6"
    proof -
      have "(\<lambda>t'. (t\<^sup>4 - t'\<^sup>4)/(t - t')) \<midarrow>t\<rightarrow> 4 * t\<^sup>3"
        using lhopital_diff_power[where n = 4] by auto
      then show ?thesis
        using Limits.tendsto_power[where f = "\<lambda>t'. (t\<^sup>4 - t'\<^sup>4)/(t - t')" and n = 2]
        by fastforce
    qed
    then show ?thesis
      using LIM_equal[where f = "\<lambda>t'. ((t\<^sup>4 - t'\<^sup>4)/(t - t'))\<^sup>2" and
                            g = "\<lambda>t'. (t\<^sup>4 - t'\<^sup>4)\<^sup>2/(t - t')\<^sup>2"]
      using power_divide by blast
  qed
  then have "(\<lambda>t'. (1/16 * a_max^4/vx\<^sup>2) * ((t\<^sup>4 - t'\<^sup>4)\<^sup>2/(t - t')\<^sup>2)) \<midarrow>t\<rightarrow> (1/16 * a_max^4/vx\<^sup>2) * (16 * t^6)"
    using Limits.tendsto_mult_left[where f = "\<lambda>t'. (t\<^sup>4 - t'\<^sup>4)\<^sup>2/(t - t')\<^sup>2" and l = "16 * t^6" and c = "1/16 * a_max^4/vx\<^sup>2"]
    by auto
  then show ?thesis
    by (auto intro: LIM_cong_limit)
qed

abbreviation "discr4 t t' \<equiv>  2 * vy\<^sup>2 * (t - t')\<^sup>2"

lemma limit_discr4: "discr4 t \<midarrow>t\<rightarrow> 0"
proof -
  have "(\<lambda>t'. (t - t')\<^sup>2) \<midarrow>t\<rightarrow> 0"
  proof -
    have "(\<lambda>t'. t - t') \<midarrow>t\<rightarrow> 0"
      using Limits.tendsto_diff[where f = "\<lambda>t'. t" and g = "\<lambda>t'. t'" and a = t and b = t] by auto
    then show ?thesis
      using Limits.tendsto_power[where f = "(\<lambda>t'. (t - t'))" and n = 2 and a = 0] by auto
  qed
  then show ?thesis using Limits.tendsto_mult_right_zero by auto
qed

abbreviation "discr5 t t' \<equiv> 1/2 * a_max\<^sup>2 * (t\<^sup>4 + t'\<^sup>4)"

lemma limit_discr5: "discr5 t \<midarrow>t\<rightarrow> t\<^sup>4 * a_max\<^sup>2"
proof -
  have "(\<lambda>t'. t\<^sup>4 + t'\<^sup>4) \<midarrow>t\<rightarrow> 2 * t\<^sup>4"
  proof -
    have "(\<lambda>t'. t'\<^sup>4) \<midarrow>t\<rightarrow> t\<^sup>4" by (auto intro: Limits.tendsto_power)
    then show ?thesis
      using Limits.tendsto_add[where f = "\<lambda>t'. t\<^sup>4" and g = "\<lambda>t'. t'\<^sup>4" and a = "t\<^sup>4" and b = "t\<^sup>4"] by auto
  qed
  then have "(\<lambda>t'. 1/2 * a_max\<^sup>2 * (t\<^sup>4 + t'\<^sup>4)) \<midarrow>t\<rightarrow> 1/2 * a_max\<^sup>2 * 2 * t\<^sup>4"
    using Limits.tendsto_mult_left[where f = "\<lambda>t'. t\<^sup>4 + t'\<^sup>4" and c = "1/2 * a_max\<^sup>2" and l = "2 * t\<^sup>4"]
    by auto
  then show ?thesis by (auto intro: LIM_cong_limit)
qed

abbreviation "discr6 t t' \<equiv> vx\<^sup>2 * (t - t')\<^sup>2"

lemma limit_discr6: "discr6 t \<midarrow>t\<rightarrow> 0"
proof -
  have "(\<lambda>t'. (t - t')\<^sup>2) \<midarrow>t\<rightarrow> 0"
  proof -
    have "(\<lambda>t'. t - t') \<midarrow>t\<rightarrow> 0"
      using Limits.tendsto_diff[where f = "\<lambda>t'. t" and g = "\<lambda>t'. t'" and a = t and b = t] by auto
    then show ?thesis
      using Limits.tendsto_power[where f = "(\<lambda>t'. (t - t'))" and n = 2 and a = 0] by auto
  qed
  then show ?thesis using Limits.tendsto_mult_right_zero by auto
qed

theorem discr_alt_def:
  "discr t t' = discr1 t t' 
              + discr2' t t' 
              - discr3 t t' - discr4 t t' + discr5 t t' - discr6 t t'"
proof -
  have 0: " (vy\<^sup>4 * (- t\<^sup>4 - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t' + 4 * t * t'\<^sup>3 - t'\<^sup>4) 
                      + 1 / 2 * vy\<^sup>2 * (t - t')\<^sup>2 * a_max\<^sup>2 * (t\<^sup>4 + t'\<^sup>4) 
                      - 1 / 16 * a_max\<^sup>4 * (t\<^sup>4 - t'\<^sup>4)\<^sup>2) / (vx * (t - t'))\<^sup>2  
        = vy\<^sup>4 * (- t\<^sup>4 - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t' + 4 * t * t'\<^sup>3 - t'\<^sup>4) / (vx * (t - t'))\<^sup>2 
                   + (1 / 2 * vy\<^sup>2 * (t - t')\<^sup>2 * a_max\<^sup>2 * (t\<^sup>4 + t'\<^sup>4)) / (vx * (t - t'))\<^sup>2 
                   - (1 / 16 * a_max\<^sup>4 * (t\<^sup>4 - t'\<^sup>4)\<^sup>2) / (vx * (t - t'))\<^sup>2" 
        (is "_ = ?rhs1 + ?rhs2 - ?rhs3")
    using add_divide_distrib[of "vy\<^sup>4 * (- t\<^sup>4 - 6 * t\<^sup>2 * t'\<^sup>2 + 4 * t\<^sup>3 * t' + 4 * t * t'\<^sup>3 - t'\<^sup>4)" 
                                "1 / 2 * vy\<^sup>2 * (t - t')\<^sup>2 * a_max\<^sup>2 * (t\<^sup>4 + t'\<^sup>4)" "(vx * (t - t'))\<^sup>2"]
          diff_divide_distrib[of _ "1 / 16 * a_max\<^sup>4 * (t\<^sup>4 - t'\<^sup>4)\<^sup>2" "(vx * (t - t'))\<^sup>2"]
    by auto
  have "?rhs1 = discr1 t t'" unfolding power_mult_distrib by auto
  moreover have "?rhs2 = (t - t')\<^sup>2 / (t - t')\<^sup>2 * discr2 t t'"
    unfolding power_mult_distrib by auto
  moreover have "?rhs3 = discr3 t t'"
    unfolding power_mult_distrib by auto
  ultimately show ?thesis
    unfolding discr_def 0 by auto      
qed

theorem limit_discr:
  "discr t \<midarrow>t\<rightarrow> t\<^sup>4 * vy\<^sup>2/vx\<^sup>2 * a_max\<^sup>2 - t^6 * a_max^4/vx\<^sup>2 + t\<^sup>4 * a_max\<^sup>2"
  unfolding discr_alt_def
  using tendsto_diff[OF 
          tendsto_add[OF 
            tendsto_diff[OF 
              tendsto_diff[OF 
                tendsto_add[OF limit_discr1 limit_discr2', of "t"] 
                limit_discr3] 
              limit_discr4] 
            limit_discr5] 
          limit_discr6]
  by auto

subsection "Limit of the solution to quadratic equation"

definition "sqrt_discr_lim t = sqrt (t\<^sup>4 * vy\<^sup>2/vx\<^sup>2 * a_max\<^sup>2 - t^6 * a_max^4/vx\<^sup>2 + t\<^sup>4 * a_max\<^sup>2)"

lemma limit_sqrt_discr:
  "(\<lambda>t'. \<sigma> * sqrt (discr t t')) \<midarrow>t\<rightarrow> \<sigma> * sqrt_discr_lim t"
proof -
  have 0: "sqrt \<midarrow>t\<^sup>4 * vy\<^sup>2 / vx\<^sup>2 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / vx\<^sup>2 + t\<^sup>4 * a_max\<^sup>2\<rightarrow> 
        sqrt (t\<^sup>4 * vy\<^sup>2 / vx\<^sup>2 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / vx\<^sup>2 + t\<^sup>4 * a_max\<^sup>2)"
    by (auto intro:tendsto_intros)
  have 1: "op * \<sigma> \<midarrow>sqrt (t\<^sup>4 * vy\<^sup>2 / vx\<^sup>2 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / vx\<^sup>2 + t\<^sup>4 * a_max\<^sup>2)\<rightarrow> 
              \<sigma> * sqrt (t\<^sup>4 * vy\<^sup>2 / vx\<^sup>2 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / vx\<^sup>2 + t\<^sup>4 * a_max\<^sup>2)"
    by (auto intro:tendsto_intros)
  thus ?thesis
    using  tendsto_compose[OF 1 tendsto_compose[OF 0 limit_discr]]
    unfolding sqrt_discr_lim_def by auto 
qed

theorem y_sol:
  "(\<lambda>t'. (b2_expr' t t' + \<sigma> * sqrt (discr t t')) / (2 * a2_expr))
    \<midarrow>t\<rightarrow>
   (b2_lim t + \<sigma> * sqrt_discr_lim t) / (2 * a2_expr)"
proof -
  have 0: "(\<lambda>x. 2 * a2_expr) \<midarrow>t\<rightarrow> 2 * a2_expr" by (auto intro:tendsto_intros)
  have 1: " 2 * a2_expr \<noteq> 0 "
    unfolding a2_expr_def
  proof (rule ccontr)
    assume "\<not> 2 * (1 + vy\<^sup>2 / vx\<^sup>2) \<noteq> 0 "    
    hence "2 * (1 + vy\<^sup>2 / vx\<^sup>2) = 0" (is "2 * ?lhs = 0") by auto
    hence "?lhs = 0" by auto
    hence "vy\<^sup>2 / vx\<^sup>2 = -1" by auto
    moreover have "vy\<^sup>2 / vx\<^sup>2 = (vy / vx)\<^sup>2" unfolding power2_eq_square by auto
    ultimately show False using zero_le_power2[of "vy / vx"] by (auto)
  qed  
  show ?thesis
  using tendsto_divide[where g="\<lambda>x. 2 * a2_expr" and b="2 * a2_expr" and F="at t", 
                       OF tendsto_add[OF limit_b2_expr limit_sqrt_discr] 0 1] 
  by auto
qed

theorem x_sol:
  "(\<lambda>t'. (b2_expr' t t' + \<sigma> * sqrt (discr t t')) / (2 * a2_expr) * a1_expr + b1_expr t t')
    \<midarrow>t\<rightarrow>
   (b2_lim t + \<sigma> * sqrt_discr_lim t) / (2 * a2_expr) * a1_expr + b1_lim t"
  using tendsto_add[OF tendsto_mult[OF y_sol tendsto_const[of "a1_expr" "at t"]]]
  by (auto simp add:limit_b1)

subsection "Closed form of boundary"

lemma boundary_y_alt_def:
  "boundary_y \<sigma> t = (b2_lim t + \<sigma> * sqrt_discr_lim t) / (2 * a2_expr)"
  unfolding b2_lim_def sqrt_discr_lim_def a2_expr_def
  by (auto simp add:field_simps)

lemma boundary_x_alt_def:
  "boundary_x \<sigma> t = (b2_lim t + \<sigma> * sqrt_discr_lim t) / (2 * a2_expr) * a1_expr + b1_lim t"
  unfolding boundary_y_alt_def a1_expr_def b1_lim_def by auto                                     

theorem boundary_closed_form:
  fixes \<sigma> :: real
  assumes "\<sigma> \<in> {-1, 1}"
  shows "is_boundary (boundary \<sigma>)"
proof -
  let ?p = "\<lambda>t t'. ( (b2_expr' t t' + \<sigma> * sqrt (discr t t')) / (2 * a2_expr) * a1_expr + b1_expr t t'
                   , (b2_expr' t t' + \<sigma> * sqrt (discr t t')) / (2 * a2_expr))"
  show ?thesis unfolding is_boundary_def    
  proof (rule exI[where x = ?p], safe, goal_cases occupancy_circle1 occupancy_circle2 limit)      
    case (occupancy_circle1 t t')        
    then show ?case   
      using assms occupancy_circle_intersection[where t = t and t' = t' and p = "?p t t'", OF \<open>t \<noteq> t'\<close>]     
      unfolding b2_expr'_def a2_expr_def a1_expr_def b1_expr_def discr_def 
      by metis
  next    
    case (occupancy_circle2 t t')
    then show ?case
      using assms occupancy_circle_intersection[where t = t and t' = t' and p = "?p t t'", OF \<open>t \<noteq> t'\<close>]
      unfolding b2_expr'_def a2_expr_def a1_expr_def b1_expr_def discr_def by metis
  next
    case (limit t)
    show ?case
      unfolding boundary_def using LIM_cong_limit[OF x_sol[of "t"] boundary_x_alt_def] 
      LIM_cong_limit[OF y_sol[of "t"] boundary_y_alt_def] by (auto intro:tendsto_Pair)
  qed
qed

end

section "Translation and rotation invariant of boundary"

subsection "Translation invariant"

lemma (in occupancy_acceleration) a2_expr_neq_zero:
  "a2_expr \<noteq> 0"
proof (rule ccontr)
  assume "\<not> a2_expr \<noteq> 0"
  hence "a2_expr = 0" by auto
  hence "1 + (vy / vx)\<^sup>2 = 0" unfolding a2_expr_def power_divide by auto
  hence "(vy / vx)\<^sup>2 = -1" by auto
  thus False using zero_le_power2[of "vy/vx"] by auto
qed

lemma (in occupancy_acceleration) summand_boundary_y:
  "b2_lim t / (2 * a2_expr) = sy + vy * t - (t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2) / (2 * a2_expr)"
proof -
  have 0: "b2_lim t = sy * (2 * (1 + vy\<^sup>2 / vx\<^sup>2)) + vy * t * (2 * (1 + vy\<^sup>2 / vx\<^sup>2)) - (t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2)"
    (is "_ = ?rhs1 + ?rhs2 - ?rhs3")
    unfolding b2_lim_def by (auto simp add:algebra_simps power_def)
  have 1: "\<And>a b c d :: real. (a + b - c) / d = a / d + b / d - c / d"
    by (auto simp add:divide_simps)
  have "b2_lim t / (2 * a2_expr) = ?rhs1 / (2 * a2_expr) + ?rhs2 / (2 * a2_expr) - ?rhs3 / (2 * a2_expr)"
    using 1[of "?rhs1" "?rhs2" "?rhs3" "2 * a2_expr"] unfolding 0 by auto
  thus ?thesis using a2_expr_neq_zero unfolding a2_expr_def by auto
qed

abbreviation (in occupancy_acceleration)
  "boundary_y_sy_free \<sigma> t \<equiv> vy * t - (t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2) / (2 * a2_expr) + 
                                                            (\<sigma> * sqrt_discr_lim t) / (2 * a2_expr)"

theorem (in occupancy_acceleration) boundary_y_alt_def2:
  "boundary_y \<sigma> t = sy + boundary_y_sy_free \<sigma> t"
  unfolding boundary_y_alt_def add_divide_distrib[of "b2_lim t"] 
  summand_boundary_y  by auto

lemma (in occupancy_acceleration) b1_lim_alt_def:
  "b1_lim t = sx + sy * vy / vx + vx * t + t * vy\<^sup>2 / vx - t\<^sup>3 * a_max\<^sup>2 / (2 * vx)"
proof -
  have "vx \<noteq> 0" using v\<^sub>x_wd unfolding oa_axioms by auto
  thus ?thesis unfolding b1_lim_def divide_simps power2_eq_square 
    unfolding add_divide_distrib[of "vx * vx" "vy * vy" "vx"]
    by (auto simp add:algebra_simps)
qed

schematic_goal (in occupancy_acceleration) finding_sx_free:
  "boundary_x \<sigma> t = sx + ?X"
proof -
  have "boundary_x \<sigma> t = sy * a1_expr + boundary_y_sy_free \<sigma> t * a1_expr + b1_lim t"
    (is "_ = ?rhs1 + b1_lim t")
    unfolding boundary_y_alt_def2 a1_expr_def b1_lim_def 
    by (auto simp add:algebra_simps)
  also have "... = ?rhs1 + sx + sy * vy / vx + vx * t + t * vy\<^sup>2 / vx - t\<^sup>3 * a_max\<^sup>2 / (2 * vx)"
    unfolding b1_lim_alt_def by auto
  also have "... =  - (t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2) * a1_expr / (2 * a2_expr) 
                 +   (\<sigma> * sqrt_discr_lim t) * a1_expr / (2 * a2_expr) 
                 +   sx + vx * t - t\<^sup>3 * a_max\<^sup>2 / (2 * vx)"
    unfolding a1_expr_def by (auto simp add:algebra_simps power2_eq_square)
  finally show "boundary_x \<sigma> t = sx + (- (t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2) * a1_expr / (2 * a2_expr)
                               +   (\<sigma> * sqrt_discr_lim t) * a1_expr / (2 * a2_expr)
                               + vx * t - t\<^sup>3 * a_max\<^sup>2 / (2 * vx))"
    by auto
qed

abbreviation (in occupancy_acceleration)
  "boundary_x_sx_free \<sigma> t \<equiv> - (t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2) * a1_expr / (2 * a2_expr) 
                          + \<sigma> * sqrt_discr_lim t * a1_expr / (2 * a2_expr) 
                          + vx * t - t\<^sup>3 * a_max\<^sup>2 / (2 * vx)"

theorem (in occupancy_acceleration) boundary_x_alt_def2:
  "boundary_x \<sigma> t = sx + boundary_x_sx_free \<sigma> t"
  using finding_sx_free by auto

theorem boundary_y_translation_freedom:
  fixes f :: "real \<Rightarrow> real"
  fixes sx sy a_max vx vy \<sigma> :: real
  assumes "occupancy_acceleration a_max (sx, sy) (vx, vy) sx sy vx vy"
  assumes "occupancy_acceleration a_max (sx, f sy) (vx, vy) sx (f sy) vx vy"
  defines "boundary_y1_syf \<equiv> occupancy_acceleration.boundary_y_sy_free a_max vx vy"
  defines "boundary_y2 \<equiv> occupancy_acceleration.boundary_y a_max (f sy) vx vy"
  shows "boundary_y2 \<sigma> t = (f sy) + boundary_y1_syf \<sigma> t"
proof -
  interpret oa1: occupancy_acceleration a_max "(sx, sy)" "(vx, vy)" sx sy vx vy
    rewrites r1: "oa1.boundary_y_sy_free \<sigma> t = boundary_y1_syf \<sigma> t"
    using assms(1) and assms(3) by auto

  interpret oa2: occupancy_acceleration a_max "(sx, f sy)" "(vx, vy)" sx "f sy" vx vy
    rewrites r2: "oa2.boundary_y \<sigma> t = boundary_y2 \<sigma> t"
    using assms(2) and assms(4) by auto

  have 0: "oa2.boundary_y \<sigma> t = f sy + oa2.boundary_y_sy_free \<sigma> t"
    using oa2.boundary_y_alt_def2 by auto
  thus ?thesis unfolding r1 r2 by auto
qed

theorem boundary_x_translation_freedom:
  fixes  f :: "real \<Rightarrow> real"
  fixes sx sy a_max vx vy \<sigma> :: real
  assumes "occupancy_acceleration a_max (sx, sy) (vx, vy) sx sy vx vy"
  assumes "occupancy_acceleration a_max (f sx, sy) (vx, vy) (f sx) sy vx vy"
  defines "boundary_x1_sxf \<equiv> occupancy_acceleration.boundary_x_sx_free a_max vx vy"
  defines "boundary_x2 \<equiv> occupancy_acceleration.boundary_x a_max (f sx) sy vx vy"  
  shows "boundary_x2 \<sigma> t = (f sx) + boundary_x1_sxf \<sigma> t"
proof -
  interpret oa1: occupancy_acceleration a_max "(sx, sy)" "(vx, vy)" sx sy vx vy
    rewrites r1: "oa1.boundary_x_sx_free \<sigma> t = boundary_x1_sxf \<sigma> t"
    using assms(1) and assms(3) by auto
  interpret oa2: occupancy_acceleration a_max "(f sx, sy)" "(vx, vy)" "f sx" sy vx vy
    rewrites r2: "oa2.boundary_x \<sigma> t = boundary_x2 \<sigma> t"
    using assms(2) and assms(4) by auto  

  have 0: "oa2.boundary_x \<sigma> t = f sx + oa2.boundary_x_sx_free \<sigma> t"
    using oa2.boundary_x_alt_def2 by auto
  thus ?thesis unfolding r1 r2 by auto
qed

subsection "Rotation invariant"

definition rotate :: "real \<Rightarrow> real2 \<Rightarrow> real2" where
  "rotate t p = (fst p * cos t - snd p * sin t, fst p * sin t + snd p * cos t)"

definition (in occupancy_acceleration) 
  "theta = (     if 0 < vx then arctan (vy / vx) 
            else if vx \<le> 0 \<and> 0 < vy then arctan (vy / vx) + pi 
            else (* vx \<le> 0 \<and> vy \<le> 0 *)   arctan (vy / vx) - pi)"

lemma (in occupancy_acceleration) cos_theta: 
  "0 < vx \<Longrightarrow> cos theta = 1 / sqrt (1 + (vy / vx)\<^sup>2)"
  unfolding theta_def using cos_arctan by auto

lemma (in occupancy_acceleration) cos_theta2: 
  "vx \<le> 0 \<Longrightarrow> cos theta = - 1 / sqrt (1 + (vy / vx)\<^sup>2)"
  unfolding theta_def using cos_minus_pi cos_periodic_pi cos_arctan by auto

lemma (in occupancy_acceleration) cos_THETA:
  "cos theta = (if 0 < vx then 1 / sqrt (1 + (vy / vx)\<^sup>2) else - 1 / sqrt (1 + (vy / vx)\<^sup>2))"
  using cos_theta cos_theta2 by auto

lemma (in occupancy_acceleration) sin_theta:
  "0 < vx \<Longrightarrow> sin theta = vy / vx / sqrt (1 + (vy / vx)\<^sup>2)"
  unfolding theta_def using sin_arctan by auto

lemma (in occupancy_acceleration) sin_theta2:
  "vx \<le> 0 \<Longrightarrow> sin theta = - vy / vx / sqrt (1 + (vy / vx)\<^sup>2)"
  unfolding theta_def using sin_periodic_pi sin_minus_pi sin_arctan 
  by (auto)

lemma (in occupancy_acceleration) sin_THETA:
  "sin theta = (if 0 < vx then vy / vx / sqrt (1 + (vy / vx)\<^sup>2) else - vy / vx / sqrt (1 + (vy / vx)\<^sup>2))"
  using sin_theta sin_theta2  by (auto)

lemma (in occupancy_acceleration) a2_expr_gt_zero:
  "0 < a2_expr"
  unfolding a2_expr_def using zero_le_power2[of "vy /vx"] unfolding power_divide 
  by linarith

lemma (in occupancy_acceleration) a2_expr_geq_zero:
  "0 \<le> a2_expr"
  using a2_expr_gt_zero by auto

lemma (in occupancy_acceleration) boundary_x_sx_free_alt_def:
  "boundary_x_sx_free \<sigma> t = vx * t - t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (1 - (sin theta)\<^sup>2)  
                          + \<sigma> * sqrt_discr_lim t * a1_expr / (2 * a2_expr)"  
  (is "_ = ?rhs + \<sigma> * sqrt_discr_lim t * a1_expr / (2 * a2_expr)")
proof -
  have "- (t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2) * a1_expr / (2 * a2_expr) = 
        - t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (- vy\<^sup>2 / vx\<^sup>2) / a2_expr"
    unfolding a1_expr_def unfolding power2_eq_square by (auto simp add:algebra_simps divide_simps)
  also have "... = - t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * - (sin theta)\<^sup>2"
  proof (cases "0 < vx")
    case True
    hence 0: "sin theta = vy / vx / sqrt (1 + (vy / vx)\<^sup>2)" using sin_THETA by auto 
    then show ?thesis 
      unfolding 0 power_divide using sin_THETA real_sqrt_pow2[OF a2_expr_geq_zero]
      unfolding a2_expr_def by auto
  next
    case False
    hence 0: "sin theta = -vy / vx / sqrt (1 + (vy / vx)\<^sup>2)" using sin_THETA by auto
    then show ?thesis 
      unfolding 0 power_divide using sin_THETA real_sqrt_pow2[OF a2_expr_geq_zero]
      unfolding a2_expr_def by auto
  qed
  finally have 0: "- (t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2) * a1_expr / (2 * a2_expr) = 
                - t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * - (sin theta)\<^sup>2" by auto
  hence "- (t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2) * a1_expr / (2 * a2_expr) - t\<^sup>3 * a_max\<^sup>2 / (2 * vx) = 
          - t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (1 - (sin theta)\<^sup>2)"
    unfolding 0 by (auto simp add:divide_simps algebra_simps)
  thus ?thesis by auto
qed

lemma (in occupancy_acceleration) abs_vx_def:
  "vx = norm v * cos theta"
  unfolding norm_prod_def sym[OF vx_def] sym[OF vy_def] 
proof (rule sym, cases "0 < vx")
  case True
  have "sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) = sqrt (vx\<^sup>2 + vy\<^sup>2)"
    by auto
  hence "sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) * (1 / sqrt (1 + (vy / vx)\<^sup>2)) =  sqrt ((vx\<^sup>2 + vy\<^sup>2) / (1 + (vy / vx)\<^sup>2))"
    (is "_ = sqrt (?term)")
    using real_sqrt_divide by auto
  moreover have "?term = vx\<^sup>2" 
    using nonzero_divide_eq_eq[OF a2_expr_neq_zero, of "vx\<^sup>2 + vy\<^sup>2" "vx\<^sup>2"] v\<^sub>x_wd 
    unfolding a2_expr_def power_divide  by (auto simp add:field_simps sym[OF vx_def])
  ultimately show "sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) * cos theta = vx"  
    unfolding cos_THETA by (auto)
next
  case False
  have "sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) = sqrt (vx\<^sup>2 + vy\<^sup>2)"
    by auto
  hence "sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) * (1 / sqrt (1 + (vy / vx)\<^sup>2)) =  sqrt ((vx\<^sup>2 + vy\<^sup>2) / (1 + (vy / vx)\<^sup>2))"
    (is "_ = sqrt (?term)")
    using real_sqrt_divide by auto
  moreover have "?term = vx\<^sup>2" 
    using nonzero_divide_eq_eq[OF a2_expr_neq_zero, of "vx\<^sup>2 + vy\<^sup>2" "vx\<^sup>2"] v\<^sub>x_wd 
    unfolding a2_expr_def power_divide  by (auto simp add:field_simps sym[OF vx_def])
  ultimately have "sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) * (1 / sqrt (1 + (vy / vx)\<^sup>2)) = \<bar>vx\<bar>"  
    by (auto)
  with False show "sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) * cos theta = vx"
    unfolding cos_THETA by auto
qed

lemma (in occupancy_acceleration) abs_vy_def:
  "vy = norm v * sin theta"
  unfolding norm_prod_def sin_theta sym[OF vx_def] sym[OF vy_def]
proof (rule sym, cases "0 < vx")
  case True
  hence 0: "sqrt ((vy / vx)\<^sup>2) = \<bar>vy\<bar> / vx"
    using real_sqrt_abs  by (auto)
  have "sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) = sqrt (vx\<^sup>2 + vy\<^sup>2)"
    by auto
  hence "sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) * (\<bar>vy\<bar> / vx / sqrt (1 + (vy / vx)\<^sup>2)) =  
         sqrt ((vx\<^sup>2 + vy\<^sup>2) * ((vy / vx)\<^sup>2 / (1 + (vy / vx)\<^sup>2)))"
    (is "_ = sqrt (?term)")
    using sym[OF real_sqrt_divide[of "(vy / vx)\<^sup>2" "1 + (vy/vx)\<^sup>2"]]  unfolding 0 real_sqrt_mult
    by auto
  moreover have "?term = vy\<^sup>2" 
    using nonzero_divide_eq_eq[OF a2_expr_neq_zero, of "vx\<^sup>2 + vy\<^sup>2" "vx\<^sup>2"] v\<^sub>x_wd 
    unfolding a2_expr_def power_divide  by (auto simp add:field_simps sym[OF vx_def])
  ultimately show " sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) * sin theta = vy"  
    unfolding sin_THETA using True by auto
next
  case False
  hence 0: "sqrt ((vy / vx)\<^sup>2) = - \<bar>vy\<bar> / vx"
    using real_sqrt_abs by (auto)
  have "sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) = sqrt (vx\<^sup>2 + vy\<^sup>2)"
    by auto
  hence "sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) * (- \<bar>vy\<bar> / vx / sqrt (1 + (vy / vx)\<^sup>2)) =  
         sqrt ((vx\<^sup>2 + vy\<^sup>2) * ((vy / vx)\<^sup>2 / (1 + (vy / vx)\<^sup>2)))"
    (is "_ = sqrt (?term)")
    using sym[OF real_sqrt_divide[of "(vy / vx)\<^sup>2" "1 + (vy/vx)\<^sup>2"]]  unfolding 0 real_sqrt_mult
    by auto
  moreover have "?term = vy\<^sup>2" 
    using nonzero_divide_eq_eq[OF a2_expr_neq_zero, of "vx\<^sup>2 + vy\<^sup>2" "vx\<^sup>2"] v\<^sub>x_wd 
    unfolding a2_expr_def power_divide  by (auto simp add:field_simps sym[OF vx_def])
  ultimately show " sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2) * sin theta = vy"  
    unfolding sin_THETA using False by auto
qed

lemma (in occupancy_acceleration) arith_aux:
  "\<bar>vx\<bar> * sqrt (1 + vy\<^sup>2 / vx\<^sup>2) = sqrt (vx\<^sup>2 + vy\<^sup>2)"
proof -
  have "1 + vy\<^sup>2 / vx\<^sup>2 = (vx\<^sup>2 + vy\<^sup>2) / vx\<^sup>2" (is "?lhs = ?rhs") 
    using v\<^sub>x_wd unfolding sym[OF vx_def] by (auto simp add: divide_simps)
  hence "sqrt ?lhs = sqrt ?rhs" by auto
  also have "... = sqrt (vx\<^sup>2 + vy\<^sup>2) / \<bar>vx\<bar>" unfolding real_sqrt_divide by auto
  finally have "sqrt ?lhs = sqrt (vx\<^sup>2 + vy\<^sup>2) / \<bar>vx\<bar>" by auto
  thus ?thesis by auto
qed

lemma (in occupancy_acceleration) vx_squared_def:
  "vx\<^sup>2 = (norm v)\<^sup>2 * (cos theta)\<^sup>2"
  using abs_vx_def power2_abs[of "vx"] by (auto simp add:field_simps power2_eq_square) 

lemma (in occupancy_acceleration) cos_theta_at_least_zero:
  "0 < vx \<Longrightarrow> 0 \<le> cos theta"
proof -
  assume "0 < vx"
  hence "-pi / 2 < theta" and "theta < pi / 2"
    unfolding theta_def using arctan_ubound arctan_lbound by auto
  thus "0 \<le> cos theta"  using cos_ge_zero by auto
qed

lemma (in occupancy_acceleration) cos_theta_at_most_zero:
  "vx \<le> 0 \<Longrightarrow> cos theta \<le> 0"
proof -
  assume "vx \<le> 0"
  hence "theta = arctan (vy / vx) + pi \<or> theta = arctan (vy / vx) - pi"
    unfolding theta_def by auto
  hence "cos theta = - cos (arctan (vy / vx))" 
    using cos_periodic_pi cos_minus_pi by auto
  also have "... = - 1 / sqrt (1 + (vy / vx)\<^sup>2)"
    using cos_arctan by auto
  finally have "cos theta = - 1 / sqrt (1 + (vy / vx)\<^sup>2)"
    by auto
  thus "cos theta \<le> 0"
    by (auto)
qed

lemma (in occupancy_acceleration) sqrt_discr_lim_alt_def':
  "0 < vx \<Longrightarrow> sqrt_discr_lim t = sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) * (1 / cos theta)"
proof -
  assume "0 < vx"
  hence "sqrt_discr_lim t = sqrt (t\<^sup>4 * a_max\<^sup>2 * (1 / (cos theta)\<^sup>2) - t^6 * a_max^4/vx\<^sup>2)"
    unfolding sqrt_discr_lim_def cos_THETA power_divide using real_sqrt_pow2[OF a2_expr_geq_zero]
    unfolding a2_expr_def  by (auto simp add:field_simps)
  hence "sqrt_discr_lim t = sqrt (t\<^sup>4 * a_max\<^sup>2 * (1 / (cos theta)\<^sup>2) - t^6 * a_max^4/(norm v)\<^sup>2 * (1 / (cos theta)\<^sup>2))"
    unfolding vx_squared_def by auto
  also have "... = sqrt ((t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) * (1 / (cos theta)\<^sup>2))"
    (is "_ = sqrt (?rhs * _)")
    by (auto simp add:algebra_simps)
  also have "... = sqrt ?rhs * (1 / \<bar>cos theta\<bar>)" 
    using real_sqrt_abs[of "1 / (cos theta)"] unfolding real_sqrt_mult
    by (auto simp add:field_simps)
  also have "... = sqrt ?rhs * (1 / cos theta)" using cos_theta_at_least_zero `0 < vx`
    by auto
  finally show ?thesis by auto
qed

lemma (in occupancy_acceleration) sqrt_discr_lim_alt_def'':
  "vx \<le> 0 \<Longrightarrow> sqrt_discr_lim t = - sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) * (1 / cos theta)"
proof -
  assume "vx \<le> 0"
  hence "sqrt_discr_lim t = sqrt (t\<^sup>4 * a_max\<^sup>2 * (1 / (cos theta)\<^sup>2) - t^6 * a_max^4/vx\<^sup>2)"
    unfolding sqrt_discr_lim_def cos_THETA power_divide using real_sqrt_pow2[OF a2_expr_geq_zero]
    unfolding a2_expr_def  by (auto simp add:field_simps)
  hence "sqrt_discr_lim t = sqrt (t\<^sup>4 * a_max\<^sup>2 * (1 / (cos theta)\<^sup>2) - t^6 * a_max^4/(norm v)\<^sup>2 * (1 / (cos theta)\<^sup>2))"
    unfolding vx_squared_def by auto
  also have "... = sqrt ((t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) * (1 / (cos theta)\<^sup>2))"
    (is "_ = sqrt (?rhs * _)")
    by (auto simp add:algebra_simps)
  also have "... = sqrt ?rhs * (1 / \<bar>cos theta\<bar>)" 
    using real_sqrt_abs[of "1 / (cos theta)"] unfolding real_sqrt_mult
    by (auto simp add:field_simps)
  also have "... = - sqrt ?rhs * (1 / cos theta)" using cos_theta_at_most_zero `vx \<le> 0`
    by auto
  finally show ?thesis by auto
qed

lemma (in occupancy_acceleration) sqrt_disc_lim_alt_def:
  "sqrt_discr_lim t = (if 0 < vx then   sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) * (1 / cos theta)
                                 else - sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) * (1 / cos theta))"
  using sqrt_discr_lim_alt_def' sqrt_discr_lim_alt_def'' by auto

lemma (in occupancy_acceleration) cos_theta_neq_zero:
  "cos theta \<noteq> 0"
  by (simp add: cos_arctan_not_zero theta_def)

lemma (in occupancy_acceleration) theta_aux:
  "0 < vx \<Longrightarrow> (1 / cos theta) * a1_expr / (2 * a2_expr) = - 1 / 2 * sin theta"
  (is "_ \<Longrightarrow> ?lhs = _")
proof -
  assume "0 < vx"
  have 0: "vy / vx * a1_expr / a2_expr = - (sin theta)\<^sup>2"
    unfolding sin_theta[OF `0 < vx`] power_divide using real_sqrt_pow2[OF a2_expr_geq_zero]
    unfolding a2_expr_def a1_expr_def power2_eq_square by auto
  have "?lhs = (1 / cos theta) * 1 / 2 * vx / vy * vy / vx * a1_expr / a2_expr" 
    using v\<^sub>x_wd sym[OF vx_def]     
  proof auto
    assume "vy = 0"
    hence "snd v = 0" using sym[OF vy_def] by auto
    assume asm: "occupancy_acceleration.a1_expr (fst v) 0 \<noteq> 0"
    have "occupancy_acceleration a_max 0 (fst v, 0) 0 0 (fst v) 0"
      unfolding occupancy_acceleration_def using v\<^sub>x_wd `snd v = 0` a_max_neq_zero
      by auto
    with occupancy_acceleration.a1_expr_def[of a_max 0 "(fst v, 0)" 0 0 "fst v" 0]
      have "occupancy_acceleration.a1_expr (fst v) 0 = - 0 / fst v"  by auto
    hence False using asm by auto
    thus "cos (occupancy_acceleration.theta (fst v) 0) = 0 " by auto
  qed
  also have "... = (1 / cos theta) * 1 / 2 * vx / vy * (vy / vx * a1_expr / a2_expr)"
    by auto
  also have "... = (1 / cos theta) * 1 / 2 * vx / vy * - (sin theta)\<^sup>2" 
    unfolding 0 by auto
  also have "... = (1 / cos theta) * 1 / 2 * (1 / tan theta) * - (sin theta)\<^sup>2"
    using `0 < vx` theta_def tan_arctan by auto 
  also have "... = (1 / cos theta) * 1 / 2 * (cos theta / sin theta) * - (sin theta)\<^sup>2"
    unfolding approximation_preproc_floatarith(12) by auto
  also have "... = - 1 / 2 * sin theta" unfolding power2_eq_square using cos_theta_neq_zero by auto
  finally show ?thesis by auto
qed

lemma (in occupancy_acceleration) theta_aux2:
  "vx \<le> 0 \<Longrightarrow> (1 / cos theta) * a1_expr / (2 * a2_expr) = - 1 / 2 * sin theta"
  (is "_ \<Longrightarrow> ?lhs = _")
proof -
  assume "vx \<le> 0"
  have 0: "vy / vx * a1_expr / a2_expr = - (sin theta)\<^sup>2"
    unfolding sin_theta2[OF `vx \<le> 0`] power_divide using real_sqrt_pow2[OF a2_expr_geq_zero]
    unfolding a2_expr_def a1_expr_def power2_eq_square by auto
  have "?lhs = (1 / cos theta) * 1 / 2 * vx / vy * vy / vx * a1_expr / a2_expr" 
    using v\<^sub>x_wd sym[OF vx_def]     
  proof auto
    assume "vy = 0"
    hence "snd v = 0" using sym[OF vy_def] by auto
    assume asm: "occupancy_acceleration.a1_expr (fst v) 0 \<noteq> 0"
    have "occupancy_acceleration a_max 0 (fst v, 0) 0 0 (fst v) 0"
      unfolding occupancy_acceleration_def using v\<^sub>x_wd `snd v = 0` a_max_neq_zero
      by auto
    with occupancy_acceleration.a1_expr_def[of a_max 0 "(fst v, 0)" 0 0 "fst v" 0]
      have "occupancy_acceleration.a1_expr (fst v) 0 = - 0 / fst v"  by auto
    hence False using asm by auto
    thus "cos (occupancy_acceleration.theta (fst v) 0) = 0 " by auto
  qed
  also have "... = (1 / cos theta) * 1 / 2 * vx / vy * (vy / vx * a1_expr / a2_expr)"
    by auto
  also have "... = (1 / cos theta) * 1 / 2 * vx / vy * - (sin theta)\<^sup>2" 
    unfolding 0 by auto
  also have "... = - 1 / 2 * sin theta"
  proof (cases "0 < vy")
    case True
    have "theta = arctan (vy / vx) + pi" unfolding theta_def using `vx \<le> 0` True by auto
    hence "tan theta = tan (arctan (vy / vx) + pi)" by auto
    also have "... = tan (arctan (vy / vx))" using tan_periodic_pi by auto
    also have "... = vy / vx" using tan_arctan by auto
    finally have "tan theta = vy / vx" by auto
    hence "(1 / cos theta) * 1 / 2 * vx / vy * - (sin theta)\<^sup>2 = 
           (1 / cos theta) * 1 / 2 * (1 / tan theta) * - (sin theta)\<^sup>2"
      by auto
    also have "... = (1 / cos theta) * 1 / 2 * (cos theta / sin theta) * - (sin theta)\<^sup>2"
      unfolding approximation_preproc_floatarith(12) by auto
    also have "... = - 1 / 2 * sin theta" unfolding power2_eq_square using cos_theta_neq_zero 
      by auto
    finally show "(1 / cos theta) * 1 / 2 * vx / vy * - (sin theta)\<^sup>2 = - 1 / 2 * sin theta"
      by auto
  next
    case False
    have "theta = arctan (vy / vx) - pi" unfolding theta_def using `vx \<le> 0` False by auto
    hence "tan theta = tan (arctan (vy / vx) - pi)" by auto
    also have "... = tan (arctan (vy / vx))" using tan_periodic_int[where i="-1"]
      by auto
    also have "... = vy / vx" using tan_arctan by auto
    finally have "tan theta = vy / vx" by auto
    hence "(1 / cos theta) * 1 / 2 * vx / vy * - (sin theta)\<^sup>2 = 
           (1 / cos theta) * 1 / 2 * (1 / tan theta) * - (sin theta)\<^sup>2"
      by auto
    also have "... = (1 / cos theta) * 1 / 2 * (cos theta / sin theta) * - (sin theta)\<^sup>2"
      unfolding approximation_preproc_floatarith(12) by auto
    also have "... = - 1 / 2 * sin theta" unfolding power2_eq_square using cos_theta_neq_zero 
      by auto
    finally show "(1 / cos theta) * 1 / 2 * vx / vy * - (sin theta)\<^sup>2 = - 1 / 2 * sin theta"
      by auto    
  qed 
  finally show ?thesis by auto
qed

lemma (in occupancy_acceleration) THETA_aux:
  "(1 / cos theta) * a1_expr / (2 * a2_expr) = - 1 / 2 * sin theta"
  using theta_aux theta_aux2 by (cases "0 < vx") (auto)

lemma (in occupancy_acceleration) sqrt_aux:
  "1 / 2 * sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) = 
           sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2)" 
  (is "?lhs = ?rhs")
proof -
  have "?lhs = sqrt (1 / 4) * sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) "  
    using real_sqrt_abs[of "1/2"] unfolding power_divide by auto
  also have "... = sqrt (1 / 4 * (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2))"
    using real_sqrt_mult[of "1 / 4" "t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2"] 
    by auto
  finally show ?thesis by (auto)
qed

lemma (in occupancy_acceleration) sqrt_term_geq_zero:
  assumes "valid_time t" and "0 \<le> t"
  shows "0 \<le> t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2"
proof - 
  have "t\<^sup>2 * a_max\<^sup>2 \<le> (norm v)\<^sup>2" (is "?lhs \<le> ?rhs")
    using assms unfolding valid_time_def using norm_Pair[of "fst v" "snd v"]
    surjective_pairing[of "v"] by (auto simp add:power2_eq_square)
  hence "?lhs * (t\<^sup>4 * a_max\<^sup>2) \<le> ?rhs * (t\<^sup>4 * a_max\<^sup>2)"
    by (simp add: mult_right_mono)
  hence "t\<^sup>6 * a_max\<^sup>4 \<le> t\<^sup>4 * a_max\<^sup>2 * (norm v)\<^sup>2" (is "?lhs2 \<le> ?rhs2 * (norm v)\<^sup>2")
    by (auto simp add:field_simps)
  moreover have "norm v \<noteq> 0" using norm_Pair[of "fst v" "snd v"]
    v\<^sub>x_wd by auto
  ultimately have "?lhs2 / (norm v)\<^sup>2 \<le> ?rhs2"
    by (auto simp add:divide_simps)
  thus ?thesis by auto
qed

schematic_goal (in occupancy_acceleration) rotation_invariant1:
  "0 < vx \<Longrightarrow> (boundary_x_sx_free \<sigma> t, boundary_y_sy_free \<sigma> t) = rotate theta (?X, ?Y)"
proof -
  (* finding _ * cos theta *)
  assume "0 < vx"
  have *: "boundary_x_sx_free \<sigma> t = vx * t - t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (1 - (sin theta)\<^sup>2)  
                               + \<sigma> * sqrt_discr_lim t * a1_expr / (2 * a2_expr)"
    (is "_ = ?rhs1 + ?rhs2")
    unfolding boundary_x_sx_free_alt_def by auto
  have 0: "?rhs1 = norm v * t * cos theta - t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (cos theta)\<^sup>2"
    using abs_vx_def  unfolding cos_squared_eq by auto
  have 1: "t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (cos theta) = t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)"
    unfolding cos_THETA norm_prod_def sym[OF vx_def] sym[OF vy_def] power_divide
  proof (cases "0 < vx")
    case True
    then show "t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (if 0 < vx then 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2) else - 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2)) =
    t\<^sup>3 * a_max\<^sup>2 / (2 * sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2))" using arith_aux by auto
  next
    case False
    then have "t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (if 0 < vx then 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2) else - 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2)) =
               t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (- 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2))"  
      by auto
    also have "... = t\<^sup>3 * a_max\<^sup>2 / 2 / (vx * - sqrt (1 + vy\<^sup>2 / vx\<^sup>2))"
      by (auto simp add:divide_simps)
    also have "... = t\<^sup>3 * a_max\<^sup>2 / 2 /  sqrt (vx\<^sup>2 + vy\<^sup>2)"
      using arith_aux False by auto
    finally show "t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (if 0 < vx then 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2) else - 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2)) =
      t\<^sup>3 * a_max\<^sup>2 / (2 * sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2))"
      by auto
  qed 
  from 0 have 2: "?rhs1 = (norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) * cos theta"
    unfolding power2_eq_square[of "cos theta"] sym[OF mult.assoc[where c = "cos theta"]] 1
    by (auto simp add:algebra_simps)
  
  (* finding _ * sin theta *)
  have "?rhs2 = \<sigma> * sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) * (1 / cos theta) * a1_expr / (2 * a2_expr)"
    unfolding sqrt_discr_lim_alt_def'[OF `0 < vx`] by auto
  also have "... = (\<sigma> * sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2)) * ((1 / cos theta) * a1_expr / (2 * a2_expr))"
    by auto
  also have "... = \<sigma> * sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) * (- 1 / 2 * sin theta)"
    unfolding THETA_aux by auto
  also have "... = - \<sigma> * (1 / 2 * sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2)) * sin theta"
    by auto
  also have "... = - \<sigma> * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * sin theta"
    unfolding sqrt_aux by auto
  finally have 3: "?rhs2 = - \<sigma> * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * sin theta"
    by auto

  (* summing up boundary_x *)
  have **: "boundary_x_sx_free \<sigma> t = (norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) * cos theta 
                               - \<sigma> * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * sin theta"
    using * 2 and 3 by auto
  
  (* validating for boundary_y, sin part *)
  have 4: "(norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) * sin theta = vy * t 
                                                             - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v) * sin theta"
    (is "_ = _ - ?rhs4")
    using abs_vy_def by (auto simp add:algebra_simps)
  have "norm v \<noteq> 0" using surjective_pairing[of "v"] norm_Pair[of "fst v" "snd v"] v\<^sub>x_wd 
    unfolding sym[OF vx_def] sym[OF vy_def] by (auto)
  hence "sin theta = vy / norm v" using abs_vy_def 
    by (auto simp add:algebra_simps divide_simps)
  hence "?rhs4 = t\<^sup>3 * vy * a_max\<^sup>2 / 2 * 1 / (norm v * norm v)" by (auto simp add:divide_simps)
  moreover have "norm v * norm v = vx\<^sup>2 + vy\<^sup>2" using surjective_pairing[of "v"] 
    norm_Pair[of "fst v" "snd v"] unfolding sym[OF vx_def] sym[OF vy_def]
    by (auto simp add:algebra_simps)
  ultimately have "?rhs4 = t\<^sup>3 * vy * a_max\<^sup>2 / 2 * 1 / (vx\<^sup>2 + vy\<^sup>2)" by auto
  moreover have "1 / vx\<^sup>2 * 1 / a2_expr = 1 / (vx\<^sup>2 + vy\<^sup>2)"
    unfolding a2_expr_def using v\<^sub>x_wd unfolding sym[OF vx_def] by (auto simp add:field_simps)
  ultimately have "?rhs4 = t\<^sup>3 * vy * a_max\<^sup>2 / 2 * (1 / vx\<^sup>2 * 1 / a2_expr)" 
    by auto
  hence "?rhs4 = t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2 / (2 * a2_expr)" by auto
  hence 5: "(norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) * sin theta =  vy * t 
                                                              -  t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2 / (2 * a2_expr)"
    using 4 by auto

  (* validating for boundary_y, cos part*)
  have "(1 / cos theta) / a2_expr = cos theta"
    unfolding cos_theta[OF `0 < vx`] a2_expr_def by (auto simp add:algebra_simps divide_simps)
  hence "sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) * cos theta = sqrt_discr_lim t / a2_expr"
    unfolding sqrt_discr_lim_alt_def'[OF `0 < vx`] by (auto simp add:divide_simps)
  hence 6: "sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * cos theta = 
         sqrt_discr_lim t / (2 * a2_expr)"
    unfolding sym[OF sqrt_aux[of "t"]]  by (auto)

  have ***: "boundary_y_sy_free \<sigma> t =  (norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) * sin theta 
                               + \<sigma> * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * cos theta"
    unfolding sym[OF 5] using sym[OF 6]
    by (metis (no_types, lifting) "6" mult.assoc times_divide_eq_right)

  let ?X = "norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)"
  let ?Y = "\<sigma> * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2)"

  show "(boundary_x_sx_free \<sigma> t, boundary_y_sy_free \<sigma> t) = rotate theta (?X, ?Y)"
    using sym[OF **] and sym[OF ***] unfolding rotate_def by auto  
qed

schematic_goal (in occupancy_acceleration) rotation_invariant2:
  "vx \<le> 0 \<Longrightarrow> (boundary_x_sx_free \<sigma> t, boundary_y_sy_free \<sigma> t) = rotate theta (?X, ?Y)"
proof -

  assume "vx \<le> 0"
  have *: "boundary_x_sx_free \<sigma> t = vx * t - t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (1 - (sin theta)\<^sup>2)  
                               + \<sigma> * sqrt_discr_lim t * a1_expr / (2 * a2_expr)"
    (is "_ = ?rhs1 + ?rhs2")  
    unfolding boundary_x_sx_free_alt_def by auto
  have 0: "?rhs1 = norm v * t * cos theta - t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (cos theta)\<^sup>2"
    using abs_vx_def  unfolding cos_squared_eq by auto
  have 1: "t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (cos theta) = t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)"
    unfolding cos_THETA norm_prod_def sym[OF vx_def] sym[OF vy_def] power_divide
  proof (cases "0 < vx")
    case True
    then show "t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (if 0 < vx then 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2) else - 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2)) =
    t\<^sup>3 * a_max\<^sup>2 / (2 * sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2))" using arith_aux by auto
  next
    case False
    then have "t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (if 0 < vx then 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2) else - 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2)) =
               t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (- 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2))"  
      by auto
    also have "... = t\<^sup>3 * a_max\<^sup>2 / 2 / (vx * - sqrt (1 + vy\<^sup>2 / vx\<^sup>2))"
      by (auto simp add:divide_simps)
    also have "... = t\<^sup>3 * a_max\<^sup>2 / 2 /  sqrt (vx\<^sup>2 + vy\<^sup>2)"
      using arith_aux False by auto
    finally show "t\<^sup>3 * a_max\<^sup>2 / (2 * vx) * (if 0 < vx then 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2) else - 1 / sqrt (1 + vy\<^sup>2 / vx\<^sup>2)) =
      t\<^sup>3 * a_max\<^sup>2 / (2 * sqrt ((norm vx)\<^sup>2 + (norm vy)\<^sup>2))"
      by auto
  qed 
  from 0 have 2: "?rhs1 = (norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) * cos theta"
    unfolding power2_eq_square[of "cos theta"] sym[OF mult.assoc[where c = "cos theta"]] 1
    by (auto simp add:algebra_simps)

  have "?rhs2 = -\<sigma> * sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) * (1 / cos theta) * a1_expr / (2 * a2_expr)"
    unfolding sqrt_discr_lim_alt_def''[OF `vx \<le> 0`] by auto
    also have "... = - (\<sigma> * sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2)) * ((1 / cos theta) * a1_expr / (2 * a2_expr))"
    by auto
  also have "... = - \<sigma> * sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) * (- 1 / 2 * sin theta)"
    unfolding THETA_aux by auto
  also have "... =   \<sigma> * (1 / 2 * sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2)) * sin theta"
    by auto
  also have "... =   \<sigma> * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * sin theta"
    unfolding sqrt_aux by auto
  finally have 3: "?rhs2 = \<sigma> * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * sin theta"
    by auto 

  have **: "boundary_x_sx_free \<sigma> t = (norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) * cos theta 
                               + \<sigma> * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * sin theta"
    using * 2 and 3 by auto
  have 4: "(norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) * sin theta = vy * t 
                                                             - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v) * sin theta"
    (is "_ = _ - ?rhs4")
    using abs_vy_def by (auto simp add:algebra_simps)
  have "norm v \<noteq> 0" using surjective_pairing[of "v"] norm_Pair[of "fst v" "snd v"] v\<^sub>x_wd 
    unfolding sym[OF vx_def] sym[OF vy_def] by (auto)
  hence "sin theta = vy / norm v" using abs_vy_def 
    by (auto simp add:algebra_simps divide_simps)
  hence "?rhs4 = t\<^sup>3 * vy * a_max\<^sup>2 / 2 * 1 / (norm v * norm v)" by (auto simp add:divide_simps)
  moreover have "norm v * norm v = vx\<^sup>2 + vy\<^sup>2" using surjective_pairing[of "v"] 
    norm_Pair[of "fst v" "snd v"] unfolding sym[OF vx_def] sym[OF vy_def]
    by (auto simp add:algebra_simps)
  ultimately have "?rhs4 = t\<^sup>3 * vy * a_max\<^sup>2 / 2 * 1 / (vx\<^sup>2 + vy\<^sup>2)" by auto
  moreover have "1 / vx\<^sup>2 * 1 / a2_expr = 1 / (vx\<^sup>2 + vy\<^sup>2)"
    unfolding a2_expr_def using v\<^sub>x_wd unfolding sym[OF vx_def] by (auto simp add:field_simps)
  ultimately have "?rhs4 = t\<^sup>3 * vy * a_max\<^sup>2 / 2 * (1 / vx\<^sup>2 * 1 / a2_expr)" 
    by auto
  hence "?rhs4 = t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2 / (2 * a2_expr)" by auto
  hence 5: "(norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) * sin theta =  vy * t 
                                                              -  t\<^sup>3 * vy * a_max\<^sup>2 / vx\<^sup>2 / (2 * a2_expr)"
    using 4 by auto

  (* validating for boundary_y, cos part*)
  have "(1 / cos theta) / a2_expr = cos theta"
    unfolding cos_theta2[OF `vx \<le> 0`] a2_expr_def by (auto simp add:algebra_simps divide_simps)
  hence "sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) * cos theta = - sqrt_discr_lim t / a2_expr"
    unfolding sqrt_discr_lim_alt_def''[OF `vx \<le> 0`] by (auto simp add:divide_simps)
  hence 6: "sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * cos theta = 
         - sqrt_discr_lim t / (2 * a2_expr)"
    unfolding sym[OF sqrt_aux[of "t"]]  by (auto)

  have ***: "boundary_y_sy_free \<sigma> t =  (norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) * sin theta 
                               + (-\<sigma>) * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * cos theta"
    unfolding sym[OF 5] using sym[OF 6] 
  proof -
    assume "- sqrt_discr_lim t / (2 * a2_expr) = 
              sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * cos theta"
      (is "?lhs = ?rhs")
    hence "-\<sigma> * ?lhs = -\<sigma> * ?rhs" by auto
    hence " \<sigma> * sqrt_discr_lim t / (2 * a2_expr) = 
           -\<sigma> *  sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * cos theta"
      by auto
    thus "(norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) * sin theta + \<sigma> * sqrt_discr_lim t / (2 * a2_expr) =
    (norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) * sin theta + - \<sigma> * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) * cos theta"
      by auto
  qed

  let ?X = "norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)"
  let ?Y = "(-\<sigma>) * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2)"

  show "(boundary_x_sx_free \<sigma> t, boundary_y_sy_free \<sigma> t) = rotate theta (?X, ?Y)"
    using sym[OF **] and sym[OF ***] unfolding rotate_def by auto   
qed

definition (in occupancy_acceleration) b_x where 
  "b_x t = norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)"

lemma (in occupancy_acceleration) b_x_at_least_zero:
  assumes "0 \<le> t" and "t \<le> norm v / a_max"
  shows "0 \<le> b_x t"
proof -
  have "1 \<le> sqrt 2"
    by (approximation 10)
  have "t \<le> sqrt 2 * (norm v / a_max)" (is "_ \<le> ?rhs0")
    using mult_mono[OF `1 \<le> sqrt 2` assms(2)] using assms(1) by auto
  have "t\<^sup>2 \<le> ?rhs0\<^sup>2"
    using power2_mono_leq[OF `t \<le> ?rhs0` `0 \<le> t`] .
  also have "... = 2 * (norm v)\<^sup>2 / a_max\<^sup>2"
    by (auto simp add:field_simps)
  finally have "t\<^sup>2 \<le> 2 * (norm v)\<^sup>2 / a_max\<^sup>2"
    by auto
  hence "t\<^sup>2 * a_max\<^sup>2 / (2 * norm v) \<le> norm v"
    using v\<^sub>x_wd a_max_neq_zero by (auto simp add:field_simps divide_simps power_def)
  hence "0 \<le> norm v - t\<^sup>2 * a_max\<^sup>2 / (2 * norm v)" (is "_ \<le> ?rhs1")
    by auto
  hence "0 * t \<le> ?rhs1 * t"
    using `0 \<le> t` by auto
  hence "0 \<le> norm v  * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)"
    by (auto simp add:field_simps power_def)
  thus ?thesis
    unfolding b_x_def by auto
qed

lemma (in occupancy_acceleration) b_x_mirror:
  "b_x (- t) = - b_x t"
  unfolding b_x_def by (auto simp add:field_simps)

lemma (in occupancy_acceleration) b_x_uniqueness:
  defines "t_max \<equiv> sqrt (2/3) * norm v / a_max"
  assumes "t1 \<le> t_max" and "t2 \<le> t_max"
  assumes "0 \<le> t1" and "0 \<le> t2"
  assumes "b_x t1 = b_x t2"
  shows "t1 = t2"
proof (rule ccontr)
  assume "t1 \<noteq> t2"
  have "norm v * t1 - t1\<^sup>3 * a_max\<^sup>2 / (2 * norm v) = norm v * t2 - t2\<^sup>3 * a_max\<^sup>2 / (2 * norm v)"
    using assms unfolding b_x_def by (auto simp add:field_simps)
  hence "norm v * (t1 - t2) = t1\<^sup>3 * a_max\<^sup>2 / (2 * norm v) - t2\<^sup>3 * a_max\<^sup>2 / (2 * norm v)"
    by (auto simp add:field_simps)
  also have "... = (t1\<^sup>3 - t2\<^sup>3) * a_max\<^sup>2 / (2 * norm  v)"
    by (auto simp add:field_simps divide_simps)
  finally have "norm v * (t1 - t2) = (t1\<^sup>3 - t2\<^sup>3) * a_max\<^sup>2 / (2 * norm v)" (is "?lhs0 = _")
    by auto
  also have "... = ((t1 - t2) * (t1\<^sup>2 + t1 * t2 + t2\<^sup>2)) * a_max\<^sup>2 / (2 * norm v)" (is "_ = ?rhs0")
    by (auto simp add:field_simps power_def)
  finally have "?lhs0 = ?rhs0"
    by auto
  hence "?lhs0 / (t1 - t2) = ?rhs0 / (t1 - t2)"
    by (auto)
  hence "norm v = (t1\<^sup>2 + t1 * t2 + t2\<^sup>2) * a_max\<^sup>2 / (2 * norm v)"
    using `t1 \<noteq> t2` by auto
  hence "t1\<^sup>2 + t1 * t2 + t2\<^sup>2 = 2 * (norm v)\<^sup>2 / a_max\<^sup>2" (is "?lhs1 = ?rhs1")
    using v\<^sub>x_wd a_max_neq_zero by (auto simp add:field_simps power_def)
  have "?lhs1 < 3 * t_max\<^sup>2"
  proof -
    have "t1\<^sup>2 \<le> t_max\<^sup>2" 
      using power2_mono_leq[OF assms(2) `0 \<le> t1`] by auto
    moreover have "t2\<^sup>2 \<le> t_max\<^sup>2"
      using power2_mono_leq[OF assms(3) `0 \<le> t2`] by auto
    moreover have "t1 * t2 < t_max * t_max"
    proof (cases "t1 = t_max")
      case True
      hence "t1 \<le> t_max" and "t2 < t_max" using `t1 \<noteq> t2` `t2 \<le> t_max`
        by (auto simp add:field_simps)
      have "0 < norm v"
        using surjective_pairing[of "v"] norm_Pair[of "fst v" "snd v"] v\<^sub>x_wd norm_ge_zero[of "v"]
        vx_def vx_squared_def by force        
      moreover hence "0 < t1" 
        unfolding True t_max_def using a_max_neq_zero by (auto simp add:field_simps)        
      then show ?thesis 
        using mult_le_less_imp_less[OF `t1 \<le> t_max` `t2 < t_max` _ `0 \<le> t2`] by auto
    next
      case False
      hence "t1 < t_max" using `t1 \<le> t_max` by auto
      moreover have "0 < norm v"
        using surjective_pairing[of "v"] norm_Pair[of "fst v" "snd v"] v\<^sub>x_wd norm_ge_zero[of "v"]
        vx_def vx_squared_def by force        
      then show ?thesis 
        using mult_less_le_imp_less[OF `t1 < t_max` `t2 \<le> t_max` `0 \<le> t1`]  `0 \<le> t2`
        unfolding t_max_def using a_max_neq_zero
        by (cases "t2 = 0") (auto)
    qed
    ultimately show ?thesis
      by (auto simp add:power2_eq_square)
  qed
  also have "... = ?rhs1"
    unfolding t_max_def by (auto simp add:field_simps)
  finally have "?lhs1 < ?rhs1"
    by auto
  with `?lhs1 = ?rhs1` show False 
    by auto
qed    

definition (in occupancy_acceleration) b_y where 
  "b_y \<sigma> t = (if 0 < vx then   \<sigma>  * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2)
                        else (-\<sigma>) * sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2))"

lemma (in occupancy_acceleration) b_y_lt_radius:
  assumes "0 < t" and "0 < vx"
  shows "b_y 1 t < 1/2 * a_max * t\<^sup>2"
proof -
  have "0 < t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2" (is "_ < ?rhs")
    using assms a_max_neq_zero v\<^sub>x_wd by (auto simp add:field_simps divide_simps)
  hence "t\<^sup>4 * a_max\<^sup>2 - ?rhs < t\<^sup>4 * a_max\<^sup>2" (is "?lhs1 < ?rhs1")
    by auto
  hence "1/4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2 < 1/4 * a_max\<^sup>2 * t\<^sup>4" (is "?lhs2 < ?rhs2")
    by (auto simp add:field_simps)
  hence "sqrt (?lhs2) < sqrt ?rhs2"
    by auto
  hence "b_y 1 t < sqrt ?rhs2"
    unfolding b_y_def using `0 < vx` by auto
  also have "... = 1/2 * a_max * t\<^sup>2"
    using real_sqrt_abs[of "1/2 * a_max * t\<^sup>2"] using a_max_neq_zero by (auto simp add:field_simps)  
  finally show ?thesis by auto
qed

lemma (in occupancy_acceleration) radius_lt_b_y:
  assumes "0 < t" and "0 < vx"
  shows "-1/2 * a_max * t\<^sup>2 < b_y (-1) t"
proof -
  have "0 < t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2" (is "_ < ?rhs")
    using assms a_max_neq_zero v\<^sub>x_wd by (auto simp add:field_simps divide_simps)
  hence "t\<^sup>4 * a_max\<^sup>2 - ?rhs < t\<^sup>4 * a_max\<^sup>2" (is "?lhs1 < ?rhs1")
    by auto
  hence "1/4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2 < 1/4 * a_max\<^sup>2 * t\<^sup>4" (is "?lhs2 < ?rhs2")
    by (auto simp add:field_simps)
  hence "sqrt (?lhs2) < sqrt ?rhs2"
    by auto
  hence "b_y 1 t < sqrt ?rhs2"
    unfolding b_y_def using `0 < vx` by auto
  also have "... = 1/2 * a_max * t\<^sup>2"
    using real_sqrt_abs[of "1/2 * a_max * t\<^sup>2"] using a_max_neq_zero by (auto simp add:field_simps) 
  finally have "-1/2 * a_max * t\<^sup>2 < b_y (-1) t"
    unfolding b_y_def by auto
  thus ?thesis by auto
qed

theorem (in occupancy_acceleration) rotation_invariant:
  "(boundary_x_sx_free \<sigma> t, boundary_y_sy_free \<sigma> t) = rotate theta (b_x t, b_y \<sigma> t)"
proof (cases "0 < vx")
  case True
  then show ?thesis 
    using rotation_invariant1[OF True, of "t"] unfolding b_x_def b_y_def by auto
next
  case False
  then show ?thesis 
    using rotation_invariant2 False unfolding b_x_def b_y_def by auto
qed

section "Preventing b_x goes backward"

lemma (in occupancy_acceleration) derivative_b_x:
  "(b_x has_vector_derivative (norm v - 3 * t\<^sup>2 * a_max\<^sup>2 / (2 * norm v))) (at t within s')"
proof -
  have 0: "((\<lambda>t. norm v * t) has_vector_derivative (norm v)) (at t within s')"
    by (auto intro:derivative_intros derivative_eq_intros)
  have "((\<lambda>t. t\<^sup>3 * (a_max\<^sup>2 / (2 * norm v))) has_field_derivative (3 * t\<^sup>2 * (a_max\<^sup>2 / (2 * norm v))))
                 (at t within s')"
    by (rule DERIV_cmult_right) (auto intro:derivative_intros)
  hence "((\<lambda>t. t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)) has_vector_derivative (3 * t\<^sup>2 * (a_max\<^sup>2 / (2 * norm v))))
         (at t within s')"
    unfolding has_field_derivative_iff_has_vector_derivative by auto
  with 0 show ?thesis unfolding b_x_def by (auto intro:derivative_intros)
qed

lemma (in occupancy_acceleration) derivative_b_y:
  defines "g \<equiv> \<lambda>t. 1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2"
  assumes "0 < t" and "t < norm v / a_max"
  shows "(b_y \<sigma> has_real_derivative (if 0 < vx then  \<sigma>   * (inverse (sqrt (g t)) / 2) * (a_max\<^sup>2 * t\<^sup>3 - 6 * t^5 * a_max\<^sup>4 / (2 * norm v)\<^sup>2)
                                               else (-\<sigma>) * (inverse (sqrt (g t)) / 2) * (a_max\<^sup>2 * t\<^sup>3 - 6 * t^5 * a_max\<^sup>4 / (2 * norm v)\<^sup>2))) (at t within s')"
proof -
  have "valid_time t"
  proof -
    have "t\<^sup>2 < (norm v / a_max)\<^sup>2"
      using power2_mono_lt[OF assms(3)] assms(2) by auto
    hence "t\<^sup>2 * a_max\<^sup>2 < (norm v)\<^sup>2"
      unfolding power_divide using a_max_neq_zero by (auto simp add:field_simps)
    thus ?thesis
      unfolding valid_time_def using surjective_pairing[of "v"] norm_Pair[of "fst v" "snd v"]
      by auto
  qed
  have g_deriv: "(g has_real_derivative a_max\<^sup>2 * t\<^sup>3 - 6 * t^5 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) (at t within s')"
    using v\<^sub>x_wd
    unfolding g_def by (auto simp add: field_simps intro!:derivative_intros derivative_eq_intros)
  have "0 \<le> g t" using sqrt_term_geq_zero[OF `valid_time t`] assms(2-3) unfolding g_def by auto
  moreover have "g t \<noteq> 0"
  proof (rule ccontr)
    assume "\<not> g t \<noteq> 0"
    hence "g t = 0" by auto
    hence "1 / 4 * t\<^sup>4 * a_max\<^sup>2 = t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2" unfolding g_def 
      by auto
    hence "t\<^sup>4 * a_max\<^sup>2 = t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2" (is "?lhs = ?rhs") 
      unfolding power_mult_distrib by auto
    hence "?lhs / t\<^sup>4 = ?rhs / t\<^sup>4" 
      by auto
    hence "a_max\<^sup>2 = t\<^sup>2 * a_max\<^sup>4 / (norm v)\<^sup>2" (is "?lhs2 = ?rhs2")
      using assms(2)  power_diff[of "t" "4" "6"] by (auto)
    hence "?lhs2 / a_max\<^sup>2 = ?rhs2 / a_max\<^sup>2"
      by auto
    hence "1 = ?rhs2 / a_max\<^sup>2"
      using a_max_neq_zero by auto
    also have  "... = t\<^sup>2 * (a_max\<^sup>4 / a_max\<^sup>2) / (norm v)\<^sup>2"
      by (auto simp add:divide_simps)
    also have "... = t\<^sup>2 * a_max\<^sup>2 / (norm v)\<^sup>2"
      using power_diff[of "a_max" "2" "4"]  a_max_neq_zero by auto
    finally have "1 = t\<^sup>2 * a_max\<^sup>2 / (norm v)\<^sup>2"
      by auto
    hence "t\<^sup>2 = (norm v)\<^sup>2 / a_max\<^sup>2"
      by auto
    hence "sqrt (t\<^sup>2) = sqrt ((norm v)\<^sup>2 / a_max\<^sup>2)"
      by auto
    hence "\<bar>t\<bar> = \<bar>norm v / a_max\<bar>"
      unfolding power_divide[of "norm v" "a_max" "2", THEN sym]  by auto
    hence "\<bar>t\<bar> = norm v / a_max"
      using norm_ge_zero a_max_neq_zero by auto
    hence "t = norm v / a_max \<or> t = - (norm v / a_max)"
      by auto
    thus False using assms(2-3) norm_ge_zero a_max_neq_zero by auto
  qed
  ultimately have "0 < g t" by auto
  have "((\<lambda>x. sqrt (g x)) has_real_derivative (inverse (sqrt (g t)) / 2) * (a_max\<^sup>2 * t\<^sup>3 - 6 * t^5 * a_max\<^sup>4 / (2 * norm v)\<^sup>2)) 
    (at t within s')"
    using `g t \<noteq> 0`
  proof (rule Deriv.derivative_intros(50))
    show "0 < g t \<Longrightarrow> inverse (sqrt (g t)) / 2 = inverse (sqrt (g t)) / 2"
      using `0 < g t` by auto
  next
    show "g t < 0 \<Longrightarrow> inverse (sqrt (g t)) / 2 = - inverse (sqrt (g t)) / 2"
      using `0 < g t` by auto
  next
    show "(g has_real_derivative a_max\<^sup>2 * t\<^sup>3 - 6 * t ^ 5 * a_max\<^sup>4 / (2 * norm v)\<^sup>2) (at t within s')"
      using g_deriv by auto
  qed
  hence base: "\<And>c. ((\<lambda>x. c * sqrt (g x)) has_real_derivative c * (inverse (sqrt (g t)) / 2) * (a_max\<^sup>2 * t\<^sup>3 - 6 * t^5 * a_max\<^sup>4 / (2 * norm v)\<^sup>2)) 
    (at t within s')"        
    by (auto intro:derivative_intros derivative_eq_intros)
  thus ?thesis       
  proof (cases "0 < vx")
    case True
    thus ?thesis
      using base[where c="\<sigma>"] unfolding b_y_def g_def by auto
  next
    case False
    then show ?thesis 
      using base[where c="-\<sigma>"] unfolding b_y_def g_def by auto
  qed
qed  

definition (in occupancy_acceleration)
  "b_y_inner t = 1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2"

definition (in occupancy_acceleration)
  "b_y_inner' t = a_max\<^sup>2 * t\<^sup>3 - 6 * t^5 * a_max\<^sup>4 / (2 * norm v)\<^sup>2"   

lemma (in occupancy_acceleration) b_y_inner'_gt_zero:
  assumes "0 < t" and "t < sqrt (2 / 3) * norm v / a_max"
  shows "0 < b_y_inner' t"
proof (rule ccontr)
  assume "\<not> 0 < b_y_inner' t"
  hence "b_y_inner' t \<le> 0" 
    by auto
  hence "a_max\<^sup>2 * t\<^sup>3 \<le> 6 * t^5 * a_max\<^sup>4 / (2 * norm v)\<^sup>2" (is "?lhs0 \<le> ?rhs0")
    unfolding b_y_inner'_def by auto
  hence "?lhs0 / a_max\<^sup>2 \<le> ?rhs0 / a_max\<^sup>2"
    using v\<^sub>x_wd a_max_neq_zero by (auto simp add:field_simps)
  hence "t\<^sup>3 \<le> 6 * t^5 * a_max\<^sup>2 / (2 * norm v)\<^sup>2" (is "?lhs1 \<le> ?rhs1")
    using v\<^sub>x_wd using a_max_neq_zero by (auto simp add:field_simps power_def)
  hence "?lhs1 / t\<^sup>3 \<le> ?rhs1 / t\<^sup>3"
    using v\<^sub>x_wd `0 < t` by (auto simp add:field_simps power_def)
  hence "1 \<le> 6 * t\<^sup>2 * a_max\<^sup>2 / (2 * norm v)\<^sup>2"
    using `0 < t` by (auto simp add:field_simps power_def)
  hence "(2 * norm v)\<^sup>2 / (6 * a_max\<^sup>2) \<le> t\<^sup>2"
    using v\<^sub>x_wd by (auto simp add:field_simps divide_simps)
  hence "2 / 3 * (norm v / a_max)\<^sup>2 \<le> t\<^sup>2" (is "?lhs2 \<le> ?rhs2")
    by (auto simp add:field_simps)
  hence "sqrt ?lhs2 \<le> sqrt ?rhs2"
    unfolding real_sqrt_le_iff by (auto simp add:field_simps)
  hence "sqrt (2 / 3) * (norm v / a_max) \<le> t"
    unfolding real_sqrt_mult real_sqrt_abs abs_divide using norm_ge_zero[of "v"] a_max_neq_zero
    assms by auto
  with assms show False 
    by auto
qed

lemma (in occupancy_acceleration) derivative_of_b_y_inner [derivative_intros]:
  "(b_y_inner has_field_derivative b_y_inner' t) (at t within s')"
  unfolding b_y_inner_def b_y_inner'_def
  using v\<^sub>x_wd by (auto simp add:field_simps intro!:derivative_intros derivative_eq_intros)

lemma (in occupancy_acceleration) b_y_inner_neq_zero:
  assumes "0 < t" and "t < norm v / a_max"
  shows "b_y_inner t \<noteq> 0"
proof (rule ccontr)
  assume "\<not> b_y_inner t \<noteq> 0"
  hence "b_y_inner t = 0 " by auto
  hence "t\<^sup>4 * a_max\<^sup>2 = t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2" (is "?lhs =?rhs")
    unfolding b_y_inner_def by auto
  hence "?lhs / t\<^sup>4 = ?rhs / t\<^sup>4"
    by auto
  hence "a_max\<^sup>2 = ?rhs / t\<^sup>4" using `0 < t` by auto
  also have "... = t\<^sup>2 * a_max\<^sup>4 / (norm v)\<^sup>2"
    using power_diff[of "t" "4" "6"] by (auto simp add:field_simps divide_simps)
  finally have "a_max\<^sup>2 = t\<^sup>2 * a_max\<^sup>4 / (norm v)\<^sup>2" (is "?lhs2 = ?rhs2")
    by auto
  hence "?lhs2 / a_max\<^sup>2 = ?rhs2 / a_max\<^sup>2"
    by auto
  hence "1 = ?rhs2 / a_max\<^sup>2" using a_max_neq_zero by auto
  also have "... = t\<^sup>2 * a_max\<^sup>2 / (norm v)\<^sup>2" using power_diff[of "a_max" "2" "4"] a_max_neq_zero
    by (auto simp add:field_simps divide_simps)
  finally have "1 = t\<^sup>2 * a_max\<^sup>2 / (norm v)\<^sup>2" by auto
  hence "t\<^sup>2 = (norm v)\<^sup>2 / a_max\<^sup>2" by auto
  with `t < norm v / a_max` show False 
  proof -
    have "t\<^sup>2 = (norm v / a_max)\<^sup>2"
      by (simp add: \<open>t\<^sup>2 = (norm v)\<^sup>2 / a_max\<^sup>2\<close> power_divide)
    then show ?thesis
      using \<open>t < norm v / a_max\<close> assms(1) by fastforce
  qed
qed

schematic_goal (in occupancy_acceleration)
  assumes "0 < t" and "t < norm v / a_max"
  shows "((\<lambda>x. sqrt (b_y_inner x)) has_field_derivative ?Da * ?Db) (at t within s')"
proof (intro derivative_intros)
  show "b_y_inner t \<noteq> 0"
  proof (rule ccontr)
    assume "\<not> b_y_inner t \<noteq> 0"
    hence "b_y_inner t = 0 " by auto
    hence "t\<^sup>4 * a_max\<^sup>2 = t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2" (is "?lhs =?rhs")
      unfolding b_y_inner_def by auto
    hence "?lhs / t\<^sup>4 = ?rhs / t\<^sup>4"
      by auto
    hence "a_max\<^sup>2 = ?rhs / t\<^sup>4" using `0 < t` by auto
    also have "... = t\<^sup>2 * a_max\<^sup>4 / (norm v)\<^sup>2"
      using power_diff[of "t" "4" "6"] by (auto simp add:field_simps divide_simps)
    finally have "a_max\<^sup>2 = t\<^sup>2 * a_max\<^sup>4 / (norm v)\<^sup>2" (is "?lhs2 = ?rhs2")
      by auto
    hence "?lhs2 / a_max\<^sup>2 = ?rhs2 / a_max\<^sup>2"
      by auto
    hence "1 = ?rhs2 / a_max\<^sup>2" using a_max_neq_zero by auto
    also have "... = t\<^sup>2 * a_max\<^sup>2 / (norm v)\<^sup>2" using power_diff[of "a_max" "2" "4"] a_max_neq_zero
      by (auto simp add:field_simps divide_simps)
    finally have "1 = t\<^sup>2 * a_max\<^sup>2 / (norm v)\<^sup>2" by auto
    hence "t\<^sup>2 = (norm v)\<^sup>2 / a_max\<^sup>2" by auto
    with `t < norm v / a_max` show False 
    proof -
      have "t\<^sup>2 = (norm v / a_max)\<^sup>2"
        by (simp add: \<open>t\<^sup>2 = (norm v)\<^sup>2 / a_max\<^sup>2\<close> power_divide)
      then show ?thesis
        using \<open>t < norm v / a_max\<close> assms(1) by fastforce
    qed
  qed
next 
  show "inverse (sqrt (b_y_inner t)) / 2 = inverse (sqrt (b_y_inner t)) / 2"
    by auto
next
  assume assm: "b_y_inner t  < 0"
  have "valid_time t" using assms 
    by (auto intro:valid_timeI)
  have "b_y_inner t \<noteq> 0"
  proof (rule ccontr)
    assume "\<not> b_y_inner t \<noteq> 0"
    hence "b_y_inner t = 0 " by auto
    hence "t\<^sup>4 * a_max\<^sup>2 = t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2" (is "?lhs =?rhs")
      unfolding b_y_inner_def by auto
    hence "?lhs / t\<^sup>4 = ?rhs / t\<^sup>4"
      by auto
    hence "a_max\<^sup>2 = ?rhs / t\<^sup>4" using `0 < t` by auto
    also have "... = t\<^sup>2 * a_max\<^sup>4 / (norm v)\<^sup>2"
      using power_diff[of "t" "4" "6"] by (auto simp add:field_simps divide_simps)
    finally have "a_max\<^sup>2 = t\<^sup>2 * a_max\<^sup>4 / (norm v)\<^sup>2" (is "?lhs2 = ?rhs2")
      by auto
    hence "?lhs2 / a_max\<^sup>2 = ?rhs2 / a_max\<^sup>2"
      by auto
    hence "1 = ?rhs2 / a_max\<^sup>2" using a_max_neq_zero by auto
    also have "... = t\<^sup>2 * a_max\<^sup>2 / (norm v)\<^sup>2" using power_diff[of "a_max" "2" "4"] a_max_neq_zero
      by (auto simp add:field_simps divide_simps)
    finally have "1 = t\<^sup>2 * a_max\<^sup>2 / (norm v)\<^sup>2" by auto
    hence "t\<^sup>2 = (norm v)\<^sup>2 / a_max\<^sup>2" by auto
    with `t < norm v / a_max` show False 
    proof -
      have "t\<^sup>2 = (norm v / a_max)\<^sup>2"
        by (simp add: \<open>t\<^sup>2 = (norm v)\<^sup>2 / a_max\<^sup>2\<close> power_divide)
      then show ?thesis
        using \<open>t < norm v / a_max\<close> assms(1) by fastforce
    qed
  qed
  moreover have "0 \<le> b_y_inner t" using sqrt_term_geq_zero[OF `valid_time t`] assms(1)
    unfolding b_y_inner_def by auto
  ultimately have "0 < b_y_inner t" by auto
  with assm have "False" by auto
  thus " inverse (sqrt (b_y_inner t)) / 2 = - inverse (sqrt (b_y_inner t)) / 2"
    by auto
next
  show "(b_y_inner has_real_derivative b_y_inner' t) (at t within s') "
    by (auto simp add:derivative_intros)
qed

lemma (in occupancy_acceleration) b_y_inner_geq_zero:
  assumes "0 \<le> t" and "t < norm v / a_max"
  shows "0 \<le> b_y_inner t" 
proof -
  have "valid_time t" using valid_timeI assms by auto
  thus ?thesis 
  using sqrt_term_geq_zero[OF `valid_time t`] assms(1)
  unfolding b_y_inner_def by auto
qed

schematic_goal (in occupancy_acceleration) inverse_sqrt_b_y_inner_derivative [derivative_intros]:
  assumes "0 < t" and "t < norm v / a_max"
  shows "((\<lambda>x. inverse (sqrt (b_y_inner x)) / 2) has_field_derivative ?D) (at t within s')"
  apply (intro derivative_eq_intros)
proof -
  show " b_y_inner t \<noteq> 0 "
    using  b_y_inner_neq_zero[OF assms] by auto
next
  assume "0 < b_y_inner t"
  thus "inverse (sqrt (b_y_inner t)) / 2 = inverse (sqrt (b_y_inner t)) / 2"
    by auto
next
  assume "b_y_inner t < 0"
  have "valid_time t" using assms 
    by (auto intro:valid_timeI)
  moreover have "0 \<le> b_y_inner t" using sqrt_term_geq_zero[OF `valid_time t`] assms(1)
    unfolding b_y_inner_def by auto
  with `b_y_inner t < 0` have False by auto
  thus " inverse (sqrt (b_y_inner t)) / 2 = - inverse (sqrt (b_y_inner t)) / 2"
    by auto
next
  show "b_y_inner' t = b_y_inner' t" by auto
next
  show "inverse (sqrt (b_y_inner t)) / 2 * b_y_inner' t = inverse (sqrt (b_y_inner t)) / 2 * b_y_inner' t"
    by auto
next
  have "b_y_inner t \<noteq> 0" using b_y_inner_neq_zero[OF assms] by auto
  thus "sqrt (b_y_inner t) \<noteq> 0"
    using real_sqrt_eq_0_iff[of "b_y_inner t"] by auto
next
  show "- (inverse (sqrt (b_y_inner t)) * (inverse (sqrt (b_y_inner t)) / 2 * b_y_inner' t) * inverse (sqrt (b_y_inner t))) = 
          - (inverse (sqrt (b_y_inner t)))\<^sup>3 / 2 * b_y_inner' t"
    unfolding power3_eq_cube by (auto simp add:field_simps divide_simps)
next
  show "0 = 0" by auto
next
  show "(2::real) \<noteq> 0" by auto
next
  show "(- (inverse (sqrt (b_y_inner t)))\<^sup>3 / 2 * b_y_inner' t * 2 - inverse (sqrt (b_y_inner t)) * 0) / (2 * 2) 
          = 
        (- (inverse (sqrt (b_y_inner t)))\<^sup>3 * b_y_inner' t) / 4 "
    by auto
qed

definition (in occupancy_acceleration)
  "b_y_inner'' t = 3 * t\<^sup>2 * a_max\<^sup>2 - 30 * t\<^sup>4 * a_max\<^sup>4 / (2 * norm v)\<^sup>2"

lemma (in occupancy_acceleration) b_y_inner'_has_derivative[derivative_intros]:
  "(b_y_inner' has_field_derivative b_y_inner'' t) (at t within s')"
  unfolding b_y_inner'_def b_y_inner''_def
  using v\<^sub>x_wd by (auto simp add:field_simps intro!:derivative_intros derivative_eq_intros)

definition (in occupancy_acceleration)
  "b_y' \<sigma> t = \<sigma> * (inverse (sqrt (b_y_inner t)) / 2) * b_y_inner' t"

lemma (in occupancy_acceleration) b_y'_aux:
  "b_y_inner' t = t\<^sup>3 * a_max\<^sup>2 * (1 - 6 * t\<^sup>2 * a_max\<^sup>2 / (2 * norm v)\<^sup>2)"
  unfolding b_y_inner'_def by (auto simp add:field_simps)

lemma (in occupancy_acceleration) b_y'_aux2:
  assumes "0 < t"
  shows "sqrt (b_y_inner t) = t\<^sup>2 * a_max * sqrt (1 - (t  * a_max / norm v)\<^sup>2) / 2" (is "?lhs = ?rhs")
proof -
  have "?rhs = t\<^sup>2 * a_max * sqrt (1 - t\<^sup>2 * a_max\<^sup>2 / (norm v)\<^sup>2) / 2" 
    using assms a_max_neq_zero by (auto simp add:field_simps)
  moreover have "t\<^sup>2 * a_max = sqrt (t\<^sup>4 * a_max\<^sup>2)"
    using real_sqrt_abs[of "t\<^sup>2 * a_max"] assms a_max_neq_zero by (auto simp add:field_simps)
  ultimately have "?rhs = sqrt (t\<^sup>4 * a_max\<^sup>2 * (1 - (t * a_max / norm v)\<^sup>2)) / 2"
    using real_sqrt_mult  by simp
  also have "... = sqrt (t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (norm v)\<^sup>2) / 2" (is "_ = sqrt (?rhs2) / 2")
    by (auto simp add:field_simps)
  also have "... = sqrt (?rhs2 / 4)"
    using v\<^sub>x_wd real_sqrt_divide[of "?rhs2" "2\<^sup>2"] by (auto simp add:divide_simps)
  finally have "?rhs = sqrt (1 / 4 * t\<^sup>4 * a_max\<^sup>2 - t\<^sup>6 * a_max\<^sup>4 / (2 * norm v)\<^sup>2)"
    by (auto)
  thus "?lhs = ?rhs"
    unfolding b_y_inner_def by auto
qed

lemma (in occupancy_acceleration) b_y'_aux3:
  assumes "0 < t"
  shows "b_y' \<sigma> t = \<sigma> * t * a_max * (1 - 3 / 2 * (t * a_max /norm v)\<^sup>2) / sqrt (1 - (t * a_max / norm v)\<^sup>2)" (is "_ = ?rhs")
proof -
  have aux: "(t\<^sup>3 * a_max\<^sup>2) / (t\<^sup>2 * a_max) = t * a_max"
    using assms a_max_neq_zero by (auto simp add:divide_simps power_def)
  have aux2: "6 * t\<^sup>2 * a_max\<^sup>2 / (2 * norm v)\<^sup>2 = 3 / 2 * (t * a_max / norm v)\<^sup>2"
    by (auto simp add:power2_eq_square field_simps)
  have "b_y' \<sigma> t = \<sigma> * (inverse (sqrt (b_y_inner t)) / 2) * b_y_inner' t"
    unfolding b_y'_def by auto
  also have "... = \<sigma> / (t\<^sup>2 * a_max * sqrt (1 - (t  * a_max / norm v)\<^sup>2)) * b_y_inner' t"
    unfolding b_y'_aux2[OF assms] using v\<^sub>x_wd by (auto)
  also have "... = \<sigma> * ( t\<^sup>3 * a_max\<^sup>2 * (1 - 6 * t\<^sup>2 * a_max\<^sup>2 / (2 * norm v)\<^sup>2)) / (t\<^sup>2 * a_max * sqrt (1 - (t  * a_max / norm v)\<^sup>2))"
    unfolding b_y'_aux by auto
  also have "... = \<sigma> * ((t\<^sup>3 * a_max\<^sup>2) / (t\<^sup>2 * a_max)) * (1 - 6 * t\<^sup>2 * a_max\<^sup>2 / (2 * norm v)\<^sup>2) / sqrt (1 - (t * a_max / norm v)\<^sup>2)"
    using assms a_max_neq_zero v\<^sub>x_wd by (auto)
  also have "... = \<sigma> * (t * a_max) * (1 - 6 * t\<^sup>2 * a_max\<^sup>2 / (2 * norm v)\<^sup>2) / sqrt (1 - (t * a_max / norm v)\<^sup>2)"
    unfolding aux by auto
  also have "... = \<sigma> * (t * a_max) * (1 - 3 / 2 * (t * a_max / norm v)\<^sup>2) / sqrt (1 - (t * a_max / norm v)\<^sup>2)"
    unfolding aux2 by auto
  finally show "b_y' \<sigma> t = ?rhs" 
    by auto
qed
   
theorem (in occupancy_acceleration) b_y_has_derivative_b_y'_vx_pos [derivative_intros]:
  assumes "0 < vx" and "0 < t" and "t < norm v / a_max"
  shows "(b_y \<sigma> has_real_derivative b_y' \<sigma> t) (at t within s')"
  using assms(1) derivative_b_y[OF assms(2-3), of "\<sigma>"] unfolding b_y'_def b_y_inner_def
  b_y_inner'_def by auto

schematic_goal (in occupancy_acceleration) b_y'_vx_pos_has_derivative:
  assumes "0 < t" and "t < norm v / a_max"
  shows "(b_y' \<sigma> has_field_derivative ?D) (at t within s')"
  unfolding b_y'_def power3_eq_cube using b_y_inner_neq_zero[OF assms] b_y_inner_geq_zero[OF order.strict_implies_order[OF assms(1)] assms(2)]   
  by (auto simp add: field_simps intro!:derivative_intros derivative_eq_intros)

lemma (in occupancy_acceleration) b_y'_vx_pos_aux:
  assumes "0 \<le> t" and "t < norm v / a_max"
  shows  "((0 * b_y_inner' t + b_y_inner'' t * \<sigma>) * (2 * sqrt (b_y_inner t)) -
   \<sigma> * b_y_inner' t * (0 * sqrt (b_y_inner t) + inverse (sqrt (b_y_inner t)) / 2 * b_y_inner' t * 2)) /
  (2 * sqrt (b_y_inner t) * (2 * sqrt (b_y_inner t))) = 
  (\<sigma> * b_y_inner'' t * (2 * sqrt (b_y_inner t)) - \<sigma> * b_y_inner' t * (inverse (sqrt (b_y_inner t)) * b_y_inner' t)) /
                                              (4 * b_y_inner t)"
  using b_y_inner_geq_zero[OF assms]
  by (auto)

lemma (in occupancy_acceleration) b_y'_vx_neg_aux:
  assumes "0 \<le> t" and "t < norm v / a_max"
  shows  "- (((0 * b_y_inner' t + b_y_inner'' t * \<sigma>) * (2 * sqrt (b_y_inner t)) - \<sigma> * b_y_inner' t * (0 * sqrt (b_y_inner t) + inverse (sqrt (b_y_inner t)) / 2 * b_y_inner' t * 2)) /
     (2 * sqrt (b_y_inner t) * (2 * sqrt (b_y_inner t)))) = 
          (b_y_inner'' t * (-\<sigma>) * (2 * sqrt (b_y_inner t)) - (-\<sigma>) * b_y_inner' t * (inverse (sqrt (b_y_inner t)) * b_y_inner' t)) /
          (4 * b_y_inner t)" (is "?lhs = ?num / ?den")
proof -
  have "?lhs = - ((b_y_inner'' t * \<sigma> * (2 * sqrt (b_y_inner t)) - \<sigma> * b_y_inner' t * (inverse (sqrt (b_y_inner t)) / 2 * b_y_inner' t * 2)) /
     (4 * b_y_inner t))" (is "_ = - (?num1 / ?den1)")
    using b_y_inner_geq_zero[OF assms] by (auto)
  also have "... = -?num1 / ?den1" 
    using minus_divide_left[of "?num1" "?den1"] by auto
  finally have "?lhs = -?num1 / ?den1" by auto
  moreover have "-?num1 = ?num"
    by auto
  finally have "?lhs = ?num / ?den1" by auto
  thus ?thesis by auto
qed
  
theorem (in occupancy_acceleration) b_y'_vx_pos_has_derivative2 [derivative_intros]: 
  assumes "0 < t" and "t < norm v / a_max"
  shows "(b_y' \<sigma> has_field_derivative (\<sigma> * b_y_inner'' t * (2 * sqrt (b_y_inner t)) - \<sigma> * b_y_inner' t * (inverse (sqrt (b_y_inner t)) * b_y_inner' t)) /
                                              (4 * b_y_inner t)) (at t within s')"  
  using b_y'_vx_pos_has_derivative[OF assms, of "\<sigma>"] unfolding b_y'_vx_pos_aux[OF order.strict_implies_order[OF assms(1)] assms(2)]
  by auto

definition (in occupancy_acceleration) 
  "b_y'' \<sigma> t = (\<sigma> * b_y_inner'' t * (2 * sqrt (b_y_inner t)) - \<sigma> * b_y_inner' t * (inverse (sqrt (b_y_inner t)) * b_y_inner' t)) /
                                              (4 * b_y_inner t)"

theorem (in occupancy_acceleration)
  "b_y'' \<sigma> t = (\<sigma> * b_y_inner'' t * (2 * sqrt (b_y_inner t)) - 2 * b_y_inner' t * b_y' \<sigma> t) / (4 * b_y_inner t)"
  unfolding b_y''_def b_y'_def by (auto simp add:field_simps)

theorem (in occupancy_acceleration) b_y'_vx_pos_has_derivative3 [derivative_intros]: 
  assumes "0 < t" and "t < norm v / a_max"
  shows "(b_y' \<sigma> has_field_derivative b_y'' \<sigma> t) (at t within s')"
  using b_y'_vx_pos_has_derivative2[OF assms] unfolding b_y''_def by auto

theorem (in occupancy_acceleration) b_y_has_derivative_b_y' [derivative_intros]:
  assumes "vx \<le> 0" and "0 < t" and "t < norm v / a_max"
  shows "(b_y \<sigma> has_real_derivative b_y' (-\<sigma>) t) (at t within s')"
  using assms(1) derivative_b_y[OF assms(2-3), of "\<sigma>"] unfolding b_y'_def b_y_inner_def
  b_y_inner'_def by auto

definition (in occupancy_acceleration) 
  "b_x' t = norm v - 3 * t\<^sup>2 * a_max\<^sup>2 / (2 * norm v)"

theorem (in occupancy_acceleration) b_x_has_derivative_b_x' [derivative_intros]:
  "(b_x has_vector_derivative b_x' t) (at t within s')"
  unfolding b_x'_def using derivative_b_x by auto

theorem (in occupancy_acceleration) b_x_has_field_derivative_b_x' [derivative_intros]:
  "(b_x has_field_derivative b_x' t) (at t within s')"
  unfolding has_field_derivative_iff_has_vector_derivative 
  using  b_x_has_derivative_b_x' by auto

theorem (in occupancy_acceleration) b_x_has_field_derivative_b_x'2 [derivative_intros]:
  "(b_x has_field_derivative b_x' t) (at t)"
  unfolding has_field_derivative_iff_has_vector_derivative 
  using  b_x_has_derivative_b_x' by auto

theorem (in occupancy_acceleration) b_x'_has_derivative [derivative_intros]:
  "(b_x' has_field_derivative - 3 * a_max\<^sup>2 / (norm v) * t) (at t within s')"
  unfolding b_x'_def using v\<^sub>x_wd by (auto intro!:derivative_intros derivative_eq_intros)

lemma (in occupancy_acceleration) root_of_b_x':
  "b_x' (sqrt (2 / 3) * (norm v) / a_max) = 0"
  unfolding b_x'_def power2_eq_square using a_max_neq_zero by (auto) 

lemma (in occupancy_acceleration) positive_derivative_b_x:
  defines "t_max \<equiv> sqrt (2 / 3) * norm v / a_max"
  shows "\<forall>t \<in> {0..<t_max}. 0 < b_x' t"
proof 
  fix t
  assume "t \<in> {0..<t_max}"
  hence "t = 0 \<or> t \<in> {0 <..< t_max}" by auto
  moreover
  { assume "t = 0"
    hence "0 < b_x' t" unfolding b_x'_def using v\<^sub>x_wd by auto }
  moreover
  { assume "t \<in> {0 <..< t_max}"    
    hence "t < sqrt (2 / 3) * norm v / a_max" (is "_ < ?rhs")  and "0 < t" unfolding t_max_def by auto
    hence "t\<^sup>2 < ?rhs\<^sup>2" using mult_less_le_imp_less[OF `t < ?rhs` _ _ `0 < t`, of "?rhs"] 
      unfolding power2_eq_square  by auto
    hence "3 / 2 * a_max\<^sup>2 * t\<^sup>2 < (norm v)\<^sup>2"  using a_max_neq_zero by (auto simp add:field_simps)
    moreover have "norm v \<noteq> 0" using v\<^sub>x_wd norm_Pair surjective_pairing[of "v"] by auto
    ultimately have "3 / 2 * t\<^sup>2 * a_max\<^sup>2 / norm v < norm v" unfolding power2_eq_square[of "norm v"]
      using norm_ge_zero[of "v"] by (auto simp add:field_simps)
    hence "0 < b_x' t" unfolding b_x'_def by (auto) }
  ultimately show "0 < b_x' t" by auto
qed

lemma (in occupancy_acceleration) nonneg_derivative_b_x:
  defines "t_max \<equiv> sqrt (2 / 3) * norm v / a_max"
  shows "\<forall>t \<in> {0..t_max}. 0 \<le> b_x' t"
proof 
  fix t
  assume "t \<in> {0..t_max}"
  hence "t = 0 \<or> t \<in> {0 <.. t_max}" by auto
  moreover
  { assume "t = 0"
    hence "0 < b_x' t" unfolding b_x'_def using v\<^sub>x_wd by auto }
  moreover
  { assume "t \<in> {0 <..t_max}"    
    hence "t \<le> sqrt (2 / 3) * norm v / a_max" (is "_ \<le> ?rhs")  and "0 < t" unfolding t_max_def by auto
    hence "t\<^sup>2 \<le> ?rhs\<^sup>2"  using mult_mono[OF `t \<le> ?rhs` `t \<le> ?rhs`] using norm_ge_zero 
      unfolding power2_eq_square  by auto
    hence "3 / 2 * a_max\<^sup>2 * t\<^sup>2 \<le> (norm v)\<^sup>2"  using a_max_neq_zero by (auto simp add:field_simps)
    moreover have "norm v \<noteq> 0" using v\<^sub>x_wd norm_Pair surjective_pairing[of "v"] by auto
    ultimately have "3 / 2 * t\<^sup>2 * a_max\<^sup>2 / norm v \<le> norm v" unfolding power2_eq_square[of "norm v"]
      using norm_ge_zero[of "v"] by (auto simp add:field_simps)
    hence "0 \<le> b_x' t" unfolding b_x'_def by (auto) }
  ultimately show "0 \<le> b_x' t" by auto
qed

theorem (in occupancy_acceleration) increasing_lt_t_max:
  defines "t_max \<equiv> sqrt (2 / 3) * norm v / a_max"
  shows  "\<forall>t t'. t \<in> {0..< t_max} \<and> t' \<in> {0..< t_max} \<and> t < t' \<longrightarrow> b_x t < b_x t'"
proof (rule allI, rule allI, rule)
  fix t t' 
  assume " t \<in> {0..<t_max} \<and> t' \<in> {0..<t_max} \<and> t < t'"
  hence 0: "\<forall>x \<in> {t .. t'}. 0 < b_x' x" and "t < t'" 
    using positive_derivative_b_x unfolding t_max_def by auto
  show "b_x t < b_x t'"   
    apply (rule DERIV_pos_imp_increasing[OF `t < t'`, of "b_x"])
    apply (rule allI, rule impI)
  proof -
    fix x
    assume "t \<le> x \<and> x \<le> t'"
    hence "(b_x has_real_derivative b_x' x) (at x) \<and> 0 < b_x' x"
      using b_x_has_field_derivative_b_x'2[of "x"] 0  by auto
    thus "\<exists>y. (b_x has_real_derivative y) (at x) \<and> 0 < y"
      by auto
  qed
qed

theorem (in occupancy_acceleration) increasing_lt_t_max2:
  fixes t t'
  defines "t_max \<equiv> sqrt (2 / 3) * norm v / a_max"
  assumes "t \<in> {0 .. t_max}" and "t' \<in> {0 .. t_max}"
  assumes "t < t'"
  shows "b_x t < b_x t'"
proof (cases "t' = t_max")
  case True
  hence "t \<in> {0 ..< t_max}" using assms by auto
  hence 0: "\<forall>x \<in> {t <..< t'}. 0 < b_x' x"  
    using positive_derivative_b_x True unfolding t_max_def by auto
  show "b_x t < b_x t'" 
    apply (rule DERIV_pos_imp_increasing_open[OF `t < t'`, of "b_x"])
  proof -
    fix x
    assume "t < x" and "x < t'"
    hence "(b_x has_real_derivative b_x' x) (at x) \<and> 0 < b_x' x"
      using b_x_has_field_derivative_b_x'2[of "x"] 0 by auto
    thus "\<exists>y. (b_x has_real_derivative y) (at x) \<and> 0 < y"
      by auto
  next
    fix x
    assume "t \<le> x" and "x \<le> t'"
    have "continuous_on {0 .. t_max} b_x"
      using DERIV_continuous_on[where s="{0 .. t_max}" and f="b_x" and D="b_x'"] 
      b_x_has_field_derivative_b_x' by auto
    thus "isCont b_x x" using `t \<le> x` `x \<le> t'` assms(2-3) 
      using DERIV_isCont b_x_has_field_derivative_b_x'2 by blast
  qed
next
  case False
  hence "t' \<in> {0 ..< t_max}" and "t \<in> {0 ..< t_max}" using assms by auto 
  then show ?thesis 
    using increasing_lt_t_max unfolding t_max_def using `t < t'` by auto
qed
  
theorem (in occupancy_acceleration) nondecreasing_lt_t_max:
  defines "t_max \<equiv> sqrt (2 / 3) * norm v / a_max"
  shows  "\<forall>t t'. t \<in> {0.. t_max} \<and> t' \<in> {0..t_max} \<and> t \<le> t' \<longrightarrow> b_x t \<le> b_x t'"
proof (rule allI, rule allI, rule)
  fix t t' 
  assume " t \<in> {0..t_max} \<and> t' \<in> {0..t_max} \<and> t \<le> t'"
  hence 0: "\<forall>x \<in> {t .. t'}. 0 \<le> b_x' x" and "t \<le> t'" 
    using nonneg_derivative_b_x unfolding t_max_def by auto
  show "b_x t \<le> b_x t'" 
    apply (rule DERIV_nonneg_imp_nondecreasing[OF `t \<le> t'`])
    apply (rule allI, rule impI)
  proof -
    fix x
    assume "t \<le> x \<and> x \<le> t'"
    hence "(b_x has_real_derivative b_x' x) (at x) \<and> 0 \<le> b_x' x"
      using b_x_has_field_derivative_b_x'2[of "x"] 0  by auto
    thus "\<exists>y. (b_x has_real_derivative y) (at x) \<and> 0 \<le> y"
      by auto
  qed
qed

theorem (in occupancy_acceleration) nondecreasing_lt_t_max2:
  defines "t_max \<equiv> sqrt (2 / 3) * norm v / a_max"
  shows  "\<And>t t'. t \<in> {0.. t_max} \<Longrightarrow> t' \<in> {0..t_max} \<Longrightarrow> t \<le> t' \<Longrightarrow> b_x t \<le> b_x t'"
  using assms nondecreasing_lt_t_max by auto

lemma (in occupancy_acceleration) norm_gt_zero:
  "0 < norm v"
  using norm_ge_zero[of "v"] v\<^sub>x_wd by auto

definition (in occupancy_acceleration) "t_max = sqrt (2 / 3) * norm v / a_max"

theorem (in occupancy_acceleration) t_max_lt_t_stop:
  "t_max < norm v / a_max"
proof -
  have "sqrt (2 / 3) < 1" by (approximation 10)
  moreover have "0 \<noteq> norm v" 
    using norm_Pair[of "fst v" "snd v"] surjective_pairing[of "v"] v\<^sub>x_wd by auto  
  ultimately have "sqrt (2 /3) * (norm v / a_max) < 1 * (norm v / a_max)"
    using mult_strict_right_mono[OF `sqrt (2 / 3) < 1`, of "norm v / a_max"]
    norm_ge_zero[of "v"] a_max_neq_zero by auto
  thus ?thesis unfolding t_max_def by auto
qed

theorem (in occupancy_acceleration) t_max_is_valid:
  "valid_time t_max"
  unfolding valid_time_def t_max_def using norm_Pair[of "fst v" "snd v"]
  unfolding surjective_pairing[of "v", THEN sym] by (auto simp add:field_simps divide_simps)

theorem (in occupancy_acceleration)
  "t \<in> {0..t_max} \<Longrightarrow> valid_time t"
  using t_max_is_valid mono_valid_time by auto

lemma (in occupancy_acceleration) t_max_gt_zero:
  "0 < t_max"
  unfolding t_max_def using norm_gt_zero a_max_neq_zero by auto

lemma (in occupancy_acceleration) b_x_strict_mono:
  "strict_mono_on b_x {0..t_max}"
proof (rule strict_mono_onI)
  fix r s
  assume "r \<in> {0..t_max}"
  assume "s \<in> {0..t_max}"
  assume "r < s"
  hence "r < t_max" using `r \<in> {0..t_max}` `s \<in> {0..t_max}`
    by (auto simp add:field_simps)
  hence "r\<^sup>2 < t_max\<^sup>2" using power2_mono_lt using `r \<in> {0..t_max}`
    by auto
  have "s = t_max \<or> s \<noteq> t_max" by auto
  moreover
  { assume "s \<noteq> t_max" 
    hence "s < t_max" and "r < t_max" using `s \<in> {0 .. t_max}` `r < s` by auto
    hence "s \<in> {0..<t_max}" and "r \<in> {0..<t_max}" using `s \<in> {0..t_max}` `r \<in> {0..t_max}`
      by auto
    hence "b_x r < b_x s" using increasing_lt_t_max `r < s` unfolding t_max_def
      by auto }
  moreover
  { assume "s = t_max"
    hence "r < t_max" using `r < s` by auto
    hence "b_x r \<le> b_x s" using nondecreasing_lt_t_max2[of "r" "s"]
      `r \<in> {0..t_max}` `s \<in> {0..t_max}` unfolding t_max_def using `r < s` by auto
    moreover have "b_x r \<noteq> b_x s"
    proof (rule ccontr)
      assume "\<not> b_x r \<noteq> b_x s"
      hence "b_x r = b_x s" by auto
      hence "norm v * r - r\<^sup>3 * a_max\<^sup>2 / (2 * norm v) = norm v * s - s\<^sup>3 * a_max\<^sup>2 / (2 * norm v)"
        unfolding b_x_def by (auto)
      hence "(r - s) * norm v = (r\<^sup>3 - s\<^sup>3) * a_max\<^sup>2 / (2 * norm v)"
        by (auto simp add:field_simps diff_divide_distrib)
      moreover have "r\<^sup>3 - s\<^sup>3 = (r - s) * (r\<^sup>2 + r * s + s\<^sup>2)"
        by (auto simp add: field_simps power2_eq_square power3_eq_cube)
      ultimately have "(r - s) * norm v = (r - s) * ((r\<^sup>2 + r * s + s\<^sup>2) * a_max\<^sup>2 / (2 * norm v))"
        by auto
      hence "norm v = (r\<^sup>2 + r * s + s\<^sup>2) * a_max\<^sup>2 / (2 * norm v)"
        using mult_left_cancel[of "r - s" "norm v" "(r\<^sup>2 + r * s + s\<^sup>2) * a_max\<^sup>2 / (2 * norm v)"] `r < s`
        by (auto)
      hence "r\<^sup>2 + r * s + s\<^sup>2 = 2 * (norm v)\<^sup>2 / a_max\<^sup>2"
        using norm_gt_zero by (auto simp add:field_simps divide_simps power_def)
      hence "r\<^sup>2 + r * s + 2 / 3 * (norm v)\<^sup>2 / a_max\<^sup>2 = 6 / 3 * (norm v)\<^sup>2 / a_max\<^sup>2"
        using `s = t_max` unfolding t_max_def by (auto simp add:field_simps)
      hence "r * s = 4 / 3 * (norm v)\<^sup>2 / a_max\<^sup>2 - r\<^sup>2"
        by (auto)
      hence "r * s \<ge> 2 / 3 * (norm v)\<^sup>2 / a_max\<^sup>2"
        using `r\<^sup>2 < t_max\<^sup>2` unfolding t_max_def by  (auto simp add:field_simps)
      hence "r * s \<ge> s\<^sup>2"
        unfolding `s = t_max` t_max_def by (auto simp add:field_simps)
      hence "r \<ge> s" 
        unfolding power2_eq_square using mult_right_cancel[of "s"] 
        t_max_gt_zero unfolding `s = t_max`[THEN sym] by auto
      with `r < s` show "False" by auto
    qed
    ultimately have "b_x r < b_x s" by auto }
  ultimately show "b_x r < b_x s" by auto
qed

lemma (in occupancy_acceleration) b_x_inj_on:
  "inj_on b_x {0..t_max}"
  using strict_mono_on_imp_inj_on[OF b_x_strict_mono] by auto

lemma (in occupancy_acceleration) open_subset_closed: 
  "{0 <..< t_max} \<subseteq> {0 .. t_max}"
  using t_max_gt_zero by auto

lemma (in occupancy_acceleration) half_open_subset_closed: 
  "{0 ..< t_max} \<subseteq> {0 .. t_max}"
  using t_max_gt_zero by auto

lemma (in occupancy_acceleration) b_x_inj_on_open:
  "inj_on b_x {0 <..< t_max}"
  using inj_on_subset[OF b_x_inj_on open_subset_closed] by auto 

lemma (in occupancy_acceleration) b_x_inj_on_half_open:
  "inj_on b_x {0 ..< t_max}"
  using inj_on_subset[OF b_x_inj_on half_open_subset_closed] by auto 
 
lemma (in occupancy_acceleration) negative_derivative_b_x:
  "\<forall>t \<in> {t_max <..} . b_x' t < 0"
proof 
  fix t
  assume "t \<in> {t_max <..}"
  hence "t > sqrt (2 / 3) * norm v / a_max" (is "t > ?rhs")  unfolding t_max_def by auto
  hence "t\<^sup>2 > ?rhs\<^sup>2" using mult_less_le_imp_less[OF `t > ?rhs` _ _ t_max_gt_zero, of "t"]
    t_max_gt_zero unfolding t_max_def power2_eq_square by auto
  hence "3 / 2 * a_max\<^sup>2 * t\<^sup>2 > (norm v)\<^sup>2"  using a_max_neq_zero by (auto simp add:field_simps)
  hence "3 / 2 * t\<^sup>2 * a_max\<^sup>2 / norm v > norm v" unfolding power2_eq_square[of "norm v"]
    using norm_ge_zero[of "v"] norm_gt_zero by (auto simp add:field_simps)
  thus "0 > b_x' t" unfolding b_x'_def by (auto) 
qed

theorem (in occupancy_acceleration) decreasing_gt_t_max:
  "\<forall>t t'. t \<in> {t_max <..} \<and> t' \<in> {t_max <..} \<and> t < t' \<longrightarrow> b_x t > b_x t'"
proof (rule, rule, rule)
  fix t t'
  assume "t \<in> {t_max<..} \<and> t' \<in> {t_max<..} \<and> t < t'"
  hence  "t \<in> {t_max<..}" "t' \<in> {t_max<..}"  "t < t'" by auto
  hence 0: "\<forall>x \<in> {t .. t'}. 0 > b_x' x" 
    using negative_derivative_b_x unfolding t_max_def by auto
  show "b_x t > b_x t'" 
  proof (rule DERIV_neg_imp_decreasing[OF `t < t'`, of "b_x"], rule, rule)
    fix x 
    assume "t \<le> x \<and> x \<le> t'"
    hence "(b_x has_real_derivative b_x' x) (at x) \<and> 0 > b_x' x"
      using b_x_has_field_derivative_b_x'2[of "x"] 0  by auto    
    thus "\<exists>y. (b_x has_real_derivative y) (at x) \<and> y < 0"  
      by auto
  qed
qed

section "Defining function f-of-x for the boundary"

definition (in occupancy_acceleration) f_of_x :: "real \<Rightarrow> real \<Rightarrow> real" where
  "f_of_x \<sigma> = b_y \<sigma> o (the_inv_into {0 .. t_max} b_x)"

lemma (in occupancy_acceleration) f_of_x_fixed_point:
  "f_of_x \<sigma> 0 = 0"  
proof -
  have "b_x 0 = 0" unfolding b_x_def by auto
  hence "the_inv_into {0 .. t_max} b_x 0 = the_inv_into {0 .. t_max} b_x (b_x 0)"
    by auto
  also have "... = 0"
    using the_inv_into_f_f[OF b_x_inj_on, of "0"] t_max_gt_zero by auto
  finally have 0: "the_inv_into {0 .. t_max} b_x 0 = 0" by auto
  have "b_y \<sigma> 0 = 0" unfolding b_y_def by auto
  thus "f_of_x \<sigma> 0 = 0" unfolding f_of_x_def using 0 by auto
qed
    
lemma (in occupancy_acceleration) b_x_range_closed:
  assumes "lb \<in> {0 .. t_max}" and "ub \<in> {0 .. t_max}"
  shows "b_x ` {lb .. ub} = {b_x lb .. b_x ub}"   
proof (rule  Set.equalityI, unfold image_subset_iff,  rule ballI)
  fix x 
  assume "x \<in> {lb .. ub}"
  hence "lb \<le> x" and "x \<le> ub" by auto
  hence "x \<in> {0 .. t_max}" using assms by auto
  hence "b_x lb \<le> b_x x" and "b_x x \<le> b_x ub" 
    using  nondecreasing_lt_t_max  assms `lb \<le> x` `x \<le> ub` unfolding t_max_def[THEN sym]
    by auto
  thus "b_x x \<in> {b_x lb .. b_x ub}" by auto
next 
  have "lb \<le> ub \<or> ub < lb" by linarith
  moreover
  { assume "lb \<le> ub"  
    { fix y
      assume y_inbetw: "y \<in> {b_x lb .. b_x ub}"
        have y_lb: "b_x lb \<le> y" and y_ub: "y \<le> b_x ub" 
          using y_inbetw by auto
        have "continuous_on {0 .. t_max} b_x"
          using DERIV_continuous_on[where s="{0 .. t_max}" and f="b_x" and D="b_x'"] 
          b_x_has_field_derivative_b_x' by auto
        hence "\<forall>x. lb \<le> x \<and> x \<le> ub \<longrightarrow> isCont b_x x" using assms 
          using DERIV_isCont b_x_has_field_derivative_b_x'2 by blast
        then obtain x where "lb \<le> x" and "x \<le> ub" and "b_x x = y"
          using IVT[where f="b_x" and a="lb" and y="y" and b="ub"] `lb \<le> ub` y_lb y_ub 
          by auto
        hence "y \<in> b_x ` {lb .. ub}" 
          by(auto intro: image_eqI[where x="x"]) }
      hence "{b_x lb..b_x ub} \<subseteq> b_x ` {lb..ub} " 
        by auto }
  moreover
  { assume "ub < lb"
      hence "b_x ub < b_x lb"
        using increasing_lt_t_max2 assms unfolding t_max_def[THEN sym] `ub < lb` by auto
      hence "{b_x lb .. b_x ub} = {}" by auto
      hence " {b_x lb..b_x ub} \<subseteq> b_x ` {lb..ub}" using `ub < lb` by auto }
    ultimately show "{b_x lb..b_x ub} \<subseteq> b_x ` {lb..ub}" by auto
  qed

lemma (in occupancy_acceleration) b_x_range_open:
  assumes "lb \<in> {0 .. t_max}" and "ub \<in> {0 .. t_max}"
  shows "b_x ` {lb <..< ub} = {b_x lb <..< b_x ub}"   
proof (rule  Set.equalityI, unfold image_subset_iff, rule ballI)
  fix x 
  assume "x \<in> {lb <..< ub}"
  hence "lb < x" and "x < ub" by auto
  hence "x \<in> {0 .. t_max}" using assms by auto
  hence "b_x lb < b_x x" and "b_x x < b_x ub" 
    using  increasing_lt_t_max2  assms `lb < x` `x < ub` unfolding t_max_def[THEN sym]
    by auto
  thus "b_x x \<in> {b_x lb <..< b_x ub}" by auto
next 
  have "lb \<le> ub \<or> ub < lb" by linarith
  moreover
  { assume "lb \<le> ub"  
    { fix y
      assume y_inbetw: "y \<in> {b_x lb <..< b_x ub}"
        have y_lb: "b_x lb < y" and y_ub: "y < b_x ub" 
          using y_inbetw by auto
        have "continuous_on {0 .. t_max} b_x"
          using DERIV_continuous_on[where s="{0 .. t_max}" and f="b_x" and D="b_x'"] 
          b_x_has_field_derivative_b_x' by auto
        hence "\<forall>x. lb \<le> x \<and> x \<le> ub \<longrightarrow> isCont b_x x" using assms 
          using DERIV_isCont b_x_has_field_derivative_b_x'2 by blast
        then obtain x where "lb \<le> x" and "x \<le> ub" and "b_x x = y"
          using IVT[where f="b_x" and a="lb" and y="y" and b="ub"] `lb \<le> ub` y_lb y_ub 
          by auto
        moreover have "x \<noteq> lb" and "x \<noteq> ub"
          using `b_x x = y` `y < b_x ub` `b_x lb < y` by auto
        ultimately have  "y \<in> b_x ` {lb <..< ub}" 
          by(auto intro: image_eqI[where x="x"]) }
      hence "{b_x lb <..< b_x ub} \<subseteq> b_x ` {lb <..< ub} " 
        by auto }
  moreover
  { assume "ub < lb"
      hence "b_x ub < b_x lb"
        using increasing_lt_t_max2 assms unfolding t_max_def[THEN sym] `ub < lb` by auto
      hence "{b_x lb <..< b_x ub} = {}" by auto
      hence " {b_x lb <..< b_x ub} \<subseteq> b_x ` {lb <..< ub}" using `ub < lb` by auto }
    ultimately show "{b_x lb <..< b_x ub} \<subseteq> b_x ` {lb <..< ub}" by auto
qed

schematic_goal (in occupancy_acceleration) derivative_b_x_inverse:
  assumes "x \<in> {b_x 0 <..< b_x t_max}"
  shows "((the_inv_into {0 .. t_max} b_x) has_real_derivative ?x) (at x)"
proof (rule DERIV_inverse_function[where f="b_x" and D="(b_x' o (the_inv_into {0.. t_max} b_x)) x"
                                     and a="b_x 0" and b="b_x t_max"])
  show " (b_x has_real_derivative (b_x' \<circ> the_inv_into {0.. t_max} b_x) x) (at (the_inv_into {0.. t_max} b_x x))"
    using b_x_has_field_derivative_b_x'[where s'="UNIV" and t="(the_inv_into {0.. t_max} b_x x)"]
    by auto
next
  show "(b_x' \<circ> the_inv_into {0.. t_max} b_x) x \<noteq> 0"
  proof -
    have 1: "x \<in> b_x ` {0 .. t_max}" using assms b_x_range_closed[of "0" "t_max"] 
      t_max_gt_zero by auto
    hence "the_inv_into {0.. t_max} b_x x \<in> {0 .. t_max}"
      using 1 the_inv_into_into[OF b_x_inj_on, of "x" "{0 .. t_max}"]  by auto
    moreover have "the_inv_into {0 .. t_max} b_x x \<noteq> t_max"
    proof (rule ccontr)
      assume "\<not> the_inv_into {0 .. t_max} b_x x \<noteq> t_max"
      hence "the_inv_into {0 .. t_max} b_x x = t_max" by auto
      hence "x = b_x t_max" 
        unfolding the_inv_into_def  using "1" \<open>the_inv_into {0..t_max} b_x x = t_max\<close> b_x_inj_on 
        f_the_inv_into_f by fastforce      
      thus False using assms by auto
    qed
    ultimately have "the_inv_into {0 .. t_max} b_x x \<in> {0 ..< t_max}"
      by auto
    thus ?thesis using positive_derivative_b_x unfolding t_max_def[THEN sym] by force 
  qed
next
  show "b_x 0 < x" and "x < b_x t_max" using assms by auto
next
  have 0: "b_x ` {0..t_max} = {b_x 0 .. b_x t_max}" using b_x_range_closed t_max_gt_zero
    by auto
  show "\<forall>y. b_x 0 < y \<and> y < b_x t_max \<longrightarrow> b_x (the_inv_into {0.. t_max} b_x y) = y"
    using f_the_inv_into_f[OF b_x_inj_on] unfolding 0 b_x_range_closed by auto
next
  have "continuous_on (b_x ` {0 .. t_max}) (the_inv_into {0 .. t_max} b_x)"
  proof (rule continuous_on_inv_into)
    show "continuous_on {0 .. t_max} b_x"
      using DERIV_continuous_on[where s="{0 .. t_max}" and f="b_x" and D="b_x'"] 
      b_x_has_field_derivative_b_x' by auto
  next
    show "compact {0 .. t_max}" by auto
  next
    show "inj_on b_x {0..t_max}" using b_x_inj_on by auto
  qed
  hence "continuous_on ({b_x 0 <..< b_x t_max}) (the_inv_into {0 .. t_max} b_x)"
    using continuous_on_subset[where f="the_inv_into {0..t_max} b_x" and t="{b_x 0 <..< b_x t_max}" 
    and s="{b_x 0 .. b_x t_max}"]  using b_x_range_closed t_max_gt_zero by force
  thus "isCont (the_inv_into {0.. t_max} b_x) x" 
    using continuous_on_eq_continuous_at[of "{b_x 0 <..< b_x t_max}" "the_inv_into {0..t_max} b_x"]
    assms by auto
qed

lemma (in occupancy_acceleration) b_x_inverse_time_range:
  assumes "x \<in> {b_x 0 <..< b_x t_max}"
  shows "the_inv_into {0 .. t_max} b_x x \<in> {0 <..< t_max}"
proof -
  have "the_inv_into {0 .. t_max} b_x x \<in> {0 .. t_max}"
    using the_inv_into_into[OF b_x_inj_on, of "x" "{0 .. t_max}"] b_x_range_closed[of "0" "t_max"]
    t_max_gt_zero assms by auto
  moreover have "the_inv_into {0..t_max} b_x x \<noteq> 0"
  proof (rule ccontr)
    assume "\<not> the_inv_into {0 .. t_max} b_x x \<noteq> 0"
    hence "the_inv_into {0 .. t_max} b_x x = 0" by auto
    hence "b_x (the_inv_into {0 .. t_max} b_x x) = b_x 0" by auto
    hence "x = b_x 0"
      using f_the_inv_into_f[OF b_x_inj_on, of "x"] assms b_x_range_closed[of "0" "t_max"] 
      t_max_gt_zero by auto
    with assms show False by auto
  qed
  moreover have "the_inv_into {0..t_max} b_x x \<noteq> t_max"
  proof (rule ccontr)
    assume "\<not> the_inv_into {0 .. t_max} b_x x \<noteq> t_max"
    hence "the_inv_into {0 .. t_max} b_x x = t_max" by auto
    hence "b_x (the_inv_into {0 .. t_max} b_x x) = b_x t_max" by auto
    hence "x = b_x t_max"
      using f_the_inv_into_f[OF b_x_inj_on, of "x"] assms b_x_range_closed[of "0" "t_max"] 
      t_max_gt_zero by auto
    with assms show False by auto
  qed
  ultimately show ?thesis by auto
qed
 
lemma (in occupancy_acceleration) b_x_inv_derivative_positive:
  assumes "x \<in> {b_x 0<..<b_x t_max} "
  shows "0 < inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x)"
proof -
  have "the_inv_into {0 .. t_max} b_x x \<in> {0 <..< t_max}"
    using b_x_inverse_time_range[OF assms] by auto
  hence "0 < b_x' (the_inv_into {0 .. t_max} b_x x)"
    using positive_derivative_b_x unfolding t_max_def by auto
  thus ?thesis by auto
qed

lemma (in occupancy_acceleration) b_x_inv_increasing:
  assumes "x1 \<in> {b_x 0 <..< b_x t_max}"
  assumes "x2 \<in> {b_x 0 <..< b_x t_max}"
  assumes "x1 \<le> x2"
  shows "the_inv_into {0 .. t_max} b_x x1 \<le> the_inv_into {0 .. t_max} b_x x2"
proof -
  have 0: "\<And>x. x1 \<le> x \<and> x \<le> x2 \<Longrightarrow> 
    (the_inv_into {0..t_max} b_x has_real_derivative inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x)) (at x) 
    \<and> (0 < inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x))"
    using assms(1-2) derivative_b_x_inverse b_x_inv_derivative_positive by auto
  show ?thesis 
    apply (intro DERIV_nonneg_imp_nondecreasing[OF `x1 \<le> x2`, of "the_inv_into {0 .. t_max} b_x"])
  proof (rule, rule)
    fix x 
    assume "x1 \<le> x \<and> x \<le> x2"
    hence "(the_inv_into {0..t_max} b_x has_real_derivative inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x)) (at x) 
        \<and> (0 < inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x))"
      using 0 by auto
    thus "\<exists>y. (the_inv_into {0..t_max} b_x has_real_derivative y) (at x) \<and> 0 \<le> y"
      by (auto intro: exI[where x="inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x)"])
  qed
qed

schematic_goal (in occupancy_acceleration) derivative_f_of_x: 
  assumes "x \<in> {b_x 0 <..< b_x t_max}"
  shows "(f_of_x \<sigma> has_field_derivative ?Da * ?Db) (at x)"
  unfolding f_of_x_def 
proof (rule DERIV_chain)
  have "the_inv_into {0 .. t_max} b_x x \<in> {0 .. t_max}"
    using the_inv_into_into[OF b_x_inj_on, of "x" "{0 .. t_max}"] assms  
    b_x_range_closed[of "0" "t_max"]  using t_max_gt_zero by auto
  moreover have "the_inv_into {0 .. t_max} b_x x \<noteq> 0"
  proof (rule ccontr)
    assume "\<not> the_inv_into {0 .. t_max} b_x x \<noteq> 0"
    hence 0: "the_inv_into {0 .. t_max} b_x x = 0" by auto
    moreover have "x \<in> b_x ` {0 .. t_max}" 
      using assms b_x_range_closed[of "0" "t_max"] greaterThanLessThan_subseteq_atLeastAtMost_iff
      [of "b_x 0" "b_x t_max" "b_x 0" "b_x t_max"] t_max_gt_zero by (auto)
    ultimately have "x = b_x 0" 
      using f_the_inv_into_f[OF b_x_inj_on, of "x"]  unfolding 0 by auto
    with assms show False by auto
  qed
  moreover have "the_inv_into {0 .. t_max} b_x x \<noteq> t_max"
  proof (rule ccontr)
    assume "\<not>the_inv_into {0 .. t_max} b_x x \<noteq> t_max"
    hence 0: "the_inv_into {0 .. t_max} b_x x = t_max"
      by auto
    moreover have "x \<in> b_x ` {0 .. t_max}" 
      using assms b_x_range_closed[of "0" "t_max"] greaterThanLessThan_subseteq_atLeastAtMost_iff
      [of "b_x 0" "b_x t_max" "b_x 0" "b_x t_max"] t_max_gt_zero by (auto)
    ultimately have "x = b_x t_max"
      using f_the_inv_into_f[OF b_x_inj_on, of "x"] unfolding 0 by auto
    with assms show False by auto
  qed
  ultimately have 0: "the_inv_into {0 .. t_max} b_x x \<in> {0 <..< t_max}"
    by auto
  define t where "t = the_inv_into {0 ..t_max} b_x x"
  have "0 < vx \<or> vx \<le> 0" by linarith
  moreover
  { assume "0 < vx"
    have "(b_y \<sigma> has_real_derivative b_y' \<sigma> t) (at t)"
      using b_y_has_derivative_b_y'_vx_pos[OF `0 < vx`, of "t" "\<sigma>" "UNIV"] 0 unfolding t_def
      using t_max_lt_t_stop by auto }
  moreover
  { assume "vx \<le> 0"
    have "(b_y \<sigma> has_real_derivative b_y' (-\<sigma>) t) (at t)"
      using b_y_has_derivative_b_y'[OF `vx \<le> 0`, of "t" "\<sigma>" "UNIV"] 0 unfolding t_def
      using t_max_lt_t_stop by auto }
  ultimately show "(b_y \<sigma> has_real_derivative (if 0 < vx then b_y' \<sigma> t else b_y' (-\<sigma>) t))
                   (at t)"
    by auto
next
  show "(the_inv_into {0..t_max} b_x has_real_derivative inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x)) (at x)"
    using derivative_b_x_inverse[OF assms] by auto
qed

definition (in occupancy_acceleration)
  "f_of_x' \<sigma> x =  
    (if 0 < vx then b_y' \<sigma> (the_inv_into {0..t_max} b_x x) else b_y' (- \<sigma>) (the_inv_into {0..t_max} b_x x)) 
        *
     inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x)"

lemma (in occupancy_acceleration) f_of_x_vx_pos:
  assumes "0 < vx"
  shows "f_of_x' \<sigma> x = b_y' \<sigma> (the_inv_into {0 .. t_max} b_x x) * inverse ((b_x' o the_inv_into {0 .. t_max} b_x) x)"
  using assms unfolding f_of_x'_def by auto

lemma (in occupancy_acceleration) f_of_x'_1_geq_zero:
  assumes "0 \<le> vx" and "x \<in> {b_x 0 <..< b_x t_max}"
  shows "0 \<le> f_of_x' 1 x"
proof -
  define tx where "tx = the_inv_into {0 .. t_max} b_x x"
  hence "0 < tx" and "tx < t_max"
    using b_x_inverse_time_range[OF assms(2)] by auto
  have "0 < vx"
    using assms v\<^sub>x_wd unfolding vx_def by auto
  have "f_of_x' 1 x = b_y' 1 (the_inv_into {0 .. t_max} b_x x) / b_x' (the_inv_into {0 .. t_max} b_x x)"
    unfolding f_of_x_vx_pos[OF `0 < vx`] by (auto simp add:divide_simps)
  moreover have "0 \<le> b_y' 1 (the_inv_into {0 .. t_max} b_x x)" 
  proof -
    have "b_y' 1 (the_inv_into {0 .. t_max} b_x x) = b_y_inner' tx / (2 * sqrt (b_y_inner tx))"
      unfolding tx_def b_y'_def by (auto simp add:field_simps)
    moreover have "0 \<le> b_y_inner' tx" 
      using b_y_inner'_gt_zero[OF `0 < tx`] `tx < t_max` unfolding t_max_def by auto
    moreover have "0 < sqrt (b_y_inner tx)" 
      using b_y_inner_geq_zero[OF order.strict_implies_order[OF `0 < tx`]] b_y_inner_neq_zero[OF `0 < tx`] `tx < t_max`
      t_max_lt_t_stop unfolding  real_sqrt_gt_0_iff  by (auto simp add:field_simps)      
    ultimately show ?thesis 
      by auto
  qed
  moreover have "0 < b_x' (the_inv_into {0 .. t_max} b_x x)" 
    using positive_derivative_b_x `tx < t_max` unfolding t_max_def tx_def 
    using \<open>0 < tx\<close> t_max_def tx_def by force
  ultimately show ?thesis 
    by (auto)
qed

lemma (in occupancy_acceleration) f_of_x'_neg_1_at_most_zero:
  assumes "0 \<le> vx" and "x \<in> {b_x 0 <..< b_x t_max}"
  shows "f_of_x' (-1) x \<le> 0"
proof -
  define tx where "tx = the_inv_into {0 .. t_max} b_x x"
  hence "0 < tx" and "tx < t_max"
    using b_x_inverse_time_range[OF assms(2)] by auto
  have "0 < vx"
    using assms v\<^sub>x_wd unfolding vx_def by auto
  have *: "f_of_x' (-1) x = b_y' (-1) (the_inv_into {0 .. t_max} b_x x) / b_x' (the_inv_into {0 .. t_max} b_x x)"
    unfolding f_of_x_vx_pos[OF `0 < vx`] by (auto simp add:divide_simps)
  have "b_y' (-1) (the_inv_into {0 .. t_max} b_x x) \<le> 0"
  proof -
    have "b_y' (-1) (the_inv_into {0 .. t_max} b_x x) = - b_y_inner' tx / (2 * sqrt (b_y_inner tx))"
      unfolding tx_def b_y'_def by (auto simp add:field_simps)
    moreover have "0 \<le> b_y_inner' tx" 
      using b_y_inner'_gt_zero[OF `0 < tx`] `tx < t_max` unfolding t_max_def by auto
    moreover have "0 < sqrt (b_y_inner tx)" 
      using b_y_inner_geq_zero[OF order.strict_implies_order[OF `0 < tx`]] b_y_inner_neq_zero[OF `0 < tx`] `tx < t_max`
      t_max_lt_t_stop unfolding  real_sqrt_gt_0_iff  by (auto simp add:field_simps)      
    ultimately show ?thesis 
      by auto
  qed
  moreover have "0 < b_x' (the_inv_into {0 .. t_max} b_x x)" 
    using positive_derivative_b_x `tx < t_max` unfolding t_max_def tx_def 
    using \<open>0 < tx\<close> t_max_def tx_def by force
  ultimately show ?thesis 
    unfolding * divide_le_0_iff by auto
qed


lemma (in occupancy_acceleration) f_of_x_non_decreasing:
  assumes "a \<in> {b_x 0 <..< b_x t_max}"
  assumes "b \<in> {b_x 0 <..< b_x t_max}"
  assumes "a \<le> b"
  assumes "0 < vx"
  shows "f_of_x 1 a \<le> f_of_x 1 b"
proof (rule DERIV_nonneg_imp_nondecreasing[where f="f_of_x 1"], rule assms, rule allI, rule impI)
  fix x
  assume "a \<le> x \<and> x \<le> b"
  hence "x \<in> {b_x 0 <..< b_x t_max}"
    using assms by (auto)
  have "(f_of_x 1 has_real_derivative f_of_x' 1 x) (at x)"
    using derivative_f_of_x[OF `x \<in> {b_x 0 <..< b_x t_max}`, of "1"] `0 < vx` 
    unfolding f_of_x'_def by (auto)
  moreover have "0 \<le> f_of_x' 1 x"
    using f_of_x'_1_geq_zero[OF order.strict_implies_order[OF `0 < vx`] `x \<in> {b_x 0 <..< b_x t_max}`]
    by auto
  ultimately show "\<exists>y. (f_of_x 1 has_real_derivative y) (at x) \<and> 0 \<le> y"
    by auto
qed

lemma (in occupancy_acceleration) f_of_x_non_increasing:
  assumes "a \<in> {b_x 0 <..< b_x t_max}"
  assumes "b \<in> {b_x 0 <..< b_x t_max}"
  assumes "a \<le> b"
  assumes "0 < vx"
  shows "f_of_x (-1) a \<ge> f_of_x (-1) b"
proof (rule DERIV_nonpos_imp_nonincreasing, rule assms, rule, rule)
  fix x
  assume "a \<le> x \<and> x \<le> b"
  hence "x \<in> {b_x 0 <..< b_x t_max}"
    using assms by auto
  have "(f_of_x (-1) has_real_derivative f_of_x' (-1) x) (at x)"
    using derivative_f_of_x[OF `x \<in> {b_x 0 <..< b_x t_max}`, of "-1"] `0 < vx` 
    unfolding f_of_x'_def by auto
  moreover have "f_of_x' (-1) x \<le> 0"
    using f_of_x'_neg_1_at_most_zero[OF order.strict_implies_order[OF `0 < vx`] `x \<in> {b_x 0 <..< b_x t_max}`]
    by auto
  ultimately show "\<exists>y. (f_of_x (- 1) has_real_derivative y) (at x) \<and> y \<le> 0"
    by auto
qed

lemma (in occupancy_acceleration) b_y'_over_b_x':
  assumes "0 < t" and "t < t_max"
  shows "b_y' \<sigma> t / b_x' t = \<sigma> * a_max / norm v * t / (sqrt (1 - (t * a_max / norm v)\<^sup>2))"
proof -
  have 0: "b_x' t = norm v * (1 - 3 / 2 * (t * a_max / norm v)\<^sup>2)"
    unfolding b_x'_def by (auto simp add:field_simps divide_simps power2_eq_square)
  have "b_y' \<sigma> t / b_x' t = \<sigma> * t * a_max * (1 - 3 / 2 * (t * a_max / norm v)\<^sup>2) / sqrt (1 - (t * a_max / norm v)\<^sup>2) 
                            / 
                            (norm v *  (1 - 3 / 2 * (t * a_max / norm v)\<^sup>2))"
    unfolding b_y'_aux3[OF assms(1)] 0 by auto
  moreover have "(1 - 3 / 2 * (t * a_max / norm v)\<^sup>2) \<noteq> 0"
  proof (rule ccontr)
    assume "\<not>(1 - 3 / 2 * (t * a_max / norm v)\<^sup>2) \<noteq> 0"
    hence "(1 - 3 / 2 * (t * a_max / norm v)\<^sup>2) = 0" by auto
    hence "3 / 2 * (t * a_max / norm v)\<^sup>2 = 1" by auto
    hence "(t * a_max / norm v)\<^sup>2 = 2 / 3" (is "?lhs = ?rhs")by auto  
    hence "sqrt ?lhs = sqrt ?rhs" unfolding real_sqrt_eq_iff by auto
    hence "t * a_max / norm v = sqrt (2 / 3)" using assms a_max_neq_zero norm_ge_zero v\<^sub>x_wd
      real_sqrt_abs[of "t * a_max / norm v"] by (auto)
    hence "t = sqrt (2 / 3) * norm v / a_max" using v\<^sub>x_wd by (auto simp add:field_simps divide_simps)
    with assms(2) show False unfolding t_max_def by auto
  qed
  ultimately have "b_y' \<sigma> t / b_x' t =  \<sigma> * t * a_max / sqrt (1 - (t * a_max / norm v)\<^sup>2) / norm v"
    by auto
  thus ?thesis by (auto simp add:field_simps)
qed

lemma (in occupancy_acceleration) f_of_x'_non_decreasing:
  assumes "0 < t1" and "t1 < t_max"
  assumes "0 < t2" and "t2 < t_max"
  assumes "t1 \<le> t2"
  shows "b_y' 1 t1 / b_x' t1 \<le> b_y' 1 t2 / b_x' t2"
proof -
  have "sqrt (1 - (t2 * a_max / norm v)\<^sup>2) \<le> sqrt (1 - (t1 * a_max / norm v)\<^sup>2)" (is "?L \<le> ?R")
  proof -
    have "t1 * a_max / norm v \<le> t2 * a_max / norm v" (is "?lhs \<le> ?rhs")
      using a_max_neq_zero norm_ge_zero v\<^sub>x_wd assms by (auto simp add:field_simps divide_simps)
    hence "?lhs\<^sup>2 \<le> ?rhs\<^sup>2" using power2_mono_leq[OF `?lhs \<le> ?rhs`]
      using assms(1) a_max_neq_zero norm_ge_zero v\<^sub>x_wd by auto
    hence "1 -?rhs\<^sup>2 \<le> 1 -?lhs\<^sup>2" by auto
    thus ?thesis using real_sqrt_le_iff[of "1 - ?rhs\<^sup>2" "1 - ?lhs\<^sup>2"]
      by auto
  qed
  moreover have aux0: "1 - (t1 * a_max / norm v)\<^sup>2 > 0"
  proof (rule ccontr)
    assume "\<not> (1 - (t1 * a_max / norm v)\<^sup>2 > 0)"
    hence "1 - (t1 * a_max / norm v)\<^sup>2 \<le> 0" by auto
    hence "(t1 * a_max / norm v)\<^sup>2 \<ge> 1" by auto
    hence "sqrt ((t1 * a_max / norm v)\<^sup>2) \<ge> sqrt 1"
      unfolding real_sqrt_le_iff by auto
    hence "t1 * a_max / norm v \<ge> 1"
      using `0 < t1` a_max_neq_zero norm_ge_zero v\<^sub>x_wd by auto
    hence "t1 \<ge> norm v / a_max" using norm_ge_zero v\<^sub>x_wd a_max_neq_zero       
      using le_divide_eq_1_pos norm_gt_zero pos_divide_le_eq by blast
    hence "t1 > t_max" unfolding t_max_def using t_max_lt_t_stop 
      using assms(2) by linarith
    thus False using assms(2) by auto
  qed
  moreover have aux1: "1 - (t2 * a_max / norm v)\<^sup>2 > 0"
  proof (rule ccontr)
    assume "\<not> (1 - (t2 * a_max / norm v)\<^sup>2 > 0)"
    hence "1 - (t2 * a_max / norm v)\<^sup>2 \<le> 0" by auto
    hence "(t2 * a_max / norm v)\<^sup>2 \<ge> 1" by auto
    hence "sqrt ((t2 * a_max / norm v)\<^sup>2) \<ge> sqrt 1"
      unfolding real_sqrt_le_iff by auto
    hence "t2 * a_max / norm v \<ge> 1"
      using `0 < t2` a_max_neq_zero norm_ge_zero v\<^sub>x_wd by auto
    hence "t2 \<ge> norm v / a_max" using norm_ge_zero v\<^sub>x_wd a_max_neq_zero       
      using le_divide_eq_1_pos norm_gt_zero pos_divide_le_eq by blast
    hence "t2 > t_max" unfolding t_max_def using t_max_lt_t_stop 
      using assms(4) by linarith
    thus False using assms(4) by auto
  qed
  moreover  have "0 \<le> ?L * ?R" and "0 < ?L" and "0 < ?R"
    using real_sqrt_ge_0_iff aux0 aux1 by auto
  have "t1  / ?R \<le> t2 / ?L" 
    using frac_le[OF _ `t1 \<le> t2` `0 < ?L` `?L \<le> ?R`] assms(1) by auto
  thus ?thesis 
    unfolding b_y'_over_b_x'[OF assms(1-2)] b_y'_over_b_x'[OF assms(3-4)]
    using mult_left_mono[OF `t1 / ?R \<le> t2 / ?L`, where c="a_max / norm v"]
    a_max_neq_zero norm_ge_zero v\<^sub>x_wd by auto
qed

theorem (in occupancy_acceleration) f_of_x_has_derivative_f_of_x' [derivative_intros]: 
  assumes "x \<in> {b_x 0 <..< b_x t_max}"
  shows "(f_of_x \<sigma> has_field_derivative f_of_x' \<sigma> x) (at x)"  
  using derivative_f_of_x[OF assms, of "\<sigma>"] unfolding f_of_x'_def by auto

theorem (in occupancy_acceleration) f_of_x'_non_decreasing2:
  assumes "x1 \<in> {b_x 0 <..< b_x t_max}"
  assumes "x2 \<in> {b_x 0 <..< b_x t_max}"
  assumes "x1 \<le> x2"
  assumes "0 < vx"
  shows "f_of_x' 1 x1 \<le> f_of_x' 1 x2"
proof -
  have *: "f_of_x' 1 x1 =  b_y' 1 (the_inv_into {0..t_max} b_x x1) / ((b_x' \<circ> the_inv_into {0..t_max} b_x) x1)"
    and
       **: "f_of_x' 1 x2 =  b_y' 1 (the_inv_into {0..t_max} b_x x2) / ((b_x' \<circ> the_inv_into {0..t_max} b_x) x2)"         
    using f_of_x_vx_pos[OF `0 < vx`, of "1"] by (auto simp add:divide_simps)
  have "the_inv_into {0 .. t_max} b_x x1 \<in> {0 .. t_max}"
    using the_inv_into_into[OF b_x_inj_on, of "x1" "{0 .. t_max}"] assms(1) b_x_range_closed[of "0" "t_max"]  
    using t_max_gt_zero by (auto)
  moreover have "the_inv_into {0 .. t_max} b_x x1 \<noteq> 0"
  proof (rule ccontr)
    assume "\<not> the_inv_into {0 .. t_max} b_x x1 \<noteq> 0"
    hence "the_inv_into {0 .. t_max} b_x x1 = 0" by auto
    hence "b_x (the_inv_into {0 .. t_max} b_x x1) = b_x 0"
      by auto
    hence "x1 = b_x 0"
      using f_the_inv_into_f[OF b_x_inj_on, of "x1"] using assms(1) b_x_range_closed[of "0" "t_max"]
      using t_max_gt_zero by auto
    with assms(1) show False by auto
  qed
  moreover have "the_inv_into {0 .. t_max} b_x x1 \<noteq> t_max"
  proof (rule ccontr)
    assume "\<not> the_inv_into {0 .. t_max} b_x x1 \<noteq> t_max"
    hence "the_inv_into {0 .. t_max} b_x x1 = t_max" by auto
    hence "b_x (the_inv_into {0 .. t_max} b_x x1) = b_x t_max" by auto
    hence "x1 = b_x t_max"
      using f_the_inv_into_f[OF b_x_inj_on, of "x1"] using assms(1) b_x_range_closed[of "0" "t_max"]
      using t_max_gt_zero by auto
    with assms(1) show "False" by auto
  qed
  ultimately have 0: "the_inv_into {0 .. t_max} b_x x1 \<in> {0 <..< t_max}"
    by auto
  have "the_inv_into {0 .. t_max} b_x x2 \<in> {0 .. t_max}"
    using the_inv_into_into[OF b_x_inj_on, of "x2" "{0 .. t_max}"] assms(2) b_x_range_closed[of "0" "t_max"]  
    using t_max_gt_zero by (auto)
  moreover have "the_inv_into {0 .. t_max} b_x x2 \<noteq> 0"
  proof (rule ccontr)
    assume "\<not> the_inv_into {0 .. t_max} b_x x2 \<noteq> 0"
    hence "the_inv_into {0 .. t_max} b_x x2 = 0" by auto
    hence "b_x (the_inv_into {0 .. t_max} b_x x2) = b_x 0"
      by auto
    hence "x2 = b_x 0"
      using f_the_inv_into_f[OF b_x_inj_on, of "x2"] using assms(2) b_x_range_closed[of "0" "t_max"]
      using t_max_gt_zero by auto
    with assms(2) show False by auto
  qed
  moreover have "the_inv_into {0 .. t_max} b_x x2 \<noteq> t_max"
  proof (rule ccontr)
    assume "\<not> the_inv_into {0 .. t_max} b_x x2 \<noteq> t_max"
    hence "the_inv_into {0 .. t_max} b_x x2 = t_max" by auto
    hence "b_x (the_inv_into {0 .. t_max} b_x x2) = b_x t_max" by auto
    hence "x2 = b_x t_max"
      using f_the_inv_into_f[OF b_x_inj_on, of "x2"] using assms(2) b_x_range_closed[of "0" "t_max"]
      using t_max_gt_zero by auto
    with assms(2) show "False" by auto
  qed
  ultimately have 1: "the_inv_into {0 .. t_max} b_x x2 \<in> {0 <..< t_max}"
    by auto
  have b_x1_leq_b_x2: "the_inv_into {0..t_max} b_x x1 \<le> (the_inv_into {0..t_max} b_x x2)"
    using b_x_inv_increasing[OF assms(1-2) `x1 \<le> x2`] by auto  
  hence "b_y' 1 (the_inv_into {0 .. t_max} b_x x1) / (b_x' (the_inv_into {0 .. t_max} b_x x1)) \<le> 
         b_y' 1 (the_inv_into {0 .. t_max} b_x x2) / (b_x' (the_inv_into {0 .. t_max} b_x x2))"
    using f_of_x'_non_decreasing[OF _ _ _ _ b_x1_leq_b_x2] 0 1 by auto
  thus ?thesis unfolding * ** by auto
qed

schematic_goal (in occupancy_acceleration) b_x'_comp_b_x_inv_has_derivative [derivative_intros]:
  assumes "x \<in> {b_x 0 <..< b_x t_max}"
  shows "(b_x' \<circ> the_inv_into {0..t_max} b_x has_field_derivative ?Da * ?Db) (at x within s')"
  unfolding comp_def
proof (rule DERIV_chain2[where g="the_inv_into {0..t_max} b_x"])
  show "(b_x' has_real_derivative -3 * a_max\<^sup>2 / (norm v) * (the_inv_into {0..t_max} b_x x)) 
    (at (the_inv_into {0..t_max} b_x x))"
    using b_x'_has_derivative[where s'="UNIV"] by auto
next
  show "(the_inv_into {0..t_max} b_x has_real_derivative 
                                inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x)) (at x within s') "
    using derivative_b_x_inverse[OF assms]  has_field_derivative_at_within by blast
qed

schematic_goal (in occupancy_acceleration) inverse_b_x'_comp_b_x_inv_has_derivative':
  assumes "x \<in> {b_x 0 <..< b_x t_max}"
  shows "((\<lambda>x. inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x)) has_field_derivative ?D) (at x within s')"
proof (rule DERIV_inverse_fun)  
  show "(b_x' o the_inv_into {0..t_max} b_x has_real_derivative 
        - 3 * a_max\<^sup>2 / norm v * the_inv_into {0..t_max} b_x x * inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x))
        (at x within s')"
    using b_x'_comp_b_x_inv_has_derivative[OF assms] by auto
next
  show "(b_x' \<circ> the_inv_into {0.. t_max} b_x) x \<noteq> 0"
  proof -
    have 1: "x \<in> b_x ` {0 .. t_max}" using assms b_x_range_closed[of "0" "t_max"] 
      t_max_gt_zero by auto
    hence "the_inv_into {0.. t_max} b_x x \<in> {0 .. t_max}"
      using 1 the_inv_into_into[OF b_x_inj_on, of "x" "{0 .. t_max}"]  by auto
    moreover have "the_inv_into {0 .. t_max} b_x x \<noteq> t_max"
    proof (rule ccontr)
      assume "\<not> the_inv_into {0 .. t_max} b_x x \<noteq> t_max"
      hence "the_inv_into {0 .. t_max} b_x x = t_max" by auto
      hence "x = b_x t_max" 
        unfolding the_inv_into_def  using "1" \<open>the_inv_into {0..t_max} b_x x = t_max\<close> b_x_inj_on 
        f_the_inv_into_f by fastforce      
      thus False using assms by auto
    qed
    ultimately have "the_inv_into {0 .. t_max} b_x x \<in> {0 ..< t_max}"
      by auto
    thus ?thesis using positive_derivative_b_x unfolding t_max_def[THEN sym] by force 
  qed
qed
  
theorem (in occupancy_acceleration) inverse_b_x'_comp_b_x_inv_has_derivative [derivative_intros]:
  assumes "x \<in> {b_x 0 <..< b_x t_max}"
  shows "((\<lambda>x. inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x)) has_field_derivative 
        3 * a_max\<^sup>2 / norm v * the_inv_into {0..t_max} b_x x * inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)\<^sup>3)) (at x within s')"
  using inverse_b_x'_comp_b_x_inv_has_derivative'[OF assms]
  by (auto simp add:field_simps power_def)

schematic_goal (in occupancy_acceleration) b_y'_comp_b_x_inv_has_derivative [derivative_intros]:
  assumes "x \<in> {b_x 0 <..< b_x t_max}"
  shows "((\<lambda>x. b_y' \<sigma> (the_inv_into {0 .. t_max} b_x x)) has_field_derivative ?Da * ?Db) (at x within s')"
proof (rule DERIV_chain2[where f="\<lambda>x. b_y' \<sigma> x"])
  have 0: "x \<in> b_x ` {0 <..< t_max}"
    using assms(1) b_x_range_open[of "0" "t_max"] t_max_gt_zero by auto
  hence "the_inv_into {0 .. t_max} b_x x \<in> {0 .. t_max}"
    using the_inv_into_into[OF b_x_inj_on, of "x" "{0 .. t_max}"] using b_x_range_open[of "0" "t_max"] 
    b_x_range_closed[of "0" "t_max"] t_max_gt_zero by auto
  moreover have "the_inv_into {0 .. t_max} b_x x \<noteq> 0"
    using 0  using b_x_inj_on the_inv_into_f_eq by fastforce
  moreover have "the_inv_into {0 .. t_max} b_x x \<noteq> t_max"
    using 0 using b_x_inj_on the_inv_into_f_eq by fastforce
  ultimately have "the_inv_into {0 .. t_max} b_x x \<in> {0 <..< t_max}"
    by auto
  hence lb: "0 < the_inv_into {0 .. t_max} b_x x" and ub: "the_inv_into {0 .. t_max} b_x x < norm v / a_max"
    using t_max_lt_t_stop by auto
  show "(b_y' \<sigma> has_real_derivative b_y'' \<sigma> (the_inv_into {0..t_max} b_x x)) (at (the_inv_into {0..t_max} b_x x))"
    using b_y'_vx_pos_has_derivative3[OF lb ub, of "\<sigma>" "UNIV"] by auto
next
  show "(the_inv_into {0..t_max} b_x has_real_derivative 
                                inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x)) (at x within s') "
    using derivative_b_x_inverse[OF assms]  has_field_derivative_at_within by blast
qed

schematic_goal (in occupancy_acceleration) second_derivative_f_of_x_pos [derivative_intros]:
  assumes "x \<in> {b_x 0 <..< b_x t_max}"
  defines "f \<equiv> (\<lambda>\<sigma> x. b_y' \<sigma> (the_inv_into {0..t_max} b_x x))"
  defines "g \<equiv> (\<lambda>x. inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x))"
  shows "((\<lambda>x. f \<sigma> x * g x) has_field_derivative ?D) (at x within s')"
proof (intro derivative_eq_intros)
  show "(f \<sigma> has_real_derivative  b_y'' \<sigma> (the_inv_into {0..t_max} b_x x) * inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x)) (at x within s')"
    using b_y'_comp_b_x_inv_has_derivative[OF assms(1), of "\<sigma>" "UNIV"] unfolding f_def
    using has_field_derivative_at_within by auto
next
  show "(g has_real_derivative  3 * a_max\<^sup>2 / norm v * the_inv_into {0..t_max} b_x x * inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)\<^sup>3)) (at x within s')"  
    using inverse_b_x'_comp_b_x_inv_has_derivative[OF assms(1)] unfolding g_def by auto
next
  have " b_y'' \<sigma> (the_inv_into {0..t_max} b_x x) * inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x) * g x +
    3 * a_max\<^sup>2 / norm v * the_inv_into {0..t_max} b_x x * inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)\<^sup>3) * f \<sigma> x =
     b_y'' \<sigma> (the_inv_into {0..t_max} b_x x) * (inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x))\<^sup>2 +
    3 * a_max\<^sup>2 / norm v * the_inv_into {0..t_max} b_x x * inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)\<^sup>3) * 
     b_y' \<sigma> (the_inv_into {0..t_max} b_x x)" (is "?lhs = _")
    unfolding g_def power2_eq_square f_def by auto
  also have "... = inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)\<^sup>2) * 
                    ( 
                      b_y'' \<sigma> (the_inv_into {0..t_max} b_x x)  + 
                      b_y' \<sigma> (the_inv_into {0..t_max} b_x x) * inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)) * 3 * a_max\<^sup>2 / norm v * the_inv_into {0..t_max} b_x x
                    )" (is "_ = ?rhs")
    by (auto simp add:field_simps power2_eq_square power3_eq_cube divide_simps)
  finally show "?lhs = ?rhs" by auto
qed

schematic_goal (in occupancy_acceleration) second_derivative_f_of_x_neg [derivative_intros]:
  assumes "x \<in> {b_x 0 <..< b_x t_max}"
  defines "f \<equiv> (\<lambda>\<sigma> x. b_y' \<sigma> (the_inv_into {0..t_max} b_x x))"
  defines "g \<equiv> (\<lambda>x. inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x))"
  shows "((\<lambda>x. f (-\<sigma>) x * g x) has_field_derivative ?D) (at x within s')"
proof (intro derivative_eq_intros)
  show "(f (-\<sigma>) has_real_derivative  b_y'' (-\<sigma>) (the_inv_into {0..t_max} b_x x) * inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x)) (at x within s')"
    using b_y'_comp_b_x_inv_has_derivative[OF assms(1), of "-\<sigma>" "UNIV"] unfolding f_def
    using has_field_derivative_at_within by auto
next
  show "(g has_real_derivative  3 * a_max\<^sup>2 / norm v * the_inv_into {0..t_max} b_x x * inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)\<^sup>3)) (at x within s')"  
    using inverse_b_x'_comp_b_x_inv_has_derivative[OF assms(1)] unfolding g_def by auto
next
  have " b_y'' (-\<sigma>) (the_inv_into {0..t_max} b_x x) * inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x) * g x +
    3 * a_max\<^sup>2 / norm v * the_inv_into {0..t_max} b_x x * inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)\<^sup>3) * f (-\<sigma>) x =
     b_y'' (-\<sigma>) (the_inv_into {0..t_max} b_x x) * (inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x))\<^sup>2 +
    3 * a_max\<^sup>2 / norm v * the_inv_into {0..t_max} b_x x * inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)\<^sup>3) * 
     b_y' (-\<sigma>) (the_inv_into {0..t_max} b_x x)" (is "?lhs = _")
    unfolding g_def power2_eq_square f_def by auto  
  also have "... = inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)\<^sup>2) * 
                    ( 
                      b_y'' (-\<sigma>) (the_inv_into {0..t_max} b_x x)  + 
                      b_y' (-\<sigma>) (the_inv_into {0..t_max} b_x x) * inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)) * 3 * a_max\<^sup>2 / norm v * the_inv_into {0..t_max} b_x x
                    )" (is "_ = ?rhs")
    by (auto simp add:field_simps power2_eq_square power3_eq_cube divide_simps)
  finally show "?lhs = ?rhs" by auto
qed

definition (in occupancy_acceleration)
  "f_of_x'' \<sigma> x =
 (if 0 < vx then                   
  inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)\<^sup>2) *
        (b_y'' \<sigma> (the_inv_into {0..t_max} b_x x) 
            +
         b_y' \<sigma> (the_inv_into {0..t_max} b_x x) * inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x) * 3 * a_max\<^sup>2 / norm v * the_inv_into {0..t_max} b_x x)
  else 
  inverse (((b_x' \<circ> the_inv_into {0..t_max} b_x) x)\<^sup>2) *
        (b_y'' (-\<sigma>) (the_inv_into {0..t_max} b_x x) 
            +
         b_y' (-\<sigma>) (the_inv_into {0..t_max} b_x x) * inverse ((b_x' \<circ> the_inv_into {0..t_max} b_x) x) * 3 * a_max\<^sup>2 / norm v * the_inv_into {0..t_max} b_x x))"

theorem (in occupancy_acceleration) f_of_x'_has_derivative_f_of_x'' [derivative_intros]:
  assumes "x \<in> {b_x 0 <..< b_x t_max}"              
  shows "(f_of_x' \<sigma> has_field_derivative f_of_x'' \<sigma> x) (at x)"
  using second_derivative_f_of_x_neg[OF assms, of "\<sigma>"] second_derivative_f_of_x_pos[OF assms, of "\<sigma>"]
  unfolding f_of_x'_def f_of_x''_def by (auto split:if_splits)

subsection "Secant line of f_of_x"

definition (in occupancy_acceleration) secant_slope where
  "secant_slope \<sigma> x0 h = (f_of_x \<sigma> (x0 + h) - f_of_x \<sigma> x0) / h"

schematic_goal (in occupancy_acceleration) secant_slope_has_derivative:
  assumes "0 < vx" 
  assumes "0 < h"
  assumes "x0 + h \<in> {b_x 0 <..< b_x t_max}"
  shows "(secant_slope \<sigma> x0 has_field_derivative ?D) (at h)"
proof -
  have 0: "((\<lambda>h. - f_of_x \<sigma> x0 / h) has_field_derivative f_of_x \<sigma> x0 / h\<^sup>2) (at h)"
    using `0 < h` by (auto simp add:power2_eq_square intro!:derivative_intros derivative_eq_intros)
  moreover have 1: "((\<lambda>h. f_of_x \<sigma> (x0 + h) / h) has_field_derivative (f_of_x' \<sigma> (x0 + h) * h - f_of_x \<sigma> (x0 + h)) / h\<^sup>2) (at h)"
  proof (intro derivative_intros derivative_eq_intros)
    have "(op + x0 has_real_derivative 1) (at h)" 
      by (auto intro:derivative_eq_intros)
    thus "((\<lambda>x. f_of_x \<sigma> (x0 + x)) has_real_derivative f_of_x' \<sigma> (x0 + h)) (at h)"
      using f_of_x_has_derivative_f_of_x'[OF assms(3), of "\<sigma>"]
      DERIV_chain2[of "f_of_x \<sigma>" "f_of_x' \<sigma> (x0 + h)" "op + x0" "h" "1" "UNIV"]
      by (auto intro:derivative_eq_intros derivative_intros)
  next
    show "1 = 1" by auto
  next
    show "h \<noteq> 0" using assms(2) by auto
  next
    show "(f_of_x' \<sigma> (x0 + h) * h - f_of_x \<sigma> (x0 + h) * 1) / (h * h) = (f_of_x' \<sigma> (x0 + h) * h - f_of_x \<sigma> (x0 + h)) / h\<^sup>2"
      unfolding power2_eq_square by auto
  qed
  show "(secant_slope \<sigma> x0 has_field_derivative (f_of_x' \<sigma> (x0 + h) * h - f_of_x \<sigma> (x0 + h)) / h\<^sup>2 +  f_of_x \<sigma> x0 / h\<^sup>2) (at h)"  
    using DERIV_add[OF 1 0]
    unfolding secant_slope_def diff_divide_distrib add_divide_distrib diff_conv_add_uminus 
    by auto
qed

definition (in occupancy_acceleration) 
  "secant_slope' x0 h = (f_of_x' 1 (x0 + h) * h - f_of_x 1 (x0 + h)) / h\<^sup>2 + f_of_x 1 x0 / h\<^sup>2"

lemma (in occupancy_acceleration) secant_slope_has_derivative_secant_slope' [derivative_intros]:
  assumes "0 < vx" 
  assumes "0 < h"
  assumes "x0 + h \<in> {b_x 0 <..< b_x t_max}"
  shows "(secant_slope 1 x0 has_field_derivative secant_slope' x0 h) (at h)"  
  using secant_slope_has_derivative[OF assms] unfolding secant_slope'_def by auto

lemma (in occupancy_acceleration) derivative_secant_slope_nonneg:
  assumes "x0 \<in> {b_x 0 <..< b_x t_max}"
  assumes "x0 + h \<in> {b_x 0 <..< b_x t_max}"
  assumes "0 < h" and "0 < vx"
  shows "0 \<le> secant_slope' x0 h"
proof -
  have "x0 < x0 + h" using `0 < h` by auto
  have 0: "\<forall>x. x0 \<le> x \<and> x \<le> x0 + h \<longrightarrow> (f_of_x 1 has_real_derivative f_of_x' 1 x) (at x)"
    using f_of_x_has_derivative_f_of_x' assms(1-2) by auto
  obtain z where "x0 < z" and "z < x0 + h" and 1: "f_of_x 1 (x0 + h) - f_of_x 1 x0 = h * f_of_x' 1 z"
    using MVT2[OF `x0 < x0 + h` 0] by auto
  hence "z \<in> {b_x 0 <..< b_x t_max}" using assms(1-2) by auto
  have "f_of_x' 1 z \<le> f_of_x' 1 (x0 + h)"
    using f_of_x'_non_decreasing2[OF `z \<in> {b_x 0 <..< b_x t_max}` assms(2) _ `0 < vx`] `z < x0 + h`
    by auto
  hence "(f_of_x 1 (x0 + h) - f_of_x 1 x0) / h \<le> f_of_x' 1 (x0 + h)"
    using 1 `0 < h` by (auto simp add:field_simps)
  thus ?thesis unfolding secant_slope'_def using `0 < h` by (auto simp add:field_simps)
qed

lemma (in occupancy_acceleration) secant_slope_nondecreasing_aux:
  assumes "x0 \<in> {b_x 0 <..< b_x t_max}"
  assumes "x0 + h1 \<in> {b_x 0 <..< b_x t_max}"
  assumes "x0 + h2 \<in> {b_x 0 <..< b_x t_max}"
  assumes "0 < h1" and "0 < h2" and "h1 \<le> h2" and "0 < vx"
  shows "secant_slope 1 x0 h1 \<le> secant_slope 1 x0 h2"
proof -
  have aux: "\<forall>x. h1 \<le> x \<and> x \<le> h2 \<longrightarrow> (\<exists>y. (secant_slope 1 x0 has_real_derivative y) (at x) \<and> 0 \<le> y)"
  proof  (rule allI, rule impI)
    fix x 
    assume "h1 \<le> x \<and> x \<le> h2"
    hence "0 < x" and "x0 + x \<in> {b_x 0 <..< b_x t_max}" 
      using `0 < h1` assms(2-3) by auto
    hence "(secant_slope 1 x0 has_real_derivative secant_slope' x0 x) (at x)" and 
      "0 \<le> secant_slope' x0 x"
      using secant_slope_has_derivative_secant_slope'[OF `0 < vx` `0 < x`, of "x0"]  
      derivative_secant_slope_nonneg[OF  _ _ `0 < x` `0 < vx`, of "x0"] assms(1)  by auto
    thus "\<exists>y. (secant_slope 1 x0 has_real_derivative y) (at x) \<and> 0 \<le> y "
      by (auto intro: exI[where x="secant_slope' x0 x"])
  qed
  show ?thesis
    using DERIV_nonneg_imp_nondecreasing[OF `h1 \<le> h2` aux]  by auto
qed

section "Over-approximating occupancy with polygon"

locale pos_ori_inv_occupancy = occupancy_acceleration + 
  assumes sx_zero: "fst s = 0"
  assumes sy_zero: "snd s = 0"
  assumes vy_zero: "snd v = 0"
  assumes vx_at_least_zero: "0 \<le> fst v"
begin

lemmas pos_ori_inv_axioms = sx_zero sy_zero vy_zero vx_at_least_zero

lemma t_max_def':
  "t_max = sqrt (2 / 3) * vx / a_max"
  using surjective_pairing[of "v"] norm_Pair vx_at_least_zero
  unfolding t_max_def  pos_ori_inv_axioms vx_def[THEN sym] by auto

lemma stopping_time:
  assumes "valid_time t" 
  shows "t \<le> fst v / a_max" 
proof -
  have "a_max\<^sup>2 * t\<^sup>2 \<le> (fst v)\<^sup>2" (is "?lhs1 \<le> ?rhs1") 
    using assms unfolding valid_time_def  pos_ori_inv_axioms using a_max_neq_zero 
    by (auto simp add:field_simps divide_simps)
  hence "sqrt ?lhs1 \<le> sqrt ?rhs1"  using real_sqrt_le_mono[OF `?lhs1 \<le> ?rhs1`] 
    by auto
  hence "a_max * t \<le> fst v"
    using real_sqrt_abs vx_at_least_zero  power_mult_distrib[of "a_max" "t" "2", THEN sym] 
    by auto
  thus "t \<le> fst v / a_max" 
    using a_max_neq_zero by (auto simp add:divide_simps field_simps)
qed

lemma stopping_time':
  assumes "0 \<le> t" and "t \<le> fst v / a_max"
  shows "valid_time t"
proof -
  have "a_max * t \<le> fst v" using assms a_max_neq_zero by (auto simp add:divide_simps field_simps)
  hence "(a_max * t)\<^sup>2 \<le> (fst v)\<^sup>2" using mult_mono[OF `a_max * t \<le> fst v` `a_max * t \<le> fst v` 
        vx_at_least_zero] a_max_neq_zero  `0 \<le> t` by (auto simp add:power2_eq_square)
  thus ?thesis
    unfolding valid_time_def pos_ori_inv_axioms power_mult_distrib by (auto simp add:field_simps)
qed

lemma valid_time_def2:
  assumes "0 \<le> t"
  shows "valid_time t \<longleftrightarrow> t \<le> fst v / a_max"
  using stopping_time'[OF assms] stopping_time by auto

lemma centre_non_decreasing:
  assumes "t \<le> t'" and "valid_time t" and "valid_time t'"
  shows "vx * t - 1/2 * a_max * t\<^sup>2 \<le> vx * t' - 1/2 * a_max * t'\<^sup>2" 
proof -
  have "t \<le> fst v / a_max" and "t' \<le> fst v / a_max"
    using stopping_time[OF assms(2)] and stopping_time[OF assms(3)]
    by auto
  hence "1/2 * (t + t') \<le> fst v / a_max" 
    by (auto simp add:field_simps divide_simps)
  hence "1/2 * a_max * (t + t') \<le> fst v" (is "?lhs1 \<le> ?rhs1") using  a_max_neq_zero 
    by (auto simp add:divide_simps field_simps)  
  hence "1/2 * a_max * (t'\<^sup>2 - t\<^sup>2) \<le> fst v * (t' - t)"
    using `t \<le> t'` mult_right_mono[OF `?lhs1 \<le> ?rhs1`, of "t' - t"]
    square_diff_square_factored[of "t'" "t", THEN sym] unfolding power2_eq_square 
    by (auto simp add:field_simps)
  thus ?thesis unfolding vx_def by (auto simp add:algebra_simps)
qed

lemma valid_in_between:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  shows "t \<le> \<tau> \<Longrightarrow> \<tau> \<le> t' \<Longrightarrow> valid_time \<tau>"
proof -
  assume "t \<le> \<tau>"
  hence "0 \<le> \<tau>" using assms by auto
  assume "\<tau> \<le> t' "
  have "t \<le> fst v / a_max" and "t' \<le> fst v / a_max" using assms
    unfolding valid_time_def2[OF assms(1)] using valid_time_def2[of "t'"] by auto  
  thus ?thesis using `t \<le> \<tau>` and `\<tau> \<le> t'` unfolding valid_time_def2[OF `0 \<le> \<tau>`]
    by (auto simp add:field_simps)
qed

text "Rewriting"

lemma valid_time_def':
  "valid_time t \<longleftrightarrow> \<bar>t\<bar> \<le> vx / a_max"
proof -
  have "valid_time t \<longleftrightarrow> t\<^sup>2 * a_max\<^sup>2 \<le> (fst v)\<^sup>2 + (snd v)\<^sup>2"
    unfolding valid_time_def by auto
  also have "... \<longleftrightarrow> t\<^sup>2 * a_max\<^sup>2 \<le> (fst v)\<^sup>2" 
    unfolding pos_ori_inv_axioms by auto
  also have "... \<longleftrightarrow> t\<^sup>2 \<le> (fst v / a_max)\<^sup>2"
    using a_max_neq_zero by (auto simp add:divide_simps)
  also have "... \<longleftrightarrow> sqrt (t\<^sup>2) \<le> fst v / a_max"
    using real_sqrt_le_iff[of "t\<^sup>2" "(fst v / a_max)\<^sup>2"] real_sqrt_abs[of "fst v / a_max"]
    vx_at_least_zero a_max_neq_zero  by auto
  finally show ?thesis using real_sqrt_abs[of "t"] unfolding vx_def[THEN sym] by auto  
qed


lemma occ_circle_alt_def:
  "occupancy_circle t = \<lparr> Center = (vx * t, 0)
                        , Radius = 1/2 * a_max * t\<^sup>2\<rparr>"
  unfolding occupancy_circle_def pos_ori_inv_axioms  using vx_def by auto

lemma b_x_def':
  "b_x t = vx * t - t\<^sup>3 * a_max\<^sup>2 / (2 * vx)"
proof -
  have "norm v = fst v" using norm_Pair[of "fst v" "snd v"] surjective_pairing[of "v"]
    vx_at_least_zero unfolding pos_ori_inv_axioms by auto
  hence "b_x t = norm v * t - t\<^sup>3 * a_max\<^sup>2 / (2 * norm v)"
    unfolding b_x_def by auto
  thus "b_x t  = vx * t - t\<^sup>3 * a_max\<^sup>2 / (2 * vx)"
    unfolding b_x_def `norm v = fst v` vx_def[THEN sym] by auto
qed

subsection "Computing six points enclosing two circles"

definition  point1 :: "real \<Rightarrow> real \<Rightarrow> real2" where
  "point1 t t' = (  (fst o Center o occupancy_circle) t - (Radius o occupancy_circle) t 
                 ,  (snd o Center o occupancy_circle) t - (Radius o occupancy_circle) t)"

lemma point1_def':
  "point1 t t' = ( vx * t - 1/2 * a_max * t\<^sup>2
                 , - 1/2 * a_max * t\<^sup>2)"
  unfolding point1_def occ_circle_alt_def comp_def by auto

definition  point2 :: "real \<Rightarrow> real \<Rightarrow> real2" where
  "point2 t t' = (  b_x t' 
                 ,  (snd o Center o occupancy_circle) t' - (Radius o occupancy_circle) t')"

lemma point2_def':
  "point2 t t' = ( b_x t' 
                 , - 1/2 * a_max * t'\<^sup>2)"
  unfolding point2_def occ_circle_alt_def comp_def by auto

definition  point3 :: "real \<Rightarrow> real \<Rightarrow> real2" where
  "point3 t t' = (  (fst o Center o occupancy_circle) t' + (Radius o occupancy_circle) t'  
                 ,  (snd o Center o occupancy_circle) t' - (Radius o occupancy_circle) t')"

lemma point3_def':
  "point3 t t' = (  vx * t' + 1/2 * a_max * t'\<^sup>2 
                 ,          - 1/2 * a_max * t'\<^sup>2)"
  unfolding point3_def occ_circle_alt_def comp_def by auto

definition  point4 :: "real \<Rightarrow> real \<Rightarrow> real2" where
  "point4 t t' = (  (fst o Center o occupancy_circle) t' + (Radius o occupancy_circle) t'
                 ,  (snd o Center o occupancy_circle) t' + (Radius o occupancy_circle) t')"

lemma point4_def':
  "point4 t t' = (  vx * t' + 1/2 * a_max * t'\<^sup>2
                 ,            1/2 * a_max * t'\<^sup>2)"
  unfolding point4_def occ_circle_alt_def comp_def by auto

definition  point5 :: "real \<Rightarrow> real \<Rightarrow> real2" where
  "point5 t t' = (  b_x t'
                 ,  (snd o Center o occupancy_circle) t' + (Radius o  occupancy_circle) t')"

lemma point5_def':
  "point5 t t' = (  b_x t'
                 ,  1/2 * a_max * t'\<^sup>2)"
  unfolding point5_def occ_circle_alt_def comp_def by auto

definition  point6 :: "real \<Rightarrow> real \<Rightarrow> real2" where
  "point6 t t' = (  (fst o Center o occupancy_circle) t - (Radius o occupancy_circle) t 
                 ,  (snd o Center o occupancy_circle) t + (Radius o occupancy_circle) t)"

lemma point6_def':
  "point6 t t' = (  vx * t - 1/2 * a_max * t\<^sup>2 
                 ,           1/2 * a_max * t\<^sup>2)"
  unfolding point6_def occ_circle_alt_def comp_def by auto

lemma b_x_at_least_fst_point6:
  assumes "t \<le> vx / a_max"
  shows "fst (point6 t t') \<le> b_x t"
proof -
  have "t\<^sup>2 * t \<le> t\<^sup>2 * (vx / a_max)"
    using mult_left_mono[OF assms, of "t\<^sup>2"] zero_le_power2 by auto
  hence "t\<^sup>3 \<le> t\<^sup>2 * vx / a_max" (is "?lhs0 \<le> ?rhs0")
    by (auto simp add:power3_eq_cube power2_eq_square)
  hence "?lhs0 * a_max\<^sup>2 \<le> ?rhs0 * a_max\<^sup>2"
    using mult_right_mono[OF `?lhs0 \<le> ?rhs0` zero_le_power2[of "a_max"]] by auto
  hence "t\<^sup>3 * a_max\<^sup>2 \<le> t\<^sup>2 * vx * a_max" (is "?lhs1 \<le> ?rhs1")
    using a_max_neq_zero by (auto simp add:power2_eq_square)
  hence "?lhs1 / (2 * vx) \<le> ?rhs1 / (2 * vx)"
    using divide_right_mono[OF `?lhs1 \<le> ?rhs1` vx_at_least_zero]
    unfolding vx_def[THEN sym] by auto
  also have "... = 1/2 * a_max * t\<^sup>2"
    using v\<^sub>x_wd unfolding vx_def[THEN sym] by auto
  finally show ?thesis
    unfolding point6_def' b_x_def' by auto
qed

lemma b_x_at_most_fst_point5:
  assumes "t \<in> {0 .. t_max}" and "t' \<in> {0 .. t_max}"
  assumes "t < t'"
  shows "b_x t < fst (point5 t t')"
  unfolding point5_def' using increasing_lt_t_max2 assms(1-3) unfolding t_max_def by auto

text "Sanity check"

lemma point1_and_6_same_x:
  "fst (point1 t t') = fst (point6 t t')"
  unfolding point1_def point6_def by auto

lemma point2_and_5_same_x:
  "fst (point2 t t') = fst (point5 t t')"
  unfolding point2_def point5_def by auto

lemma point3_and_4_same_x:
  "fst (point3 t t') = fst (point4 t t')"
  unfolding point4_def point3_def by auto

lemma point2_and_3_same_y:
  "snd (point2 t t') = snd (point3 t t')"
  unfolding point3_def point2_def by auto

lemma point4_and_5_same_y:
  "snd (point4 t t') = snd (point5 t t')"
  unfolding point4_def point5_def by auto

definition 
  "enclosing_polygon t t' = ( if t \<noteq> 0 then
                            [(point1 t t', point2 t t'), 
                             (point2 t t', point3 t t'), 
                             (point3 t t', point4 t t'), 
                             (point4 t t', point5 t t'), 
                             (point5 t t', point6 t t'), 
                             (point6 t t', point1 t t')]
                              else 
                            [(point1 t t', point2 t t'), 
                             (point2 t t', point3 t t'), 
                             (point3 t t', point4 t t'), 
                             (point4 t t', point5 t t'), 
                             (point5 t t', point1 t t')])"

lemma enclosing_poly_len:
  "t \<noteq> 0 \<Longrightarrow> length (enclosing_polygon t t') = 6"
  unfolding enclosing_polygon_def list.size by auto

lemma enclosing_poly_len':
  "t = 0 \<Longrightarrow> length (enclosing_polygon t t') = 5"
  unfolding enclosing_polygon_def list.size by auto

subsection "Proving that the enclosing polygon is indeed a polygon "

lemma polygon_aux:
  assumes "0 \<le> t" and "t < t'"
  shows "- t'\<^sup>3 * a_max\<^sup>2 / (2 * vx) < 1 / 2 * a_max * t'\<^sup>2"
    (is "?lhs < ?rhs")
proof -
  have "?lhs < 0" using assms a_max_neq_zero vx_at_least_zero  v\<^sub>x_wd unfolding vx_def[THEN sym]
    by (auto simp add:divide_simps)
  moreover have "0 < ?rhs"
    using a_max_neq_zero assms by auto
  ultimately show ?thesis by auto
qed

lemma  polychain_enclosing_polygon:
  "t \<noteq> 0 \<Longrightarrow> polychain (enclosing_polygon t t')"
proof (rule polychainI)
  fix i
  assume "t \<noteq> 0"
  assume "Suc i < length (enclosing_polygon t t')"
  hence "i < 5" unfolding enclosing_poly_len[OF `t \<noteq> 0`] by auto
  then consider "i = 0" | "i = 1" | "i = 2" | "i = 3" | "i = 4"  
    by linarith
  thus "snd (enclosing_polygon t t' ! i) = fst (enclosing_polygon t t' ! Suc i)"
    unfolding enclosing_polygon_def using `t \<noteq> 0`
    by (cases) auto
qed

lemma polychain_enclosing_polygon':
  "t = 0 \<Longrightarrow> polychain (enclosing_polygon t t')"
proof (rule polychainI)
  fix i
  assume "t = 0"
  assume "Suc i < length (enclosing_polygon t t')"
  hence "i < 4" unfolding enclosing_poly_len'[OF `t = 0`] by auto
  then consider "i = 0" | "i = 1" | "i = 2" | "i = 3"
    by linarith
  thus "snd (enclosing_polygon t t' ! i) = fst (enclosing_polygon t t' ! Suc i) "
    unfolding enclosing_polygon_def using `t = 0`
    by (cases) auto
qed

lemma point2x_lt_point3x:
  assumes "0 \<le> t" and "t < t'"
  shows "fst (point2 t t') < fst (point3 t t')"
proof -
  from assms have "0 < t'" by auto
  hence "- t'\<^sup>3 * a_max\<^sup>2 / (2 * vx) < 0" using a_max_neq_zero vx_at_least_zero v\<^sub>x_wd
    unfolding vx_def[THEN sym] by (auto)
  moreover have "0 < 1/2 * a_max * t'\<^sup>2" using `0 < t'` a_max_neq_zero by auto
  ultimately show ?thesis
    unfolding point2_def' point3_def' comp_def b_x_def' by auto
qed


lemma point6x_lt_point5x:
  assumes "0 \<le> t" and "t < t'"
  assumes "valid_time t" and "valid_time t'"
  shows "fst (point6 t t') < fst (point5 t t')"
proof -
  from assms have "t \<le> vx / a_max" and "t' \<le> vx / a_max" unfolding valid_time_def' by auto
  hence "t + t' < 2 * vx / a_max" (is "?lhs1 < ?rhs1")
    using assms(2) by auto
  hence "a_max * ?lhs1 < 2 * vx" (is "?lhs2 < ?rhs2")
    using a_max_neq_zero by (auto simp add:algebra_simps divide_simps)
  hence "?lhs2 * (t' - t) * vx < ?rhs2 * (t' - t) * vx" using assms(2) 
    v\<^sub>x_wd vx_at_least_zero mult_strict_right_mono[OF `?lhs2 < ?rhs2`, of "(t' - t) * (fst v)"]
    unfolding vx_def[THEN sym] by auto  
  hence "a_max * (t'\<^sup>2 - t\<^sup>2) * vx < 2 * vx\<^sup>2 * (t' - t)" (is "?lhs3 < ?rhs3")
    unfolding power2_eq_square by (auto simp add:algebra_simps)
  hence "a_max\<^sup>2 * (t'\<^sup>2 * vx / a_max - t\<^sup>2 * vx / a_max) < ?rhs3" (is "?lhs4 < _")
    using a_max_neq_zero unfolding power2_eq_square  by (auto simp add:field_simps)
  moreover have "a_max\<^sup>2 * (t'\<^sup>2 * t' - t\<^sup>2 * vx / a_max) \<le> ?lhs4"
    using mult_left_mono[OF `t' \<le> vx / a_max`, of "t'\<^sup>2"] 
    by (simp add: mult_left_mono)
  ultimately have "a_max\<^sup>2 * (t'\<^sup>2 * t' - t\<^sup>2 * vx / a_max) < ?rhs3" by auto
  hence "a_max\<^sup>2 * (t'\<^sup>3 - t\<^sup>2 * vx / a_max) < ?rhs3" 
    unfolding power3_eq_cube power2_eq_square by auto
  hence "a_max\<^sup>2 * t'\<^sup>3 - t\<^sup>2 * vx * a_max < ?rhs3" using a_max_neq_zero 
    unfolding power2_eq_square by (auto simp add:field_simps)
  hence "a_max\<^sup>2 * t'\<^sup>3 < ?rhs3 + t\<^sup>2 * vx * a_max" (is "_ < ?rhs4")by auto
  hence "a_max\<^sup>2 * t'\<^sup>3 / (2 * vx) < ?rhs4 / (2 * vx)"
    using v\<^sub>x_wd vx_at_least_zero unfolding vx_def[THEN sym] by (auto simp add:divide_simps)
  hence "vx * t + a_max\<^sup>2 * t'\<^sup>3 / (2 * vx) < vx * t' + 1/2 * a_max * t\<^sup>2"
    using v\<^sub>x_wd vx_at_least_zero unfolding vx_def[THEN sym] power2_eq_square 
    by (auto simp add:field_simps)
  thus ?thesis unfolding point5_def' point6_def' b_x_def'
    by (auto simp add:algebra_simps)
qed

lemma point123:
  assumes "0 \<le> t" and "t < t'"
  shows "det3 (point1 t t') (point2 t t') (point3 t t') > 0"
proof -
  have "\<exists>k. 0 < k \<and> fst (point3 t t') = fst (point2 t t') + k"
    apply (rule exI[where x="fst (point3  t t') - fst (point2 t t')"])
    using point2x_lt_point3x[OF assms] by auto
  then obtain k where "0 < k" and 0: "fst (point3 t t') = fst (point2 t t') + k"
    by auto
  have "t\<^sup>2 < t'\<^sup>2" unfolding power2_eq_square 
    using mult_strict_mono[OF `t < t'` `t < t'` _ `0 \<le> t`] assms by auto
  hence "snd (point2 t t') < snd (point1 t t')" unfolding point1_def' point2_def'
    using `t < t'` `0 \<le> t` a_max_neq_zero by (auto simp add:power2_eq_square)
  from det3_aux1[OF this `0 < k`]  show ?thesis using 0[THEN sym] 
    by (metis point2_and_3_same_y prod.collapse)
qed

lemma point234:
  assumes "0 \<le> t" and "t < t'"
  shows "det3 (point2 t t') (point3 t t') (point4 t t') > 0"
proof -
  have "\<exists>k. 0 < k \<and> fst (point3 t t') = fst (point2 t t') + k"
    apply (rule exI[where x="fst (point3  t t') - fst (point2 t t')"])
    using point2x_lt_point3x[OF assms] by auto
  then obtain l where "0 < l" and 0: "fst (point3 t t') = fst (point2 t t') + l"
    by auto
  have 1: "snd (point3 t t') < snd (point4 t t')"
    unfolding point3_def' point4_def' using a_max_neq_zero `0 \<le> t` `t < t'`
    by (auto simp add:field_simps power2_eq_square)
  have "\<exists>l. 0 < l \<and> snd (point4 t t') = snd (point3 t t') + l"
    apply (rule exI[where x="snd (point4 t t') - snd (point3 t t')"])
    using 1 by auto
  then obtain k where "0 < k" and 2: "snd (point4 t t') = snd (point3 t t') + k"
    by auto
  have "snd (point3 t t') = snd (point2 t t')"
    unfolding point2_def' point3_def' by auto
  with det3_aux2[OF `0 < l` `0 < k`, of "fst (point2 t t')" "snd (point2 t t')" "fst (point4 t t')"]  
  show ?thesis using 0 2 surjective_pairing[of "point2 t t'"] surjective_pairing[of "point3 t t'"]
      surjective_pairing[of "point4 t t'"] by auto
qed

lemma point345:
  assumes "0 \<le> t" and "t < t'"
  shows "det3 (point3 t t') (point4 t t') (point5 t t') > 0"
proof -
  have "snd (point3 t t') < snd (point4 t t')"
    unfolding point3_def' point4_def' using assms a_max_neq_zero by auto
  hence "\<exists>l. 0 < l \<and> snd (point4 t t') = snd (point3 t t') + l"
    by (auto intro: exI[where x="snd (point4 t t') - snd (point3 t t')"])
  then obtain l where "0 < l" and 0: "snd (point4 t t') = snd (point3 t t') + l"
    by auto
  from polygon_aux[OF assms] have "fst (point5 t t') < fst (point3 t t')"
    unfolding point5_def' point3_def' b_x_def' by auto
  hence "\<exists>k. 0 < k \<and> fst (point5 t t') = fst (point3 t t') - k"
    by (auto intro: exI[where x="fst (point3 t t') - fst (point5 t t')"])
  then obtain k where "0 < k" and 1: "fst (point5 t t') = fst (point3 t t') - k"
    by auto
  have 2: "fst (point4 t t') = fst (point3 t t')" 
    unfolding point4_def' point3_def' by auto
  have 3: "snd (point5 t t') = snd (point4 t t')"
    unfolding point5_def' point4_def' by auto
  show ?thesis using det3_aux3[OF `0 < l` `0 < k`, of "fst (point3 t t')" "snd (point3 t t')"] 
    surjective_pairing[of "point3 t t'"] surjective_pairing[of "point4 t t'"] surjective_pairing
    [of "point5 t t'"] 0 1 2 3 by auto
qed


lemma point456:
  assumes "0 \<le> t" and "t < t'"
  shows "det3 (point4 t t') (point5 t t') (point6 t t') > 0"
proof -
  have "fst (point5 t t') < fst (point4 t t')"
    unfolding point5_def' point4_def' b_x_def' using polygon_aux[OF assms] by auto
  then obtain l where "0 < l" and 0: "fst (point5 t t') = fst (point4 t t') - l"
    using obt_neg_diff by auto
  have 1: "snd (point4 t t') = snd (point5 t t')"
    unfolding point4_def' point5_def' by auto
  have "snd (point6 t t') < snd (point5 t t')"
    using power2_mono_lt[OF assms(2) assms(1)] unfolding point6_def' point5_def' power2_eq_square 
    using assms a_max_neq_zero  by (auto)
  from det3_aux4[OF `0 < l` this, of "fst (point4 t t')" "fst (point6 t t')"] show ?thesis using 
    surjective_pairing [of "point4 t t'"] surjective_pairing[of "point5 t t'"] 
    surjective_pairing[of "point6 t t'"] 0 1 by auto
qed


lemma point561:
  assumes "0 < t" and "t < t'"
  assumes "valid_time t" and "valid_time t'"
  shows "det3 (point5 t t') (point6 t t') (point1 t t') > 0"
proof -
  have "fst (point6 t t') < fst (point5 t t')" using point6x_lt_point5x  assms by auto
  with obt_neg_diff[OF this] obtain l where "0 < l" and 0: "fst (point6 t t') = fst (point5 t t') - l"
    by auto
  have 1: "snd (point1 t t') < snd (point6 t t')"
    unfolding point1_def' point6_def' using a_max_neq_zero assms unfolding power2_eq_square 
    by (auto simp add:field_simps)
  have 3: "snd (point6 t t') < snd (point5 t t')"
    unfolding point6_def' point5_def' using `t < t'` a_max_neq_zero power2_mono_lt[OF `t < t'`] 
    assms(1) unfolding power2_eq_square  by (auto simp add:field_simps)
  with obt_neg_diff[OF this] obtain k where "0 < k" and 4: "snd (point6 t t') = snd (point5 t t') - k"
    by auto
  have *: "snd (point1 t t') < snd (point5 t t')"
    using power2_mono_lt[OF `t < t'`] assms(1) unfolding point1_def' point5_def' 
    using a_max_neq_zero 
    by (smt divide_cancel_right divide_minus_left divide_nonneg_nonneg mult_minus_left prod.sel(2) 
        real_mult_le_cancel_iff1 zero_less_power)
  with obt_neg_diff[OF this] obtain m' where "0 < m'" and **: "snd (point1 t t') = snd (point5 t t') - m'"
    by auto
  have "k < m'" using 1 ** 4 by linarith
  have "fst (point6 t t') = fst (point1 t t')"
    unfolding point6_def' point1_def' by auto
  thus ?thesis using det3_aux5[OF `0 < m'` `0 < l` `0 < k` `k < m'`, of "fst (point5 t t')" "snd (point5 t t')"]
    0 4 ** using surjective_pairing[of "point5 t t'"] surjective_pairing[of "point6 t t'"] 
    surjective_pairing[of "point1 t t'"] by auto
qed

theorem convex_polychain_enclosing_polygon:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  shows "convex_polychain (enclosing_polygon t t')"
  unfolding convex_polychain_def
proof (rule, rule_tac[2] allI, rule_tac[2] impI)
  show "polychain (enclosing_polygon t t')" using polychain_enclosing_polygon 
    polychain_enclosing_polygon' by (cases "t = 0") auto
next
  fix i
  assume *: "Suc i < length (enclosing_polygon t t')"
  have "t \<noteq> 0 \<or> t = 0" by auto
  moreover
  { assume "t \<noteq> 0"
    with * have "i < 5" using enclosing_poly_len[OF `t \<noteq> 0`] by auto
    then consider "i = 0" | "i = 1" | "i = 2" | "i = 3" | "i = 4"  
      by linarith
    hence "0 < det3 (fst (enclosing_polygon t t' ! i)) 
                    (snd (enclosing_polygon t t' ! i)) 
                    (snd (enclosing_polygon t t' ! Suc i))"
      unfolding enclosing_polygon_def using point123[OF assms(1-2)] point234[OF assms(1-2)] 
      point345[OF assms(1-2)]  point456[OF assms(1-2)] point561[of "t", OF _ assms(2-4)] assms(1) 
      `t \<noteq> 0`  by (cases) auto }
  moreover
  { assume "t = 0"
    with * have "i < 4" using enclosing_poly_len'[OF `t = 0`] by auto
    have "point6 t t' = point1 t t'" unfolding `t = 0` point6_def' point1_def' by auto
    from `i < 4` consider "i = 0" | "i = 1" | " i = 2" | "i = 3" by linarith
    hence "0 < det3 (fst (enclosing_polygon t t' ! i)) 
                    (snd (enclosing_polygon t t' ! i)) 
                    (snd (enclosing_polygon t t' ! Suc i))"
      unfolding enclosing_polygon_def using point123[OF assms(1-2)] point234[OF assms(1-2)]
      point345[OF assms(1-2)] point456[OF assms(1-2)] `t = 0` using `point6 t t' = point1 t t'`
      by (cases) auto }
  ultimately show "0 < det3 (fst (enclosing_polygon t t' ! i)) 
                    (snd (enclosing_polygon t t' ! i)) 
                    (snd (enclosing_polygon t t' ! Suc i))" by auto
qed

theorem convex_polygon_enclosing_polygon:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  shows "convex_polygon (enclosing_polygon t t')"
  using convex_polychain_enclosing_polygon[OF assms] unfolding convex_polygon_def
  enclosing_polygon_def by auto

end

section "Defining the set of all points in a polygon"

subsection "Naive definition of points in polygon"

type_synonym poly_type = "(real2 \<times> real2) list"

definition in_poly :: "real2 \<Rightarrow> poly_type \<Rightarrow> bool" where
  "in_poly p pol = list_all (encompasses p) pol"

lemma in_poly:
  fixes poly :: poly_type
  assumes "in_poly p pol"
  shows "\<And>c. c \<in> set pol \<Longrightarrow> det3 p (fst c) (snd c) \<ge> 0"
  using assms unfolding in_poly_def
  by (metis (no_types, lifting) det3_rotate list_allD)

definition (in pos_ori_inv_occupancy) encl_poly_range where
  "encl_poly_range t t' = {p. in_poly p (enclosing_polygon t t')}"

lemma (in pos_ori_inv_occupancy) encl_poly_rangeI:
  assumes "list_all (encompasses p) (enclosing_polygon t t')"
  shows "p \<in> encl_poly_range t t'"
  using assms unfolding encl_poly_range_def in_poly_def by auto

subsection "Alternative definition of points in polygon"

definition (in pos_ori_inv_occupancy) slope where
  "slope t t' = (let x1 = fst (point6 t t');
                      y1 = snd (point6 t t'); 
                      x2 = fst (point5 t t');
                      y2 = snd (point5 t t') 
                  in (y2 - y1) / (x2 - x1))"

lemma (in pos_ori_inv_occupancy) slope_is_positive:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  shows "0 < slope t t'"
proof -
  define x1 where "x1 = fst (point6 t t')"
  define y1 where "y1 = snd (point6 t t')"
  define x2 where "x2 = fst (point5 t t')"
  define y2 where "y2 = snd (point5 t t')"
  have "x1 < x2"
    using point6x_lt_point5x[OF assms(1-4)] unfolding x1_def x2_def by auto
  have 3: "snd (point6 t t') < snd (point5 t t')"
    using power2_mono_lt[OF assms(2) assms(1)] unfolding point6_def' point5_def' power2_eq_square 
    using assms a_max_neq_zero  by (auto) 
  hence "y1 < y2" unfolding y1_def y2_def by auto
  thus ?thesis
    using `x1 < x2`
    unfolding slope_def Let_def x1_def x2_def y1_def y2_def by (auto)
qed

definition (in pos_ori_inv_occupancy) upper_bnd1 where
  "upper_bnd1 t t' = (let x1 = fst (point6 t t'); y1 = snd (point6 t t') in 
                      (\<lambda>x. slope t t' * (x - x1) + y1))"

lemma (in pos_ori_inv_occupancy) upper_bnd1_at_least_snd_point6:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point6 t t') \<le> x"
  shows "snd (point6 t t') \<le> upper_bnd1  t t' x"
  using slope_is_positive[OF assms(1-4)] assms(5) unfolding upper_bnd1_def Let_def 
  by (auto simp add:field_simps)

lemma (in pos_ori_inv_occupancy) upper_bnd1_def':
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  shows "upper_bnd1 t t' = (let x1 = fst (point5 t t'); y1 = snd (point5 t t') in 
                      (\<lambda>x. slope t t' * (x - x1) + y1))"
  unfolding upper_bnd1_def  slope_def Let_def using point6x_lt_point5x[OF assms]
  by (auto simp add:field_simps divide_simps)

lemma (in pos_ori_inv_occupancy) DERIV_upper_bnd1:
  "(upper_bnd1 t t' has_vector_derivative slope t t') (at x within s')"
  unfolding upper_bnd1_def Let_def
  by (auto intro!:derivative_eq_intros derivative_intros simp add:field_simps)

lemma (in pos_ori_inv_occupancy) upper_bound1_closed_segment:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point6 t t') \<le> x" and "x \<le> fst (point5 t t')"
  shows "(x, upper_bnd1 t t' x) \<in> closed_segment (point6 t t') (point5 t t')"
proof -
  define x1 where "x1 = fst (point6 t t')"
  define x2 where "x2 = fst (point5 t t')"
  obtain u ::real where u_def: "u = (x - x1) / (x2 - x1)" by auto
  have "x1 < x2" unfolding x1_def x2_def using point6x_lt_point5x[OF assms(1-4)] 
    by auto
  hence "0 \<le> u" and "u \<le> 1" using assms(5-6) unfolding u_def x1_def[THEN sym] x2_def[THEN sym]
    by auto
  have x_wit: "x = (1 - u) * x1 + u * x2" (is "_ = ?rhs")
  proof -
    have "(1 - u) * x1 = (x2 - x) / (x2 - x1) * x1" (is "_ = ?rhs1") unfolding u_def using `x1 < x2`
      by (auto simp add:divide_simps)
    moreover have "u * x2 = (x - x1) / (x2 - x1) * x2" (is "_ = ?rhs2") unfolding u_def by auto
    ultimately have "?rhs = ((x2 - x) * x1 + (x - x1) * x2) / (x2 - x1)" 
      by (auto simp add:field_simps divide_simps)
    also have "... = (x * (x2 - x1)) / (x2 - x1)" 
      by (auto simp add:field_simps)
    finally show ?thesis using `x1 < x2` by auto          
  qed
  define y1 where "y1 = snd (point6 t t')"
  define y2 where "y2 = snd (point5 t t')"
  have ub_def: "upper_bnd1 t t' x = (y2 - y1) / (x2 - x1) * (x - x1) + y1" (is "_ = ?lhs")
    unfolding upper_bnd1_def Let_def y2_def y1_def x2_def x1_def slope_def by auto
  have y_wit: "upper_bnd1 t t' x = (1 - u) * y1 + u * y2" (is "_ = ?rhs")
  proof -
    have "?lhs = ((y2 - y1) * (x - x1) + y1 * (x2 - x1))/ (x2 - x1) " (is "_ = ?lhs1 / (x2 - x1)")
      using `x1 < x2` by auto
    moreover have "?rhs = ((x2 - x) * y1 + (x - x1) * y2) / (x2 - x1)" (is "_ = ?rhs1 / (x2 - x1)")
      using `x1 < x2` unfolding u_def by (auto simp add:divide_simps)
    moreover have "?lhs1 = ?rhs1" by (auto simp add:field_simps)
    ultimately have "?lhs = ?rhs" using `x1 < x2` by (auto)
    thus ?thesis unfolding ub_def by auto
  qed
  show ?thesis 
    apply (rule closed_segmentI[of "u"])
    using `0 \<le> u` `u \<le> 1` x_wit y_wit surjective_pairing[of "point6 t t'"] 
    surjective_pairing[of "point5 t t'"] unfolding x1_def[THEN sym] x2_def[THEN sym] y1_def[THEN sym]
    y2_def[THEN sym] by (auto simp add:field_simps)
qed

definition (in pos_ori_inv_occupancy) "upper_bnd2 t t' = snd (point5 t t')"

definition (in pos_ori_inv_occupancy) lower_bnd1 where
  "lower_bnd1 t t' = (let x1 = fst (point1 t t'); y1 = snd (point1 t t') in 
                      (\<lambda>x. -slope t t' * (x - x1) + y1))"

lemma (in pos_ori_inv_occupancy) lower_bnd1_at_most_snd_point1: 
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point1 t t') \<le> x"
  shows "lower_bnd1 t t' x \<le> snd (point1 t t')"
  unfolding lower_bnd1_def Let_def using assms(5) slope_is_positive[OF assms(1-4)]
  by auto

lemma (in pos_ori_inv_occupancy) lower_bnd1_at_most_zero:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point1 t t') \<le> x"
  shows "lower_bnd1 t t' x \<le> 0"
proof -
  have "0 < slope t t'" using slope_is_positive[OF assms(1-4)] by auto
  have "snd (point1 t t') \<le> 0"
    unfolding point1_def' using a_max_neq_zero `0 \<le> t` unfolding power2_eq_square by auto
  moreover have "-slope t t' * (x - fst (point1 t t')) \<le> 0"
    using `0 < slope t t'` assms(5) by auto
  ultimately show "lower_bnd1 t t' x \<le> 0"
    unfolding lower_bnd1_def Let_def by auto
qed

lemma (in pos_ori_inv_occupancy) DERIV_lower_bnd1:
  "(lower_bnd1 t t' has_vector_derivative -slope t t') (at x within s')"
  unfolding lower_bnd1_def Let_def
  by (auto intro!:derivative_eq_intros derivative_intros simp add:field_simps)

lemma (in pos_ori_inv_occupancy) mirror_slope:
  "snd (point2 t t') - snd (point1 t t') = - (snd (point5 t t') - snd (point6 t t'))"
  unfolding point2_def' point1_def' point5_def' point6_def' by auto

lemma (in pos_ori_inv_occupancy) lower_upper_bnd_mirror:
  "lower_bnd1 t t' x = - upper_bnd1  t t' x"
proof -
  have "lower_bnd1 t t' x = -slope t t' * (x - fst (point1 t t')) + snd (point1 t t')"
    unfolding lower_bnd1_def by auto
  moreover have "fst (point1 t t') = fst (point6 t t')"
    using point1_and_6_same_x[of "t" "t'"] by auto
  moreover have "snd (point1 t t') = - snd (point6 t t')"
    unfolding point1_def' point6_def' by auto
  ultimately have "lower_bnd1 t t' x = -slope t t' * (x - fst (point6 t t')) - snd (point6 t t')"
    by auto
  also have "... = - (slope t t' * (x - fst (point6 t t')) + snd (point6 t t'))"
    by auto
  finally show ?thesis 
    unfolding upper_bnd1_def Let_def by auto
qed

lemma (in pos_ori_inv_occupancy) upper_bnd1_at_least_zero:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point6 t t') \<le> x"
  shows "0 \<le> upper_bnd1 t t' x"
proof -
  have "lower_bnd1 t t' x \<le> 0" using lower_bnd1_at_most_zero[OF assms(1-4)]
    assms(5) point1_and_6_same_x by auto
  hence "-upper_bnd1 t t' x \<le> 0" using lower_upper_bnd_mirror by auto
  thus ?thesis by auto
qed

lemma (in pos_ori_inv_occupancy) lower_bound1_closed_segment:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point1 t t') \<le> x" and "x \<le> fst (point2 t t')"
  shows "(x, lower_bnd1 t t' x) \<in> closed_segment (point1 t t') (point2 t t')"
proof -
  define x1 where "x1 = fst (point1 t t')"
  define x2 where "x2 = fst (point2 t t')"
  define y1 where "y1 = snd (point1 t t')"
  define y2 where "y2 = snd (point2 t t')"
  define slope2 where "slope2 = (y2 - y1) / (x2 - x1)"
  have "slope2 = - slope t t'" 
    unfolding slope_def Let_def slope2_def x1_def x2_def y1_def y2_def
    point1_and_6_same_x[of "t" "t'"] point2_and_5_same_x[of "t" "t'"]
    mirror_slope[of "t" "t'"] by (auto simp add:divide_simps)

  obtain u ::real where u_def: "u = (x - x1) / (x2 - x1)" by auto
  have "x1 < x2" unfolding x1_def x2_def 
    point1_and_6_same_x[of "t" "t'"] point2_and_5_same_x[of "t" "t'"]
    using point6x_lt_point5x[OF assms(1-4)] by auto
  hence "0 \<le> u" and "u \<le> 1" using assms(5-6) unfolding u_def x1_def[THEN sym] x2_def[THEN sym]
    by auto
  have x_wit: "x = (1 - u) * x1 + u * x2" (is "_ = ?rhs")
  proof -
    have "(1 - u) * x1 = (x2 - x) / (x2 - x1) * x1" (is "_ = ?rhs1") unfolding u_def using `x1 < x2`
      by (auto simp add:divide_simps)
    moreover have "u * x2 = (x - x1) / (x2 - x1) * x2" (is "_ = ?rhs2") unfolding u_def by auto
    ultimately have "?rhs = ((x2 - x) * x1 + (x - x1) * x2) / (x2 - x1)" 
      by (auto simp add:field_simps divide_simps)
    also have "... = (x * (x2 - x1)) / (x2 - x1)" 
      by (auto simp add:field_simps)
    finally show ?thesis using `x1 < x2` by auto          
  qed  
  have lb_def: "lower_bnd1 t t' x = (y2 - y1) / (x2 - x1) * (x - x1) + y1" (is "_ = ?lhs")
    unfolding lower_bnd1_def `slope2 = -slope t t'`[THEN sym] Let_def y2_def y1_def x2_def x1_def
    slope2_def by auto
  have y_wit: "lower_bnd1 t t' x = (1 - u) * y1 + u * y2" (is "_ = ?rhs")
  proof -
    have "?lhs = ((y2 - y1) * (x - x1) + y1 * (x2 - x1))/ (x2 - x1) " (is "_ = ?lhs1 / (x2 - x1)")
      using `x1 < x2` by auto
    moreover have "?rhs = ((x2 - x) * y1 + (x - x1) * y2) / (x2 - x1)" (is "_ = ?rhs1 / (x2 - x1)")
      using `x1 < x2` unfolding u_def by (auto simp add:divide_simps)
    moreover have "?lhs1 = ?rhs1" by (auto simp add:field_simps)
    ultimately have "?lhs = ?rhs" using `x1 < x2` by (auto)
    thus ?thesis unfolding lb_def by auto
  qed
  show ?thesis 
    apply (rule closed_segmentI[of "u"])
    using `0 \<le> u` `u \<le> 1` x_wit y_wit surjective_pairing[of "point1 t t'"] 
    surjective_pairing[of "point2 t t'"] unfolding x1_def[THEN sym] x2_def[THEN sym] y1_def[THEN sym]
    y2_def[THEN sym] by (auto simp add:field_simps)
qed

definition (in pos_ori_inv_occupancy) "lower_bnd2 t t' = snd (point2 t t')"

lemma (in pos_ori_inv_occupancy) lower_bnd1_eq_lower_bnd2:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  shows "lower_bnd1 t t' (fst (point2 t t')) = lower_bnd2 t t'"
proof -
  define x where "x = fst (point2 t t')"
  have "lower_bnd1 t t' x = - slope t t' * (x - fst (point1 t t')) + snd (point1 t t')"
    unfolding lower_bnd1_def by auto
  also have "... = - snd (point5 t t') + snd (point6 t t') + snd (point1 t t')"
    unfolding x_def slope_def Let_def point1_and_6_same_x point2_and_5_same_x 
    using point6x_lt_point5x [OF assms] by auto
  also have "... = - snd (point5 t t')"
    unfolding point6_def' point1_def' by auto
  finally show ?thesis unfolding lower_bnd2_def unfolding point2_def' point5_def' x_def
    by auto
qed

lemma (in pos_ori_inv_occupancy) upper_bnd1_eq_upper_bnd2:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  shows "upper_bnd1 t t' (fst (point2 t t')) = upper_bnd2 t t'"
proof -
  have "lower_bnd1 t t' (fst (point2 t t')) = lower_bnd2 t t'"
    using lower_bnd1_eq_lower_bnd2[OF assms] by auto
  hence "-upper_bnd1  t t' (fst (point2 t t')) = lower_bnd2 t t'"
    using lower_upper_bnd_mirror by auto
  also have "... = -upper_bnd2 t t'"
    unfolding lower_bnd2_def upper_bnd2_def point2_def' point5_def' by auto
  finally show ?thesis by auto
qed

lemma closed_segment_to_det:
  assumes "p \<in> closed_segment end1 end2"
  shows "det3 p end1 end2 = 0"
proof -
  from assms have cs: "\<exists> u. 0  \<le> u \<and> u \<le> 1 \<and> p = (1-u) *\<^sub>R end1 + u *\<^sub>R end2" by (simp add: in_segment(1))
  obtain u :: real where u_prec: "0 \<le> u \<and> u \<le> 1 \<and> p = (1 + - 1 * u) *\<^sub>R end1 + u *\<^sub>R end2" using cs by auto
  hence "det3 p end1 end2 = fst p * snd end1 + snd p * fst end2 + fst end1 * snd end2 - snd end1 * fst end2 - snd p * fst end1 - fst p * snd end2" 
      using det3_def' by auto
  hence "... = fst ((1-u) *\<^sub>R end1 + u *\<^sub>R end2) * snd end1 + snd ((1-u) *\<^sub>R end1 + u *\<^sub>R end2) * fst end2 + fst end1 * snd end2 - snd end1 * fst end2 - snd ((1-u) *\<^sub>R end1 + u *\<^sub>R end2) * fst end1 - fst ((1-u) *\<^sub>R end1 + u *\<^sub>R end2) * snd end2"
      by (simp add: u_prec)
  hence "... = ((1-u) * fst end1 * snd end1 + u * fst end2 * snd end1)
              + ((1-u) * snd end1 * fst end2 + u * snd end2 * fst end2)
              + fst end1 * snd end2
              - snd end1 * fst end2 
              - ((1-u) * snd end1 * fst end1+ u * snd end2 * fst end1)
              - ((1-u) * fst end1 * snd end2 + u * fst end2 * snd end2)" by (simp add: distrib_right)
  hence "... = 0" by (simp add: left_diff_distrib')
  then show ?thesis by (simp add: coll_convex det3_rotate u_prec)
qed

lemma (in pos_ori_inv_occupancy) encompasses_pt12:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point1 t t') \<le> x" and "x \<le> fst (point2 t t')"
  assumes "lower_bnd1 t t' x \<le> y" 
  shows "encompasses (x, y) (point1 t t', point2 t t')"
proof -
  have lb_le: "lower_bnd1 t t' = line_equation (point1 t t') (point2 t t')"
    unfolding lower_bnd1_def slope_def Let_def line_equation_def
    mirror_slope point1_and_6_same_x[of "t" "t'"] point2_and_5_same_x[of "t" "t'"]
    by (auto simp add:field_simps divide_simps)
  have "lower_bnd1 t t' x < y \<or> lower_bnd1 t t' x = y" (is "?case1 \<or> ?case2")
    using assms(7) by auto
  moreover
  { assume "?case1"
    hence " ccw' (x, y) (point1 t t') (point2 t t')" 
      using ccw_ray_downwards2[of "point1 t t'" "point2 t t'" "(x, y)"]
      using point6x_lt_point5x[OF assms(1-4)] unfolding point1_and_6_same_x[of "t" "t'"]
      point2_and_5_same_x[of "t" "t'"] lb_le by auto     
    hence "encompasses (x, y) (point1 t t', point2 t t')" unfolding ccw'_def using det3_rotate
      by auto }
  moreover
  { assume "?case2" 
    hence "(x, y) \<in> closed_segment (point1 t t') (point2 t t')"
      using lower_bound1_closed_segment[OF assms(1-6)] by auto
    hence "det3 (x, y) (point1 t t') (point2 t t') = 0"
      using closed_segment_to_det by auto
    hence "encompasses (x, y) (point1 t t', point2 t t')" using det3_rotate by auto }
  ultimately show "encompasses (x, y) (point1 t t', point2 t t')" 
    by auto
qed

lemma (in pos_ori_inv_occupancy) encompasses_pt56:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point1 t t') \<le> x" and "x \<le> fst (point2 t t')"
  assumes                             "y \<le> upper_bnd1 t t' x"
  shows "encompasses (x, y) (point5 t t', point6 t t')"
proof -
  have ub_le: "upper_bnd1 t t' = line_equation (point6 t t') (point5 t t')"
    unfolding upper_bnd1_def slope_def Let_def line_equation_def
    mirror_slope point1_and_6_same_x[of "t" "t'"] point2_and_5_same_x[of "t" "t'"]
    by (auto simp add:field_simps divide_simps)
  have "y < upper_bnd1 t t' x \<or> upper_bnd1 t t' x = y" (is "?case1 \<or> ?case2")
    using assms(7) by auto
  moreover
  { assume "?case1"
    hence "det3 (x, y) (point6 t t') (point5 t t') < 0"
      using cw_ray_upwards2[of "(x, y)", OF _ point6x_lt_point5x[OF assms(1-4)]] unfolding ub_le 
      by auto
    hence "encompasses (x, y) (point5 t t', point6 t t')" 
      using det3_switch[where b="point6 t t'" and c="point5 t t'"] det3_rotate by auto }
  moreover
  { assume "?case2"
    hence "(x, y) \<in> closed_segment (point6 t t') (point5 t t')"
      using upper_bound1_closed_segment[OF assms(1-4)] assms(5-6)
      point1_and_6_same_x[of "t" "t'"] point2_and_5_same_x[of "t" "t'"] by auto
    hence "det3 (x, y) (point6 t t') (point5 t t') = 0"
      using closed_segment_to_det by auto
    hence "encompasses (x, y) (point5 t t', point6 t t')" using det3_rotate 
      det3_switch[where b="point6 t t'" and c="point5 t t'"] by auto }
  ultimately show "encompasses (x, y) (point5 t t', point6 t t')" 
    by auto
qed

lemma (in pos_ori_inv_occupancy) encompasses_pt34:
  assumes "0 \<le> t" and "t < t'"
  assumes "x \<le> fst (point2 t t')"
  shows "encompasses (x, y) (point3 t t', point4 t t')"
proof -
  have "x < fst (point3 t t')" using point2x_lt_point3x[OF assms(1-2)] 
    using assms(3) by auto
  have "snd (point3 t t') < snd (point4 t t')"
    unfolding point3_def' point4_def' using a_max_neq_zero `0 \<le> t` `t < t'`
    by (auto simp add:field_simps power2_eq_square)
  from det3_aux6[OF `x < fst (point3 t t')` this, of "y"] show ?thesis
    unfolding point3_def' point4_def' by auto
qed

lemma (in pos_ori_inv_occupancy) encompasses_pt61:
  assumes "0 < t" and "t < t'"
  assumes "fst (point1 t t') \<le> x"
  shows "encompasses (x, y) (point6 t t', point1 t t')"
proof -
  have "fst (point1 t t') < x \<or> fst (point1 t t') = x" using assms(3) 
    by auto
  moreover
  { assume "fst (point1 t t') < x"
    hence "fst (point6 t t') < x"  using point1_and_6_same_x by auto
    have "snd (point1 t t') < snd (point6 t t')"
      unfolding point1_def' point6_def' using a_max_neq_zero assms unfolding power2_eq_square 
      by (auto simp add:field_simps)
    from det3_aux7[OF `fst (point1 t t') < x` this, of "y"] have ?thesis
      using point1_and_6_same_x[of "t" "t'"] surjective_pairing[of "point1 t t'"]
      surjective_pairing[of "point6 t t'"] by auto }
  moreover
  { assume "fst (point1 t t') = x"
    hence ?thesis using point1_and_6_same_x[of "t" "t'"] unfolding det3_def'
      by auto }
  ultimately show ?thesis by auto
qed

lemma (in pos_ori_inv_occupancy) lower_bnd1_nonincreasing:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "x1 \<le> x2"
  shows "lower_bnd1 t t' x1 \<ge> lower_bnd1 t t' x2"
proof -
  have *: "\<forall>x. x1 \<le> x \<and> x \<le> x2 \<longrightarrow> (\<exists>y. ((\<lambda>u. lower_bnd1 t t' u) has_real_derivative y) (at x) \<and> y \<le> 0)"
    using slope_is_positive[OF assms(1-4)] DERIV_lower_bnd1[of "t" "t'"]
    unfolding has_field_derivative_iff_has_vector_derivative
    by (auto intro:exI[where x="-slope t t'"])
  show ?thesis using DERIV_nonpos_imp_nonincreasing[OF `x1 \<le> x2` *] by auto
qed

lemma (in pos_ori_inv_occupancy) upper_bnd1_nondecreasing:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "x1 \<le> x2"
  shows "upper_bnd1 t t' x1 \<le> upper_bnd1 t t' x2"
proof -
  have *: "\<forall>x. x1 \<le> x \<and> x \<le> x2 \<longrightarrow> (\<exists>y. ((\<lambda>y. upper_bnd1 t t' y) has_real_derivative y) (at x) \<and> 0 \<le> y)"
    using slope_is_positive[OF assms(1-4)] DERIV_upper_bnd1[of "t" "t'"] 
    unfolding has_field_derivative_iff_has_vector_derivative 
    by (auto intro:exI[where x="slope t t'"])
  show ?thesis using DERIV_nonneg_imp_nondecreasing[OF `x1 \<le> x2` *] by auto
qed

(* TODO: taken verbatim from Environment_Executable.thy. Cannot reference it because this lemma
  is unnamed. Please re-name in Environment_Executable, re-build the image, and remove this lemma. *)
lemma line_equation_snd:
  assumes "fst p1 \<noteq> fst p2"
  shows "line_equation p1 p2 (fst p2) = (snd p2)"
  unfolding line_equation_def using assms by (auto simp add:divide_simps) 

lemma (in pos_ori_inv_occupancy) min_lower_bnd1:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "x \<le> fst (point2 t t')"
  shows "lower_bnd1 t t' x \<ge> snd (point2 t t')"
proof -
  define x2 where "x2 = fst (point2 t t')"
  define y2 where "y2 = snd (point2 t t')"
  have "lower_bnd1  t t' x \<ge> lower_bnd1 t t' x2"
    using lower_bnd1_nonincreasing[OF assms(1-5)] unfolding x2_def by auto
  have lb_le: "lower_bnd1 t t' = line_equation (point1 t t') (point2 t t')"
    unfolding lower_bnd1_def slope_def Let_def line_equation_def
    mirror_slope point1_and_6_same_x[of "t" "t'"] point2_and_5_same_x[of "t" "t'"]
    by (auto simp add:field_simps divide_simps)  
  have "fst (point1 t t') < fst (point2 t t')"
    using point6x_lt_point5x[OF assms(1-4)] unfolding point1_and_6_same_x point2_and_5_same_x
    by auto
  hence "lower_bnd1 t t' x2 = y2"
    unfolding x2_def y2_def lb_le using line_equation_snd by auto
  with `lower_bnd1 t t' x \<ge> lower_bnd1 t t' x2` have "lower_bnd1 t t' x \<ge> y2"
    by auto
  thus ?thesis unfolding y2_def by auto
qed

lemma (in pos_ori_inv_occupancy) max_upper_bnd1:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "x \<le> fst (point2 t t')"
  shows "upper_bnd1 t t' x \<le> snd (point5 t t')"
proof -
  define x2 where "x2 = fst (point5 t t')"
  define y2 where "y2 = snd (point5 t t')"
  have 0: "upper_bnd1 t t' x \<le> upper_bnd1  t t' x2"
    using upper_bnd1_nondecreasing[OF assms(1-4) `x \<le> fst (point2 t t')`] 
    unfolding x2_def point2_and_5_same_x by auto
  have ub_le: "upper_bnd1 t t' = line_equation (point6 t t') (point5 t t')"
    unfolding upper_bnd1_def slope_def Let_def line_equation_def by auto
  have "fst (point6 t t') < fst (point5 t t')" 
    using point6x_lt_point5x assms by auto
  hence "upper_bnd1 t t' x2 = y2"
    unfolding x2_def y2_def ub_le using line_equation_snd by auto
  with 0 have "upper_bnd1 t t' x \<le> y2" by auto
  thus ?thesis unfolding y2_def by auto
qed

lemma (in pos_ori_inv_occupancy) encompasses_pt23:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "x \<le> fst (point2 t t')"
  assumes "lower_bnd1 t t' x \<le> y" 
  shows "encompasses (x, y) (point2 t t', point3 t t')"
proof -
  have "\<exists>k. 0 < k \<and> fst (point3 t t') = fst (point2 t t') + k"
    apply (rule exI[where x="fst (point3  t t') - fst (point2 t t')"])
    using point2x_lt_point3x[OF assms(1-2)] by auto  
  then obtain k where "0 < k" and 0: "fst (point3 t t') = fst (point2 t t') + k"
    by auto
  have "lower_bnd1 t t' x \<ge> snd (point2 t t')" using min_lower_bnd1[OF assms(1-5)]
    by auto
  hence "snd (point2 t t') \<le> y" using assms(6) by auto 
  from det3_aux1'[OF this `0 < k`, of "x" "fst (point2 t t')"] have "det3 (x, y) (point2 t t') (point3 t t') \<ge> 0"
    using 0[THEN sym] surjective_pairing[of "point2 t t'"] surjective_pairing[of "point3 t t'"]
    point2_and_3_same_y[of "t" "t'"] by auto
  thus ?thesis using det3_rotate by auto    
qed 

lemma (in pos_ori_inv_occupancy) encompasses_pt45:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "x \<le> fst (point2 t t')"
  assumes "y \<le> upper_bnd1 t t' x" 
  shows "encompasses (x, y) (point4 t t', point5 t t')"
proof -
  have "fst (point5 t t') < fst (point4 t t')"
    unfolding point5_def' point4_def' b_x_def' using polygon_aux[OF assms(1-2)] by auto
  then obtain l where "0 < l" and 0: "fst (point5 t t') = fst (point4 t t') - l"
    using obt_neg_diff by auto
  have 1: "snd (point4 t t') = snd (point5 t t')"
    unfolding point4_def' point5_def' by auto
  have "upper_bnd1 t t' x \<le> snd (point5 t t')" using max_upper_bnd1[OF assms(1-5)] by auto
  hence "y \<le> snd (point5 t t')" using assms(6) by auto
  from det3_aux4'[OF `0 < l` this, of "fst (point4 t t')" "x"] show ?thesis
    using 0 point4_and_5_same_y[of "t" "t'"] surjective_pairing[of "point4 t t'"]
    surjective_pairing[of "point5 t t'"] by auto
qed  
  
theorem (in pos_ori_inv_occupancy) alt_def_encl_poly1:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point1 t t') \<le> x" and "x \<le> fst (point2 t t')"
  assumes "lower_bnd1 t t' x \<le> y" and "y \<le> upper_bnd1 t t' x"
  shows "(x, y) \<in> encl_poly_range t t'"
proof (rule encl_poly_rangeI)
  have "0 < t \<or> t = 0" using assms(1) by auto
  moreover
  { assume "0 < t"
    hence "enclosing_polygon t t' =  
        [(point1 t t', point2 t t'), (point2 t t', point3 t t'), (point3 t t', point4 t t'), 
         (point4 t t', point5 t t'), (point5 t t', point6 t t'), (point6 t t', point1 t t')]"
      unfolding enclosing_polygon_def by auto
    moreover have "encompasses (x, y) (point1 t t', point2 t t')"
      using encompasses_pt12[OF _ assms(2-7)] assms(1) by auto
    moreover have "encompasses (x, y) (point2 t t', point3 t t')"
      using encompasses_pt23[OF _ assms(2-4) assms(6-7)] assms(1) by auto
    moreover have "encompasses (x, y) (point3 t t', point4 t t')"
      using encompasses_pt34[OF _ assms(2) assms(6)] assms(1) by auto
    moreover have "encompasses (x, y) (point4 t t', point5 t t')"
      using encompasses_pt45[OF _ assms(2-4) assms(6) assms(8)] assms(1) by auto
    moreover have "encompasses (x, y) (point5 t t', point6 t t')"
      using encompasses_pt56[OF _ assms(2-6) assms(8)] assms(1) by auto
    moreover have "encompasses (x, y) (point6 t t', point1 t t')"
      using encompasses_pt61[OF `0 < t` assms(2) assms(5)] by auto
    ultimately have "list_all (encompasses (x, y)) (enclosing_polygon t t')"
      by auto }
  moreover
  { assume "t = 0"
    hence "enclosing_polygon t t' =  
        [(point1 t t', point2 t t'), (point2 t t', point3 t t'), (point3 t t', point4 t t'), 
         (point4 t t', point5 t t'), (point5 t t', point1 t t')]"
      unfolding enclosing_polygon_def by auto
    moreover have "encompasses (x, y) (point1 t t', point2 t t')"
      using encompasses_pt12[OF _ assms(2-7)] assms(1) by auto
    moreover have "encompasses (x, y) (point2 t t', point3 t t')"
      using encompasses_pt23[OF _ assms(2-4) assms(6-7)] assms(1) by auto
    moreover have "encompasses (x, y) (point3 t t', point4 t t')"
      using encompasses_pt34[OF _ assms(2) assms(6)] assms(1) by auto
    moreover have "encompasses (x, y) (point4 t t', point5 t t')"
      using encompasses_pt45[OF _ assms(2-4) assms(6) assms(8)] assms(1) by auto
    moreover have "encompasses (x, y) (point5 t t', point1 t t')"
    proof -
      have "encompasses (x, y) (point5 t t', point6 t t')"
        using encompasses_pt56[OF _ assms(2-6) assms(8)] assms(1) by auto    
      moreover have "point6 t t' = point1 t t'" unfolding `t = 0` point6_def' point1_def' by auto
      ultimately show ?thesis by auto
    qed
    ultimately have "list_all (encompasses (x, y)) (enclosing_polygon t t')"
      by auto }
  ultimately show " list_all (\<lambda>seg. 0 \<le> det3 (fst seg) (snd seg) (x, y)) (enclosing_polygon t t')"
    by auto
qed

lemma (in pos_ori_inv_occupancy) encompasses_pt23':
  assumes "0 \<le> t" and "t < t'"
  assumes "fst (point2 t t') \<le> x" and "x \<le> fst (point3 t t')"
  assumes "snd (point2 t t') \<le> y" 
  shows "encompasses (x, y) (point2 t t', point3 t t')"
proof -
  have "\<exists>k. 0 < k \<and> fst (point3 t t') = fst (point2 t t') + k"
    apply (rule exI[where x="fst (point3  t t') - fst (point2 t t')"])
    using point2x_lt_point3x[OF assms(1-2)] by auto
  then obtain l where "0 < l" and 0: "fst (point3 t t') = fst (point2 t t') + l"
    by auto
  have "snd (point3 t t') \<le> y"
    using assms(5) unfolding point2_and_3_same_y by auto
  hence "\<exists>l. 0 \<le> l \<and> y = snd (point3 t t') + l"
    by (auto intro: exI[where x="y - snd (point3 t t')"])
  then obtain k where "0 \<le> k" and 1: "y = snd (point3 t t') + k"
    by auto
  show ?thesis using det3_aux2'[OF `0 < l` `0 \<le> k`, of "fst (point2 t t')" "snd (point2 t t')" "x"] 
    using 0 1 point2_and_3_same_y[of "t" "t'"] surjective_pairing[of "point2 t t'"] 
    surjective_pairing[of "point3 t t'"] by auto
qed

lemma (in pos_ori_inv_occupancy) encompasses_pt34':
  assumes "0 \<le> t" and "t < t'"
  assumes "x \<le> fst (point3 t t')"
  shows "encompasses (x, y) (point3 t t', point4 t t')"  
proof -
  have "snd (point3 t t') < snd (point4 t t')"
    unfolding point3_def' point4_def' using assms a_max_neq_zero by auto
  hence "\<exists>l. 0 < l \<and> snd (point4 t t') = snd (point3 t t') + l"
    by (auto intro: exI[where x="snd (point4 t t') - snd (point3 t t')"])
  then obtain l where "0 < l" and 0: "snd (point4 t t') = snd (point3 t t') + l"
    by auto
  have "\<exists>k. 0 \<le> k \<and> x = fst (point3 t t') - k"
    using assms(3) by (auto intro:exI[where x="fst (point3 t t') - x"])
  then obtain k where "0 \<le> k" and 1: "x = fst (point3 t t') - k"
    by auto
  show ?thesis using det3_aux3'[OF `0 < l` `0 \<le> k`, of "fst (point3 t t')" "snd (point3 t t')" "y"]
    using 0 1 point3_and_4_same_x[of "t" "t'"] surjective_pairing[of "point3 t t'"]
    surjective_pairing[of "point4 t t'"] by auto
qed

lemma (in pos_ori_inv_occupancy) encompasses_pt45':
  assumes "0 \<le> t" and "t < t'"
  assumes "y \<le> snd (point5 t t')"
  shows "encompasses (x, y) (point4 t t', point5 t t')"
proof -
  have "fst (point5 t t') < fst (point4 t t')"
    unfolding point5_def' point4_def' b_x_def' using polygon_aux[OF assms(1-2)] by auto
  then obtain l where "0 < l" and 0: "fst (point5 t t') = fst (point4 t t') - l"
    using obt_neg_diff by auto
  have 1: "snd (point4 t t') = snd (point5 t t')"
    unfolding point4_def' point5_def' by auto
  show ?thesis using det3_aux4'[OF `0 < l` assms(3), of "fst (point4 t t')" "x"]
    using 0 1 using surjective_pairing[of "point4 t t'"] surjective_pairing[of "point5 t t'"]
    by auto
qed

lemma (in pos_ori_inv_occupancy) encompasses_pt61':
  assumes "0 < t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point2 t t') \<le> x"
  shows "encompasses (x, y) (point6 t t', point1 t t')"
proof -
  have 1: "snd (point1 t t') < snd (point6 t t')"
    unfolding point1_def' point6_def' using a_max_neq_zero assms unfolding power2_eq_square 
    by (auto simp add:field_simps)
  have "fst (point1 t t') < fst (point2 t t')" using point6x_lt_point5x[OF _ assms(2-4)] assms(1) 
    unfolding point1_and_6_same_x point2_and_5_same_x by auto
  hence "fst (point6 t t') < x"
    unfolding point1_and_6_same_x using assms(5) by auto
  show ?thesis using det3_aux7[OF `fst (point6 t t') < x` 1, of "y"] 
    using point1_and_6_same_x[of "t" "t'"] surjective_pairing[of "point6 t t'"]
    surjective_pairing[of "point1 t t'"] by auto
qed

lemma (in pos_ori_inv_occupancy) encompasses_12':
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point2 t t') \<le> x"
  assumes "snd (point2 t t') \<le> y"
  shows "encompasses (x, y) (point1 t t', point2 t t')"
proof -
  have 0: "fst (point1 t t') \<le> fst (point2 t t')" using point6x_lt_point5x[OF assms(1-4)]
    unfolding point1_and_6_same_x point2_and_5_same_x by auto
  have "t\<^sup>2 < t'\<^sup>2" unfolding power2_eq_square 
    using mult_strict_mono[OF `t < t'` `t < t'` _ `0 \<le> t`] assms by auto
  hence 1: "snd (point2 t t') < snd (point1 t t')" unfolding point1_def' point2_def'
    using `t < t'` `0 \<le> t` a_max_neq_zero by (auto simp add:power2_eq_square)
  have "\<exists>k. 0 \<le> k \<and> x = fst (point2 t t') + k" 
    using assms(5) by (auto intro:exI[where x="x - fst (point2 t t')"])
  then obtain k where "0 \<le> k" and 2: "x = fst (point2 t t') + k"
    by auto
  show ?thesis using det3_aux1''[OF 1 `0 \<le> k` assms(6) 0]
    using 2 by auto
qed

lemma (in pos_ori_inv_occupancy) encompasses_56':
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point2 t t') \<le> x"
  assumes "y \<le> snd (point5 t t')"
  shows "encompasses (x, y) (point5 t t', point6 t t')"
proof -  
  have "\<exists>l. 0 \<le> l \<and> fst (point2 t t') = x - l" using `fst (point2 t t') \<le> x`
    by (auto intro: exI[where x="x - fst (point2 t t')"])
  then obtain l where "0 \<le> l" and 0: "fst (point2 t t') = x - l"
    by auto
  have "fst (point6 t t') < fst (point5 t t')"
    using point6x_lt_point5x[OF assms(1-4)] by auto
  hence 2: "fst (point6 t t') \<le> x - l" using 0 unfolding point2_and_5_same_x by auto
  have 3: "snd (point6 t t') < snd (point5 t t')"
    using power2_mono_lt[OF assms(2) assms(1)] unfolding point6_def' point5_def' power2_eq_square 
    using assms a_max_neq_zero  by (auto)  
  have "0 \<le> det3 (x, y) (fst (point5 t t', point6 t t')) (snd (point5 t t', point6 t t'))" 
    using det3_aux4''[OF `0 \<le> l` 3 assms(6) 2] 0 unfolding point2_and_5_same_x 
    using surjective_pairing[of "point5 t t'"]  surjective_pairing[of "point6 t t'"] by auto
  thus ?thesis using det3_rotate by auto
qed

theorem (in pos_ori_inv_occupancy) alt_def_encl_poly2:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "fst (point2 t t') \<le> x" and "x \<le> fst (point3 t t')"
  assumes "snd (point2 t t') \<le> y" and "y \<le> snd (point5 t t')"
  shows "(x, y) \<in> encl_poly_range t t'"
proof (rule encl_poly_rangeI)
  have "t = 0 \<or> 0 < t" using `0 \<le> t` by auto
  moreover
  { assume "t = 0"
    hence "enclosing_polygon t t' = [(point1 t t', point2 t t'), 
                                     (point2 t t', point3 t t'), 
                                     (point3 t t', point4 t t'), 
                                     (point4 t t', point5 t t'), 
                                     (point5 t t', point1 t t')]"
      unfolding enclosing_polygon_def by auto
  moreover have "encompasses (x, y) (point1 t t', point2 t t')"
    using encompasses_12'[OF assms(1-5) assms(7)]  by auto
  moreover have "encompasses (x, y) (point2 t t', point3 t t')"
    using encompasses_pt23'[OF assms(1-2) assms(5-7)]  by auto
  moreover have "encompasses (x, y) (point3 t t', point4 t t')"
    using encompasses_pt34'[OF assms(1-2) assms(6)] by auto
  moreover have "encompasses (x, y) (point4 t t', point5 t t')"
    using encompasses_pt45'[OF assms(1-2) assms(8)]  by auto
  moreover have "encompasses (x, y) (point5 t t', point1 t t')"
  proof -
    have "point6 t t' = point1 t t'" unfolding `t = 0` point6_def' point1_def' by auto
    thus ?thesis using encompasses_56'[OF assms(1-5) assms(8)] by auto
  qed
  ultimately have "list_all (encompasses (x, y)) (enclosing_polygon t t')"
    by auto }
  moreover
  { assume "0 < t"
    hence "enclosing_polygon t t' =  
      [(point1 t t', point2 t t'), (point2 t t', point3 t t'), (point3 t t', point4 t t'), 
       (point4 t t', point5 t t'), (point5 t t', point6 t t'), (point6 t t', point1 t t')]"
      unfolding enclosing_polygon_def by auto
  moreover have "encompasses (x, y) (point1 t t', point2 t t')"
    using encompasses_12'[OF assms(1-5) assms(7)]  by auto
  moreover have "encompasses (x, y) (point2 t t', point3 t t')"
    using encompasses_pt23'[OF assms(1-2) assms(5-7)]  by auto
  moreover have "encompasses (x, y) (point3 t t', point4 t t')"
    using encompasses_pt34'[OF assms(1-2) assms(6)]  by auto
  moreover have "encompasses (x, y) (point4 t t', point5 t t')"
    using encompasses_pt45'[OF assms(1-2) assms(8)]  by auto
  moreover have "encompasses (x, y) (point5 t t', point6 t t')"
    using encompasses_56'[OF assms(1-4) assms(5) assms(8)]  by auto
  moreover have "encompasses (x, y) (point6 t t', point1 t t')"
    using encompasses_pt61'[OF `0 < t` assms(2-5)] by auto
  ultimately have "list_all (encompasses (x, y)) (enclosing_polygon t t')"
    by auto }
  ultimately show "list_all (encompasses (x, y)) (enclosing_polygon t t')"
    by auto
qed

definition (in pos_ori_inv_occupancy) is_bounded where 
  "is_bounded p t t' = (if fst (point1 t t') \<le> fst p \<and> fst p \<le> fst (point2 t t') then 
                          lower_bnd1 t t' (fst p) \<le> snd p \<and> snd p \<le> upper_bnd1 t t' (fst p)
                        else if fst (point2 t t') < fst p \<and>  fst p \<le> fst (point3 t t') then
                          snd (point2 t t') \<le> snd p \<and> snd p \<le> snd (point5 t t')
                        else 
                          False)"

theorem (in pos_ori_inv_occupancy) encl_poly_rangeI2:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  shows "is_bounded (x, y) t t' \<Longrightarrow>(x, y) \<in> encl_poly_range t t'"
  using alt_def_encl_poly1[OF assms(1-4), of "x" "y"]  alt_def_encl_poly2[OF assms(1-4), of "x" "y"]
  unfolding is_bounded_def by (auto split:if_splits)

section "Proving boundary encloses all occupancy circles"

lemma (in pos_ori_inv_occupancy) range_circle_fst_at_least_point1:
  assumes "0 < t" and "t < t'" and "valid_time t" and "valid_time t'"
  shows "\<And>p \<tau>. t \<le> \<tau> \<Longrightarrow> \<tau> \<le> t' \<Longrightarrow> p \<in> range_of_circle (occupancy_circle \<tau>) \<Longrightarrow> fst (point1 t t') \<le> fst p"
proof -
  fix p \<tau>
  assume "t \<le> \<tau>" and "\<tau> \<le> t'" 
  have "valid_time \<tau>" using valid_in_between[OF _ assms(2-4) `t \<le> \<tau>` `\<tau> \<le> t'`]
    using `0 < t` by auto
  from `t \<le> \<tau>` have "vx * t \<le> vx * \<tau>" using mult_le_cancel_left_pos[of "vx" "t" "\<tau>"]
    using v\<^sub>x_wd vx_at_least_zero unfolding vx_def[THEN sym] by auto
  assume "p \<in> range_of_circle (occupancy_circle \<tau>)"
  with range_circle_decompose have "p \<in> left_half_range (occupancy_circle \<tau>) \<or>
                                    p \<in> right_half_range (occupancy_circle \<tau>)"
    by auto
  moreover
  { assume "p \<in> right_half_range (occupancy_circle \<tau>)"
    hence "vx * \<tau> \<le> fst p"
      unfolding right_half_range_def Let_def vx_def occupancy_circle_def pos_ori_inv_axioms
      by auto
    moreover have "0 \<le> 1 / 2 * a_max * t\<^sup>2" using a_max_neq_zero `0 < t` by auto
    ultimately have "vx * \<tau> - 1 / 2 * a_max * t\<^sup>2 \<le> fst p" using `vx * \<tau> \<le> fst p` 
      by auto
    hence "fst (point1 t t') \<le> fst p"
      unfolding point1_def' using `vx * t \<le> vx * \<tau>` by auto }
  moreover
  { assume "p \<in> left_half_range (occupancy_circle \<tau>)"
    hence "vx * \<tau> - \<bar>1/2 * a_max * \<tau>\<^sup>2\<bar> \<le> fst p"
      unfolding left_half_range_def occupancy_circle_def pos_ori_inv_axioms vx_def[THEN sym] Let_def 
      by auto
    moreover have "\<bar>1/2 * a_max * \<tau>\<^sup>2\<bar> = 1/2 * a_max * \<tau>\<^sup>2"
      using a_max_neq_zero `0 < t` `t \<le> \<tau>` by auto
    ultimately have "vx * \<tau> - 1/2 * a_max * \<tau>\<^sup>2 \<le> fst p"
      by auto
    hence "fst (point1 t t') \<le> fst p"
      unfolding point1_def' using centre_non_decreasing[OF `t \<le> \<tau>` assms(3) `valid_time \<tau>`]
      by auto }
  ultimately show "fst (point1 t t') \<le> fst p" 
    by auto
qed

lemma (in pos_ori_inv_occupancy) range_circle_fst_at_most_point3:
  assumes "0 < t" and "t < t'" and "valid_time t" and "valid_time t'"
  shows "\<And>p \<tau>. t \<le> \<tau> \<Longrightarrow> \<tau> \<le> t' \<Longrightarrow> p \<in> range_of_circle (occupancy_circle \<tau>) \<Longrightarrow> fst p \<le> fst (point3 t t')"
proof -
  fix p \<tau>
  assume "t \<le> \<tau>"
  assume "\<tau> \<le> t'"
  assume "p \<in> range_of_circle (occupancy_circle \<tau>)"
  hence "p \<in> left_half_range (occupancy_circle \<tau>) \<or> p \<in> right_half_range (occupancy_circle \<tau>)"
    using range_circle_decompose by auto
  moreover
  { assume "p \<in> left_half_range (occupancy_circle \<tau>)"
    hence "fst p \<le> vx * \<tau>"
      unfolding left_half_range_def Let_def occupancy_circle_def pos_ori_inv_axioms 
      vx_def[THEN sym] by auto
    also have "... \<le> vx * t'" using mult_left_mono[OF `\<tau> \<le> t'` vx_at_least_zero]
      unfolding vx_def[THEN sym] by auto
    also have "... \<le> vx * t' + 1/2 * a_max * t'\<^sup>2"
      using `\<tau> \<le> t'` vx_at_least_zero a_max_neq_zero `0 < t` `t < t'` power2_mono_lt[OF `t < t'`]
      by (auto simp add:field_simps divide_simps)
    finally have "fst p \<le> vx * t' + 1/2 * a_max * t'\<^sup>2"
      by auto }
  moreover
  { assume "p \<in> right_half_range (occupancy_circle \<tau>)"
    hence "fst p \<le> vx * \<tau> + 1/2 * a_max * \<tau>\<^sup>2"
      unfolding right_half_range_def Let_def unfolding occupancy_circle_def pos_ori_inv_axioms
      using a_max_neq_zero unfolding vx_def[THEN sym] by auto
    moreover have "vx * \<tau> \<le> vx * t'"
      using vx_at_least_zero mult_left_mono[OF `\<tau> \<le> t'` vx_at_least_zero] unfolding vx_def[THEN sym]
      by auto
    moreover have "1/2 * a_max * \<tau>\<^sup>2 \<le> 1/2 * a_max * t'\<^sup>2"
      using mult_left_mono[of "\<tau>\<^sup>2" "t'\<^sup>2" "1/2 * a_max"] a_max_neq_zero abs_le_square_iff[of "\<tau>" "t'"]
      `0 < t` `t \<le> \<tau>` `\<tau> \<le> t'` by auto
    ultimately have "fst p \<le> vx * t' + 1/2 * a_max * t'\<^sup>2"
      by auto }
  ultimately have "fst p \<le> vx * t' + 1/2 * a_max * t'\<^sup>2" by auto
  thus "fst p \<le> fst (point3 t t')"
    unfolding point3_def' by auto
qed

lemma line_intersect_aux1: 
  fixes x1 x2 y1 y2 y3 y4 :: real
  assumes "x1 < x2" and "y1 < y2" and "y3 < y4"
  defines "m1 \<equiv> (y3 - y1) / (x2 - x1)"
  defines "m2 \<equiv> (y4 - y2) / (x2 - x1)"
  defines "f1 \<equiv> \<lambda>x. m1 * (x - x1) + y1"
  defines "f2 \<equiv> \<lambda>x. m2 * (x - x1) + y2"
  shows "\<forall>x. x1 \<le> x \<and> x \<le> x2 \<longrightarrow>  f1 x \<noteq> f2 x"
proof (rule ccontr)
  assume "\<not> (\<forall>x. x1 \<le> x \<and> x \<le> x2 \<longrightarrow>  f1 x \<noteq> f2 x)"
  then obtain x where "x1 \<le> x" and "x \<le> x2" and "f1 x = f2 x"
    by auto
  have "m1 < m2 \<or> m1 = m2 \<or> m1 > m2" by linarith
  moreover
  { assume "m1 < m2"
    have "m1 * (x - x1) + y1 = m2 * (x - x1) + y2"
      using `f1 x = f2 x` unfolding f1_def f2_def by auto
    hence "(m1 - m2) * (x - x1) = y2 - y1" 
      by (auto simp add:field_simps)
    hence "x - x1 = (y2 - y1) / (m1 - m2)"
      using `m1 < m2` by (auto simp add:divide_simps field_simps)
    hence "x = x1 + (y2 - y1) / (m1 - m2)" 
      by auto
    moreover have "(y2 - y1) / (m1 - m2) < 0" 
      using `m1 < m2` `y1 < y2` by (auto simp add:divide_simps)
    ultimately have "x < x1" by auto
    hence "False"
      using `x1 \<le> x` by auto }
  moreover
  { assume "m2 < m1"
    have f1_def': "f1 = (\<lambda>x. m1 * (x - x2) + y3)" 
      unfolding f1_def m1_def using `x1 < x2` by (auto simp add:field_simps)
    have f2_def': "f2 = (\<lambda>x. m2 * (x - x2) + y4)"
      unfolding f2_def m2_def using `x1 < x2` by (auto simp add:field_simps)
    have "m1 * (x - x2) + y3 = m2 * (x - x2) + y4"
      using `f1 x = f2 x` unfolding f1_def' f2_def' by (auto)
    hence "(m1 - m2) * (x - x2) = (y4 - y3)"
      by (auto simp add:field_simps)
    hence "x - x2 = (y4 - y3) / (m1 - m2)"
      using `m2 < m1` by (auto simp add:field_simps divide_simps)
    hence "x = x2 + (y4 - y3) / (m1 - m2)"
      by auto
    moreover have "0 < (y4 - y3) / (m1 - m2)"
      using `y3 < y4` and `m1 > m2` by auto
    ultimately have "x2 < x" by auto
    hence "False"
      using `x \<le> x2` by auto }
  moreover
  { assume "m1 = m2" 
    have "m1 * (x - x1) + y1 = m2 * (x - x1) + y2"
      using `f1 x = f2 x` unfolding f1_def f2_def by auto
    hence "(m1 - m2) * (x - x1) = y2 - y1"
      by (auto simp add:field_simps)
    with `m1 = m2` have "y1 = y2" by auto
    with `y1 < y2` have "False" by auto }
  ultimately show "False" by auto
qed

lemma line_intersect_aux2:
  fixes x1 x2 y1 y2 y3 y4 :: real
  assumes "x1 < x2" and "y1 < y2" and "y3 < y4"
  defines "m1 \<equiv> (y3 - y1) / (x2 - x1)"
  defines "m2 \<equiv> (y4 - y2) / (x2 - x1)"
  defines "f1 \<equiv> \<lambda>x. m1 * (x - x1) + y1"
  defines "f2 \<equiv> \<lambda>x. m2 * (x - x1) + y2"
  shows "\<forall>x. x1 \<le> x \<and> x \<le> x2 \<longrightarrow>  f1 x < f2 x"
proof (rule ccontr)
  assume "\<not> (\<forall>x. x1 \<le> x \<and> x \<le> x2 \<longrightarrow>  f1 x < f2 x)"
  then obtain x where "x1 \<le> x" and "x \<le> x2" and "f1 x \<ge> f2 x"
    by auto
  hence "f1 x > f2 x" 
    using line_intersect_aux1[OF assms(1-3)] unfolding f1_def f2_def m1_def m2_def  by auto
  define g where "g = (\<lambda>x. f2 x - f1 x)"
  have "0 < g x1" and "0 \<le> g x1"
    unfolding g_def f2_def f1_def m1_def m2_def using `x1 < x2` `y1 < y2` 
    by (auto simp add:field_simps divide_simps)
  have "g x < 0" and "g x \<le> 0"
    using `f1 x > f2 x` unfolding g_def by auto
  have 0: "\<forall>xa. x1 \<le> xa \<and> xa \<le> x \<longrightarrow> isCont g xa"
    unfolding g_def f2_def f1_def by (auto intro: isCont_diff)
  then obtain xa where "x1 \<le> xa" and "xa \<le> x" and  "g xa = 0"
    using IVT2[where f="g" and b="x" and y ="0", OF `g x \<le> 0` `0 \<le> g x1` `x1 \<le> x` 0]
    by auto
  have "\<forall>x. x1 \<le> x \<and> x \<le> x2 \<longrightarrow> f1 x \<noteq> f2 x"
    using line_intersect_aux1[OF assms(1-3)] unfolding f1_def f2_def m1_def m2_def by auto
  thus "False" using `x1 \<le> xa` `xa \<le> x` `x \<le> x2` and `g xa = 0` unfolding g_def 
    by (auto simp add:field_simps)    
qed


lemma (in pos_ori_inv_occupancy) IVT_b_x:
  assumes "0 \<le> t" and "t \<le> 2/3 * t_max"
  shows "\<exists>t'. t \<le> t' \<and> t' \<le> t_max \<and> b_x t' = vx * t"
proof -
  have 0: "b_x t \<le> vx * t"
    unfolding b_x_def' using a_max_neq_zero vx_at_least_zero assms unfolding vx_def[THEN sym]
    by auto
  have "vx * t \<le> vx * 2/3 * t_max" 
    using mult_left_mono[OF assms(2) vx_at_least_zero] unfolding vx_def[THEN sym] by auto
  also have "... \<le> b_x t_max" (is "?lhs \<le> ?rhs")  
  proof -
    have "t_max\<^sup>2 * a_max\<^sup>2 / (2 * vx) \<le> 1 / 3 * vx" (is "?lhs1 \<le> ?rhs1")
      unfolding t_max_def' using vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym] 
      by (auto simp add:power_mult_distrib field_simps power2_eq_square)
    have "?lhs1 * t_max \<le> ?rhs1 * t_max"
      using mult_right_mono[OF `?lhs1 \<le> ?rhs1`] using t_max_gt_zero by auto
    hence "t_max\<^sup>3 * a_max\<^sup>2 / (2 * vx) \<le> 1/3 * vx * t_max"
      by (auto simp add:power_def field_simps)
    thus ?thesis unfolding b_x_def' 
      by (auto simp add:field_simps)
  qed
  finally have 1: "vx * t \<le> b_x t_max" by auto
  have 2: "t \<le> t_max" using assms(2) using t_max_gt_zero by auto
  have 3: "\<forall>x. t \<le> x \<and> x \<le> t_max \<longrightarrow> isCont b_x x"
    using DERIV_isCont[OF b_x_has_field_derivative_b_x'] by auto
  thus ?thesis using IVT[OF 0 1 2 3] by auto
qed

lemma (in pos_ori_inv_occupancy) 
  assumes "2 / 3 * t_max < t" and "t \<le> 73 / 81 * t_max"
  shows "\<exists>t'. t < t' \<and> t' \<le> t_max \<and> b_x t' < vx * t"
proof -
  have "{t <..< t + 8 / 81 * t_max} \<noteq> {}"
    using t_max_gt_zero by auto
  then obtain t' where 0: "t' \<in> {t <..< t + 8 / 81 * t_max}" 
    using ex_in_conv[where A="{t <..< t + 8 / 81 * t_max}"] by auto
  with assms(2) have "t' \<le> t_max" by auto
  hence "t' - t \<le> 8 / 81 * t_max" 
    using 0 by auto  
  also have "... < (t')\<^sup>3 * a_max\<^sup>2 / (2 * vx\<^sup>2)"
  proof -
    have "t < t'" using 0 by auto
    have "2 / 3 * t_max < t'"
      using assms(1) 0 by auto
    hence "(2/3 * t_max)\<^sup>2 < t'\<^sup>2"
      using power2_mono_lt[OF `2/3 * t_max < t'`] t_max_gt_zero by auto
    hence "(2/3 * t_max)\<^sup>2 * (2 / 3 * t_max) < t'\<^sup>2 * t'"
      using mult_le_less_imp_less[of "(2/3 * t_max)\<^sup>2" "t'\<^sup>2" "2/3 * t_max" "t'"]
      `2/3 * t_max < t'` t_max_gt_zero by auto
    hence "(2/3 * t_max)\<^sup>3 < t'\<^sup>3"
      unfolding power3_eq_cube power2_eq_square by auto
    hence "8 / 27 * t_max\<^sup>3 < t'\<^sup>3" 
      unfolding power3_eq_cube by auto
    hence "16 / 81 * sqrt (2 / 3) * vx\<^sup>3 / a_max\<^sup>3 < t'\<^sup>3" (is "?lhs < ?rhs")
      unfolding t_max_def' power3_eq_cube by (auto simp add:field_simps)
    moreover have "?lhs * a_max\<^sup>2 / (2 * vx\<^sup>2) = 8 / 81 * t_max"
      unfolding t_max_def' using a_max_neq_zero v\<^sub>x_wd vx_at_least_zero unfolding vx_def[THEN sym]
      by (auto simp add:field_simps divide_simps power_def)
    moreover have "?lhs * a_max\<^sup>2 / (2 *  vx\<^sup>2) < t'\<^sup>3 * a_max\<^sup>2 / (2 * vx\<^sup>2)"
      using `?lhs < ?rhs` mult_strict_right_mono[OF `?lhs < ?rhs`, of "a_max\<^sup>2 / (2 * vx\<^sup>2)"]
      a_max_neq_zero v\<^sub>x_wd vx_at_least_zero unfolding vx_def[THEN sym] by auto
    ultimately show ?thesis by auto
  qed
  finally have "t' - t < t'\<^sup>3 * a_max\<^sup>2 / (2 * vx\<^sup>2)" (is "?lhs < ?rhs")
    by auto
  hence "vx * ?lhs < vx * ?rhs"
    using mult_strict_left_mono[OF `?lhs < ?rhs`, of "vx"] v\<^sub>x_wd vx_at_least_zero unfolding vx_def[THEN sym]
    by auto
  also have "... = t'\<^sup>3 * a_max\<^sup>2 / (2 * vx)"
    using v\<^sub>x_wd vx_at_least_zero unfolding vx_def[THEN sym] by (auto simp add:power2_eq_square)
  finally have "vx * ?lhs < t'\<^sup>3 * a_max\<^sup>2 / (2 * vx)"
    by auto
  hence "b_x t' < vx * t"
    unfolding b_x_def' by (auto simp add:field_simps)
  with 0 and `t' \<le> t_max` show ?thesis by auto
qed

theorem (in pos_ori_inv_occupancy) ysup_occupancy_circle2: 
  assumes "0 < t" and "t < t'" and "t' < t_max"
  assumes "vx * t \<le> vx * \<tau>" and "vx * \<tau> \<le> fst (point2 t t')"
  shows "is_bounded (vx * \<tau>, 1/2 * a_max * \<tau>\<^sup>2) t t'"
proof -
  have "0 < vx" using v\<^sub>x_wd vx_at_least_zero unfolding vx_def[THEN sym] 
    by auto
  have "t \<le> \<tau>" 
    using `vx * t \<le> vx * \<tau>` mult_le_cancel_left_pos[of "vx" "t" "\<tau>"] v\<^sub>x_wd vx_at_least_zero
    unfolding vx_def[THEN sym] by auto
  hence "0 \<le> \<tau>" using assms(1) by auto
  have "\<tau> \<le> 2/3 * t_max"
  proof (rule ccontr)
    assume " \<not> \<tau> \<le> 2 / 3 * t_max "
    hence "2/3 * t_max < \<tau>" by auto
    hence "vx * 2/3 * t_max < vx * \<tau>" (is "?lhs1 < ?rhs1")
      using mult_strict_left_mono[OF `2/3 * t_max < \<tau>`] vx_at_least_zero v\<^sub>x_wd  unfolding 
      vx_def[THEN sym] by auto
    moreover have "?lhs1 = b_x t_max"
    proof -
      have "vx * 2/3 = vx - a_max\<^sup>2 / (2 * vx) * t_max\<^sup>2" (is "?lhs2 = ?rhs2")
        unfolding t_max_def' using a_max_neq_zero power2_eq_square[of "vx"] 
        by (auto simp add: power_mult_distrib divide_simps)
      hence "?lhs2 * t_max = ?rhs2 * t_max"
        by auto
      thus ?thesis unfolding b_x_def'
        by (auto simp add:power_def field_simps)
    qed
    ultimately have "b_x t_max < vx * \<tau>" by auto
    moreover have "b_x t' \<le> b_x t_max" using nondecreasing_lt_t_max2[of "t'" "t_max"]
      using assms(1-3) unfolding t_max_def by auto
    ultimately have "b_x t' < vx * \<tau>" by auto
    with assms(5) show "False" unfolding point2_def' by auto
  qed

  obtain \<tau>' where "b_x \<tau>' = vx * \<tau>" and "\<tau> \<le> \<tau>'" and "\<tau>' \<le> t_max"
    using IVT_b_x[OF `0 \<le> \<tau>` `\<tau> \<le> 2/3 * t_max`] by auto

  have "\<tau> = \<tau>' \<or> \<tau> < \<tau>'" using `\<tau> \<le> \<tau>'` by auto
  moreover
  { assume "\<tau> = \<tau>'"
    hence "b_x \<tau> = vx * \<tau>" using `b_x \<tau>' = vx * \<tau>` by auto
    hence "a_max\<^sup>2 / (2 * vx) * \<tau>\<^sup>3 = 0"  unfolding b_x_def' by auto
    hence "\<tau> = 0" using a_max_neq_zero vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym]
      by auto
    hence "t = 0" using assms(1) and `t \<le> \<tau>` by auto
    have *: "fst (point1 t t') \<le> vx * \<tau>"
      unfolding point1_def' `t = 0` `\<tau> = 0` by auto
    have "b_x t = 0" unfolding b_x_def' `t = 0` by auto
    hence "0 \<le> b_x t'" using nondecreasing_lt_t_max2[of "t" "t'"] using `t < t'` `t = 0`
      `t' < t_max` unfolding t_max_def by auto
    hence **: "vx * \<tau> \<le> fst (point2 t t')"
      unfolding point2_def' b_x_def' `\<tau> = 0` by auto
    have ***: "lower_bnd1 t t' (vx * \<tau>) \<le> 1/2 * a_max * \<tau>\<^sup>2"
      unfolding lower_bnd1_def Let_def point1_def' `\<tau> =0` `t = 0` by auto
    have ****: "1/2 * a_max * \<tau>\<^sup>2 \<le> upper_bnd1 t t' (vx * \<tau>)"
      unfolding upper_bnd1_def Let_def point6_def' `\<tau> = 0` `t = 0` by auto
    with * ** *** have "is_bounded (vx * \<tau>, 1/2 * a_max * \<tau>\<^sup>2) t t'"
      unfolding is_bounded_def by auto }
  moreover
  { assume "\<tau> < \<tau>'"
    with `0 \<le> \<tau>` have "0 < \<tau>'" and "\<tau>' \<noteq> 0" by auto

    have "2/3 \<le> \<tau> / \<tau>'"
    proof (rule ccontr)
      have "0 < \<tau>'" using `\<tau> < \<tau>'` and `0 \<le> \<tau>` by auto 
      assume "\<not> 2/3 \<le> \<tau> / \<tau>'"
      hence "\<tau> / \<tau>' < 2/3" by auto
      hence "\<tau> < 2/3 * \<tau>'" using `\<tau> < \<tau>'` `0 \<le> \<tau>` 
        by (auto simp add:field_simps)
      hence "-2/3 * \<tau>' < -\<tau>" by auto
      hence "\<tau>' - 2/3 * \<tau>' < \<tau>' - \<tau>" by auto
      moreover have "\<tau>' - \<tau> = a_max\<^sup>2 / (2 * vx\<^sup>2) * \<tau>'\<^sup>3"
        using `b_x \<tau>' = vx * \<tau>` unfolding b_x_def' using vx_at_least_zero v\<^sub>x_wd 
        unfolding vx_def[THEN sym] by (auto simp add:field_simps divide_simps power2_eq_square)
      ultimately have  "1/3 * \<tau>' < a_max\<^sup>2 / (2 * vx\<^sup>2) * \<tau>'\<^sup>3" (is "?lhs3 < ?rhs3")
        by (auto simp add:field_simps)
      hence  "?lhs3 / \<tau>' < ?rhs3 / \<tau>'"
        using divide_strict_right_mono[OF `?lhs3 < ?rhs3` `0 < \<tau>'`] by auto
      hence "1/3 < a_max\<^sup>2 / (2 * vx\<^sup>2) * \<tau>'\<^sup>2"
        using `0 < \<tau>'` by (auto simp add:field_simps divide_simps power_def)
      hence "2/3 * vx\<^sup>2 / a_max\<^sup>2 < \<tau>'\<^sup>2" (is "?lhs4 < ?rhs4")
        using v\<^sub>x_wd vx_at_least_zero unfolding vx_def[THEN sym] 
        by (auto simp add:field_simps divide_simps)
      hence "sqrt ?lhs4 < sqrt ?rhs4" using real_sqrt_less_mono[OF `?lhs4 < ?rhs4`] 
        by auto
      hence "t_max < \<tau>'"
        unfolding t_max_def' using vx_at_least_zero a_max_neq_zero unfolding vx_def[THEN sym]
        using `0 < \<tau>'` real_sqrt_divide real_sqrt_mult_distrib2 by auto
      with `\<tau>' \<le> t_max` show "False" by auto
    qed
    hence "1 \<le> \<tau> / \<tau>' + (\<tau> / \<tau>')\<^sup>2 + (\<tau> / \<tau>')\<^sup>3" (is "_ \<le> ?rhs5")
    proof -  
      have "4/9 \<le> (\<tau> / \<tau>')\<^sup>2" using mult_mono[OF `2/3 \<le> \<tau>/\<tau>'` `2/3 \<le> \<tau>/\<tau>'`]
        unfolding power2_eq_square using `2/3 \<le> \<tau> / \<tau>'`by linarith
      moreover hence "8/27 \<le> (\<tau> / \<tau>')\<^sup>3" using mult_mono[OF `4/9 \<le> (\<tau>/\<tau>')\<^sup>2` `2/3 \<le> \<tau>/\<tau>'`]
        using zero_le_power2 unfolding power3_eq_cube power2_eq_square
        by linarith
      ultimately show ?thesis 
        using `2/3 \<le> \<tau> / \<tau>'` by auto
    qed
    have "\<tau>'\<^sup>3 \<le> ?rhs5 * \<tau>'\<^sup>3"
      using mult_right_mono[OF `1 \<le> ?rhs5`, of "\<tau>'\<^sup>3"] using `\<tau> \<le> \<tau>'` `0 \<le> \<tau>`
      by auto
    hence "\<tau>'\<^sup>3 \<le> \<tau> * \<tau>'\<^sup>2 + \<tau>\<^sup>2 * \<tau>' + \<tau>\<^sup>3" (is "_ \<le> ?rhs6")using `0 < \<tau>'` unfolding power2_eq_square power3_eq_cube
      by (auto simp add:field_simps)
    hence "2 * \<tau>'\<^sup>3 \<le> ?rhs6 + \<tau>'\<^sup>3" (is "?lhs7 \<le> ?rhs7") by (auto)
    hence "?lhs7 * (\<tau>' - \<tau>) \<le> ?rhs7 * (\<tau>' - \<tau>)"
      using `\<tau> < \<tau>'` using mult_right_mono[OF `?lhs7 \<le> ?rhs7`] by auto
    moreover have "?lhs7 * (\<tau>' - \<tau>) = a_max\<^sup>2 / vx\<^sup>2 * \<tau>'\<^sup>3 * \<tau>'\<^sup>3"
    proof -
      have "vx * (\<tau>' - \<tau>) = a_max\<^sup>2 / (2 * vx) * \<tau>'\<^sup>3" 
        using `b_x \<tau>' = vx * \<tau>` unfolding b_x_def' by (auto simp add:field_simps)
      hence "2 * (\<tau>' - \<tau>) = a_max\<^sup>2 / vx\<^sup>2 * \<tau>'\<^sup>3"
        using vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym]
        by (auto simp add:field_simps divide_simps power2_eq_square)
      thus ?thesis by (algebra)
    qed
    ultimately have "a_max\<^sup>2 / vx\<^sup>2 * \<tau>'\<^sup>6 \<le> ?rhs7 * (\<tau>' - \<tau>)"
      by (auto simp add:power_def)
    also have "... = \<tau>'\<^sup>4 - \<tau>\<^sup>4" by (algebra)
    finally have "a_max\<^sup>2 / vx\<^sup>2 * \<tau>'\<^sup>6 \<le> \<tau>'\<^sup>4 - \<tau>\<^sup>4" (is "?lhs8 \<le> ?rhs8")
      by auto
    hence "?lhs8 * (1/4 * a_max\<^sup>2) \<le> ?rhs8 * (1/4 * a_max\<^sup>2)"
      using mult_right_mono[OF `?lhs8 \<le> ?rhs8`, of "1/4 * a_max\<^sup>2"] a_max_neq_zero by auto
    moreover have "?lhs8 * (1/4 * a_max\<^sup>2) = (a_max\<^sup>2 / (2 * vx) * \<tau>'\<^sup>3)\<^sup>2"
      unfolding power2_eq_square by (auto simp add:field_simps)
    ultimately have "(a_max\<^sup>2 / (2 * vx) * \<tau>'\<^sup>3)\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * (\<tau>'\<^sup>4 - \<tau>\<^sup>4)"
      by (auto simp add:field_simps)
    hence "1/4 * a_max\<^sup>2 * \<tau>\<^sup>4 \<le> 1/4 * a_max\<^sup>2 * \<tau>'\<^sup>4 - (a_max\<^sup>2 / (2 * vx) * \<tau>'\<^sup>3)\<^sup>2" (is "?lhs9 \<le> ?rhs9")
      by (auto simp add:field_simps)
    hence "sqrt ?lhs9 \<le> sqrt ?rhs9" using real_sqrt_le_mono by auto  
    moreover have "sqrt ?lhs9 = 1/2 * a_max * \<tau>\<^sup>2"
    proof -
      have *: "(1/2 * a_max * \<tau>\<^sup>2)\<^sup>2 = 1/4 * a_max\<^sup>2 * \<tau>\<^sup>4"
        unfolding power2_eq_square by algebra
      show ?thesis unfolding *[THEN sym] using a_max_neq_zero `0 \<le> \<tau>` by (auto)
    qed
    ultimately have "1/2 * a_max * \<tau>\<^sup>2 \<le> sqrt ?rhs9" by metis
    hence "1/2 * a_max * \<tau>\<^sup>2 \<le> b_y 1 \<tau>'"
      unfolding b_y_def using vx_at_least_zero v\<^sub>x_wd norm_Pair[of "fst v" "snd v"]
      surjective_pairing[of "v"] unfolding pos_ori_inv_axioms vx_def[THEN sym]
      by (auto simp add:field_simps)
    also have "... \<le> upper_bnd1 t t' (vx * \<tau>)"
    proof -
      have "the_inv_into {0 .. t_max} b_x (vx * \<tau>) = \<tau>'"
        using `b_x \<tau>' = vx * \<tau>` the_inv_into_f_f[OF b_x_inj_on, of "\<tau>'"] `0 < \<tau>'` `\<tau>' \<le> t_max`
        by auto
      hence "b_y 1 \<tau>' = f_of_x 1 (vx * \<tau>)"
        unfolding f_of_x_def by auto
      define slope0 where "slope0 \<equiv> secant_slope 1 (b_x t) (vx * \<tau> - b_x t)"
      have "f_of_x 1 (b_x t) = b_y 1 t"
        unfolding f_of_x_def
        using the_inv_into_f_f[OF b_x_inj_on, of "t"] assms(1-3) by auto
      hence *: "f_of_x 1 (vx * \<tau>) = slope0 * (vx * \<tau> - b_x t) + b_y 1 t"
        unfolding slope0_def secant_slope_def by (auto simp add:field_simps divide_simps)
      define slope1 where "slope1 \<equiv> secant_slope 1 (b_x t) (b_x t' - b_x t)"
      have 0: "b_x t \<in> {b_x 0 <..< b_x t_max}"
        using `0 < t` `t < t'` `t' < t_max` b_x_range_open[of "0" "t_max"] using t_max_gt_zero
        by auto
      have 1: "0 < vx * \<tau> - b_x t"
      proof -
        have "0 \<le> vx * (\<tau> - t)"
          using `0 < vx` `t \<le> \<tau>` by auto
        moreover have "0 < t\<^sup>3 * a_max\<^sup>2 / (2 * vx)"
          using `0 < t` a_max_neq_zero `0 < vx` by auto
        ultimately show ?thesis
          unfolding b_x_def' by (auto simp add:field_simps)
      qed
      have 2: "0 < b_x t' - b_x t"
      proof -
        have "b_x t < b_x t'" 
          using increasing_lt_t_max2[of "t" "t'"] using `0 < t` `t < t'` `t' < t_max`
          unfolding t_max_def by auto
        thus ?thesis by auto
      qed
      have 3: "b_x t + (vx * \<tau> - b_x t) \<in> {b_x 0 <..< b_x t_max}"
      proof -
        have "0 < vx * \<tau>" using `0 < vx` and `0 < t` and `t \<le> \<tau>` by auto
        moreover have "b_x 0 = 0" unfolding b_x_def by auto
        ultimately have 0: "b_x 0 < vx * \<tau>" by auto

        have "vx * \<tau> \<le> b_x t'" 
          using `vx * \<tau> \<le> fst (point2 t t')`
          unfolding  point2_def' by auto
        also have "... < b_x t_max"
          using `t' < t_max` using increasing_lt_t_max2[of "t'" "t_max"] t_max_gt_zero
          unfolding t_max_def[THEN sym] using `0 < t` and `t < t'` by auto
        finally have "vx * \<tau>  < b_x t_max" by auto
        with 0 have "vx * \<tau> \<in> {b_x 0 <..< b_x t_max}" by auto
        thus ?thesis by auto
      qed
      have 4: "b_x t + (b_x t' - b_x t) \<in> {b_x 0 <..< b_x t_max}"
      proof -
        have "b_x 0 < b_x t'" 
          using increasing_lt_t_max2[of "0" "t'"] t_max_gt_zero unfolding t_max_def[THEN sym]
          using `t' < t_max` `0 < t` `t < t'` by auto
        moreover have "b_x t' < b_x t_max"
          using increasing_lt_t_max2[of "t'" "t_max"] t_max_gt_zero unfolding t_max_def[THEN sym]
          using `0 < t` `t < t'` `t' < t_max` by auto
        ultimately show ?thesis by auto
      qed
      have 5: "vx * \<tau> - b_x t \<le> b_x t' - b_x t"
        using `vx * \<tau> \<le> fst (point2 t t')` unfolding point2_def' by auto
      have "secant_slope 1 (b_x t) (vx * \<tau> - b_x t) \<le> secant_slope 1 (b_x t) (b_x t' - b_x t)"
        using secant_slope_nondecreasing_aux[OF 0 3 4 1 2 5 `0 < vx`] .
      hence "slope0 \<le> slope1" unfolding slope0_def slope1_def by auto
      have "f_of_x 1 (vx * \<tau>)  \<le> slope1 * (vx * \<tau> - b_x t) + b_y 1 t"
      proof -
        have "slope0 * (vx * \<tau> - b_x t) \<le> slope1 * (vx * \<tau> - b_x t)" (is "?lhs \<le> ?rhs")
          using mult_right_mono[OF `slope0 \<le> slope1`, of "(vx * \<tau> - b_x t)"] 1 by auto
        hence "?lhs + b_y 1 t \<le> ?rhs + b_y 1 t" by auto
        thus ?thesis unfolding * by auto
      qed
      also have "... \<le> upper_bnd1 t t' (vx * \<tau>)"
      proof -
        have "slope1 = (f_of_x 1 (b_x t') - f_of_x 1 (b_x t)) / (b_x t' - b_x t)"
          unfolding slope1_def secant_slope_def by auto
        moreover have "the_inv_into {0 .. t_max} b_x (b_x t') = t'"
          using the_inv_into_f_f[OF b_x_inj_on, of "t'"] `0 < t` `t < t'` `t' < t_max`
          t_max_gt_zero by auto
        moreover have "the_inv_into {0 .. t_max} b_x (b_x t) = t"
          using the_inv_into_f_f[OF b_x_inj_on, of "t"] using `0 < t` `t < t'` `t' < t_max`
          t_max_gt_zero by auto
        ultimately have slope1_alt_def: "slope1 = (b_y 1 t' - b_y 1 t) / (b_x t' - b_x t)"
          unfolding f_of_x_def by auto
        have "b_x t < b_x t'" 
          using increasing_lt_t_max2[of "t" "t'"] using `0 < t` `t < t'` `t' < t_max` 
          unfolding t_max_def by auto
        have *: "b_y 1 t < upper_bnd1 t t' (b_x t)" 
        proof -
          have "norm v = vx"
            using norm_Pair[of "fst v" "snd v"] surjective_pairing[of "v"]
            unfolding pos_ori_inv_axioms vx_def[THEN sym] using vx_at_least_zero by (auto)
          hence "t \<le> vx / a_max"
            using `t < t'` `t' < t_max` t_max_lt_t_stop by auto
          hence "fst (point6 t t') \<le> b_x t"
            using b_x_at_least_fst_point6 by auto            
          have "b_y 1 t < 1/2 * a_max * t\<^sup>2"
            using b_y_lt_radius[OF `0 < t` `0 < vx`] by auto
          also have "... = snd (point6 t t')"
            unfolding point6_def' by auto
          also have "... = upper_bnd1 t t' (fst (point6 t t'))"
            unfolding upper_bnd1_def Let_def by auto
          also have "... \<le> upper_bnd1 t t' (b_x t)"
            using upper_bnd1_nondecreasing[of "t" "t'" "fst (point6 t t')" "b_x t", OF _ `t < t'`] `0 < t` `t' < t_max`
            valid_timeI[of "t"] t_max_lt_t_stop valid_timeI[of "t'"] `t < t'` `fst (point6 t t') \<le> b_x t`
            by auto
          finally show ?thesis by auto
        qed
        have "0 \<le> t" and "valid_time t" and "valid_time t'"
          using valid_timeI[of "t"] valid_timeI[of "t'"] `0 < t` `t < t'` `t' < t_max`
          t_max_lt_t_stop by auto
        have slope_alt_def: "slope t t' = (1/2 * a_max * t'\<^sup>2 -  upper_bnd1 t t' (b_x t)) / (b_x t' - b_x t)"
        proof -
          have "1/2 * a_max * t'\<^sup>2 = snd (point5 t t')" and "b_x t' = fst (point5 t t')" 
            unfolding point5_def' by auto
          have "b_x t < fst (point5 t t')"
            using b_x_at_most_fst_point5[of "t" "t'"] using `0 < t` `t < t'` `t' < t_max`
            by auto
          hence "slope t t' = (snd (point5 t t') - (slope t t' * (b_x t - fst (point5 t t')) + snd (point5 t t'))) / (fst (point5 t t') - b_x t)"
            by (auto simp add:field_simps divide_simps)
          thus ?thesis
            unfolding upper_bnd1_def'[OF `0 \<le> t` `t < t'` `valid_time t` `valid_time t'`] Let_def 
            point5_def' by auto
        qed
        have xxx: "(\<lambda>x. slope t t' * (x - b_x t) + upper_bnd1 t t' (b_x t)) = upper_bnd1 t t'"
          unfolding upper_bnd1_def Let_def by (auto simp add:field_simps)   
        hence xxx': "slope t t' * (vx * \<tau> - b_x t) + upper_bnd1 t t' (b_x t) = upper_bnd1 t t' (vx * \<tau>)"
          by metis
        from line_intersect_aux2[of "b_x t" "b_x t'" "b_y 1 t" "upper_bnd1 t t' (b_x t)" "b_y 1 t'" "1/2 * a_max * t'\<^sup>2", 
                                OF `b_x t < b_x t'` * b_y_lt_radius ]
        have "\<forall>x. b_x t \<le> x \<and> x \<le> b_x t' \<longrightarrow>
          slope1 * (x - b_x t) + b_y 1 t < slope t t' * (x - b_x t) + upper_bnd1 t t' (b_x t)"
          unfolding slope1_alt_def slope_alt_def using `0 < t` `t < t'` `0 < vx` 
          by auto
        moreover have "b_x t \<le> vx * \<tau>"
        proof -
          have "vx * t \<le> vx * \<tau>"
            using `t \<le> \<tau>` `0 < vx` by auto
          moreover have "0 < t\<^sup>3 * a_max\<^sup>2 / (2 * vx)" using `0 < t` a_max_neq_zero `0 < vx`
            by auto
          ultimately show ?thesis
            unfolding b_x_def' by auto
        qed
        moreover have "vx * \<tau> \<le> b_x t'"
          using `vx * \<tau> \<le> fst (point2 t t')` unfolding point2_def' by auto
        ultimately show ?thesis
          unfolding  Let_def using xxx' by (auto)
      qed
      finally show ?thesis 
        using `b_y 1 \<tau>' = f_of_x 1 (vx * \<tau>)` by auto
    qed
    finally have "1/2 * a_max * \<tau>\<^sup>2 \<le> upper_bnd1 t t' (vx * \<tau>)"
      by auto
    moreover have "fst (point1 t t') \<le> vx * \<tau>"      
    proof -
      have "vx * t \<le> vx * \<tau>"
        using `0 < vx` `t \<le> \<tau>` by (auto)
      moreover have "0 < 1/2 * a_max * t\<^sup>2"
        using a_max_neq_zero zero_less_power2[of "t"] `0 < t` by auto
      ultimately show ?thesis
        unfolding point1_def' by auto
    qed
    moreover have "vx * \<tau> \<le> fst (point2 t t')"
      using assms(5) by auto
    moreover have "lower_bnd1 t t' (vx * \<tau>) \<le> 1/2 * a_max * \<tau>\<^sup>2"
    proof -
      have "valid_time t" and "valid_time t'"
        using valid_timeI `0 < t` `t < t'` `t' < t_max` t_max_lt_t_stop by auto
      have "lower_bnd1 t t' (vx * \<tau>) \<le> 0"
        using lower_bnd1_at_most_zero[of "t" "t'", OF _ `t < t'` `valid_time t` `valid_time t'`] 
        `0 < t` `fst (point1 t t') \<le> vx * \<tau>` by auto
      also have "... \<le> 1/2 * a_max * \<tau>\<^sup>2"
        using a_max_neq_zero zero_le_power2[of "\<tau>"] by auto
      finally show ?thesis by auto
    qed
    ultimately have "is_bounded (vx * \<tau>, 1/2 * a_max * \<tau>\<^sup>2) t t'"
      unfolding is_bounded_def by auto }
  ultimately show ?thesis by auto
qed

lemma (in pos_ori_inv_occupancy) b_x_t_max_vx:
  "b_x t_max = vx * 2/3 * t_max"
proof -
  have "b_x t_max = vx * t_max - a_max\<^sup>2 * t_max\<^sup>3 / (2 * vx)"
    unfolding b_x_def' by auto
  also have "... = sqrt (2/3) * vx\<^sup>2 / a_max - a_max\<^sup>2 * 2/3 * sqrt (2/3) * vx\<^sup>3 / a_max\<^sup>3 / (2 * vx)"
    (is "_ = ?term0 - ?term1")
    unfolding t_max_def' by (auto simp add:field_simps power3_eq_cube power2_eq_square)
  finally have "b_x t_max = ?term0 - ?term1"
    by auto
  moreover have "?term1 = 1/3 * sqrt (2/3) * vx\<^sup>2 / a_max" (is "_ = ?term2")
    using a_max_neq_zero by (auto simp add:field_simps power3_eq_cube power2_eq_square)
  ultimately have "b_x t_max = ?term0 - ?term2"
    by auto
  also have "... = 2/3 * sqrt (2/3) * vx\<^sup>2 / a_max"
    using v\<^sub>x_wd unfolding vx_def[THEN sym] by (auto simp add:field_simps)
  finally show ?thesis unfolding t_max_def' 
    by (auto simp add:field_simps power2_eq_square)
qed

theorem (in pos_ori_inv_occupancy) b_y_leq_upper_bnd1:
  assumes "0 < t" and "t < t'" and "t' < t_max"
  assumes "t \<le> \<tau>'" and "\<tau>' \<le> t'"  
  assumes "b_x \<tau>' = vx * \<tau>"
  shows "b_y 1 \<tau>' \<le> upper_bnd1 t t' (vx * \<tau>)"
proof (cases "b_x t \<noteq> vx * \<tau>")
  case True
  have "0 < vx"
    using vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym] by auto
  have "0 < \<tau>'" and "\<tau>' \<le> t_max"
    using assms by auto    
  have "vx * \<tau> \<le> fst (point2 t t')"
    using `b_x \<tau>' = vx * \<tau>` nondecreasing_lt_t_max2[of "\<tau>'" "t'"]  assms 
    unfolding t_max_def point2_def' by auto
  have "the_inv_into {0 .. t_max} b_x (vx * \<tau>) = \<tau>'"
    using `b_x \<tau>' = vx * \<tau>` the_inv_into_f_f[OF b_x_inj_on, of "\<tau>'"] `0 < \<tau>'` `\<tau>' \<le> t_max`
    by auto
  hence "b_y 1 \<tau>' = f_of_x 1 (vx * \<tau>)"
    unfolding f_of_x_def by auto
  define slope0 where "slope0 \<equiv> secant_slope 1 (b_x t) (vx * \<tau> - b_x t)"
  have "f_of_x 1 (b_x t) = b_y 1 t"
    unfolding f_of_x_def
    using the_inv_into_f_f[OF b_x_inj_on, of "t"] assms(1-3) by auto
  hence *: "f_of_x 1 (vx * \<tau>) = slope0 * (vx * \<tau> - b_x t) + b_y 1 t"
    unfolding slope0_def secant_slope_def by (auto simp add:field_simps divide_simps)
  define slope1 where "slope1 \<equiv> secant_slope 1 (b_x t) (b_x t' - b_x t)"
  have 0: "b_x t \<in> {b_x 0 <..< b_x t_max}"
    using `0 < t` `t < t'` `t' < t_max` b_x_range_open[of "0" "t_max"] using t_max_gt_zero
    by auto
  have 1: "0 < vx * \<tau> - b_x t"
  proof -
    have "b_x t \<le> b_x \<tau>'"
      using nondecreasing_lt_t_max2[of "t" "\<tau>'"] using `t \<le> \<tau>'` `0 < t` `t < t'` `t' < t_max`
      `\<tau>' \<le> t'` unfolding t_max_def by auto
    also have "... = vx * \<tau>"
      using `b_x \<tau>' = vx* \<tau>` by auto
    finally have "b_x t \<le> vx * \<tau>"
      by auto
    with True show ?thesis 
      by auto
  qed
  hence "b_x t \<le> vx * \<tau>"
    by auto
  have 2: "0 < b_x t' - b_x t"
  proof -
    have "b_x t < b_x t'" 
      using increasing_lt_t_max2[of "t" "t'"] using `0 < t` `t < t'` `t' < t_max`
      unfolding t_max_def by auto
    thus ?thesis by auto
  qed
  have 3: "b_x t + (vx * \<tau> - b_x t) \<in> {b_x 0 <..< b_x t_max}"
  proof -
    have "b_x 0 < b_x \<tau>'"
      using `0 < t` `t \<le> \<tau>'` increasing_lt_t_max2[of "0" "t"] `t < t'` `t' < t_max`
      nondecreasing_lt_t_max2[of "t" "\<tau>'"] `\<tau>' \<le> t'` unfolding t_max_def `0 < t` `t \<le> \<tau>'`
      by auto
    moreover have "b_x \<tau>' < b_x t_max"
      using nondecreasing_lt_t_max2[of "\<tau>'" "t'", OF _ _ `\<tau>' \<le> t'`] 
      increasing_lt_t_max2[of "t'" "t_max", OF _ _ `t' < t_max`] `0 < t` `t \<le> \<tau>'` `t' < t_max`
      `\<tau>' \<le> t'` `t < t'` unfolding t_max_def by auto
    ultimately have "b_x \<tau>' \<in> {b_x 0 <..< b_x t_max}"
      by auto
    thus ?thesis
      using `b_x \<tau>' = vx * \<tau>` by auto
  qed
  have 4: "b_x t + (b_x t' - b_x t) \<in> {b_x 0 <..< b_x t_max}"
  proof -
    have "b_x 0 < b_x t'" 
      using increasing_lt_t_max2[of "0" "t'"] t_max_gt_zero unfolding t_max_def[THEN sym]
      using `t' < t_max` `0 < t` `t < t'` by auto
    moreover have "b_x t' < b_x t_max"
      using increasing_lt_t_max2[of "t'" "t_max"] t_max_gt_zero unfolding t_max_def[THEN sym]
      using `0 < t` `t < t'` `t' < t_max` by auto
    ultimately show ?thesis by auto
  qed
  have 5: "vx * \<tau> - b_x t \<le> b_x t' - b_x t"
    using `vx * \<tau> \<le> fst (point2 t t')` unfolding point2_def' by auto
  have "secant_slope 1 (b_x t) (vx * \<tau> - b_x t) \<le> secant_slope 1 (b_x t) (b_x t' - b_x t)"
    using secant_slope_nondecreasing_aux[OF 0 3 4 1 2 5 `0 < vx`] .
  hence "slope0 \<le> slope1" unfolding slope0_def slope1_def by auto
  have "f_of_x 1 (vx * \<tau>)  \<le> slope1 * (vx * \<tau> - b_x t) + b_y 1 t"
  proof -
    have "slope0 * (vx * \<tau> - b_x t) \<le> slope1 * (vx * \<tau> - b_x t)" (is "?lhs \<le> ?rhs")
      using mult_right_mono[OF `slope0 \<le> slope1`, of "(vx * \<tau> - b_x t)"] 1 by auto
    hence "?lhs + b_y 1 t \<le> ?rhs + b_y 1 t" by auto
    thus ?thesis unfolding * by auto
  qed
  also have "... \<le> upper_bnd1 t t' (vx * \<tau>)"
  proof -
    have "slope1 = (f_of_x 1 (b_x t') - f_of_x 1 (b_x t)) / (b_x t' - b_x t)"
      unfolding slope1_def secant_slope_def by auto
    moreover have "the_inv_into {0 .. t_max} b_x (b_x t') = t'"
      using the_inv_into_f_f[OF b_x_inj_on, of "t'"] `0 < t` `t < t'` `t' < t_max`
      t_max_gt_zero by auto
    moreover have "the_inv_into {0 .. t_max} b_x (b_x t) = t"
      using the_inv_into_f_f[OF b_x_inj_on, of "t"] using `0 < t` `t < t'` `t' < t_max`
      t_max_gt_zero by auto
    ultimately have slope1_alt_def: "slope1 = (b_y 1 t' - b_y 1 t) / (b_x t' - b_x t)"
      unfolding f_of_x_def by auto
    have "b_x t < b_x t'" 
      using increasing_lt_t_max2[of "t" "t'"] using `0 < t` `t < t'` `t' < t_max` 
      unfolding t_max_def by auto
    have *: "b_y 1 t < upper_bnd1 t t' (b_x t)" 
    proof -
      have "norm v = vx"
        using norm_Pair[of "fst v" "snd v"] surjective_pairing[of "v"]
        unfolding pos_ori_inv_axioms vx_def[THEN sym] using vx_at_least_zero by (auto)
      hence "t \<le> vx / a_max"
        using `t < t'` `t' < t_max` t_max_lt_t_stop by auto
      hence "fst (point6 t t') \<le> b_x t"
        using b_x_at_least_fst_point6 by auto            
      have "b_y 1 t < 1/2 * a_max * t\<^sup>2"
        using b_y_lt_radius[OF `0 < t` `0 < vx`] by auto
      also have "... = snd (point6 t t')"
        unfolding point6_def' by auto
      also have "... = upper_bnd1 t t' (fst (point6 t t'))"
        unfolding upper_bnd1_def Let_def by auto
      also have "... \<le> upper_bnd1 t t' (b_x t)"
        using upper_bnd1_nondecreasing[of "t" "t'" "fst (point6 t t')" "b_x t", OF _ `t < t'`] `0 < t` `t' < t_max`
        valid_timeI[of "t"] t_max_lt_t_stop valid_timeI[of "t'"] `t < t'` `fst (point6 t t') \<le> b_x t`
        by auto
      finally show ?thesis by auto
    qed
    have "0 \<le> t" and "valid_time t" and "valid_time t'"
      using valid_timeI[of "t"] valid_timeI[of "t'"] `0 < t` `t < t'` `t' < t_max`
      t_max_lt_t_stop by auto
    have slope_alt_def: "slope t t' = (1/2 * a_max * t'\<^sup>2 -  upper_bnd1 t t' (b_x t)) / (b_x t' - b_x t)"
    proof -
      have "1/2 * a_max * t'\<^sup>2 = snd (point5 t t')" and "b_x t' = fst (point5 t t')" 
        unfolding point5_def' by auto
      have "b_x t < fst (point5 t t')"
        using b_x_at_most_fst_point5[of "t" "t'"] using `0 < t` `t < t'` `t' < t_max`
        by auto
      hence "slope t t' = (snd (point5 t t') - (slope t t' * (b_x t - fst (point5 t t')) + snd (point5 t t'))) / (fst (point5 t t') - b_x t)"
        by (auto simp add:field_simps divide_simps)
      thus ?thesis
        unfolding upper_bnd1_def'[OF `0 \<le> t` `t < t'` `valid_time t` `valid_time t'`] Let_def 
        point5_def' by auto
    qed
    have xxx: "(\<lambda>x. slope t t' * (x - b_x t) + upper_bnd1 t t' (b_x t)) = upper_bnd1 t t'"
      unfolding upper_bnd1_def Let_def by (auto simp add:field_simps)   
    hence xxx': "slope t t' * (vx * \<tau> - b_x t) + upper_bnd1 t t' (b_x t) = upper_bnd1 t t' (vx * \<tau>)"
      by metis
    from line_intersect_aux2[of "b_x t" "b_x t'" "b_y 1 t" "upper_bnd1 t t' (b_x t)" "b_y 1 t'" "1/2 * a_max * t'\<^sup>2", 
                            OF `b_x t < b_x t'` * b_y_lt_radius ]
    have "\<forall>x. b_x t \<le> x \<and> x \<le> b_x t' \<longrightarrow>
      slope1 * (x - b_x t) + b_y 1 t < slope t t' * (x - b_x t) + upper_bnd1 t t' (b_x t)"
      unfolding slope1_alt_def slope_alt_def using `0 < t` `t < t'` `0 < vx` 
      by auto
    moreover have "vx * \<tau> \<le> b_x t'"
      using `vx * \<tau> \<le> fst (point2 t t')` unfolding point2_def' by auto
    ultimately show ?thesis
      unfolding  Let_def using xxx' `b_x t \<le> vx * \<tau>` by (auto)
  qed
  finally show ?thesis 
    using `b_y 1 \<tau>' = f_of_x 1 (vx * \<tau>)` by auto
next
  case False
  have "valid_time t" and "valid_time t'"
    using valid_timeI assms t_max_lt_t_stop by (auto)
  have "norm v = vx"
    using norm_Pair[of "fst v" "snd v"] surjective_pairing[of "v"] vx_at_least_zero unfolding pos_ori_inv_axioms
    vx_def[THEN sym] by auto
  have "b_x t = vx * \<tau>"
    using False by auto
  hence "fst (point6 t t') \<le> vx * \<tau>"
    using b_x_at_least_fst_point6[of "t" "t'"] using `t < t'` `t' < t_max` t_max_lt_t_stop unfolding 
    `norm v = vx` by auto
  hence "t = \<tau>'" 
    using `b_x t = vx * \<tau>` assms(6)  `t < t'` `t' < t_max` `\<tau>' \<le> t'` `0 < t` `t \<le> \<tau>'` unfolding t_max_def'
    by (auto intro!:b_x_uniqueness[of "t" "\<tau>'"] simp add:field_simps `norm v = vx`)
  hence "b_y 1 \<tau>' = b_y 1 t"
    by auto
  also have "... < 1/2 * a_max * t\<^sup>2"
    using b_y_lt_radius[OF `0 < t`] vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym]
    by auto
  also have "... \<le> upper_bnd1 t t' (vx * \<tau>)"
    using upper_bnd1_at_least_snd_point6[OF _ `t < t'` `valid_time t` `valid_time t'` `fst (point6 t t') \<le> vx * \<tau>`]
    unfolding point6_def' using `0 < t` by auto
  finally show ?thesis by auto
qed

theorem (in pos_ori_inv_occupancy) lower_bnd1_leq_b_y: 
  assumes "0 < t" and "t < t'" and "t' < t_max"
  assumes "t \<le> \<tau>'" and "\<tau>' \<le> t'"  
  assumes "b_x \<tau>' = vx * \<tau>"
  shows "lower_bnd1 t t' (vx * \<tau>) \<le> b_y (-1) \<tau>'"
proof -
  have "b_y 1 \<tau>' \<le> upper_bnd1 t t' (vx * \<tau>)"
    using b_y_leq_upper_bnd1[OF assms] by auto
  hence "-upper_bnd1 t t' (vx * \<tau>) \<le> - b_y 1 \<tau>'"
    by auto
  hence "lower_bnd1 t t' (vx * \<tau>) \<le> - b_y 1 \<tau>'"
    unfolding lower_upper_bnd_mirror by auto
  also have "... = b_y (-1) \<tau>'"
    unfolding b_y_def by auto
  finally show ?thesis by auto
qed
  
lemma (in pos_ori_inv_occupancy) is_bounded_abs:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "is_bounded (x, y) t t'"
  assumes "\<bar>y'\<bar> \<le> \<bar>y\<bar>"
  shows "is_bounded (x, y') t t'"
proof -
  have "fst (point1 t t') \<le> x \<and> x \<le> fst (point2 t t') \<or> 
        fst (point2 t t') < x \<and> x \<le> fst (point3 t t')" (is "?case1 \<or> ?case2")
    using assms(5) unfolding is_bounded_def 
    by (auto split:if_splits)
  moreover
  { assume "fst (point1 t t') \<le> x \<and> x \<le> fst (point2 t t')"
    hence lb: "lower_bnd1 t t' x \<le> y" and ub: "y \<le> upper_bnd1 t t' x"
      using assms(5) unfolding is_bounded_def by auto
    consider (pos_pos) "0 \<le> y \<and> 0 \<le> y'" | (pos_neg) "0 \<le> y \<and> y' < 0" | (neg_pos) "y < 0 \<and> 0 \<le> y'" | 
             (neg_neg) "y < 0 \<and> y' < 0"
      by linarith
    hence ?thesis
    proof (cases)
      case pos_pos
      hence "y' \<le> y" using assms(6) by auto
      hence "y' \<le> upper_bnd1 t t' x" using ub by auto
      moreover have "lower_bnd1 t t' x \<le> 0" using lower_bnd1_at_most_zero[OF assms(1-4)] `?case1`
        by auto
      with pos_pos have "lower_bnd1 t t' x \<le> y'" by auto
      thus ?thesis 
        using `y' \<le> upper_bnd1 t t' x` `?case1` unfolding is_bounded_def by auto
    next
      case pos_neg
      hence "y' < y" by auto
      hence "y' \<le> upper_bnd1 t t' x" using ub by auto
      have "-y' \<le> y" using pos_neg and assms(6) by auto
      with ub have "-y' \<le> upper_bnd1 t t' x" by auto
      hence "-y' \<le> -lower_bnd1 t t' x" using lower_upper_bnd_mirror by auto
      hence "lower_bnd1 t t' x \<le> y'" by auto
      then show ?thesis using `y' \<le> upper_bnd1 t t' x` `?case1` unfolding is_bounded_def by auto
    next
      case neg_pos
      hence "y \<le> -y'" using assms(6) by auto
      with lb have "lower_bnd1 t t' x \<le> -y'" by auto
      hence "y' \<le> -lower_bnd1 t t' x" by auto
      hence u: "y' \<le> upper_bnd1 t t' x" using lower_upper_bnd_mirror by auto
      
      have "lower_bnd1 t t' x \<le> 0" using lower_bnd1_at_most_zero[OF assms(1-4)] `?case1` by auto
      also have "... \<le> y'" using neg_pos by auto
      finally have "lower_bnd1 t t' x \<le> y'" by auto
      then show ?thesis using u `?case1` unfolding is_bounded_def by auto
    next
      case neg_neg
      hence "y' \<le> upper_bnd1 t t' x" using upper_bnd1_at_least_zero[OF assms(1-4), of "x"] `?case1` 
        unfolding point1_and_6_same_x by linarith
      moreover have "lower_bnd1 t t' x \<le> y'" using neg_neg lb assms(6) by auto
      ultimately show ?thesis using `?case1` unfolding is_bounded_def by auto
    qed } 

  moreover
  { assume "fst (point2 t t') < x \<and> x \<le> fst (point3 t t')"
    hence "\<not> ?case1" using assms(5) unfolding is_bounded_def 
      by (auto split:if_splits)
    then consider "\<not> ?case1" |  "x = fst (point2 t t')" using assms(5) unfolding is_bounded_def
      by (auto split:if_splits)
    hence lb: "lower_bnd2 t t' \<le> y \<and> y \<le> upper_bnd2 t t'"
    proof (cases)
      case 1
      then show ?thesis using `fst (point2 t t') < x \<and> x \<le> fst (point3 t t')`
        assms(5) unfolding is_bounded_def lower_bnd2_def upper_bnd2_def by (auto split:if_splits)
    next                          
      case 2
      hence 0: "lower_bnd1 t t' x \<le> y" and 00: "y \<le> upper_bnd1 t t' x"
        using assms(5) unfolding is_bounded_def using  point6x_lt_point5x[OF assms(1-4)]
        unfolding point1_and_6_same_x[THEN sym] point2_and_5_same_x[THEN sym] by auto
      with 2 have "lower_bnd1 t t' x = lower_bnd2 t t'" and "upper_bnd1 t t' x = upper_bnd2 t t'"
        using lower_bnd1_eq_lower_bnd2[OF assms(1-4)] upper_bnd1_eq_upper_bnd2[OF assms(1-4)]
        by auto
      with 0 and 00 show ?thesis by auto   
    qed
    consider (pos_pos) "0 \<le> y \<and> 0 \<le> y'" | (pos_neg) "0 \<le> y \<and> y' < 0" | (neg_pos) "y < 0 \<and> 0 \<le> y'" | 
             (neg_neg) "y < 0 \<and> y' < 0"
      by linarith 
    hence ?thesis
    proof (cases)
      case pos_pos
      hence "y' \<le> upper_bnd2 t t'" using assms(6) lb by auto
      have "lower_bnd2 t t' \<le> 0"
        unfolding lower_bnd2_def point2_def' using a_max_neq_zero assms(1-2) by auto
      with pos_pos have "lower_bnd2 t t' \<le> y'" by auto
      then show ?thesis using `y' \<le> upper_bnd2 t t'` `\<not>?case1` `?case2` unfolding is_bounded_def
        unfolding lower_bnd2_def upper_bnd2_def by auto
    next
      case pos_neg
      have "0 \<le> upper_bnd2 t t'" unfolding upper_bnd2_def point5_def' using a_max_neq_zero
        assms(1-2) by auto
      with pos_neg have "y' \<le> upper_bnd2 t t'" by auto
      have "-y' \<le> y" using pos_neg assms(6) by auto
      hence "-y' \<le> upper_bnd2 t t'" using lb by auto
      also have "... = - lower_bnd2 t t'" unfolding upper_bnd2_def lower_bnd2_def point5_def' 
        point2_def' by auto
      finally have "lower_bnd2 t t' \<le> y'" by auto
      with `y' \<le> upper_bnd2 t t'` show ?thesis using `\<not> ?case1` `?case2` unfolding is_bounded_def
        lower_bnd2_def upper_bnd2_def by auto
    next
      case neg_pos
      have "lower_bnd2 t t' \<le> 0"
        unfolding lower_bnd2_def point2_def' using a_max_neq_zero assms(1-2) by auto
      with neg_pos have "lower_bnd2 t t' \<le> y'" by auto
      have "y \<le> -y'" using neg_pos assms(6) by auto
      hence "lower_bnd2 t t' \<le> -y'" using lb by auto
      hence "-upper_bnd2 t t' \<le> -y'" unfolding lower_bnd2_def upper_bnd2_def point2_def' point5_def'
        by auto
      hence "y' \<le> upper_bnd2 t t'" by auto
      thus ?thesis using `lower_bnd2 t t' \<le> y'` `\<not>?case1` `?case2` unfolding is_bounded_def 
        lower_bnd2_def upper_bnd2_def by auto
    next
      case neg_neg
      hence "y \<le> y'" using assms(6) by auto
      have "0 \<le> upper_bnd2 t t'" unfolding upper_bnd2_def point5_def' using a_max_neq_zero
        assms(1-2) by auto
      hence "y' \<le> upper_bnd2 t t'" using neg_neg by auto
      moreover have "lower_bnd2 t t' \<le> y'" using lb `y <= y'` by auto
      ultimately show ?thesis using `\<not>?case1` `?case2` unfolding is_bounded_def 
        lower_bnd2_def upper_bnd2_def by auto
    qed }
  ultimately show ?thesis by auto
qed

lemma (in pos_ori_inv_occupancy) is_bounded_to_right:
  assumes "0 \<le> t" and "t < t'" and "valid_time t" and "valid_time t'"
  assumes "is_bounded (x, y) t t'"
  shows "\<And>x' y'.  x \<le> x' \<Longrightarrow>  x' \<le> fst (point3 t t') \<Longrightarrow>  \<bar>y'\<bar> \<le> \<bar>y\<bar> \<Longrightarrow> is_bounded (x', y') t t'"
proof -
  fix x' y' 
  assume "x \<le> x'"
  assume "x' \<le> fst (point3 t t')"
  assume abs: "\<bar>y'\<bar> \<le> \<bar>y\<bar>"

  have "fst (point1 t t') \<le> x" and "x \<le> fst (point3 t t')" using assms(5) unfolding is_bounded_def 
    using point6x_lt_point5x[OF assms(1-4)] unfolding point1_and_6_same_x 
    using point2x_lt_point3x[OF assms(1-2)] unfolding point2_and_5_same_x
    by (auto split:if_splits)
  hence "fst (point1 t t') \<le> x'" using `x \<le> x'` by auto
  have "x' \<le> fst (point2 t t') \<or> (fst (point2 t t') < x')" by linarith
  moreover
  { assume "x' \<le> fst (point2 t t')"
    hence "x \<le> fst (point2 t t')" using `x \<le> x'` by auto
    hence lb: "lower_bnd1 t t' x \<le> y" and ub: "y \<le> upper_bnd1 t t' x"
      using `is_bounded (x, y) t t'` unfolding is_bounded_def by (auto split:if_splits)
    consider (pos_pos) "0 \<le> y \<and> 0 \<le> y'" | (pos_neg) "0 \<le> y \<and> y' < 0" | (neg_pos) "y < 0 \<and> 0 \<le> y'" | 
             (neg_neg) "y < 0 \<and> y' < 0"
      by linarith     
    hence "lower_bnd1 t t' x' \<le> y' \<and> y' \<le> upper_bnd1 t t' x'"
    proof (cases)
      case pos_pos
      have "lower_bnd1 t t' x' \<le> 0"
        using lower_bnd1_at_most_zero[OF assms(1-4) `fst (point1 t t') \<le> x'`] by auto
      have "y' \<le> y" using abs pos_pos by auto
      hence "y' \<le> upper_bnd1 t t' x" using ub by auto
      also have "... \<le> upper_bnd1 t t' x'"
        using upper_bnd1_nondecreasing[OF assms(1-4) `x \<le> x'`] by auto
      finally have "y' \<le> upper_bnd1 t t' x'" by auto
      thus ?thesis using `lower_bnd1 t t' x' \<le>0` pos_pos by auto
    next
      case pos_neg
      hence "-y' \<le> y" using `\<bar>y'\<bar> \<le> \<bar>y\<bar>` by auto
      also have "... \<le> upper_bnd1 t t' x" using `y \<le> upper_bnd1 t t' x` by auto
      also have "... = -lower_bnd1 t t' x" using lower_upper_bnd_mirror by auto
      finally have "-y' \<le> -lower_bnd1 t t' x" by auto
      hence "lower_bnd1 t t' x \<le> y'" by auto
      moreover have "lower_bnd1 t t' x' \<le> lower_bnd1 t t' x"
        using lower_bnd1_nonincreasing[OF assms(1-4) `x \<le> x'`] by auto
      ultimately have *: "lower_bnd1 t t' x' \<le> y'" by auto
      have "0 \<le> upper_bnd1 t t' x'" using upper_bnd1_at_least_zero[OF assms(1-4)] 
        using `fst (point1 t t') \<le> x'` unfolding point1_and_6_same_x by auto
      hence "y' \<le> upper_bnd1 t t' x'" using pos_neg by auto
      thus ?thesis using * by auto 
    next
      case neg_pos
      have "lower_bnd1 t t' x' \<le> 0"
        using lower_bnd1_at_most_zero[OF assms(1-4) `fst (point1 t t') \<le> x'`] by auto
      hence *: "lower_bnd1 t t' x' \<le> y'" using neg_pos by auto
      have "y \<le> -y'" using abs neg_pos by auto
      hence "lower_bnd1 t t' x \<le> -y'" using lb by auto
      hence "-upper_bnd1 t t' x \<le> -y'" unfolding lower_upper_bnd_mirror by auto
      hence "y' \<le> upper_bnd1 t t' x" by auto
      also have "... \<le> upper_bnd1 t t' x'"
        using upper_bnd1_nondecreasing[OF assms(1-4) `x \<le> x'`] by auto
      finally show ?thesis using * by auto
    next
      case neg_neg
      hence "y \<le> y'" using `\<bar>y'\<bar> \<le> \<bar>y\<bar>` by auto
      hence "lower_bnd1 t t' x \<le> y'" using `lower_bnd1 t t' x \<le> y` by auto
      moreover have "lower_bnd1 t t' x' \<le> lower_bnd1 t t' x"
        using lower_bnd1_nonincreasing[OF assms(1-4) `x \<le> x'`] by auto
      ultimately have *: "lower_bnd1 t t' x' \<le> y'" by auto
      have "0 \<le> upper_bnd1 t t' x'" using upper_bnd1_at_least_zero[OF assms(1-4)] `fst (point1 t t') \<le> x'`
        unfolding point1_and_6_same_x by auto
      thus ?thesis using neg_neg *  by auto
    qed
    hence "is_bounded (x', y') t t'" using `fst (point1 t t') \<le> x'` `x' \<le> fst (point2 t t')`
      unfolding is_bounded_def by auto }
  moreover
  { assume "fst (point2 t t') < x'"
    consider "fst (point2 t t') < x" | "x \<le> fst (point2 t t')" by linarith
    hence lbub: "lower_bnd2 t t' \<le> y \<and> y \<le> upper_bnd2 t t'"
    proof (cases)
      case 1
      then show "lower_bnd2 t t' \<le> y \<and> y \<le> upper_bnd2 t t'"  
        using `is_bounded (x, y) t t'` unfolding is_bounded_def using `x \<le> fst (point3 t t')` 1
        unfolding upper_bnd2_def lower_bnd2_def by auto
    next
      case 2
      hence lb: "lower_bnd1 t t' x \<le> y" and ub: "y \<le> upper_bnd1 t t' x"
        using `is_bounded (x, y) t t'` unfolding is_bounded_def using `fst (point1 t t') \<le> x`
        by auto
      hence lb': "lower_bnd2 t t' \<le> y" and ub': "y \<le> upper_bnd2 t t'"
          using lower_bnd1_nonincreasing[OF assms(1-4) 2] upper_bnd1_nondecreasing[OF assms(1-4) 2]
          lower_bnd1_eq_lower_bnd2[OF assms(1-4), THEN sym] upper_bnd1_eq_upper_bnd2[OF assms(1-4)]
          by auto
      then show ?thesis by auto
    qed
    have "0 \<le> y \<and> 0 \<le> y' \<or> 0 \<le> y \<and> y' < 0 \<or> y < 0 \<and> 0 \<le> y' \<or> y < 0 \<and> y' < 0"
      (is "?pos_pos \<or> ?pos_neg \<or> ?neg_pos \<or> ?neg_neg") by auto
    moreover
    { assume "?pos_pos"
      hence "y' \<le> y" using abs by auto
      hence *: "y' \<le> upper_bnd2 t t'" using lbub by auto
      have "lower_bnd2 t t' \<le> 0" unfolding lower_bnd2_def point2_def' using a_max_neq_zero
        assms(1-2) by auto
      hence "lower_bnd2 t t' \<le> y'" using `?pos_pos` by auto
      with * have "lower_bnd2 t t' \<le> y' \<and> y' \<le> upper_bnd2 t t'" by auto }
    moreover
    { assume "?pos_neg"
      have "0 \<le> upper_bnd2 t t'" unfolding upper_bnd2_def point5_def' using a_max_neq_zero
        assms(1-2) by auto
      hence *: "y' \<le> upper_bnd2 t t'" using `?pos_neg` by auto
      have "-y' \<le> y" using abs `?pos_neg` by auto
      also have "... \<le> upper_bnd2 t t'" using lbub by auto
      also have "... = - lower_bnd2 t t'" unfolding lower_bnd2_def upper_bnd2_def point2_def'
        point5_def' by auto
      finally have "lower_bnd2 t t' \<le> y'" by auto
      with * have "lower_bnd2 t t' \<le> y' \<and> y' \<le> upper_bnd2 t t'" by auto }
    moreover
    { assume "?neg_pos"
      have "lower_bnd2 t t' \<le> 0" unfolding lower_bnd2_def point2_def' using a_max_neq_zero
        assms(1-2) by auto
      hence *: "lower_bnd2 t t' \<le> y'" using `?neg_pos` by auto
      have "y \<le> -y'" using abs `?neg_pos` by auto
      hence "lower_bnd2 t t' \<le> -y'" using lbub by auto
      hence "-upper_bnd2 t t' \<le> -y'" unfolding lower_bnd2_def upper_bnd2_def point2_def' point5_def'
        by auto
      hence "y' \<le> upper_bnd2 t t'" by auto
      with * have "lower_bnd2 t t' \<le> y' \<and> y' \<le> upper_bnd2 t t'" by auto }
    moreover
    { assume "?neg_neg"
      have "0 \<le> upper_bnd2 t t'" unfolding upper_bnd2_def point5_def' using a_max_neq_zero 
        assms(1-2) by auto
      hence *: "y' \<le> upper_bnd2 t t'" using `?neg_neg` by auto
      have "y \<le> y'" using abs `?neg_neg` by auto
      with lbub have "lower_bnd2 t t' \<le> y'" by auto
      with * have "lower_bnd2 t t' \<le> y' \<and> y' \<le> upper_bnd2 t t'" by auto }
    ultimately have "lower_bnd2 t t' \<le> y' \<and> y' \<le> upper_bnd2 t t'" by auto
    hence "is_bounded (x', y') t t'" 
      using `fst (point2 t t') < x'` `x' \<le> fst (point3 t t')`
      unfolding is_bounded_def lower_bnd2_def upper_bnd2_def by (auto split:if_splits) }
  ultimately  show "is_bounded (x', y') t t'" by auto
qed

theorem (in pos_ori_inv_occupancy) ysup_occupancy_circle3: 
  assumes "0 < t" and "t < t'" and "t' < t_max"
  assumes "fst (point2 t t') < vx * \<tau>" and "vx * \<tau> \<le> vx * t'"
  shows "is_bounded (vx * \<tau>, 1/2 * a_max * \<tau>\<^sup>2) t t'"
proof -
  have "vx * \<tau> \<le> fst (point3 t t')" 
  proof -
    have "0 \<le> 1/2 * a_max * t'\<^sup>2"
      using a_max_neq_zero by (auto)
    thus ?thesis
      unfolding point3_def' using assms(5) by auto
  qed

  moreover have "snd (point2 t t') \<le> 1/2 * a_max * \<tau>\<^sup>2"    
  proof -
    have 0: "- 1/2 * a_max * t'\<^sup>2 < 0" 
      using a_max_neq_zero zero_le_power2[of "t'"] `0 < t` `t < t'` by auto
    have 1: "0 \<le> 1/2 * a_max * \<tau>\<^sup>2"
      using a_max_neq_zero by (auto simp add:field_simps)
    thus ?thesis
      unfolding point2_def' using 0 by auto
  qed

  moreover have "1/2 * a_max * \<tau>\<^sup>2 \<le> snd (point5 t t')"
  proof -
    have "\<tau> \<le> t'"
      using assms(5) using v\<^sub>x_wd vx_at_least_zero unfolding vx_def[THEN sym] by auto
    have "0 \<le> b_x t'"
      using b_x_at_least_zero[of "t'"] using `0 < t` `t < t'` `t' < t_max` t_max_lt_t_stop
      by auto
    with assms(4) have "0 < vx * \<tau>" 
      unfolding point2_def' by auto
    hence "0 < \<tau>" 
      using v\<^sub>x_wd vx_at_least_zero unfolding vx_def[THEN sym]  by (auto simp add:sign_simps)
    hence "\<tau>\<^sup>2 \<le> t'\<^sup>2"
      using power2_mono_leq[OF `\<tau> \<le> t'`] by auto
    hence "1/2 * a_max * \<tau>\<^sup>2 \<le> 1/2 * a_max * t'\<^sup>2"
      using a_max_neq_zero by (auto)
    thus ?thesis
      unfolding point5_def' by auto
  qed

  ultimately show ?thesis using assms(4) unfolding is_bounded_def
    by auto
qed
        
theorem (in pos_ori_inv_occupancy) right_half_range_subseteq_encl_poly_range:
  assumes "0 < t" and "t < t'" and "t' < t_max"
  shows "right_half_range (occupancy_circle t) \<subseteq> encl_poly_range t t'"
proof (rule subsetI)
  have "valid_time t" and "valid_time t'" 
    using valid_timeI assms t_max_lt_t_stop by auto

  fix p 
  assume asm: "p \<in> right_half_range (occupancy_circle t)"

  --\<open>basic information\<close> 
  then obtain x and y where p_def: "p = (x, y)" by force
  define r where "r = Radius (occupancy_circle t)"
  hence r_def': "r = 1/2 * a_max * t\<^sup>2" unfolding occupancy_circle_def by auto
  define x1 where "x1 = fst (Center (occupancy_circle t))"
  hence x1_def': "x1 = vx * t"
    unfolding occupancy_circle_def vx_def pos_ori_inv_axioms by auto
  define y1 where "y1 = snd (Center (occupancy_circle t))"
  hence y1_def': "y1 = 0" 
    unfolding occupancy_circle_def pos_ori_inv_axioms by auto

  have "(x - x1)\<^sup>2 + y\<^sup>2 \<le> r\<^sup>2" and ri: "x1 \<le> x \<and> x \<le> x1 + \<bar>r\<bar>"
    using asm unfolding right_half_range_def p_def x1_def r_def Let_def using y1_def y1_def' 
    by auto  
  have "y\<^sup>2 \<le> r\<^sup>2"  
  proof -
    have "y\<^sup>2 \<le> r\<^sup>2 - (x - x1)\<^sup>2" using `(x - x1)\<^sup>2 + y\<^sup>2 \<le> r\<^sup>2` by auto
    also have "... \<le> r\<^sup>2" by auto
    finally show "y\<^sup>2 \<le> r\<^sup>2" by auto
  qed
  hence "sqrt (y\<^sup>2) \<le> sqrt (r\<^sup>2)"  using real_sqrt_le_mono[OF `y\<^sup>2 \<le> r\<^sup>2`]  by auto
  hence "\<bar>y\<bar> \<le> \<bar>r\<bar>"  using r_def' a_max_neq_zero by auto   

  --\<open>case distinction\<close>
  have "x1 \<le> fst (point2 t t') \<or> fst (point2 t t') < x1"
    by auto
  moreover
  { assume "x1 \<le> fst (point2 t t')"
    have "is_bounded (x1, r) t t'"
      using ysup_occupancy_circle2 [OF assms(1-3)] `x1 \<le> fst (point2 t t')`
      unfolding x1_def' r_def' by auto }
  moreover
  { assume "fst (point2 t t') < x1"
    have "is_bounded (x1, r) t t'"  
      using ysup_occupancy_circle3 [OF assms(1-3), of "t"] `fst (point2 t t') < x1` `t < t'`
      v\<^sub>x_wd vx_at_least_zero unfolding vx_def[THEN sym] x1_def' r_def' by (auto) }
  ultimately have "is_bounded (x1, r) t t'" by auto
  
  have "x \<le> fst (point3 t t')" and "fst (point1 t t') \<le> x"
  proof -
    have 0: "p \<in> range_of_circle (occupancy_circle t)"
      using asm unfolding range_circle_decompose by auto
    show "x \<le> fst (point3 t t')"
      using range_circle_fst_at_most_point3[OF `0 < t` `t < t'` `valid_time t` `valid_time t'` order.refl]
      `t < t'` 0 p_def by auto
    show "fst (point1 t t') \<le> x"
      using range_circle_fst_at_least_point1[OF `0 < t` `t < t'` `valid_time t` `valid_time t'` order.refl]
      `t < t'` 0 p_def by auto
  qed      
  hence "is_bounded (x, y) t t'" 
    using is_bounded_to_right[OF _ `t < t'` `valid_time t` `valid_time t'` `is_bounded (x1, r) t t'`, of "x" "y"]
    assms(1) ri `x \<le> fst (point3 t t')` `\<bar>y\<bar> \<le> \<bar>r\<bar>` by auto
  thus "p \<in> encl_poly_range t t'"  using encl_poly_rangeI2[OF _ `t < t'` `valid_time t` `valid_time t'`] assms(1)
    unfolding p_def by auto
qed

theorem (in pos_ori_inv_occupancy) left_half_init_occupancy:
  assumes "0 \<le> t" and "t < t'" and "t' \<le> t_max"
  assumes "(x - vx * t)\<^sup>2 + y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4"  \<comment> \<open>Equation for the circle\<close>
  assumes "x \<le> vx * t"                             \<comment> \<open>Position-x is less than the centre\<close>
  shows "is_bounded (x, y) t t'"
proof -              
  have "valid_time t" and "valid_time t'"
    using valid_timeI assms(1-3) t_max_lt_t_stop by auto
  have "fst (point1 t t') \<le> x"
  proof -
    have "(x - vx * t)\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4 - y\<^sup>2" 
      using assms(4) by auto
    also have "... \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4"
      by auto
    finally have "(x - vx * t)\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4" (is "?lhs0 \<le> ?rhs0")
      by auto
    hence "sqrt ?lhs0 \<le> sqrt ?rhs0" 
      using real_sqrt_le_iff[of "?lhs0" "?rhs0"] by auto
    moreover have "(1/2 * a_max * t\<^sup>2)\<^sup>2 = 1/4 * a_max\<^sup>2 * t\<^sup>4"
      by (auto simp add:power_def)
    ultimately have "\<bar>x - vx * t\<bar> \<le> \<bar>1/2 * a_max * t\<^sup>2\<bar>"
      using real_sqrt_abs[of "1/2 * a_max * t\<^sup>2"] by (auto simp add:field_simps)
    hence "vx * t - x \<le> 1/2 * a_max * t\<^sup>2"
      using assms(5) a_max_neq_zero by auto
    thus ?thesis
      unfolding point1_def' by auto
  qed
    
  have "x \<le> fst (point2 t t') \<or> fst (point2 t t') < x" by linarith
  moreover
  { assume "x \<le> fst (point2 t t')"

    have "y \<le> upper_bnd1 t t' x"
    proof -
      have "y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4 - (x - vx * t)\<^sup>2"
        using assms(4) by auto
      also have "... \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4"
        by auto
      also have "... = (1/2 * a_max * t\<^sup>2)\<^sup>2"
        by (auto simp add:field_simps)
      finally have "y\<^sup>2 \<le> (1/2 * a_max * t\<^sup>2)\<^sup>2" (is "?lhs0 \<le> ?rhs0") 
        by auto
      hence "sqrt (y\<^sup>2) \<le> sqrt ((1/2 * a_max * t\<^sup>2)\<^sup>2)"
        using real_sqrt_le_iff[of "?lhs0" "?rhs0"] by auto
      hence 0: "\<bar>y\<bar> \<le> 1/2 * a_max * t\<^sup>2"
        using a_max_neq_zero by auto
      have "0 \<le> y \<or> y < 0" by auto
      moreover 
      { assume "0 \<le> y"
        have "y \<le> 1/2 * a_max * t\<^sup>2"
          using 0 by auto
        also have "... = snd (point6 t t')"
          unfolding point6_def' by auto
        also have "... \<le> upper_bnd1 t t' x"
          using upper_bnd1_at_least_snd_point6[OF assms(1-2) `valid_time t` `valid_time t'`]
          `fst (point1 t t') \<le> x` unfolding point1_and_6_same_x by auto
        finally have ub: "y \<le> upper_bnd1 t t' x"
          by auto }
      moreover
      { assume "y < 0"
        hence "y \<le> upper_bnd1 t t' x"
          using upper_bnd1_at_least_zero[OF `0 \<le> t` `t < t'` `valid_time t` `valid_time t'`, of "x"]
          `fst (point1 t t') \<le> x` unfolding point1_and_6_same_x by linarith }
      ultimately show ?thesis by auto 
    qed

    moreover have "lower_bnd1 t t' x \<le> y"
    proof -
      have "0 \<le> y \<or> y < 0"
        by linarith
      moreover
      { assume "0 \<le> y"
        moreover have "lower_bnd1 t t' x \<le> 0"
          using lower_bnd1_at_most_zero[OF `0 \<le> t` `t < t'` `valid_time t` `valid_time t'`, of "x"]
          `fst (point1 t t') \<le> x` by auto
        ultimately have "lower_bnd1 t t' x \<le> y"
          by auto }
      moreover
      { assume "y < 0"
        have "y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4 - (x - vx * t)\<^sup>2"
          using assms(4) by (auto simp add:field_simps)
        also have "... \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4"
          by (auto)
        also have "... = (1/2 * a_max * t\<^sup>2)\<^sup>2"
          by (auto simp add:power_def)
        finally have "y\<^sup>2 \<le> (1/2 * a_max * t\<^sup>2)\<^sup>2" (is "?lhs0 \<le> ?rhs0")
          by auto
        hence "sqrt ?lhs0 \<le> sqrt ?rhs0"
          using real_sqrt_le_mono[of "?lhs0" "?rhs0"] by auto
        hence "-1/2 * a_max * t\<^sup>2 \<le> y" 
          using a_max_neq_zero `y < 0` by auto
        hence "snd (point1 t t') \<le> y"
          unfolding point1_def' by auto
        moreover have "lower_bnd1 t t' x \<le> snd (point1 t t')"
          using lower_bnd1_at_most_snd_point1[OF `0 \<le> t` `t < t'` `valid_time t` `valid_time t'`]
          `fst (point1 t t') \<le> x` by auto
        ultimately have "lower_bnd1 t t' x \<le> y"
          by auto }
      ultimately show "lower_bnd1 t t' x \<le> y" 
        by auto
    qed

    ultimately have "is_bounded (x, y) t t'"
      using `fst (point1 t t') \<le> x` `x \<le> fst (point2 t t')` unfolding is_bounded_def 
      by auto }

  moreover
  { assume "fst (point2 t t') < x"

    have "y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4 - (x - vx * t)\<^sup>2"
      using assms(4) by (auto)
    also have "... \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4"
      by auto
    also have "... = (1/2 * a_max * t\<^sup>2)\<^sup>2"
      by (auto simp add:power_def)
    finally have "y\<^sup>2 \<le> (1/2 * a_max * t\<^sup>2)\<^sup>2"
      by auto
    hence "sqrt (y\<^sup>2) \<le> sqrt ((1/2 * a_max * t\<^sup>2)\<^sup>2)"
      using real_sqrt_le_iff by (blast)
    hence 0: "\<bar>y\<bar> \<le> 1/2 * a_max * t\<^sup>2"
      using a_max_neq_zero by auto

    have "y \<le> snd (point5 t t')"
    proof -
      have "0 \<le> y \<or> y < 0" 
        by auto
      moreover
      { assume "0 \<le> y"
        hence "y \<le> 1/2 * a_max * t\<^sup>2"
          using 0 by auto    
        also have "... \<le> 1/2 * a_max * t'\<^sup>2"
          using power2_mono_leq[of "t" "t'"] `0 \<le> t` `t < t'` a_max_neq_zero
          by (auto simp add:field_simps)
        also have "... = snd (point5 t t')"
          unfolding point5_def' by auto
        finally have ?thesis by auto }
      moreover
      { assume "y < 0"
        also have "... \<le> 1/2 * a_max * t'\<^sup>2"
          using a_max_neq_zero by (auto)
        finally have ?thesis 
          unfolding point5_def' by auto }
      ultimately show ?thesis by auto
    qed

    moreover have "snd (point2 t t') \<le> y"
    proof (cases "0 \<le> y")
      case True
      have "- 1/2 * a_max * t'\<^sup>2 \<le> 0"
        using a_max_neq_zero by (auto)
      with True have "-1/2 * a_max * t'\<^sup>2 \<le> y"
        by auto
      then show ?thesis 
        unfolding point2_def' by auto
    next
      case False
      hence "y < 0" by auto
      with 0 have "- y \<le> 1/2 * a_max * t\<^sup>2" 
        by auto
      also have "... \<le> 1/2 * a_max * t'\<^sup>2"
        using power2_mono_leq[of "t" "t'"] `0 \<le> t` `t < t'` a_max_neq_zero by auto
      finally have "-y \<le> 1/2 * a_max * t'\<^sup>2" by auto
      hence "-1/2 * a_max * t'\<^sup>2 \<le> y" by auto       
      then show ?thesis 
        unfolding point2_def' by auto
    qed

    moreover have "x \<le> fst (point3 t t')"
      unfolding point3_def' using `x \<le> vx * t` `t < t'` vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym]
      using a_max_neq_zero power2_mono_leq[of "t" "t'"] `0 \<le> t` 
      by (smt divide_nonneg_nonneg mult_left_mono mult_sign_intros(1) power2_less_0 prod.sel(1)) 

    ultimately have "is_bounded (x, y) t t'"
      unfolding is_bounded_def using `fst (point2 t t') < x` `fst (point1 t t') \<le> x`
      by auto }
  ultimately show ?thesis by auto
qed

theorem (in pos_ori_inv_occupancy) right_half_last_occupancy:
  assumes "0 \<le> t" and "t < t'" and "t' \<le> t_max"
  assumes "(x - vx * t')\<^sup>2 + y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t'\<^sup>4"
  assumes "vx * t' \<le> x"
  shows "is_bounded (x, y) t t'"
proof - 
  have "valid_time t" and "valid_time t'"
    using valid_timeI assms(1-3) t_max_lt_t_stop by auto  

  thm is_bounded_def
  have lb_x: "fst (point2 t t') < x"
  proof -
    have "b_x t' < vx * t'" 
    proof (rule ccontr)
      assume "\<not> (b_x t' < vx * t')"
      hence "b_x t' \<ge> vx * t'"
        by auto
      hence "t'\<^sup>3 * (a_max\<^sup>2 / (2 * vx)) \<le> 0 " (is "?a * ?b \<le> 0")
        unfolding b_x_def' by auto
      hence "0 \<le> ?a \<and> ?b \<le> 0 \<or> ?a \<le> 0 \<and> 0 \<le> ?b"
        unfolding mult_le_0_iff by auto
      moreover have "0 < ?b " and "?a \<noteq> 0"
        using v\<^sub>x_wd vx_at_least_zero a_max_neq_zero `0 \<le> t` `t < t'` unfolding vx_def[THEN sym] by (auto)
      ultimately have "?a < 0 \<and> 0 < ?b" 
        by auto  
      hence "t' < 0" 
        using zero_le_odd_power[of "3" "t'"] by auto
      with `0 \<le> t` `t < t'` show False by auto
    qed
    thus ?thesis 
      unfolding point2_def' using `vx * t' \<le> x` by auto
  qed

  have ub_x: "x \<le> fst (point3 t t')"
  proof -
    have "(x - vx * t')\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t'\<^sup>4 - y\<^sup>2"
      using assms(4) by auto
    also have "... \<le> 1/4 * a_max\<^sup>2 * t'\<^sup>4"
      by auto
    also have "... = (1/2 * a_max * t'\<^sup>2)\<^sup>2"
      by (auto simp add:power_def)
    finally have "(x - vx * t')\<^sup>2 \<le> (1 / 2 * a_max * t'\<^sup>2)\<^sup>2" (is "?lhs0 \<le> ?rhs0")
      by auto    
    hence "sqrt ?lhs0 \<le> sqrt ?rhs0"
      unfolding real_sqrt_le_iff by auto
    also have "... = 1/2 * a_max * t'\<^sup>2"
      using a_max_neq_zero by auto
    finally have "sqrt ?lhs0 \<le> 1/2 * a_max * t'\<^sup>2"
      by auto
    hence "x - vx * t' \<le> 1/2 * a_max * t'\<^sup>2"
      using assms(5) by auto
    thus ?thesis
      unfolding point3_def' by auto
  qed  

  have "y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t'\<^sup>4 - (x - vx * t')\<^sup>2"
    using assms(4) by auto
  also have "... \<le> 1/4 * a_max\<^sup>2 * t'\<^sup>4"
    by auto
  also have "... = (1/2 * a_max * t'\<^sup>2)\<^sup>2"
    by (auto simp add:power_def)
  finally have "y\<^sup>2 \<le> (1/2 * a_max * t'\<^sup>2)\<^sup>2" (is "?lhs0 \<le> ?rhs0")
    by (auto)
  hence "sqrt ?lhs0 \<le> sqrt ?rhs0"
    using real_sqrt_le_mono by blast
  hence 0: "\<bar>y\<bar> \<le> 1/2 * a_max * t'\<^sup>2"
    using a_max_neq_zero by auto

  have "0 \<le> y \<or> y < 0" by linarith
  moreover
  { assume "0 \<le> y"
    hence "y \<le> 1/2 * a_max * t'\<^sup>2" and "-1/2 * a_max * t'\<^sup>2 \<le> y"
      using 0 by auto
    hence ?thesis unfolding is_bounded_def using lb_x ub_x unfolding point2_def' point5_def' by auto }
  moreover
  { assume "y < 0" 
    hence "0 \<le> 1/2 * a_max * t'\<^sup>2"
      using a_max_neq_zero by auto
    with `y < 0` have "y \<le> 1/2 * a_max * t'\<^sup>2" 
      by auto
    moreover have "-1/2 * a_max * t'\<^sup>2 \<le> y" 
      using 0 by auto
    ultimately have ?thesis unfolding is_bounded_def using lb_x ub_x unfolding point2_def' point5_def'
      by auto }
  ultimately show ?thesis by auto
qed

theorem (in pos_ori_inv_occupancy) circle_below_b_y1:
  assumes "0 < t" and "t \<le> t'" and "t' < t_max" 
  assumes "(b_x t - vx * t')\<^sup>2 + y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t'\<^sup>4" \<comment> \<open>circle equation at @{term "t'"}\<close>
  shows "y \<le> b_y 1 t"
proof -
  have "y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t'\<^sup>4 - (b_x t - vx * t')\<^sup>2"
    using assms(4) by auto
  also have "... = 1/4 * a_max\<^sup>2 * t'\<^sup>4 - (vx * t' - b_x t)\<^sup>2"
    by (auto simp add:field_simps power_def)
  also have "... = 1/4 * a_max\<^sup>2 * t'\<^sup>4 - (vx * (t' - t) + t\<^sup>3 * a_max\<^sup>2 / (2 * vx))\<^sup>2" (is "_ = ?term0")
    unfolding b_x_def' by (auto simp add:field_simps)
  finally have "y\<^sup>2 \<le> ?term0" 
    by auto
  hence "sqrt (y\<^sup>2) \<le> sqrt ?term0"
    unfolding real_sqrt_le_iff by auto
  hence 0: "\<bar>y\<bar> \<le> sqrt ?term0" 
    by auto
  have "y < 0 \<or> 0 \<le> y" 
    by linarith
  moreover
  { assume "y < 0"
    also have "... \<le> b_y 1 t"
      unfolding b_y_def using vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym] 
      using b_y_inner_geq_zero[of "t"] unfolding b_y_inner_def using assms(1-3) t_max_lt_t_stop
      by auto
    finally have ?thesis by auto }
  moreover
  { assume "0 \<le> y"
    hence "y \<le> sqrt ?term0" 
      using 0 by auto
    also have "... \<le> b_y 1 t" 
    proof -
      have "t < t_max" 
        using `0 < t` `t <= t'` `t' < t_max` by auto
      hence "t'\<^sup>2 + 2 * t' * t + 3 * t\<^sup>2 \<le> 4 * (vx / a_max)\<^sup>2" (is "?lhs0 \<le> ?rhs0")
        using power2_mono_lt[OF `t' < t_max`] power2_mono_lt[OF `t < t_max`] `0 < t` `t \<le> t'`
        mult_strict_mono[OF `t < t_max` `t' < t_max`, OF t_max_gt_zero] unfolding t_max_def'
        by (auto simp add:field_simps power2_eq_square)
      hence "?lhs0 * (t' - t) \<le> ?rhs0 * (t' - t)"
        unfolding mult_le_cancel_right using `t \<le> t'` by (auto simp add:field_simps)
      moreover have "?lhs0 * (t' - t) = t'\<^sup>3  + t'\<^sup>2 * t + t' * t\<^sup>2 - 3 * t\<^sup>3" (is "_ = ?lhs1")
        by (auto simp add:field_simps power_def)
      ultimately have "?lhs1 \<le> (2 * vx / a_max)\<^sup>2 * (t' - t)" (is "_ \<le> ?rhs1")
        by (auto simp add:power_def)
      hence "t'\<^sup>3  + t'\<^sup>2 * t + t' * t\<^sup>2 + t\<^sup>3 \<le> ?rhs1 + 4 * t\<^sup>3" (is "?lhs2 \<le> _")
        by auto
      moreover have "?lhs2 = (t' + t) * (t'\<^sup>2 + t\<^sup>2)"
        by (auto simp add:field_simps power_def)
      ultimately have "(t' + t) * (t'\<^sup>2 +  t\<^sup>2) \<le> ?rhs1 + 4 * t\<^sup>3" (is "?lhs3 \<le> ?rhs3")
        by auto
      hence "?lhs3 * (t' - t) \<le> ?rhs3 * (t' - t)"
        unfolding mult_le_cancel_right using `t \<le> t'` by auto
      moreover have "?lhs3 * (t' - t) = t'\<^sup>4 - t\<^sup>4"
        by (auto simp add:field_simps power_def)
      ultimately have "t'\<^sup>4 - t\<^sup>4 \<le> ?rhs3 * (t' - t)"
        by auto
      also have "... = (2 * vx / a_max)\<^sup>2 * (t'- t)\<^sup>2 + 4 * t\<^sup>3 * (t' - t)" (is "_ = ?rhs4")
        by (auto simp add:field_simps power_def)
      finally have "t'\<^sup>4 - t\<^sup>4 \<le> ?rhs4" by auto
      hence "1/4 * a_max\<^sup>2 * (t'\<^sup>4 - t\<^sup>4) \<le> 1/4 * a_max\<^sup>2 * ?rhs4" (is "?lhs5 \<le> _")
        by (intro mult_left_mono[OF `t'\<^sup>4 - t\<^sup>4 \<le> ?rhs4`, of "1/4 * a_max\<^sup>2"]) 
           (auto simp add:field_simps)              
      also have "... = vx\<^sup>2 * (t' - t)\<^sup>2 + a_max\<^sup>2 * t\<^sup>3 * (t' - t)" (is "_ = ?rhs5")
        using a_max_neq_zero unfolding power2_eq_square by (auto simp add:field_simps)
      finally have "?lhs5 \<le> ?rhs5" 
        by auto
      hence "    ?lhs5 - vx\<^sup>2 * (t' - t)\<^sup>2 - a_max\<^sup>2 * t\<^sup>3 * (t' - t) - (a_max\<^sup>2 * t\<^sup>3 / (2 * vx))\<^sup>2 
            \<le> - (a_max\<^sup>2 * t\<^sup>3 / (2 * vx))\<^sup>2"
        by auto
      hence "1/4 * a_max\<^sup>2 * t'\<^sup>4 - ((vx * (t' - t))\<^sup>2 + a_max\<^sup>2 * t\<^sup>3 * (t' - t) + (a_max\<^sup>2 * t\<^sup>3 / (2 * vx))\<^sup>2)
            \<le>1/4 * a_max\<^sup>2 * t\<^sup>4  - (a_max\<^sup>2 * t\<^sup>3 / (2 * vx))\<^sup>2" (is "?lhs6 \<le> ?rhs6")
        unfolding power_mult_distrib by (auto simp add:field_simps)
      moreover have "?lhs6 = 1/4 * a_max\<^sup>2 * t'\<^sup>4 - (vx * (t' - t) + (a_max\<^sup>2 * t\<^sup>3 / (2 * vx)))\<^sup>2" 
        using v\<^sub>x_wd unfolding vx_def[THEN sym] by (auto simp add:power2_eq_square field_simps)
      ultimately have "1/4 * a_max\<^sup>2 * t'\<^sup>4 - (vx * (t' - t) + (a_max\<^sup>2 * t\<^sup>3 / (2 * vx)))\<^sup>2 \<le> ?rhs6" (is "?lhs7 \<le> _")
        by auto
      hence "sqrt ?lhs7 \<le> sqrt ?rhs6"
        by auto
      moreover have "norm v = vx"
        using norm_Pair[of "fst v" "snd v"]  surjective_pairing[of "v"] vx_at_least_zero
        unfolding vx_def[THEN sym] pos_ori_inv_axioms by (auto simp add:field_simps)
      ultimately show ?thesis 
        unfolding b_y_def  using vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym] 
        by (auto simp add:field_simps) 
    qed
    finally have ?thesis by auto }
  ultimately show ?thesis by auto
qed

theorem (in pos_ori_inv_occupancy) circle_below_b_y1':
  assumes "0 <= t" and "t \<le> t'" and "t' \<le> t_max" 
  assumes "(b_x t - vx * t')\<^sup>2 + y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t'\<^sup>4" \<comment> \<open>circle equation at @{term "t'"}\<close>
  shows "y \<le> b_y 1 t"
proof -
  have "y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t'\<^sup>4 - (b_x t - vx * t')\<^sup>2"
    using assms(4) by auto
  also have "... = 1/4 * a_max\<^sup>2 * t'\<^sup>4 - (vx * t' - b_x t)\<^sup>2"
    by (auto simp add:field_simps power_def)
  also have "... = 1/4 * a_max\<^sup>2 * t'\<^sup>4 - (vx * (t' - t) + t\<^sup>3 * a_max\<^sup>2 / (2 * vx))\<^sup>2" (is "_ = ?term0")
    unfolding b_x_def' by (auto simp add:field_simps)
  finally have "y\<^sup>2 \<le> ?term0" 
    by auto
  hence "sqrt (y\<^sup>2) \<le> sqrt ?term0"
    unfolding real_sqrt_le_iff by auto
  hence 0: "\<bar>y\<bar> \<le> sqrt ?term0" 
    by auto
  have "y < 0 \<or> 0 \<le> y" 
    by linarith
  moreover
  { assume "y < 0"
    also have "... \<le> b_y 1 t"
      unfolding b_y_def using vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym] 
      using b_y_inner_geq_zero[of "t"] unfolding b_y_inner_def using assms(1-3) t_max_lt_t_stop
      by auto
    finally have ?thesis by auto }
  moreover
  { assume "0 \<le> y"
    hence "y \<le> sqrt ?term0" 
      using 0 by auto
    also have "... \<le> b_y 1 t" 
    proof -
      have "t <= t_max" 
        using `0 \<le> t` `t <= t'` `t' <= t_max` by auto
      hence "t'\<^sup>2 + 2 * t' * t + 3 * t\<^sup>2 \<le> 4 * (vx / a_max)\<^sup>2" (is "?lhs0 \<le> ?rhs0")
        using power2_mono_leq[OF `t' \<le> t_max`] power2_mono_leq[OF `t \<le> t_max`] `0 \<le> t` `t \<le> t'` 
        mult_mono[OF ` t \<le> t_max` `t' \<le> t_max`, OF order.strict_implies_order[OF t_max_gt_zero]]
        unfolding t_max_def' by (auto simp add:field_simps power2_eq_square)
      hence "?lhs0 * (t' - t) \<le> ?rhs0 * (t' - t)"
        unfolding mult_le_cancel_right using `t \<le> t'` by (auto simp add:field_simps)
      moreover have "?lhs0 * (t' - t) = t'\<^sup>3  + t'\<^sup>2 * t + t' * t\<^sup>2 - 3 * t\<^sup>3" (is "_ = ?lhs1")
        by (auto simp add:field_simps power_def)
      ultimately have "?lhs1 \<le> (2 * vx / a_max)\<^sup>2 * (t' - t)" (is "_ \<le> ?rhs1")
        by (auto simp add:power_def)
      hence "t'\<^sup>3  + t'\<^sup>2 * t + t' * t\<^sup>2 + t\<^sup>3 \<le> ?rhs1 + 4 * t\<^sup>3" (is "?lhs2 \<le> _")
        by auto
      moreover have "?lhs2 = (t' + t) * (t'\<^sup>2 + t\<^sup>2)"
        by (auto simp add:field_simps power_def)
      ultimately have "(t' + t) * (t'\<^sup>2 +  t\<^sup>2) \<le> ?rhs1 + 4 * t\<^sup>3" (is "?lhs3 \<le> ?rhs3")
        by auto
      hence "?lhs3 * (t' - t) \<le> ?rhs3 * (t' - t)"
        unfolding mult_le_cancel_right using `t \<le> t'` by auto
      moreover have "?lhs3 * (t' - t) = t'\<^sup>4 - t\<^sup>4"
        by (auto simp add:field_simps power_def)
      ultimately have "t'\<^sup>4 - t\<^sup>4 \<le> ?rhs3 * (t' - t)"
        by auto
      also have "... = (2 * vx / a_max)\<^sup>2 * (t'- t)\<^sup>2 + 4 * t\<^sup>3 * (t' - t)" (is "_ = ?rhs4")
        by (auto simp add:field_simps power_def)
      finally have "t'\<^sup>4 - t\<^sup>4 \<le> ?rhs4" by auto
      hence "1/4 * a_max\<^sup>2 * (t'\<^sup>4 - t\<^sup>4) \<le> 1/4 * a_max\<^sup>2 * ?rhs4" (is "?lhs5 \<le> _")
        by (intro mult_left_mono[OF `t'\<^sup>4 - t\<^sup>4 \<le> ?rhs4`, of "1/4 * a_max\<^sup>2"]) 
           (auto simp add:field_simps)              
      also have "... = vx\<^sup>2 * (t' - t)\<^sup>2 + a_max\<^sup>2 * t\<^sup>3 * (t' - t)" (is "_ = ?rhs5")
        using a_max_neq_zero unfolding power2_eq_square by (auto simp add:field_simps)
      finally have "?lhs5 \<le> ?rhs5" 
        by auto
      hence "    ?lhs5 - vx\<^sup>2 * (t' - t)\<^sup>2 - a_max\<^sup>2 * t\<^sup>3 * (t' - t) - (a_max\<^sup>2 * t\<^sup>3 / (2 * vx))\<^sup>2 
            \<le> - (a_max\<^sup>2 * t\<^sup>3 / (2 * vx))\<^sup>2"
        by auto
      hence "1/4 * a_max\<^sup>2 * t'\<^sup>4 - ((vx * (t' - t))\<^sup>2 + a_max\<^sup>2 * t\<^sup>3 * (t' - t) + (a_max\<^sup>2 * t\<^sup>3 / (2 * vx))\<^sup>2)
            \<le>1/4 * a_max\<^sup>2 * t\<^sup>4  - (a_max\<^sup>2 * t\<^sup>3 / (2 * vx))\<^sup>2" (is "?lhs6 \<le> ?rhs6")
        unfolding power_mult_distrib by (auto simp add:field_simps)
      moreover have "?lhs6 = 1/4 * a_max\<^sup>2 * t'\<^sup>4 - (vx * (t' - t) + (a_max\<^sup>2 * t\<^sup>3 / (2 * vx)))\<^sup>2" 
        using v\<^sub>x_wd unfolding vx_def[THEN sym] by (auto simp add:power2_eq_square field_simps)
      ultimately have "1/4 * a_max\<^sup>2 * t'\<^sup>4 - (vx * (t' - t) + (a_max\<^sup>2 * t\<^sup>3 / (2 * vx)))\<^sup>2 \<le> ?rhs6" (is "?lhs7 \<le> _")
        by auto
      hence "sqrt ?lhs7 \<le> sqrt ?rhs6"
        by auto
      moreover have "norm v = vx"
        using norm_Pair[of "fst v" "snd v"]  surjective_pairing[of "v"] vx_at_least_zero
        unfolding vx_def[THEN sym] pos_ori_inv_axioms by (auto simp add:field_simps)
      ultimately show ?thesis 
        unfolding b_y_def  using vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym] 
        by (auto simp add:field_simps) 
    qed
    finally have ?thesis by auto }
  ultimately show ?thesis by auto
qed

theorem (in pos_ori_inv_occupancy) circle_above_b_y1:
  assumes "0 \<le> t" and "t \<le> t'" and "t' \<le> t_max"
  assumes "(b_x t - vx * t')\<^sup>2 + y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t'\<^sup>4"
  shows "b_y (-1) t \<le> y"
proof -
  have "-y \<le> b_y 1 t"
    using circle_below_b_y1'[OF assms(1-3), of "-y"] assms(4)
    by (auto simp add:field_simps)
  hence "- b_y 1 t \<le> y"
    by auto
  thus ?thesis
    unfolding b_y_def by auto
qed

theorem (in pos_ori_inv_occupancy) circle_below_b_y2:
  assumes "0 < t" and "t < t'" and "t' \<le> t_max"
  assumes "(b_x t' - vx * t)\<^sup>2 + y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4" 
  shows "y \<le> b_y 1 t'"
proof -
  have "y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4 - (b_x t' - vx * t)\<^sup>2"
    using assms(4) by auto
  also have "... = 1/4 * a_max\<^sup>2 * t\<^sup>4 - (vx * (t' - t) - t'\<^sup>3 * a_max\<^sup>2 / (2 * vx))\<^sup>2" (is "_ = ?term0")
    unfolding b_x_def' by (auto simp add: field_simps)
  finally have "y\<^sup>2 \<le> ?term0"
    by auto
  hence "sqrt (y\<^sup>2) \<le> sqrt ?term0"
    unfolding real_sqrt_le_iff by auto
  hence 0: "\<bar>y\<bar> \<le> sqrt ?term0"
    by auto
  have "y < 0 \<or> 0 \<le> y"
    by linarith
  moreover
  { assume "y < 0"
    also have "... \<le> b_y 1 t'"
      unfolding b_y_def using vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym] 
      using b_y_inner_geq_zero[of "t'"] unfolding b_y_inner_def using assms(1-3) t_max_lt_t_stop
      by auto
    finally have ?thesis by auto }
  moreover
  { assume "0 \<le> y"
    hence "y \<le> sqrt ?term0"
      using 0 by auto
    also have "... \<le> b_y 1 t'"
    proof -
      have "t < t_max" 
        using `0 < t` `t < t'` `t' <= t_max` by auto
      hence "3 * t'\<^sup>2 + 2 * t' * t + t\<^sup>2 \<le> 4 * (vx / a_max)\<^sup>2" (is "?lhs0 \<le> ?rhs0")
        using power2_mono_leq[OF `t' \<le> t_max`] power2_mono_lt[OF `t < t_max`] `0 < t` `t < t'`
        mult_mono[OF order.strict_implies_order[OF `t < t_max`] `t' \<le> t_max`] unfolding t_max_def'
        by (auto simp add:field_simps power2_eq_square)
      hence "?lhs0 * (t' - t) \<le> ?rhs0 * (t' - t)"
        using `t < t'` by auto
      moreover have "?lhs0 * (t' - t) = 3 * t'\<^sup>3 - t'\<^sup>2 * t - t' * t\<^sup>2 - t\<^sup>3" (is "_ = ?lhs1")
        by (auto simp add:field_simps power_def)
      ultimately have "?lhs1 \<le> (2 * vx / a_max)\<^sup>2 * (t' - t)" (is "_ \<le> ?rhs1")
        by (auto simp add:power_def)
      hence "-(t'\<^sup>3 + t'\<^sup>2 * t + t' * t\<^sup>2 + t\<^sup>3) \<le> (2 * vx / a_max)\<^sup>2 * (t' - t) - 4 * t'\<^sup>3" (is "?lhs2 \<le> ?rhs2")
        by auto
      moreover have "?lhs2 = -(t' + t) * (t'\<^sup>2 + t\<^sup>2)"
        by (auto simp add:field_simps power_def)
      ultimately have "- (t' + t) * (t'\<^sup>2 + t\<^sup>2) \<le> ?rhs1 - 4 * t'\<^sup>3" (is "?lhs3 \<le> ?rhs3")
        by auto
      hence "?lhs3 * (t' - t) \<le> ?rhs3 * (t' - t)"
        using `t < t'` by auto
      moreover have "?lhs3 * (t' - t) = - (t'\<^sup>4 - t\<^sup>4)"
        by (auto simp add:field_simps power_def)
      ultimately have "- (t'\<^sup>4 - t\<^sup>4) \<le> ?rhs3 * (t' - t)"
        by auto
      also have "... = (2 * vx / a_max)\<^sup>2 * (t'- t)\<^sup>2 - 4 * t'\<^sup>3 * (t' - t)" (is "_ = ?rhs4")
        by (auto simp add:field_simps power_def)
      finally have "(t\<^sup>4 - t'\<^sup>4) \<le> ?rhs4"
        by auto
      hence "1/4 * a_max\<^sup>2 * (t\<^sup>4 - t'\<^sup>4) \<le> 1/4 * a_max\<^sup>2 * ?rhs4" (is "?lhs5 \<le> _")
        by (intro mult_left_mono[OF `t\<^sup>4 - t'\<^sup>4 \<le> ?rhs4`, of "1/4 * a_max\<^sup>2"]) 
           (auto simp add:field_simps)      
      also have "... = vx\<^sup>2 * (t' - t)\<^sup>2 - a_max\<^sup>2 * t'\<^sup>3 * (t' - t)" (is "_ = ?rhs5")
        using a_max_neq_zero unfolding power2_eq_square by (auto simp add:field_simps)
      finally have "?lhs5 \<le> ?rhs5"
        by auto
      hence "?lhs5 - vx\<^sup>2 * (t' - t)\<^sup>2 + a_max\<^sup>2 * t'\<^sup>3 * (t' - t) - (a_max\<^sup>2 * t'\<^sup>3 / (2 * vx))\<^sup>2
            \<le> - (a_max\<^sup>2 * t'\<^sup>3 / (2 * vx))\<^sup>2"
        by auto
      hence "1/4 * a_max\<^sup>2 * t\<^sup>4 - ((vx * (t' - t))\<^sup>2 - a_max\<^sup>2 * t'\<^sup>3 * (t' - t) + (a_max\<^sup>2 * t'\<^sup>3 / (2 * vx))\<^sup>2)
            \<le> 1/4 * a_max\<^sup>2 * t'\<^sup>4 - (a_max\<^sup>2 * t'\<^sup>3 / (2 * vx))\<^sup>2" (is "?lhs6 \<le> ?rhs6")
        unfolding power_mult_distrib by (auto simp add:field_simps)
      moreover have "?lhs6 = 1/4 * a_max\<^sup>2 * t\<^sup>4 - (vx * (t' - t) - (a_max\<^sup>2 * t'\<^sup>3 / (2 * vx)))\<^sup>2" (is "_ = ?lhs7")
        using v\<^sub>x_wd unfolding vx_def[THEN sym] by (auto simp add:power2_eq_square field_simps)
      ultimately have "?lhs7 \<le> ?rhs6"
        by auto
      hence "sqrt ?lhs7 \<le> sqrt ?rhs6"
        by auto
      moreover have "norm v = vx"
        using norm_Pair[of "fst v" "snd v"]  surjective_pairing[of "v"] vx_at_least_zero
        unfolding vx_def[THEN sym] pos_ori_inv_axioms by (auto simp add:field_simps)
      ultimately show ?thesis
        unfolding b_y_def using vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym]
        by (auto simp add:field_simps)
    qed
    finally have ?thesis by auto }
  ultimately show ?thesis by auto
qed

theorem (in pos_ori_inv_occupancy) circle_above_b_y2:
  assumes "0 < t" and "t < t'" and "t' \<le> t_max"
  assumes "(b_x t' - vx * t)\<^sup>2 + y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * t\<^sup>4" 
  shows "b_y (-1) t'\<le> y"
proof -
  have "-y \<le> b_y 1 t'"
    using circle_below_b_y2[OF assms(1-3), of "-y"] assms(4) by auto
  hence "- b_y 1 t' \<le> y"
    by auto
  thus ?thesis
    unfolding b_y_def by auto
qed
      
theorem (in pos_ori_inv_occupancy) occupancy_circle_subseteq_encl_poly_range:
  assumes "0 < t" and "t \<le> \<tau>" and "\<tau> \<le> t'" and "t' < t_max" and "t < t'"
  shows "range_of_circle (occupancy_circle \<tau>) \<subseteq> encl_poly_range t t'"
proof 
  fix p 
  assume asm: "p \<in> range_of_circle (occupancy_circle \<tau>)"
  then obtain x and y where p_def: "p = (x, y)"
    using surjective_pairing[of "p"] by blast
  hence circle_eq: "(x - vx * \<tau>)\<^sup>2 + y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * \<tau>\<^sup>4" 
    using asm a_max_neq_zero assms unfolding range_of_circle_def Let_def occupancy_circle_def pos_ori_inv_axioms
    vx_def[THEN sym] by (auto simp add:field_simps)
  have "x \<le> vx * \<tau> + 1/2 * a_max * \<tau>\<^sup>2"
  proof -
    have "(x - vx * \<tau>)\<^sup>2 \<le> (1/2 * a_max * \<tau>\<^sup>2)\<^sup>2 - y\<^sup>2"
      using circle_eq by (auto simp add:field_simps)
    also have "... \<le> (1/2 * a_max * \<tau>\<^sup>2)\<^sup>2"
      by (auto)
    finally have "(x - vx * \<tau>)\<^sup>2 \<le> (1/2 * a_max * \<tau>\<^sup>2)\<^sup>2" (is "?lhs0 \<le> ?rhs0")
      by auto
    hence "sqrt ?lhs0 \<le> sqrt ?rhs0"
      unfolding real_sqrt_le_iff by auto
    hence *: "\<bar>x - vx * \<tau>\<bar> \<le> 1/2 * a_max * \<tau>\<^sup>2"
      using a_max_neq_zero by (auto simp add:algebra_simps)
    have "x \<le> vx * \<tau> \<or> vx * \<tau> < x"
      by auto
    moreover
    { assume "x \<le> vx * \<tau>" 
      also have "... \<le> vx * \<tau> + 1/2 * a_max * \<tau>\<^sup>2"
        using a_max_neq_zero by (auto)
      finally have ?thesis by auto }
    moreover
    { assume "vx * \<tau> < x"
      hence "x - vx * \<tau> \<le> 1/2 * a_max * \<tau>\<^sup>2"
        using * by auto
      hence ?thesis by auto }
    ultimately show ?thesis by auto
  qed
      
  have "vx * \<tau> - 1/2 * a_max * \<tau>\<^sup>2 \<le> x"
  proof -
    have "(x - vx * \<tau>)\<^sup>2 \<le> (1/2 * a_max * \<tau>\<^sup>2)\<^sup>2 - y\<^sup>2"
      using circle_eq by (auto simp add:field_simps)
    also have "... \<le> (1/2 * a_max * \<tau>\<^sup>2)\<^sup>2"
      by (auto)
    finally have "(x - vx * \<tau>)\<^sup>2 \<le> (1/2 * a_max * \<tau>\<^sup>2)\<^sup>2" (is "?lhs0 \<le> ?rhs0")
      by auto
    hence "sqrt ?lhs0 \<le> sqrt ?rhs0"
      unfolding real_sqrt_le_iff by auto
    hence *: "\<bar>x - vx * \<tau>\<bar> \<le> 1/2 * a_max * \<tau>\<^sup>2"
      using a_max_neq_zero by (auto simp add:algebra_simps)
    have "x \<le> vx * \<tau> \<or> vx * \<tau> < x"
      by auto
    moreover
    { assume "vx * \<tau> < x"
      have "0 \<le> 1/2 * a_max * \<tau>\<^sup>2"
        using a_max_neq_zero by auto
      hence "vx * \<tau> - 1/2 * a_max * \<tau>\<^sup>2 \<le> vx * \<tau>"
        by (auto)
      hence ?thesis 
        using `vx * \<tau> < x` by auto }
    moreover
    { assume "x \<le> vx * \<tau>"
      hence "vx * \<tau> - x \<le> 1/2 * a_max * \<tau>\<^sup>2"
        using * by auto
      hence ?thesis by auto }
    ultimately show ?thesis by auto
  qed

  have "\<exists>tx. x = vx * tx"
    using v\<^sub>x_wd unfolding vx_def[THEN sym]
    by (auto intro: exI[where x="x / vx"])
  then obtain tx where x_def: "x = vx * tx"  
    by blast
  have "0 < tx"
  proof -
    have "vx * \<tau> - 1/2 * a_max * \<tau>\<^sup>2 \<le> vx * tx"
      using `vx * \<tau> - 1/2 * a_max * \<tau>\<^sup>2 \<le> x` unfolding x_def by auto
    moreover have "0 < vx * \<tau> - 1/2 * a_max * \<tau>\<^sup>2"
    proof (rule ccontr)
      assume "\<not> (0 < vx * \<tau> - 1/2 * a_max * \<tau>\<^sup>2)"
      hence "vx * \<tau> - 1/2 * a_max * \<tau>\<^sup>2 \<le> 0"
        by auto
      hence "\<tau> * (vx - 1/2 * a_max * \<tau>) \<le> 0"
        by (auto simp add:field_simps power_def)
      hence "vx - 1/2 * a_max * \<tau> \<le> 0"
        unfolding mult_le_0_iff using assms by auto
      hence "2 * vx / a_max \<le> \<tau>"
        using a_max_neq_zero by (auto simp add:field_simps)
      moreover have "norm v = vx"
        using norm_Pair[of "fst v" "snd v"] surjective_pairing[of "v"] vx_at_least_zero 
        unfolding pos_ori_inv_axioms vx_def[THEN sym] by (auto simp add:field_simps)
      ultimately show False
        using assms t_max_lt_t_stop unfolding vx_def[THEN sym] by auto
    qed
    ultimately have "0 < vx * tx" by auto        
    thus "0 < tx" 
      using vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym] zero_less_mult_iff  by auto
  qed

  have x_lb: "fst (point1 t t') \<le> x"
  proof -
    have "t + \<tau> < 2 * t_max"
      using `t \<le> \<tau>` `\<tau> \<le> t'` `t' < t_max` by (auto simp add:field_simps)
    also have "... \<le> 2 * vx / a_max"
      using t_max_lt_t_stop norm_Pair[of "fst v" "snd v"] surjective_pairing[of "v"]
      vx_at_least_zero unfolding pos_ori_inv_axioms  vx_def[THEN sym] 
      by (fastforce)
    finally have "t + \<tau> < 2 * vx / a_max" (is "?lhs0 < ?rhs0")
      by auto
    hence "1/2 * a_max * ?lhs0 < 1/2 * a_max * ?rhs0"
      using a_max_neq_zero by (auto simp add:field_simps)
    also have "... = vx"
      using a_max_neq_zero by auto
    finally have "0 < vx - 1/2 * a_max * ?lhs0 " (is "_ < ?rhs1")
      by auto
    hence "0 * (\<tau> - t) \<le> ?rhs1 * (\<tau> - t)"
      using `t \<le> \<tau>` by (auto)
    hence "0 \<le> vx * (\<tau> - t) - 1/2 * a_max * (\<tau>\<^sup>2 - t\<^sup>2)"
      by (auto simp add:field_simps power_def)
    hence "vx * t - 1/2 * a_max * t\<^sup>2 \<le> vx * \<tau> - 1/2 * a_max * \<tau>\<^sup>2"
      by (auto simp add:field_simps)
    also have "... \<le> x"
      using `vx * \<tau> - 1/2 * a_max * \<tau>\<^sup>2 \<le> x` by auto
    finally show ?thesis
      unfolding point1_def' by auto
  qed

  have "x \<le> b_x \<tau> \<or> b_x \<tau> < x"
    by linarith
  moreover
  { assume "x \<le> b_x \<tau>"    

    hence "tx \<le> 2/3 * t_max"
    proof (rule contrapos_pp)
      assume "\<not> (tx \<le> 2 / 3 * t_max)"
      hence "2/3 * t_max < tx"
        by auto
      have "b_x \<tau> \<le> b_x t_max"
        using increasing_lt_t_max2[of "\<tau>" "t_max"] assms unfolding t_max_def 
        by (auto simp add:field_simps)
      also have "... = vx * 2/3 * t_max"
        using b_x_t_max_vx by auto
      also have "...< vx * tx"
        using `2/3 * t_max < tx` vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym]
        using mult_strict_left_mono[OF `2/3 * t_max < tx`, of "vx"] by (auto)
      also have "... = x"
        unfolding x_def by auto
      finally have "b_x \<tau> < x" by auto
      thus "\<not> x \<le> b_x \<tau>"
        by auto
    qed

    obtain \<tau>' where *: "vx * tx = b_x \<tau>'" and **: "tx \<le> \<tau>'" and ***: "\<tau>' \<le> t_max"
      using IVT_b_x[OF order.strict_implies_order[OF `0 < tx`] `tx \<le> 2/3 * t_max`]  by metis      

    have "\<tau>' \<le> \<tau>" and "0 < \<tau>'"
    proof (rule ccontr)
      assume "\<not> \<tau>' \<le> \<tau>"
      hence "\<tau> < \<tau>'"
        by auto
      hence "b_x \<tau> < b_x \<tau>'"
        using increasing_lt_t_max2[of "\<tau>" "\<tau>'"] using `\<tau>' \<le> t_max` assms unfolding t_max_def
        by auto
      also have "... = vx * tx"
        using `vx * tx = b_x \<tau>'` by auto
      also have "... = x"
        using x_def by auto
      finally have "b_x \<tau> < x"
        by auto
      with `x \<le> b_x \<tau>` show False
        by auto
    next
      show "0 < \<tau>'"
        using `0 < tx` `tx \<le> \<tau>'` by (auto simp add:field_simps)
    qed

    hence  "y \<le> b_y 1 \<tau>'"
      using circle_below_b_y1[of "\<tau>'" "\<tau>" "y"] circle_eq unfolding x_def using assms `0 < tx`
      * ** *** by auto

    have x_ub: "x \<le> fst (point2 t t')"
        unfolding point2_def' using `x \<le> b_x \<tau>`  nondecreasing_lt_t_max2[of "\<tau>" "t'"] `0 < t` `t \<le> \<tau>`
        `\<tau> \<le> t'` `t' < t_max` unfolding t_max_def by (auto simp add:field_simps)

    have "t \<le> \<tau>' \<or> \<tau>' < t" 
      by linarith
    moreover
    { assume "t \<le> \<tau>'"
      have "b_y 1 \<tau>' \<le> upper_bnd1 t t' (vx * tx)"
        using b_y_leq_upper_bnd1[OF `0 < t` `t < t'` `t' < t_max`, of "\<tau>'" "tx"] `t \<le> \<tau>'` ** `\<tau>' \<le> \<tau>`
        `\<tau> \<le> t'` * by auto
      hence ub: "y \<le> upper_bnd1 t t' (vx * tx)"
        using `y \<le> b_y 1 \<tau>'` by auto 
      have "b_y (-1) \<tau>' \<le> y"
        using circle_above_b_y1[of "\<tau>'" "\<tau>" "y"] circle_eq unfolding x_def using assms `0 < tx`
        * ** *** `\<tau>' \<le> \<tau>` `0 < \<tau>'` by auto
      moreover have "lower_bnd1 t t' (vx * tx) \<le> b_y (-1) \<tau>'"
        using lower_bnd1_leq_b_y[OF `0 < t` `t < t'` `t' < t_max`, of "\<tau>'" "tx"]
        `t \<le> \<tau>'` ** `\<tau>' \<le> \<tau>` `\<tau> \<le> t'` * by auto
      ultimately have "lower_bnd1 t t' (vx * tx) \<le> y"
        by auto    
      hence "is_bounded (x, y) t t'"
        using `y \<le> upper_bnd1 t t' (vx * tx)` x_lb x_ub unfolding is_bounded_def x_def
        by (auto) 
      have "valid_time t" and "valid_time t'"
        using valid_timeI `0 < t` `t < t'` `t' < t_max` t_max_lt_t_stop by auto
      have "(x, y) \<in> encl_poly_range t t'"
        using encl_poly_rangeI2[OF order.strict_implies_order[OF `0 < t`] `t < t'` `valid_time t` 
        `valid_time t'` `is_bounded (x, y) t t'`] by auto }
    moreover
    { assume "\<tau>' < t"
      hence "b_x \<tau>' < b_x t"
        using increasing_lt_t_max2[of "\<tau>'" "t"] `0 < \<tau>'` `\<tau>' \<le> t_max` `0 < t` `t < t'` `t' < t_max`
        unfolding t_max_def by auto
      moreover have "b_x \<tau>' \<in> {b_x 0 <..< b_x t_max}" 
      proof -
        have "\<tau>' \<in> {0 <..< t_max}"
          using `0 < \<tau>'` `\<tau>' < t` `t < t'` `t' < t_max` by auto
        thus ?thesis
          using b_x_range_open[of "0" "t_max"] using t_max_gt_zero by auto
      qed
      moreover have "b_x t \<in> {b_x 0 <..< b_x t_max}"
      proof -
        have "t \<in> {0 <..< t_max}"
          using `0 < t` `t < t'` `t' < t_max` by auto
        thus ?thesis
          using b_x_range_open[of "0" "t_max"] using t_max_gt_zero by auto
      qed
      ultimately have "f_of_x 1 (b_x \<tau>') \<le> f_of_x 1 (b_x t)"
        using f_of_x_non_decreasing[of "b_x \<tau>'" "b_x t"] `b_x \<tau>' < b_x t` 
        vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym] by auto
      also have "... = b_y 1 t"
        unfolding f_of_x_def comp_def using the_inv_into_f_f[OF b_x_inj_on, of "t"] `0 < t`
        `t < t'` `t' < t_max` by auto
      also have "... < 1/2 * a_max * t\<^sup>2"
        using b_y_lt_radius[OF `0 < t`] using vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym]
        by auto
      also have "... \<le> upper_bnd1 t t' x"
        using upper_bnd1_at_least_snd_point6[OF order.strict_implies_order[OF `0 < t`] `t < t'`]
        valid_timeI `0 < t` `t < t'` `t' < t_max` t_max_lt_t_stop x_lb 
        unfolding point1_and_6_same_x point6_def' by auto
      finally have "f_of_x 1 (b_x \<tau>') \<le> upper_bnd1 t t' x"
        by auto
      moreover have "f_of_x 1 (b_x \<tau>') = b_y 1 \<tau>'"
        unfolding f_of_x_def comp_def using the_inv_into_f_f[OF b_x_inj_on, of "\<tau>'"] `0 < \<tau>'`
        `\<tau>' \<le> t_max` by auto
      ultimately have "b_y 1 \<tau>' \<le> upper_bnd1 t t' x"
        by auto
      hence "y \<le> upper_bnd1 t t' x"
        using `y \<le> b_y 1 \<tau>'` by auto
      have "b_y (-1) \<tau>' \<le> y"
        using circle_above_b_y1[of "\<tau>'" "\<tau>" "y"] circle_eq unfolding x_def using assms `0 < tx`
        * ** *** `\<tau>' \<le> \<tau>` `0 < \<tau>'` by auto      
      have "f_of_x (-1) (b_x \<tau>') \<ge> f_of_x (-1) (b_x t)"
        using f_of_x_non_increasing[of "b_x \<tau>'" "b_x t"] `b_x \<tau>' < b_x t` 
        vx_at_least_zero v\<^sub>x_wd ` b_x \<tau>' \<in> {b_x 0<..<b_x t_max}` `b_x t \<in> {b_x 0<..<b_x t_max}`
        unfolding vx_def[THEN sym] by auto
      moreover have "f_of_x (-1) (b_x t) = b_y (-1) t" and "f_of_x (-1) (b_x \<tau>') = b_y (-1) \<tau>'"
        unfolding f_of_x_def comp_def using the_inv_into_f_f[OF b_x_inj_on, of "t"] 
        the_inv_into_f_f[OF b_x_inj_on, of "\<tau>'"] `0 < t` `t < t'` `t' < t_max` `0 < \<tau>'` `\<tau>' \<le> t_max`
        by auto
      ultimately have "b_y (-1) t \<le> b_y (-1) \<tau>'"
        by auto
      moreover have "-1/2 * a_max * t\<^sup>2 \<le> b_y (-1) t"
        using radius_lt_b_y[OF `0 < t`] vx_at_least_zero v\<^sub>x_wd unfolding vx_def by auto
      ultimately have "-1/2 * a_max * t\<^sup>2 \<le> b_y (-1) \<tau>'"
        by auto
      with `b_y (-1) \<tau>' \<le> y` have "-1/2 * a_max * t\<^sup>2 \<le> y"
        by auto
      hence "lower_bnd1 t t' x \<le> y"
        using lower_bnd1_at_most_snd_point1[OF order.strict_implies_order[OF `0 < t`] `t < t'`, of "x"]
        valid_timeI[of "t"] valid_timeI[of "t'"] `0 < t` `t < t'` `t' < t_max` t_max_lt_t_stop
        `fst (point1 t t') \<le> x` unfolding point1_def' by auto
      with `y \<le> upper_bnd1 t t' x` have "is_bounded (x, y) t t'"
        using x_lb x_ub unfolding is_bounded_def by auto 
      have "valid_time t" and "valid_time t'"
        using valid_timeI `0 < t` `t < t'` `t' < t_max` t_max_lt_t_stop by auto
      have "(x, y) \<in> encl_poly_range t t'"
        using encl_poly_rangeI2[OF order.strict_implies_order[OF `0 < t`] `t < t'` `valid_time t` 
        `valid_time t'` `is_bounded (x, y) t t'`] by auto }
    ultimately have "(x, y) \<in> encl_poly_range t t'"
      by auto }

  moreover
  { assume "b_x \<tau> < x"
    have "x \<le> b_x t' \<or> b_x t' < x"
      by auto

    moreover
    { assume "x \<le> b_x t'"
      hence "tx \<le> 2/3 * t_max"
      proof -
        have "b_x t' \<le> b_x (t_max)"
          using nondecreasing_lt_t_max2[of "t'" "t_max"] `0 < t` `t < t'` `t' < t_max`
          t_max_gt_zero unfolding t_max_def by auto
        also have "... = vx * 2/3 * t_max"
          unfolding b_x_t_max_vx by auto
        finally have "b_x t' \<le> vx * 2/3 * t_max"
          by auto
        hence "x \<le> vx * 2/3 * t_max"
          using `x \<le> b_x t'` by auto
        hence "vx * tx \<le> vx * 2/3 * t_max" (is "?lhs0 \<le> ?rhs0")
          unfolding x_def by auto
        hence "?lhs0 / vx \<le> ?rhs0 / vx"
          using vx_at_least_zero unfolding vx_def by (auto)
        thus ?thesis
          using vx_at_least_zero v\<^sub>x_wd unfolding vx_def[THEN sym] by auto
      qed      
      then obtain \<tau>' where "b_x \<tau>' = vx * tx" and "tx \<le> \<tau>'" and "\<tau>' \<le> t_max"  
        using IVT_b_x[OF _ `tx \<le> 2/3 * t_max`] `0 < tx` by auto
      have "\<tau> < \<tau>'"
      proof (rule ccontr)
        assume "\<not> \<tau> < \<tau>'"
        hence "\<tau>' \<le> \<tau>"
          by auto
        hence "b_x \<tau>' \<le> b_x \<tau>"
          using nondecreasing_lt_t_max2[of "\<tau>'" "\<tau>", OF _ _ `\<tau>' \<le> \<tau>`] `tx \<le> \<tau>'` `0 < tx` `\<tau>' \<le> t_max`
          `0 < t` `t \<le> \<tau>` `\<tau> \<le> t'` `t' < t_max` unfolding t_max_def by auto
        with `b_x \<tau> < x` show False
          unfolding x_def `b_x \<tau>' = vx * tx`[THEN sym] by auto
      qed
      have "y \<le> b_y 1 \<tau>'"
        using circle_below_b_y2[OF order.strict_trans2[OF `0 < t` `t \<le> \<tau>`] `\<tau> < \<tau>'` `\<tau>' \<le> t_max`, of "y"]
        circle_eq unfolding x_def `b_x \<tau>' = vx * tx` by auto
      have x_ub: "x \<le> fst (point2 t t')"
        using `x \<le> b_x t'` unfolding point2_def' by auto
      have "t \<le> \<tau>'"
      proof (rule ccontr)
        assume "\<not> t \<le> \<tau>'"
        hence "\<tau>' < t"
          by auto
        hence "b_x \<tau>' < b_x t"
          using increasing_lt_t_max2[of "\<tau>'" "t"] `0 < tx` `tx \<le> \<tau>'` `\<tau>' \<le> t_max` `0 < t` `t < t'`
          `t' < t_max` unfolding t_max_def by auto
        hence "vx * tx < b_x t"
          using `b_x \<tau>' = vx * tx` by auto
        hence "x < b_x t"
          unfolding x_def by auto
        hence *: "b_x \<tau> < b_x t" 
          using `b_x \<tau> < x` by auto
        moreover have "b_x t \<le> b_x \<tau>"
          using `t \<le> \<tau>` nondecreasing_lt_t_max2[of "t" "\<tau>"] `0 < t` `t < t'` `t' < t_max`
          `t \<le> \<tau>` `\<tau> \<le> t'` `t' < t_max` unfolding t_max_def by auto
        ultimately show False 
          by auto
      qed

      have "\<tau>' \<le> t' \<or> t' < \<tau>'"
        by linarith      
      moreover
      { assume "\<tau>' \<le> t'"
        have "b_y 1 \<tau>' \<le> upper_bnd1 t t' (vx * tx)"
          using b_y_leq_upper_bnd1[OF `0 < t` `t < t'` `t' < t_max` `t \<le> \<tau>'` `\<tau>' \<le> t'` `b_x \<tau>' = vx * tx`]
          by auto
        hence ub: "y \<le> upper_bnd1 t t' (vx * tx)"
          using `y \<le> b_y 1 \<tau>'` by auto
        
        have "b_y (-1) \<tau>' \<le> y"
          using circle_above_b_y2[OF _ `\<tau> < \<tau>'` `\<tau>' \<le> t_max`, of "y"] circle_eq unfolding x_def `b_x \<tau>' = vx * tx`[THEN sym]
          using `0 < t` `t \<le> \<tau>` by auto
        moreover have "lower_bnd1 t t' (vx * tx) \<le> b_y (-1) \<tau>'"
          using lower_bnd1_leq_b_y[OF `0 < t` `t < t'` `t' < t_max`, of "\<tau>'" "tx"]
          `t \<le> \<tau>'` `\<tau>' \<le> t'` `b_x \<tau>' = vx * tx` by auto
        ultimately have lb: "lower_bnd1 t t' (vx * tx) \<le> y"
          by auto
        hence "is_bounded (x, y) t t'"
          using `y \<le> upper_bnd1 t t' (vx * tx)` x_lb x_ub unfolding is_bounded_def x_def
          by (auto)         
        have "valid_time t" and "valid_time t'"
          using valid_timeI `0 < t` `t < t'` `t' < t_max` t_max_lt_t_stop by auto
        have "(x, y) \<in> encl_poly_range t t'"
          using encl_poly_rangeI2[OF order.strict_implies_order[OF `0 < t`] `t < t'` `valid_time t` 
          `valid_time t'` `is_bounded (x, y) t t'`] by auto }

      moreover
      { assume "t' < \<tau>'"
        have "b_x t' < b_x \<tau>'"
          using increasing_lt_t_max2[of "t'" "\<tau>'", OF _ _ `t' < \<tau>'`] `0 < t` `t < t'` `t' < t_max`
          `tx \<le> \<tau>'` `0 < tx` `\<tau>' \<le> t_max` unfolding t_max_def by auto
        hence x_lb': "fst (point2 t t') < x"
          unfolding x_def using `b_x \<tau>' = vx * tx` unfolding point2_def' by auto
        with x_ub have "False" by auto
        hence "(x, y) \<in> encl_poly_range t t'"
          by auto }
      ultimately have "(x, y) \<in> encl_poly_range t t'" 
        by auto }
    moreover
    { assume "b_x t' < x"
      hence x_lb': "fst (point2 t t') < x"
        unfolding point2_def' by auto

      hence x_ub': "x \<le> fst (point3 t t')"
      proof -
        have "vx * \<tau> \<le> vx * t'"
          using `\<tau> \<le> t'` vx_at_least_zero v\<^sub>x_wd unfolding vx_def by (auto)
        moreover have "1/2 * a_max * \<tau>\<^sup>2 \<le> 1/2 * a_max * t'\<^sup>2"
          using a_max_neq_zero power2_mono_leq[of "\<tau>" "t'", OF `\<tau> \<le> t'`] `0 < t` `t \<le> \<tau>`
          by (auto)
        ultimately have "vx * \<tau> + 1/2 * a_max * \<tau>\<^sup>2 \<le> vx * t' + 1/2 * a_max * t'\<^sup>2"
          by (auto)
        hence "x \<le> vx * t' + 1/2 * a_max * t'\<^sup>2"
          using ` x \<le> vx * \<tau> + 1/2 * a_max * \<tau>\<^sup>2` by auto
        thus ?thesis
          unfolding point3_def' by auto
      qed
      have "\<bar>y\<bar> \<le> 1/2 * a_max * \<tau>\<^sup>2"
      proof -
        have "y\<^sup>2 \<le> 1/4 * a_max\<^sup>2 * \<tau>\<^sup>4 - (x - vx * \<tau>)\<^sup>2"
          using circle_eq by auto
        also have "... \<le> 1/4 * a_max\<^sup>2 * \<tau>\<^sup>4"
          by auto
        also have "... = (1/2 * a_max * \<tau>\<^sup>2)\<^sup>2"
          by (auto simp add: power_def)
        finally have "y\<^sup>2 \<le> (1/2 * a_max * \<tau>\<^sup>2)\<^sup>2"
          by auto
        hence "sqrt (y\<^sup>2) \<le> sqrt ((1/2 * a_max * \<tau>\<^sup>2)\<^sup>2)"
          unfolding real_sqrt_le_iff by auto
        thus "\<bar>y\<bar> \<le> 1/2 * a_max * \<tau>\<^sup>2"
          using a_max_neq_zero `0 < t` `t \<le> \<tau>` by (auto)
      qed

      hence "y \<le> 1/2 * a_max * \<tau>\<^sup>2" and "-1/2 * a_max * \<tau>\<^sup>2 \<le> y"
        by (auto)
      hence "y \<le> 1/2 * a_max * t'\<^sup>2" and "-1/2 * a_max * t'\<^sup>2 \<le> y"
      proof -
        assume " y \<le> 1 / 2 * a_max * \<tau>\<^sup>2 "
        have "\<tau>\<^sup>2 \<le> t'\<^sup>2"
          using power2_mono_leq[OF `\<tau> \<le> t'`] `0 < t` `t \<le> \<tau>` by auto
        hence "(1/2 * a_max) * \<tau>\<^sup>2 \<le> 1/2 * a_max * t'\<^sup>2"
          using a_max_neq_zero by (auto simp add:field_simps)
        thus "y \<le> 1/2 * a_max * t'\<^sup>2"
          using `y \<le> 1/2 * a_max * \<tau>\<^sup>2` by auto
      next
        assume "-1/2 * a_max * \<tau>\<^sup>2 \<le> y"
        have "\<tau>\<^sup>2 \<le> t'\<^sup>2"
          using power2_mono_leq[OF `\<tau> \<le> t'`] `0 < t` `t \<le> \<tau>` by auto
        hence "-1/2 * a_max * t'\<^sup>2 \<le> -1/2 * a_max * \<tau>\<^sup>2"
          using a_max_neq_zero by (auto simp add:field_simps)
        thus "-1/2 * a_max * t'\<^sup>2 \<le> y"
          using `-1/2 * a_max * \<tau>\<^sup>2 \<le> y` by auto
      qed
      hence "is_bounded (x, y) t t'"
        using x_lb' x_ub' unfolding is_bounded_def point2_def' point5_def' 
        by auto
      have "valid_time t" and "valid_time t'"
        using valid_timeI `0 < t` `t < t'` `t' < t_max` t_max_lt_t_stop by auto
      have "(x, y) \<in> encl_poly_range t t'"
        using encl_poly_rangeI2[OF order.strict_implies_order[OF `0 < t`] `t < t'` `valid_time t` 
        `valid_time t'` `is_bounded (x, y) t t'`] by auto }
    ultimately have "(x, y) \<in> encl_poly_range t t'"
      by auto }
  ultimately have "(x, y) \<in> encl_poly_range t t'"
    by auto
  thus "p \<in> encl_poly_range t t'"
    unfolding p_def by auto
qed

end


