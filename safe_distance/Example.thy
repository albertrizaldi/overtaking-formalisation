(* Author : Albert Rizaldi *)

theory Example
  imports Complex_Main 
          Safe_Distance_Isar
begin

definition prec :: "nat" where
  "prec = 20"

section \<open>Float\<close>

(* The following formalisation is based on the work of Immler and modified accordingly. *)

text \<open>division with fixed precision (in contrast to @{term real_divr}, @{term real_divl})\<close>

definition real_div_down::"nat \<Rightarrow> int \<Rightarrow> int \<Rightarrow> real"
  where "real_div_down p i j = truncate_down (Suc p) (i / j)"
    
definition real_div_up::"nat \<Rightarrow> int \<Rightarrow> int \<Rightarrow> real"
  where "real_div_up p i j = truncate_up (Suc p) (i / j)"
    
lift_definition float_div_down::"nat \<Rightarrow> int \<Rightarrow> int \<Rightarrow> float" is real_div_down
  by (simp add: real_div_down_def)                                 

lift_definition float_div_up::"nat \<Rightarrow> int \<Rightarrow> int \<Rightarrow> float" is real_div_up
  by (simp add: real_div_up_def)
    
(* albert: unused in Isabelle 2016-1?*)
lemma floor_log_divide_eq:
  assumes "i > 0" "j > 0" "p > 1"
  shows "\<lfloor>log p (i / j)\<rfloor> = floor (log p i) - floor (log p j) -
      (if i \<ge> j * p powr (floor (log p i) - floor (log p j)) then 0 else 1)"
proof -
  let ?l = "log p"
  let ?fl = "\<lambda>x. floor (?l x)"
  have "\<lfloor>?l (i / j)\<rfloor> = \<lfloor>?l i - ?l j\<rfloor>" using assms
    by (auto simp: log_divide)
  also have "\<dots> = floor (real_of_int (?fl i - ?fl j) + (?l i - ?fl i - (?l j - ?fl j)))"
    (is "_ = floor (_ + ?r)")
    by (simp add: algebra_simps)
  also note floor_add2
  also note \<open>p > 1\<close>
  note powr = powr_le_cancel_iff[symmetric, OF \<open>1 < p\<close>, THEN iffD2]
  note powr_strict = powr_less_cancel_iff[symmetric, OF \<open>1 < p\<close>, THEN iffD2]
  have "floor ?r = (if i \<ge> j * p powr (?fl i - ?fl j) then 0 else -1)" (is "_ = ?if")
    using assms
    by (linarith |
      auto
        intro!: floor_eq2
        intro: powr_strict powr
        simp: powr_diff powr_add divide_simps algebra_simps bitlen_def)+
  finally
  show ?thesis by simp
qed

lemma compute_float_div_down[code]:
  "float_div_down p i j = lapprox_rat (Suc p) i j"
  apply transfer                              
  apply (case_tac "i = 0"; case_tac "j = 0")
  by (auto simp add: algebra_simps bitlen_def floor_log_divide_eq 
          real_div_down_def truncate_down_def rat_precision_def)

lemma compute_float_div_up[code]: "float_div_up p i j = - float_div_down p (-i) j"
  by transfer (simp add: real_div_up_def real_div_down_def truncate_up_eq_truncate_down)

type_synonym ivl = "float * float"

definition real_ivl_of_real::"nat \<Rightarrow> real \<Rightarrow> (real * real)" where
  "real_ivl_of_real p x = (truncate_down (Suc p) x, truncate_up (Suc p) x)"

lift_definition float_ivl_of_real::"nat \<Rightarrow> real \<Rightarrow> ivl" is real_ivl_of_real
  by (auto simp: real_ivl_of_real_def)

lemma real_of_rat_Fract[simp]: "real_of_rat (Rat.Fract a b) = a / b"
  by (simp add: Fract_of_int_quotient of_rat_divide)

lemma [code]: "float_ivl_of_real p (Ratreal r) =
  (let (a, b) = quotient_of r in
  (float_div_down p a b, float_div_up p a b))"
  apply transfer
  apply (auto split: prod.split simp: real_ivl_of_real_def real_div_down_def real_div_up_def)
  apply (metis of_rat_divide of_rat_of_int_eq quotient_of_div of_int_def)
  apply (metis of_rat_divide of_rat_of_int_eq quotient_of_div of_int_def)
  done

definition float_ivl_of_rat2 :: "nat \<Rightarrow> rat \<Rightarrow> ivl" where
  "float_ivl_of_rat2 p r = (let (a,b) = quotient_of r in (float_div_down p a b, float_div_up p a b))"

value "(fst) (float_ivl_of_rat2 prec (2.333 :: rat))"
value [code] "truncate_down (prec-1) (Float 10506897930655367 (-52))"  
value "bitlen 10506897930655367"  
  
  
lemma float_ivl_of_rat_interval:
  assumes "float_ivl_of_rat2 p (Rat.Fract a b) = (l, u)"
  shows "real_of_float l \<le> real_of_rat(Rat.Fract a b) \<and> real_of_rat (Rat.Fract a b) \<le> real_of_float u"
proof 
  from quotient_of_Fract have quot: "quotient_of(Rat.Fract a b) = Rat.normalize (a, b)" by simp
  with assms have "l = float_div_down p (fst (Rat.normalize(a,b))) (snd (Rat.normalize(a,b)))" 
    unfolding float_ivl_of_rat2_def Rat.normalize_def Let_def by (auto split:prod.split)
  hence ineq1: "real_of_float l \<le> real_of_rat (Rat.Fract (fst (Rat.normalize(a,b))) (snd (Rat.normalize(a,b))))"
    using float_div_down.abs_eq  float_div_down.rep_eq real_div_down_def truncate_down by simp
  from quot have eq: "Rat.Fract (fst (Rat.normalize(a,b))) (snd (Rat.normalize(a,b))) = Rat.Fract a b"
    using Rat.normalize_eq by auto
  with ineq1 show "real_of_float l \<le> real_of_rat (Rat.Fract a b)" by simp

  from assms and quot have "u = float_div_up p   (fst (Rat.normalize(a,b))) (snd (Rat.normalize(a,b)))"
    unfolding float_ivl_of_rat2_def Rat.normalize_def Let_def  by (auto split:prod.split)
  hence ineq2: "real_of_rat (Rat.Fract (fst (Rat.normalize(a,b))) (snd (Rat.normalize(a,b)))) \<le> real_of_float u"
    using float_div_up.abs_eq float_div_up.rep_eq real_div_up_def truncate_up by simp
  with quot show "real_of_rat (Rat.Fract a b) \<le> real_of_float u" using Rat.normalize_eq by simp
qed

section "Example"

definition Se where "Se \<equiv> float_ivl_of_rat2 prec 0"
definition Sf where "Sf \<equiv> float_ivl_of_rat2 prec 66.97"
definition Ve where "Ve \<equiv> float_ivl_of_rat2 prec 45.00"
definition Vf where "Vf \<equiv> float_ivl_of_rat2 prec 38.66"
definition Ae where "Ae \<equiv> float_ivl_of_rat2 prec (-25.72178)"
definition Af where "Af \<equiv> float_ivl_of_rat2 prec (-22.50656)"      
definition D  where "D  \<equiv> float_ivl_of_rat2 prec 1"

theorem test_data0: 
  "checker_reaction_approx prec Se Ve Ae Sf Vf Af D"
  by eval

  

end