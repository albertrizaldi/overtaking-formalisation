(*Authors: Albert Rizaldi, Monika Huber*)

theory Overtaking_Aux
  imports Analysis "Affine_Arithmetic/Polygon"
begin
      
type_synonym real2 = "real \<times> real"
        
definition min2D :: "real2 \<Rightarrow> real2 \<Rightarrow> real2" where
  "min2D z1 z2 = (let x1 = fst z1; x2 = fst z2; y1 = snd z1; y2 = snd z2 in
                    if x1 < x2 then z1 else
                    if x1 = x2 then (if y1 \<le> y2 then z1 else z2) else
                    (* x1 > x2 *)   z2)"
  
theorem min2D_D:
  assumes "min2D x y = z"
  shows "fst z \<le> fst x \<and> fst z \<le> fst y"
  using assms unfolding min2D_def by smt
    
theorem min2D_D2:
  assumes "min2D x y = z"
  shows "z = x \<or> z = y"
  using assms unfolding min2D_def by presburger

theorem min2D_D3:
  assumes "min2D x y = x"
  shows "fst x < fst y \<or> (fst x = fst y \<and> snd x \<le> snd y)"
  using assms unfolding min2D_def by smt  
    
theorem min2D_D4:
  assumes "min2D x y = y"
  shows "fst y < fst x \<or> (fst x = fst y \<and> snd y \<le> snd x)"
  using assms unfolding min2D_def by smt 

section "Rectangle"    
  
record rectangle = 
  Xcoord :: real
  Ycoord :: real
  Orient :: real
  Length :: real
  Width  :: real  
              
definition rotation_matrix' :: "real \<Rightarrow> real2 \<Rightarrow> real2" where
  "rotation_matrix' theta \<equiv>  (\<lambda>p :: real2. (cos theta * fst p - sin theta * snd p, 
                                             sin theta * fst p + cos theta * snd p))"
(* 
definition rotate_rect :: "rectangle \<Rightarrow> real2 \<Rightarrow> real2" where
  "rotate_rect rect \<equiv> (let centre = (Xcoord rect, Ycoord rect); ori = Orient rect in 
                              (\<lambda>p. p + centre) \<circ> (rotation_matrix' ori) \<circ> (\<lambda>p. p - centre))"   *)
  
definition rotate_rect :: "rectangle \<Rightarrow> real2 \<Rightarrow> real2" where
  "rotate_rect rect \<equiv> (let centre = (Xcoord rect, Ycoord rect); ori = Orient rect in 
                                                                            (rotation_matrix' ori))"    
  
(* the vertices are sorted as follows:
get_vertices rect = [v0,v1,v2,v3] 
\<rightarrow> in counterclockwise order if length and width of the rectangle are both positive or both negative
     v0 +-----------------+ v3            v2 +-----------------+ v1
        |  l > 0 \<and> w > 0  |         or       |  l < 0 \<and> w < 0  |
     v1 +-----------------+ v2            v3 +-----------------+ v0

\<rightarrow> in clockwise order if either length or width is negative (and the other one is positive)
     v3 +-----------------+ v0            v1 +-----------------+ v2
        |  l < 0 \<and> w > 0  |         or       |  l > 0 \<and> w < 0  |
     v2 +-----------------+ v1            v0 +-----------------+ v3

*)    
definition get_vertices :: "rectangle \<Rightarrow> real2 list" where
  "get_vertices rect \<equiv> (let x = Xcoord rect; y = Ycoord rect; l = Length rect; w = Width rect in 
                          [(x - l / 2, y + w / 2),
                           (x - l / 2, y - w / 2),
                           (x + l / 2, y - w / 2),
                           (x + l / 2, y + w / 2)])"

definition get_vertices_zero :: "rectangle \<Rightarrow> real2 list" where
  "get_vertices_zero rect \<equiv> (let l = Length rect; w = Width rect in 
                          [(- l / 2,   w / 2),
                           (- l / 2, - w / 2),
                           (  l / 2, - w / 2),
                           (  l / 2,   w / 2)])"
    
theorem 
  assumes "vertices = get_vertices rect"
  assumes "0 < Length rect" and "0 < Width rect"  
  shows "ccw' (vertices ! 0) (vertices ! 1) (vertices ! 2)"
proof -
  define x where "x \<equiv> Xcoord rect"
  define y where "y \<equiv> Ycoord rect"
  define l where "l \<equiv> Length rect"
  define w where "w \<equiv> Width rect"      
  note params = x_def y_def l_def w_def
  from assms(2-3) have "0 < l" and "0 < w" unfolding params by auto  
  from assms have 0: "vertices ! 0 = (x - l / 2, y + w / 2)" and 1: "vertices ! 1 = (x - l/2, y - w/2)"
      and 2: "vertices ! 2 = (x + l /2, y - w / 2)"
    unfolding get_vertices_def Let_def params by auto
  have "ccw' (vertices ! 0) (vertices ! 1) (vertices ! 2) = 
        ccw' 0 (vertices ! 1 - vertices ! 0) (vertices ! 2 - vertices ! 0)" 
    unfolding ccw'_def using det3_translate_origin by auto    
  also have "... = ccw' 0 (0, -w) (l, -w)" unfolding 0 1 2 by auto
  finally have *: "ccw' (vertices ! 0) (vertices ! 1) (vertices ! 2) = ccw' 0 (0,-w) (l,-w)" by auto
  have "det3 0 (0,-w) (l,-w) =  w * l" unfolding det3_def'  by (auto simp add:algebra_simps)   
  with `0 < w` `0 < l` have "0 < det3 0 (0,-w) (l,-w)" by auto
  hence "ccw' 0 (0,-w) (l,-w)" unfolding ccw'_def by auto
  with * show ?thesis by auto  
qed  
  
theorem nbr_of_vertex:
  "length (get_vertices rect) = 4"
  unfolding get_vertices_def Let_def by auto 
    
theorem nbr_of_vertex_zero:
  "length (get_vertices_zero rect) = 4"
  unfolding get_vertices_zero_def Let_def by auto 
        
definition get_vertices_rotated :: "rectangle \<Rightarrow> real2 list" where
  "get_vertices_rotated rect \<equiv> map (rotation_matrix' (Orient rect)) (get_vertices_zero rect)"  
  
theorem nbr_of_vertex_rotated:
  "length (get_vertices_rotated rect) = 4"
  unfolding get_vertices_rotated_def using length_map nbr_of_vertex_zero by auto

definition get_vertices_rotated_translated :: "rectangle \<Rightarrow> real2 list" where
  "get_vertices_rotated_translated rect \<equiv> 
                               map (\<lambda>p. p + (Xcoord rect, Ycoord rect)) (get_vertices_rotated rect)"

theorem nbr_of_vertex_rotated_translated:
  "length (get_vertices_rotated_translated rect) = 4"
  unfolding get_vertices_rotated_translated_def using length_map nbr_of_vertex_rotated by auto  
        
definition get_lines :: "rectangle \<Rightarrow> (real2 \<times> real2) list" where
  "get_lines rect \<equiv> (let vertices = get_vertices_rotated_translated rect; 
                         zero = vertices ! 0; one = vertices ! 1; 
                         two = vertices ! 2; three = vertices ! 3                  
                      in 
                        [(zero, one), (one, two), (two, three), (three, zero)])"
  
theorem nbr_of_lines:
  "length (get_lines rect) = 4"
  unfolding get_lines_def unfolding Let_def by auto  
  
(* Definition of point inside a rectangle *)
definition inside_rectangle :: "real2 \<Rightarrow> rectangle \<Rightarrow> bool" where
  "inside_rectangle p rect \<equiv> (let lines = get_lines rect;
                                line0 = lines ! 0; line1 = lines ! 1; line2 = lines ! 2; line3 = lines ! 3;
                                l = Length rect; w = Width rect
                              in 
                              if l = 0 \<or> w = 0 then False
                              else 
                                if (l > 0 \<and> w > 0) \<or> (l < 0 \<and> w < 0) then
                                  ccw' p (fst line0) (snd line0) \<and> ccw' p (fst line1) (snd line1) \<and> 
                                  ccw' p (fst line2) (snd line2) \<and> ccw' p (fst line3) (snd line3)
                                else (*(l \<le> 0 \<and> w > 0) \<or> (l < 0 \<and> w \<ge> 0) \<or> (l \<ge> 0 \<and> w < 0) \<or> (l > 0 \<and> w \<le> 0) *)
                                  ccw' p (snd line0)(fst line0) \<and> ccw' p (snd line1) (fst line1) \<and> 
                                  ccw' p (snd line2) (fst line2) \<and> ccw' p (snd line3) (fst line3))"

theorem centre_point_inside: 
  assumes "Length rect \<noteq> 0" and "Width rect \<noteq> 0"
  shows "inside_rectangle (Xcoord rect, Ycoord rect) rect"
proof -
  define x where "x \<equiv> Xcoord rect"
  define y where "y \<equiv> Ycoord rect"
  define l where "l \<equiv> Length rect"
  define w where "w \<equiv> Width rect"
  define \<theta> where "\<theta> \<equiv> Orient rect"
  note params = x_def y_def l_def w_def \<theta>_def
      
  define zero where "zero \<equiv> get_vertices_rotated_translated rect ! 0"    
  define one where "one \<equiv> get_vertices_rotated_translated rect ! 1"
  define two where "two \<equiv> get_vertices_rotated_translated rect ! 2"
  define three where "three \<equiv> get_vertices_rotated_translated rect ! 3"
  note vertices_def = zero_def one_def two_def three_def  
  have "zero = rotation_matrix' \<theta> (- l / 2, w / 2) + (x,y)" and 
       "one = rotation_matrix' \<theta> (- l / 2, - w / 2) + (x,y)" and
       "two = rotation_matrix' \<theta> (l / 2, - w / 2) + (x,y)" and 
       "three = rotation_matrix' \<theta> (l / 2, w / 2) + (x,y)"
    unfolding vertices_def get_vertices_rotated_translated_def get_vertices_rotated_def
      get_vertices_zero_def Let_def params by auto
  hence "zero = (x - cos \<theta> * l / 2 - sin \<theta> * w / 2, y - sin \<theta> * l / 2 + cos \<theta> * w / 2)" and 
       "one = (x - cos \<theta> * l / 2 + sin \<theta> * w / 2, y - sin \<theta> * l / 2 - cos \<theta> * w / 2)" and
       "two = (x + cos \<theta> * l / 2 + sin \<theta> * w / 2, y + sin \<theta> * l / 2 - cos \<theta> * w / 2)" and 
       "three = (x + cos \<theta> * l / 2 - sin \<theta> * w / 2, y + sin \<theta> * l / 2 + cos \<theta> * w / 2)"
    unfolding rotation_matrix'_def using \<open>zero = rotation_matrix' \<theta> (- l / 2, w / 2) + (x,y)\<close> \<open>one = rotation_matrix' \<theta> (- l / 2, - w / 2) + (x,y)\<close> \<open>two = rotation_matrix' \<theta> (l / 2, - w / 2) + (x,y)\<close> \<open>three = rotation_matrix' \<theta> (l / 2, w / 2) + (x,y)\<close> by auto

  define line0 where "line0 \<equiv> get_lines rect ! 0"    
  define line1 where "line1 \<equiv> get_lines rect ! 1"
  define line2 where "line2 \<equiv> get_lines rect ! 2"
  define line3 where "line3 \<equiv> get_lines rect ! 3"
  note lines_def = line0_def line1_def line2_def line3_def 
  have "line0 = (zero,one)" "line1 = (one,two)" "line2 = (two, three)" "line3 = (three, zero)"
    unfolding lines_def get_lines_def Let_def vertices_def by auto

  have "(0 < l \<and> 0 < w) \<or> (0 > l \<and> 0 > w) | (0 < l \<and> 0 > w) \<or> (0 > l \<and> 0 < w)" using assms using l_def w_def by auto
  moreover
  { assume "(0 < l \<and> 0 < w) \<or> (0 > l \<and> 0 > w)"
    hence inside_rect: "inside_rectangle (Xcoord rect, Ycoord rect) rect = (let lines = get_lines rect;
                                                                                          line0 = lines ! 0; line1 = lines ! 1; line2 = lines ! 2; line3 = lines ! 3
                                                                                       in ccw' (Xcoord rect, Ycoord rect) (fst line0) (snd line0) \<and> ccw' (Xcoord rect, Ycoord rect) (fst line1) (snd line1) \<and> 
                                                                                          ccw' (Xcoord rect, Ycoord rect) (fst line2) (snd line2) \<and> ccw' (Xcoord rect, Ycoord rect) (fst line3) (snd line3))" using inside_rectangle_def
      by (simp add: assms(1) assms(2) l_def w_def)
        
    have "(cos \<theta> * cos \<theta> + sin \<theta> * sin \<theta>) * w * l > 0" by (simp add: \<open>0 < l \<and> 0 < w \<or> l < 0 \<and> w < 0\<close> linordered_field_class.sign_simps(44) mult.commute)
    hence "(cos \<theta> * l / 2) * (cos \<theta> * w / 2) + (sin \<theta> * w / 2) * (sin \<theta> * l / 2) + (sin \<theta> * l / 2) * (sin \<theta> * w / 2) + (cos \<theta> * w / 2) * (cos \<theta> * l / 2) > 0"
      by (smt linordered_field_class.sign_simps(44) linordered_field_class.sign_simps(45) real_sum_of_halves)
    hence "x * y - x * (sin \<theta> * l / 2) + x * (cos \<theta> * w / 2) + y * x - y * (cos \<theta> * l / 2) + y * (sin \<theta> * w / 2)
          + x * y - x * (sin \<theta> * l / 2) - x * (cos \<theta> * w / 2) - (cos \<theta> * l / 2) * y + (cos \<theta> * l / 2) * (sin \<theta> * l / 2) + (cos \<theta> * l / 2) * (cos \<theta> * w / 2) - (sin \<theta> * w / 2) * y + (sin \<theta> * w / 2) * (sin \<theta> * l / 2) +  (sin \<theta> * w / 2) * (cos \<theta> * w / 2)          
          - y * x + y * (cos \<theta> * l / 2) - y * (sin \<theta> * w / 2) + (sin \<theta> * l / 2) * x - (sin \<theta> * l / 2) * (cos \<theta> * l / 2) + (sin \<theta> * l / 2) * (sin \<theta> * w / 2) - (cos \<theta> * w / 2) * x + (cos \<theta> * w / 2) * (cos \<theta> * l / 2) - (cos \<theta> * w / 2) * (sin \<theta> * w / 2) 
          - y * x + y * (cos \<theta> * l / 2) + y * (sin \<theta> * w / 2) - x * y + x * (sin \<theta> * l / 2) + x * (cos \<theta> * w / 2) > 0" by (simp add: mult.commute)
    hence det3_vals: "x * (y - sin \<theta> * l / 2 - cos \<theta> * w / 2) + y * (x + cos \<theta> * l / 2 + sin \<theta> * w / 2)
           + (x - cos \<theta> * l / 2 + sin \<theta> * w / 2) * (y + sin \<theta> * l / 2 - cos \<theta> * w / 2) 
           - (y - sin \<theta> * l / 2 - cos \<theta> * w / 2) * (x + cos \<theta> * l / 2 + sin \<theta> * w / 2)
           - y * (x - cos \<theta> * l / 2 + sin \<theta> * w / 2) - x * (y + sin \<theta> * l / 2 - cos \<theta> * w / 2) > 0" by (simp add: distrib_left mult.commute right_diff_distrib')    
    
    have "ccw' (x,y) (fst line0) (snd line0)"
      proof - 
        from det3_vals have "ccw' (x,y) (x - cos \<theta> * l / 2 - sin \<theta> * w / 2, y - sin \<theta> * l / 2 + cos \<theta> * w / 2) (x - cos \<theta> * l / 2 + sin \<theta> * w / 2, y - sin \<theta> * l / 2 - cos \<theta> * w / 2)" 
          using ccw'_def det3.simps by (simp add: mult.commute right_diff_distrib' ring_class.ring_distribs(1))
        thus ?thesis by (simp add: \<open>line0 = (zero, one)\<close> \<open>one = (x - cos \<theta> * l / 2 + sin \<theta> * w / 2, y - sin \<theta> * l / 2 - cos \<theta> * w / 2)\<close> \<open>zero = (x - cos \<theta> * l / 2 - sin \<theta> * w / 2, y - sin \<theta> * l / 2 + cos \<theta> * w / 2)\<close>)
      qed
    moreover
    have "ccw' (x,y) (fst line1) (snd line1)"
      proof -
        from det3_vals have "ccw' (x,y) (x - cos \<theta> * l / 2 + sin \<theta> * w / 2, y - sin \<theta> * l / 2 - cos \<theta> * w / 2) (x + cos \<theta> * l / 2 + sin \<theta> * w / 2, y + sin \<theta> * l / 2 - cos \<theta> * w / 2)" using ccw'_def det3.simps by auto
        thus ?thesis by (simp add: \<open>line1 = (one, two)\<close> \<open>one = (x - cos \<theta> * l / 2 + sin \<theta> * w / 2, y - sin \<theta> * l / 2 - cos \<theta> * w / 2)\<close> \<open>two = (x + cos \<theta> * l / 2 + sin \<theta> * w / 2, y + sin \<theta> * l / 2 - cos \<theta> * w / 2)\<close> ccw'_def)
      qed
    moreover
    have "ccw' (x,y) (fst line2) (snd line2)"
      proof -
        from det3_vals have "ccw' (x,y) (x + cos \<theta> * l / 2 + sin \<theta> * w / 2, y + sin \<theta> * l / 2 - cos \<theta> * w / 2) (x + cos \<theta> * l / 2 - sin \<theta> * w / 2, y + sin \<theta> * l / 2 + cos \<theta> * w / 2)" using ccw'_def det3.simps by (simp add: mult.commute right_diff_distrib' ring_class.ring_distribs(1))
        thus ?thesis by (simp add: \<open>line2 = (two, three)\<close> \<open>three = (x + cos \<theta> * l / 2 - sin \<theta> * w / 2, y + sin \<theta> * l / 2 + cos \<theta> * w / 2)\<close> \<open>two = (x + cos \<theta> * l / 2 + sin \<theta> * w / 2, y + sin \<theta> * l / 2 - cos \<theta> * w / 2)\<close>)
      qed
    moreover
    have "ccw' (x,y) (fst line3) (snd line3)" 
      proof -
        from det3_vals have "ccw' (x,y) (x + cos \<theta> * l / 2 - sin \<theta> * w / 2, y + sin \<theta> * l / 2 + cos \<theta> * w / 2) (x - cos \<theta> * l / 2 - sin \<theta> * w / 2, y - sin \<theta> * l / 2 + cos \<theta> * w / 2)" using ccw'_def det3.simps by (simp add: mult.commute right_diff_distrib' ring_class.ring_distribs(1))
        thus ?thesis  by (simp add: \<open>line3 = (three, zero)\<close> \<open>three = (x + cos \<theta> * l / 2 - sin \<theta> * w / 2, y + sin \<theta> * l / 2 + cos \<theta> * w / 2)\<close> \<open>zero = (x - cos \<theta> * l / 2 - sin \<theta> * w / 2, y - sin \<theta> * l / 2 + cos \<theta> * w / 2)\<close>)
      qed
      ultimately have "inside_rectangle (Xcoord rect, Ycoord rect) rect" unfolding inside_rect Let_def params lines_def by auto
    }
    moreover
    { assume "(0 < l \<and> 0 > w) \<or> (0 > l \<and> 0 < w)"
      hence inside_rect: "inside_rectangle (Xcoord rect, Ycoord rect) rect = (let lines = get_lines rect;
                                                                                                line0 = lines ! 0; line1 = lines ! 1; line2 = lines ! 2; line3 = lines ! 3
                                                                                             in ccw' (Xcoord rect, Ycoord rect) (snd line0) (fst line0) \<and> ccw' (Xcoord rect, Ycoord rect) (snd line1) (fst line1) \<and> 
                                                                                                ccw' (Xcoord rect, Ycoord rect) (snd line2) (fst line2) \<and> ccw' (Xcoord rect, Ycoord rect) (snd line3) (fst line3))" using inside_rectangle_def
      by (smt l_def w_def)
    
      have "(cos \<theta> * cos \<theta> + sin \<theta> * sin \<theta>) * w * l < 0" using \<open>0 < l \<and> w < 0 \<or> l < 0 \<and> 0 < w\<close> mult_pos_neg mult_pos_neg2 by auto
      hence "(cos \<theta> * l / 2) * (cos \<theta> * w / 2) + (sin \<theta> * w / 2) * (sin \<theta> * l / 2) + (sin \<theta> * l / 2) * (sin \<theta> * w / 2) + (cos \<theta> * w / 2) * (cos \<theta> * l / 2) < 0"
        by (smt linordered_field_class.sign_simps(44) linordered_field_class.sign_simps(45) real_sum_of_halves)
      hence "x * y - x * (sin \<theta> * l / 2) + x * (cos \<theta> * w / 2) + y * x - y * (cos \<theta> * l / 2) + y * (sin \<theta> * w / 2)
            + x * y - x * (sin \<theta> * l / 2) - x * (cos \<theta> * w / 2) - (cos \<theta> * l / 2) * y + (cos \<theta> * l / 2) * (sin \<theta> * l / 2) + (cos \<theta> * l / 2) * (cos \<theta> * w / 2) - (sin \<theta> * w / 2) * y + (sin \<theta> * w / 2) * (sin \<theta> * l / 2) +  (sin \<theta> * w / 2) * (cos \<theta> * w / 2)          
            - y * x + y * (cos \<theta> * l / 2) - y * (sin \<theta> * w / 2) + (sin \<theta> * l / 2) * x - (sin \<theta> * l / 2) * (cos \<theta> * l / 2) + (sin \<theta> * l / 2) * (sin \<theta> * w / 2) - (cos \<theta> * w / 2) * x + (cos \<theta> * w / 2) * (cos \<theta> * l / 2) - (cos \<theta> * w / 2) * (sin \<theta> * w / 2) 
            - y * x + y * (cos \<theta> * l / 2) + y * (sin \<theta> * w / 2) - x * y + x * (sin \<theta> * l / 2) + x * (cos \<theta> * w / 2) < 0" by (simp add: mult.commute)
      hence det3_vals: "x * (y - sin \<theta> * l / 2 - cos \<theta> * w / 2) + y * (x + cos \<theta> * l / 2 + sin \<theta> * w / 2)
             + (x - cos \<theta> * l / 2 + sin \<theta> * w / 2) * (y + sin \<theta> * l / 2 - cos \<theta> * w / 2) 
             - (y - sin \<theta> * l / 2 - cos \<theta> * w / 2) * (x + cos \<theta> * l / 2 + sin \<theta> * w / 2)
             - y * (x - cos \<theta> * l / 2 + sin \<theta> * w / 2) - x * (y + sin \<theta> * l / 2 - cos \<theta> * w / 2) < 0" by (simp add: distrib_left mult.commute right_diff_distrib')    
      
      have "ccw' (x,y) (snd line0) (fst line0)"
        proof - 
          from det3_vals have "ccw' (x,y) (x - cos \<theta> * l / 2 + sin \<theta> * w / 2, y - sin \<theta> * l / 2 - cos \<theta> * w / 2) (x - cos \<theta> * l / 2 - sin \<theta> * w / 2, y - sin \<theta> * l / 2 + cos \<theta> * w / 2)" 
            using ccw'_def det3.simps by (simp add: mult.commute right_diff_distrib' ring_class.ring_distribs(1))
          thus ?thesis by (simp add: \<open>line0 = (zero, one)\<close> \<open>one = (x - cos \<theta> * l / 2 + sin \<theta> * w / 2, y - sin \<theta> * l / 2 - cos \<theta> * w / 2)\<close> \<open>zero = (x - cos \<theta> * l / 2 - sin \<theta> * w / 2, y - sin \<theta> * l / 2 + cos \<theta> * w / 2)\<close>)
        qed
      moreover
      have "ccw' (x,y) (snd line1) (fst line1)"
        proof -
          from det3_vals have "ccw' (x,y) (x + cos \<theta> * l / 2 + sin \<theta> * w / 2, y + sin \<theta> * l / 2 - cos \<theta> * w / 2) (x - cos \<theta> * l / 2 + sin \<theta> * w / 2, y - sin \<theta> * l / 2 - cos \<theta> * w / 2)" using ccw'_def det3.simps by (simp add: mult.commute right_diff_distrib' ring_class.ring_distribs(1))
          thus ?thesis by (simp add: \<open>line1 = (one, two)\<close> \<open>one = (x - cos \<theta> * l / 2 + sin \<theta> * w / 2, y - sin \<theta> * l / 2 - cos \<theta> * w / 2)\<close> \<open>two = (x + cos \<theta> * l / 2 + sin \<theta> * w / 2, y + sin \<theta> * l / 2 - cos \<theta> * w / 2)\<close> ccw'_def)
        qed
      moreover
      have "ccw' (x,y) (snd line2) (fst line2)"
        proof -
          from det3_vals have "ccw' (x,y) (x + cos \<theta> * l / 2 - sin \<theta> * w / 2, y + sin \<theta> * l / 2 + cos \<theta> * w / 2) (x + cos \<theta> * l / 2 + sin \<theta> * w / 2, y + sin \<theta> * l / 2 - cos \<theta> * w / 2)" using ccw'_def det3.simps by (simp add: mult.commute right_diff_distrib' ring_class.ring_distribs(1))
          thus ?thesis by (simp add: \<open>line2 = (two, three)\<close> \<open>three = (x + cos \<theta> * l / 2 - sin \<theta> * w / 2, y + sin \<theta> * l / 2 + cos \<theta> * w / 2)\<close> \<open>two = (x + cos \<theta> * l / 2 + sin \<theta> * w / 2, y + sin \<theta> * l / 2 - cos \<theta> * w / 2)\<close>)
        qed
      moreover
      have "ccw' (x,y) (snd line3) (fst line3)" 
        proof -
          from det3_vals have "ccw' (x,y) (x - cos \<theta> * l / 2 - sin \<theta> * w / 2, y - sin \<theta> * l / 2 + cos \<theta> * w / 2) (x + cos \<theta> * l / 2 - sin \<theta> * w / 2, y + sin \<theta> * l / 2 + cos \<theta> * w / 2)" using ccw'_def det3.simps by (simp add: mult.commute right_diff_distrib' ring_class.ring_distribs(1))
          thus ?thesis  by (simp add: \<open>line3 = (three, zero)\<close> \<open>three = (x + cos \<theta> * l / 2 - sin \<theta> * w / 2, y + sin \<theta> * l / 2 + cos \<theta> * w / 2)\<close> \<open>zero = (x - cos \<theta> * l / 2 - sin \<theta> * w / 2, y - sin \<theta> * l / 2 + cos \<theta> * w / 2)\<close>)
        qed
        ultimately have "inside_rectangle (Xcoord rect, Ycoord rect) rect" unfolding inside_rect Let_def params lines_def by auto
    }
    ultimately show ?thesis by blast
qed  

lemma lines_connect_vertices:
  assumes "(p,q) \<in> set (get_lines rect)"
  shows "p \<in> set (get_vertices_rotated_translated rect) \<and> q \<in> set (get_vertices_rotated_translated rect)"
proof - 
  from assms have "(p,q) \<in> {((get_vertices_rotated_translated rect) ! 0, (get_vertices_rotated_translated rect) ! 1),
                          ((get_vertices_rotated_translated rect) ! 1, (get_vertices_rotated_translated rect) ! 2),
                          ((get_vertices_rotated_translated rect) ! 2, (get_vertices_rotated_translated rect)! 3), 
                          ((get_vertices_rotated_translated rect)! 3, (get_vertices_rotated_translated rect) ! 0)}" 
    unfolding get_lines_def by (metis list.set(1) list.simps(15))
  hence "p \<in> {(get_vertices_rotated_translated rect) ! 0, (get_vertices_rotated_translated rect) ! 1,
  (get_vertices_rotated_translated rect) ! 2, (get_vertices_rotated_translated rect) ! 3} \<and> q \<in> {(get_vertices_rotated_translated rect) ! 0, (get_vertices_rotated_translated rect) ! 1,
  (get_vertices_rotated_translated rect) ! 2, (get_vertices_rotated_translated rect) ! 3}" by auto
  thus ?thesis  using nbr_of_vertex_rotated_translated by auto
qed    
end